/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanbanVisibilityProcessor;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YKanban Visibility Processor</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YKanbanVisibilityProcessorTest extends TestCase {

	/**
	 * The fixture for this YKanban Visibility Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YKanbanVisibilityProcessor fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YKanbanVisibilityProcessorTest.class);
	}

	/**
	 * Constructs a new YKanban Visibility Processor test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YKanbanVisibilityProcessorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YKanban Visibility Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YKanbanVisibilityProcessor fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YKanban Visibility Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YKanbanVisibilityProcessor getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYKanbanVisibilityProcessor());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YKanbanVisibilityProcessorTest
