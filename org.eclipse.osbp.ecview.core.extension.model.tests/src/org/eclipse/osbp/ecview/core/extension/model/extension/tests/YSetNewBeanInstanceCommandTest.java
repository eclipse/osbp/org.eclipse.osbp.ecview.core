/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSetNewBeanInstanceCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YSet New Bean Instance Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCommand#getView() <em>Get View</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YSetNewBeanInstanceCommandTest extends TestCase {

	/**
	 * The fixture for this YSet New Bean Instance Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSetNewBeanInstanceCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YSetNewBeanInstanceCommandTest.class);
	}

	/**
	 * Constructs a new YSet New Bean Instance Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSetNewBeanInstanceCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YSet New Bean Instance Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YSetNewBeanInstanceCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YSet New Bean Instance Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSetNewBeanInstanceCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYSetNewBeanInstanceCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCommand#getView() <em>Get View</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCommand#getView()
	 * @generated
	 */
	public void testGetView() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YSetNewBeanInstanceCommandTest
