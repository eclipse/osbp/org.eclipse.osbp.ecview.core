/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YButton;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YButton</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YButton#addClickListener(org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener) <em>Add Click Listener</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YButton#removeClickListener(org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener) <em>Remove Click Listener</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YButton#createClickEndpoint() <em>Create Click Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YButtonTest extends TestCase {

	/**
	 * The fixture for this YButton test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YButton fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YButtonTest.class);
	}

	/**
	 * Constructs a new YButton test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YButtonTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YButton test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YButton fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YButton test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YButton getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYButton());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable()
	 * @generated
	 */
	public void testIsEditable() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean) <em>Editable</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean)
	 * @generated
	 */
	public void testSetEditable() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YButton#addClickListener(org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener) <em>Add Click Listener</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YButton#addClickListener(org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener)
	 * @generated
	 */
	public void testAddClickListener__YButtonClickListener() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YButton#removeClickListener(org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener) <em>Remove Click Listener</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YButton#removeClickListener(org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener)
	 * @generated
	 */
	public void testRemoveClickListener__YButtonClickListener() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YButton#createClickEndpoint() <em>Create Click Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YButton#createClickEndpoint()
	 * @generated
	 */
	public void testCreateClickEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YButtonTest
