/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>extension</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExtensionModelTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ExtensionModelTests("extension Tests");
		suite.addTestSuite(YGridLayoutTest.class);
		suite.addTestSuite(YGridLayoutCellStyleTest.class);
		suite.addTestSuite(YHorizontalLayoutTest.class);
		suite.addTestSuite(YVerticalLayoutTest.class);
		suite.addTestSuite(YTableTest.class);
		suite.addTestSuite(YTreeTest.class);
		suite.addTestSuite(YOptionsGroupTest.class);
		suite.addTestSuite(YListTest.class);
		suite.addTestSuite(YLabelTest.class);
		suite.addTestSuite(YImageTest.class);
		suite.addTestSuite(YTextFieldTest.class);
		suite.addTestSuite(YBeanReferenceFieldTest.class);
		suite.addTestSuite(YTextAreaTest.class);
		suite.addTestSuite(YCheckBoxTest.class);
		suite.addTestSuite(YBrowserTest.class);
		suite.addTestSuite(YDateTimeTest.class);
		suite.addTestSuite(YDecimalFieldTest.class);
		suite.addTestSuite(YNumericFieldTest.class);
		suite.addTestSuite(YComboBoxTest.class);
		suite.addTestSuite(YButtonTest.class);
		suite.addTestSuite(YSliderTest.class);
		suite.addTestSuite(YToggleButtonTest.class);
		suite.addTestSuite(YProgressBarTest.class);
		suite.addTestSuite(YTabSheetTest.class);
		suite.addTestSuite(YTabTest.class);
		suite.addTestSuite(YMasterDetailTest.class);
		suite.addTestSuite(YFormLayoutTest.class);
		suite.addTestSuite(YTextSearchFieldTest.class);
		suite.addTestSuite(YBooleanSearchFieldTest.class);
		suite.addTestSuite(YNumericSearchFieldTest.class);
		suite.addTestSuite(YReferenceSearchFieldTest.class);
		suite.addTestSuite(YPanelTest.class);
		suite.addTestSuite(YSplitPanelTest.class);
		suite.addTestSuite(YSearchPanelTest.class);
		suite.addTestSuite(YEnumOptionsGroupTest.class);
		suite.addTestSuite(YEnumComboBoxTest.class);
		suite.addTestSuite(YEnumListTest.class);
		suite.addTestSuite(YAddToTableCommandTest.class);
		suite.addTestSuite(YRemoveFromTableCommandTest.class);
		suite.addTestSuite(YSetNewBeanInstanceCommandTest.class);
		suite.addTestSuite(YCssLayoutTest.class);
		suite.addTestSuite(YAbsoluteLayoutTest.class);
		suite.addTestSuite(YSuggestTextFieldTest.class);
		suite.addTestSuite(YPasswordFieldTest.class);
		suite.addTestSuite(YFilteringComponentTest.class);
		suite.addTestSuite(YKanbanTest.class);
		suite.addTestSuite(YDialogComponentTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionModelTests(String name) {
		super(name);
	}

} //ExtensionModelTests
