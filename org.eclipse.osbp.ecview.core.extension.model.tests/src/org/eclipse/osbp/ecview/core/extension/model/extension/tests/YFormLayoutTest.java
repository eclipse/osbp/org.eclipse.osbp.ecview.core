/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayout;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YForm Layout</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YAlignmentContainer#applyAlignment(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable, org.eclipse.osbp.ecview.core.common.model.core.YAlignment) <em>Apply Alignment</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YFormLayoutTest extends TestCase {

	/**
	 * The fixture for this YForm Layout test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFormLayout fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YFormLayoutTest.class);
	}

	/**
	 * Constructs a new YForm Layout test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFormLayoutTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YForm Layout test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YFormLayout fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YForm Layout test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFormLayout getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYFormLayout());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YAlignmentContainer#applyAlignment(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable, org.eclipse.osbp.ecview.core.common.model.core.YAlignment) <em>Apply Alignment</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YAlignmentContainer#applyAlignment(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable, org.eclipse.osbp.ecview.core.common.model.core.YAlignment)
	 * @generated
	 */
	public void testApplyAlignment__YEmbeddable_YAlignment() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YFormLayoutTest
