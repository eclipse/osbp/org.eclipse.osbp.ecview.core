/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YComboBoxDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YCombo Box Datatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YComboBoxDatatypeTest extends TestCase {

	/**
	 * The fixture for this YCombo Box Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YComboBoxDatatype fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YComboBoxDatatypeTest.class);
	}

	/**
	 * Constructs a new YCombo Box Datatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YComboBoxDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YCombo Box Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YComboBoxDatatype fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YCombo Box Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YComboBoxDatatype getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtDatatypesFactory.eINSTANCE.createYComboBoxDatatype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YComboBoxDatatypeTest
