/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YSplit Panel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel#getCellStyle(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable) <em>Get Cell Style</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YSplitPanelTest extends TestCase {

	/**
	 * The fixture for this YSplit Panel test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSplitPanel fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YSplitPanelTest.class);
	}

	/**
	 * Constructs a new YSplit Panel test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSplitPanelTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YSplit Panel test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YSplitPanel fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YSplit Panel test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSplitPanel getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYSplitPanel());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel#getCellStyle(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable) <em>Get Cell Style</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel#getCellStyle(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable)
	 * @generated
	 */
	public void testGetCellStyle__YEmbeddable() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YSplitPanelTest
