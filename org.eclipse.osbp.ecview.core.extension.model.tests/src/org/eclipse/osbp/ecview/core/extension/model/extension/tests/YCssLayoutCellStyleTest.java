/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCssLayoutCellStyle;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YCss Layout Cell Style</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YCssLayoutCellStyleTest extends TestCase {

	/**
	 * The fixture for this YCss Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YCssLayoutCellStyle fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YCssLayoutCellStyleTest.class);
	}

	/**
	 * Constructs a new YCss Layout Cell Style test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YCssLayoutCellStyleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YCss Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YCssLayoutCellStyle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YCss Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YCssLayoutCellStyle getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYCssLayoutCellStyle());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YCssLayoutCellStyleTest
