/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YText Datatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YTextDatatypeTest extends TestCase {

	/**
	 * The fixture for this YText Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTextDatatype fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YTextDatatypeTest.class);
	}

	/**
	 * Constructs a new YText Datatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YTextDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YText Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YTextDatatype fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YText Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTextDatatype getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtDatatypesFactory.eINSTANCE.createYTextDatatype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YTextDatatypeTest
