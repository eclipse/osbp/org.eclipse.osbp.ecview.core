/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumComboBox;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEnum Combo Box</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint() <em>Create Collection Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YEnumComboBoxTest extends YInputTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YEnumComboBoxTest.class);
	}

	/**
	 * Constructs a new YEnum Combo Box test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEnumComboBoxTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YEnum Combo Box test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YEnumComboBox getFixture() {
		return (YEnumComboBox)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYEnumComboBox());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint()
	 * @generated
	 */
	public void testCreateSelectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint() <em>Create Collection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint()
	 * @generated
	 */
	public void testCreateCollectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YEnumComboBoxTest
