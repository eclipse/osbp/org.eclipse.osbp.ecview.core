/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanServiceConsumer;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBean Service Consumer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YBeanServiceConsumerTest extends TestCase {

	/**
	 * The fixture for this YBean Service Consumer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBeanServiceConsumer fixture = null;

	/**
	 * Constructs a new YBean Service Consumer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBeanServiceConsumerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBean Service Consumer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBeanServiceConsumer fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBean Service Consumer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBeanServiceConsumer getFixture() {
		return fixture;
	}

} //YBeanServiceConsumerTest
