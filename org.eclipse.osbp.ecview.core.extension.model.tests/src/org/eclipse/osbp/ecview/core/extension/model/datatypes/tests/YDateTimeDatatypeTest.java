/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YDate Time Datatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YDateTimeDatatypeTest extends TestCase {

	/**
	 * The fixture for this YDate Time Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDateTimeDatatype fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YDateTimeDatatypeTest.class);
	}

	/**
	 * Constructs a new YDate Time Datatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDateTimeDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YDate Time Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YDateTimeDatatype fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YDate Time Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDateTimeDatatype getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtDatatypesFactory.eINSTANCE.createYDateTimeDatatype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YDateTimeDatatypeTest
