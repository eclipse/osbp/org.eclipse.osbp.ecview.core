/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayoutCellStyle;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YVertical Layout Cell Style</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YVerticalLayoutCellStyleTest extends TestCase {

	/**
	 * The fixture for this YVertical Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVerticalLayoutCellStyle fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YVerticalLayoutCellStyleTest.class);
	}

	/**
	 * Constructs a new YVertical Layout Cell Style test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVerticalLayoutCellStyleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YVertical Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YVerticalLayoutCellStyle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YVertical Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVerticalLayoutCellStyle getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYVerticalLayoutCellStyle());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YVerticalLayoutCellStyleTest
