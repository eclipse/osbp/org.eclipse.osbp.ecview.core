/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanban;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YKanban</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YKanbanTest extends YInputTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YKanbanTest.class);
	}

	/**
	 * Constructs a new YKanban test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YKanbanTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YKanban test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YKanban getFixture() {
		return (YKanban)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYKanban());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint()
	 * @generated
	 */
	public void testCreateSelectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YKanbanTest
