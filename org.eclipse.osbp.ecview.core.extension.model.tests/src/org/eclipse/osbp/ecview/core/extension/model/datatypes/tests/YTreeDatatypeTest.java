/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTreeDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YTree Datatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YTreeDatatypeTest extends TestCase {

	/**
	 * The fixture for this YTree Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTreeDatatype fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YTreeDatatypeTest.class);
	}

	/**
	 * Constructs a new YTree Datatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YTreeDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YTree Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YTreeDatatype fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YTree Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTreeDatatype getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtDatatypesFactory.eINSTANCE.createYTreeDatatype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YTreeDatatypeTest
