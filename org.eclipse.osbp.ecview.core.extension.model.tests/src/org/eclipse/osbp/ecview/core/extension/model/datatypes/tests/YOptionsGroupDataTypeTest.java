/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YOptionsGroupDataType;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YOptions Group Data Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YOptionsGroupDataTypeTest extends TestCase {

	/**
	 * The fixture for this YOptions Group Data Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YOptionsGroupDataType fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YOptionsGroupDataTypeTest.class);
	}

	/**
	 * Constructs a new YOptions Group Data Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YOptionsGroupDataTypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YOptions Group Data Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YOptionsGroupDataType fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YOptions Group Data Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YOptionsGroupDataType getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtDatatypesFactory.eINSTANCE.createYOptionsGroupDataType());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YOptionsGroupDataTypeTest
