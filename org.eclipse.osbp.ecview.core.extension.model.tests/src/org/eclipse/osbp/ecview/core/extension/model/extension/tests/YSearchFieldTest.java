/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchField;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YSearch Field</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YValueBindable#createValueEndpoint() <em>Create Value Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YSearchFieldTest extends YInputTest {

	/**
	 * Constructs a new YSearch Field test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSearchFieldTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YSearch Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YSearchField getFixture() {
		return (YSearchField)fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YValueBindable#createValueEndpoint() <em>Create Value Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YValueBindable#createValueEndpoint()
	 * @generated
	 */
	public void testCreateValueEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YSearchFieldTest
