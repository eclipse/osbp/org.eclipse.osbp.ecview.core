/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YBrowserDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBrowser Datatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YBrowserDatatypeTest extends TestCase {

	/**
	 * The fixture for this YBrowser Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBrowserDatatype fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YBrowserDatatypeTest.class);
	}

	/**
	 * Constructs a new YBrowser Datatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBrowserDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBrowser Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBrowserDatatype fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBrowser Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBrowserDatatype getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtDatatypesFactory.eINSTANCE.createYBrowserDatatype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YBrowserDatatypeTest
