/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.tests.ExtensionModelTests;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Extension</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExtensionAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ExtensionAllTests("Extension Tests");
		suite.addTest(ExtensionModelTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionAllTests(String name) {
		super(name);
	}

} //ExtensionAllTests
