/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YFiltering Component</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YFilteringComponentTest extends TestCase {

	/**
	 * The fixture for this YFiltering Component test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFilteringComponent fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YFilteringComponentTest.class);
	}

	/**
	 * Constructs a new YFiltering Component test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFilteringComponentTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YFiltering Component test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YFilteringComponent fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YFiltering Component test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFilteringComponent getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYFilteringComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YFilteringComponentTest
