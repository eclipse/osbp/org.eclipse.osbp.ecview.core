/**
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YDecimal Datatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YDecimalDatatypeTest extends YNumericDatatypeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YDecimalDatatypeTest.class);
	}

	/**
	 * Constructs a new YDecimal Datatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDecimalDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YDecimal Datatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YDecimalDatatype getFixture() {
		return (YDecimalDatatype)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtDatatypesFactory.eINSTANCE.createYDecimalDatatype());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YDecimalDatatypeTest
