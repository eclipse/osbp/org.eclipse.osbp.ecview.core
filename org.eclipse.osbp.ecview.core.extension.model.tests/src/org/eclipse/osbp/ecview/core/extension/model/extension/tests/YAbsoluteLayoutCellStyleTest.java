/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayoutCellStyle;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YAbsolute Layout Cell Style</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YAbsoluteLayoutCellStyleTest extends TestCase {

	/**
	 * The fixture for this YAbsolute Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAbsoluteLayoutCellStyle fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YAbsoluteLayoutCellStyleTest.class);
	}

	/**
	 * Constructs a new YAbsolute Layout Cell Style test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YAbsoluteLayoutCellStyleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YAbsolute Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YAbsoluteLayoutCellStyle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YAbsolute Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAbsoluteLayoutCellStyle getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYAbsoluteLayoutCellStyle());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YAbsoluteLayoutCellStyleTest
