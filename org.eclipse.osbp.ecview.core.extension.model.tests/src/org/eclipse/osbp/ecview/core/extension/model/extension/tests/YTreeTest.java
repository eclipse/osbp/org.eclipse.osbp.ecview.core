/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTree;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YTree</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable#createMultiSelectionEndpoint() <em>Create Multi Selection Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint() <em>Create Collection Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YTreeTest extends YInputTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YTreeTest.class);
	}

	/**
	 * Constructs a new YTree test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YTreeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YTree test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YTree getFixture() {
		return (YTree)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYTree());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable#createMultiSelectionEndpoint() <em>Create Multi Selection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable#createMultiSelectionEndpoint()
	 * @generated
	 */
	public void testCreateMultiSelectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint()
	 * @generated
	 */
	public void testCreateSelectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint() <em>Create Collection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#createCollectionEndpoint()
	 * @generated
	 */
	public void testCreateCollectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YTreeTest
