/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YGrid Layout Cell Style</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle#addSpanInfo(int, int, int, int) <em>Add Span Info</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YGridLayoutCellStyleTest extends TestCase {

	/**
	 * The fixture for this YGrid Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YGridLayoutCellStyle fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YGridLayoutCellStyleTest.class);
	}

	/**
	 * Constructs a new YGrid Layout Cell Style test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YGridLayoutCellStyleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YGrid Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YGridLayoutCellStyle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YGrid Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YGridLayoutCellStyle getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYGridLayoutCellStyle());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle#addSpanInfo(int, int, int, int) <em>Add Span Info</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle#addSpanInfo(int, int, int, int)
	 * @generated
	 */
	public void testAddSpanInfo__int_int_int_int() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YGridLayoutCellStyleTest
