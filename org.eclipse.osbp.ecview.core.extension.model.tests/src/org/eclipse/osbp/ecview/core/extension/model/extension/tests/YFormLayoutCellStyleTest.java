/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayoutCellStyle;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YForm Layout Cell Style</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YFormLayoutCellStyleTest extends TestCase {

	/**
	 * The fixture for this YForm Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFormLayoutCellStyle fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YFormLayoutCellStyleTest.class);
	}

	/**
	 * Constructs a new YForm Layout Cell Style test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFormLayoutCellStyleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YForm Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YFormLayoutCellStyle fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YForm Layout Cell Style test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFormLayoutCellStyle getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYFormLayoutCellStyle());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YFormLayoutCellStyleTest
