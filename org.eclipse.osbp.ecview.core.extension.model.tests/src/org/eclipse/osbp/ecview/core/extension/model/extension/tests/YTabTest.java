/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTab;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YTab</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YTab#getView() <em>Get View</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YTabTest extends TestCase {

	/**
	 * The fixture for this YTab test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTab fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YTabTest.class);
	}

	/**
	 * Constructs a new YTab test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YTabTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YTab test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YTab fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YTab test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTab getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExtensionModelFactory.eINSTANCE.createYTab());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YTab#getView() <em>Get View</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YTab#getView()
	 * @generated
	 */
	public void testGetView() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YTabTest
