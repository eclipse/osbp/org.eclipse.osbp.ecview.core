/**
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YInput</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YInputTest extends TestCase {

	/**
	 * The fixture for this YInput test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YInput fixture = null;

	/**
	 * Constructs a new YInput test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YInputTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YInput test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YInput fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YInput test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YInput getFixture() {
		return fixture;
	}

} //YInputTest
