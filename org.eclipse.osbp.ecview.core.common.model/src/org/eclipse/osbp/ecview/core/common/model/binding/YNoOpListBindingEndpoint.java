/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.binding;

import org.eclipse.osbp.ecview.core.common.model.core.YElement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YNo Op List Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YNoOpListBindingEndpoint#getElement <em>Element</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYNoOpListBindingEndpoint()
 * @model
 * @generated
 */
public interface YNoOpListBindingEndpoint extends YListBindingEndpoint {

	/**
	 * Returns the value of the '<em><b>Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' reference.
	 * @see #setElement(YElement)
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYNoOpListBindingEndpoint_Element()
	 * @model
	 * @generated
	 */
	YElement getElement();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YNoOpListBindingEndpoint#getElement <em>Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' reference.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(YElement value);
} // YNoOpListBindingEndpoint
