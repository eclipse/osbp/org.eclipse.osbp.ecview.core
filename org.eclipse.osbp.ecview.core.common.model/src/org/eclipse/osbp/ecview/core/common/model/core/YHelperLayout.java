/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YHelperLayoutImpl;

/**
 * <!-- begin-user-doc --> A helper layout is a layout which contains
 * YEmbeddables without containing them. For some reasons an embeddable may be
 * contained by a different model object, which is not a layout. But for
 * focusing issues we need to determine the next focusable element by YLayout.
 * <p>
 * For instance the CxGrid contains YColumns and each column contains an editor
 * field. So the eContainer of editorfield is YColumn. But the focusing strategy
 * needs to use an YLayout to find the next focusable element. Grid will use
 * this layout to collect all editorfields inside this layout.
 * <p>
 * Calls to {@link #getElements()} are forwarded to
 * {@link #getVirtualElements()}. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YHelperLayout#getVirtualElements <em>Virtual Elements</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYHelperLayout()
 * @model
 * @generated
 */
public interface YHelperLayout extends YLayout {
	/**
	 * Returns the value of the '<em><b>Virtual Elements</b></em>' reference
	 * list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable}. <!--
	 * begin-user-doc -->
	 * <p>
	 * A virtual element is an element that is <b>NOT</b> contained by its
	 * parent. For instance see CxGrid#column#editorfield. See
	 * {@link YHelperLayoutImpl}. <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Virtual Elements</em>' reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYHelperLayout_VirtualElements()
	 * @model
	 * @generated
	 */
	EList<YEmbeddable> getVirtualElements();

} // YHelperLayout
