/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.validation;

import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YUnique Attribute Validator Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getValType <em>Val Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getValTypeFullyQualifiedName <em>Val Type Fully Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getPropertyPath <em>Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getContainerValueBindingEndpoint <em>Container Value Binding Endpoint</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage#getYUniqueAttributeValidatorConfig()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YUniqueAttributeValidatorConfig extends YValidationConfig {
	/**
	 * Returns the value of the '<em><b>Val Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Type</em>' attribute.
	 * @see #setValType(Class)
	 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage#getYUniqueAttributeValidatorConfig_ValType()
	 * @model
	 * @generated
	 */
	Class<?> getValType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getValType <em>Val Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Val Type</em>' attribute.
	 * @see #getValType()
	 * @generated
	 */
	void setValType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Val Type Fully Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Val Type Fully Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Val Type Fully Qualified Name</em>' attribute.
	 * @see #setValTypeFullyQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage#getYUniqueAttributeValidatorConfig_ValTypeFullyQualifiedName()
	 * @model
	 * @generated
	 */
	String getValTypeFullyQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getValTypeFullyQualifiedName <em>Val Type Fully Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Val Type Fully Qualified Name</em>' attribute.
	 * @see #getValTypeFullyQualifiedName()
	 * @generated
	 */
	void setValTypeFullyQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Path</em>' attribute.
	 * @see #setPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage#getYUniqueAttributeValidatorConfig_PropertyPath()
	 * @model
	 * @generated
	 */
	String getPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getPropertyPath <em>Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Path</em>' attribute.
	 * @see #getPropertyPath()
	 * @generated
	 */
	void setPropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Container Value Binding Endpoint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Value Binding Endpoint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Value Binding Endpoint</em>' containment reference.
	 * @see #setContainerValueBindingEndpoint(YValueBindingEndpoint)
	 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage#getYUniqueAttributeValidatorConfig_ContainerValueBindingEndpoint()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	YValueBindingEndpoint getContainerValueBindingEndpoint();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig#getContainerValueBindingEndpoint <em>Container Value Binding Endpoint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Value Binding Endpoint</em>' containment reference.
	 * @see #getContainerValueBindingEndpoint()
	 * @generated
	 */
	void setContainerValueBindingEndpoint(YValueBindingEndpoint value);

} // YUniqueAttributeValidatorConfig
