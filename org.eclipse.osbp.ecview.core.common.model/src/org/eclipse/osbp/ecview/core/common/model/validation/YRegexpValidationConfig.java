/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.validation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YRegexp Validation Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidationConfig#getRegExpression <em>Reg Expression</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage#getYRegexpValidationConfig()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YRegexpValidationConfig extends YValidationConfig {
	/**
	 * Returns the value of the '<em><b>Reg Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reg Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reg Expression</em>' attribute.
	 * @see #setRegExpression(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage#getYRegexpValidationConfig_RegExpression()
	 * @model required="true"
	 * @generated
	 */
	String getRegExpression();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidationConfig#getRegExpression <em>Reg Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reg Expression</em>' attribute.
	 * @see #getRegExpression()
	 * @generated
	 */
	void setRegExpression(String value);

} // YRegexpValidationConfig
