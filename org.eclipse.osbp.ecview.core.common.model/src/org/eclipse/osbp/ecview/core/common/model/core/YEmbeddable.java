/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatadescription;
import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatatype;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YUi Embeddable</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getOrphanDatatypes <em>Orphan Datatypes</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getOrphanDatadescriptions <em>Orphan Datadescriptions</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#isMementoEnabled <em>Memento Enabled</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getMementoId <em>Memento Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLastContextClick <em>Last Context Click</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#isReadonly <em>Readonly</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable()
 * @model abstract="true"
 * @generated
 */
public interface YEmbeddable extends YElement, YCssAble, YVisibleable, YAuthorizationable {
	/**
	 * Returns the value of the '<em><b>Orphan Datatypes</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.core.common.model.datatypes.YDatatype}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Orphan Datatypes</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Orphan Datatypes</em>' containment
	 *         reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_OrphanDatatypes()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YDatatype> getOrphanDatatypes();

	/**
	 * Returns the value of the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.datatypes.YDatadescription}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Orphan Datadescriptions</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Orphan Datadescriptions</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_OrphanDatadescriptions()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YDatadescription> getOrphanDatadescriptions();

	/**
	 * Returns the value of the '<em><b>Memento Enabled</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memento Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memento Enabled</em>' attribute.
	 * @see #setMementoEnabled(boolean)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_MementoEnabled()
	 * @model default="false"
	 * @generated
	 */
	boolean isMementoEnabled();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#isMementoEnabled <em>Memento Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memento Enabled</em>' attribute.
	 * @see #isMementoEnabled()
	 * @generated
	 */
	void setMementoEnabled(boolean value);

	/**
	 * Returns the value of the '<em><b>Memento Id</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memento Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memento Id</em>' attribute.
	 * @see #setMementoId(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_MementoId()
	 * @model default=""
	 * @generated
	 */
	String getMementoId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getMementoId <em>Memento Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memento Id</em>' attribute.
	 * @see #getMementoId()
	 * @generated
	 */
	void setMementoId(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_Label()
	 * @model volatile="true" derived="true"
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #setLabelI18nKey(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_LabelI18nKey()
	 * @model volatile="true" derived="true"
	 * @generated
	 */
	String getLabelI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabelI18nKey <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #getLabelI18nKey()
	 * @generated
	 */
	void setLabelI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Context Click</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Context Click</em>' reference.
	 * @see #setLastContextClick(YContextClickEvent)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_LastContextClick()
	 * @model transient="true"
	 * @generated
	 */
	YContextClickEvent getLastContextClick();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLastContextClick <em>Last Context Click</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Context Click</em>' reference.
	 * @see #getLastContextClick()
	 * @generated
	 */
	void setLastContextClick(YContextClickEvent value);

	/**
	 * Returns the value of the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Readonly</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Readonly</em>' attribute.
	 * @see #setReadonly(boolean)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddable_Readonly()
	 * @model
	 * @generated
	 */
	boolean isReadonly();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#isReadonly <em>Readonly</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Readonly</em>' attribute.
	 * @see #isReadonly()
	 * @generated
	 */
	void setReadonly(boolean value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the view
	 * @model kind="operation"
	 * @generated
	 */
	YView getView();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the parent
	 * @model kind="operation"
	 * @generated
	 */
	YLayout getParent();

//	/**
//	 * Sets the label by creating a new datadescription.
//	 *
//	 * @param label
//	 *            the new label
//	 */
//	public void setLabel(String label);
//
//	/**
//	 * Sets the label i18nKey by creating a new datadescription.
//	 *
//	 * @param i18nKey
//	 *            the new label i18n key
//	 */
//	public void setLabelI18nKey(String i18nKey);
//
//	/**
//	 * Returns the label.
//	 *
//	 * @return the label
//	 */
//	public String getLabel();
//	
//	/**
//	 * Returns the label i18n key.
//	 *
//	 * @return the label i18n key
//	 */
//	public String getLabelI18nKey();
	
	
	/**
	 * Creates a binding endpoint to observe the visible property.
	 *
	 * @return the y value binding endpoint
	 */
	YValueBindingEndpoint createVisibleEndpoint();

	/**
	 * Creates a binding endpoint to observe the cssId property.
	 *
	 * @return the y value binding endpoint
	 */
	YValueBindingEndpoint createCssIdEndpoint();

	/**
	 * Creates a binding endpoint to observe the cssClass property.
	 *
	 * @return the y value binding endpoint
	 */
	YValueBindingEndpoint createCssClassEndpoint();

} // YUiEmbeddable
