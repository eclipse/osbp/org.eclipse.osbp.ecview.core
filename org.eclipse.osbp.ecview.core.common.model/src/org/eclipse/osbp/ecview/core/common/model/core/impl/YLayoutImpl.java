/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YEditable;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YEnable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>YUi Layout</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#isInitialEditable <em>Initial Editable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#isEditable <em>Editable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#isInitialEnabled <em>Initial Enabled</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#isEnabled <em>Enabled</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#getLastComponentAttach <em>Last Component Attach</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#getLastComponentDetach <em>Last Component Detach</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#getNumberColumns <em>Number Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl#isSaveAndNew <em>Save And New</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YLayoutImpl extends YEmbeddableImpl implements YLayout {
	/**
	 * The default value of the '{@link #isInitialEditable() <em>Initial Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitialEditable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INITIAL_EDITABLE_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isInitialEditable() <em>Initial Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitialEditable()
	 * @generated
	 * @ordered
	 */
	protected boolean initialEditable = INITIAL_EDITABLE_EDEFAULT;
	/**
	 * The default value of the '{@link #isEditable() <em>Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEditable()
	 * @generated
	 * @ordered
	 */
	protected static final boolean EDITABLE_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isEditable() <em>Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEditable()
	 * @generated
	 * @ordered
	 */
	protected boolean editable = EDITABLE_EDEFAULT;
	/**
	 * The default value of the '{@link #isInitialEnabled() <em>Initial Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitialEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INITIAL_ENABLED_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isInitialEnabled() <em>Initial Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInitialEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean initialEnabled = INITIAL_ENABLED_EDEFAULT;
	/**
	 * The default value of the '{@link #isEnabled() <em>Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnabled()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ENABLED_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isEnabled() <em>Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isEnabled()
	 * @generated
	 * @ordered
	 */
	protected boolean enabled = ENABLED_EDEFAULT;
	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<YEmbeddable> elements;

	/**
	 * The default value of the '{@link #getLastComponentAttach() <em>Last Component Attach</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastComponentAttach()
	 * @generated
	 * @ordered
	 */
	protected static final Object LAST_COMPONENT_ATTACH_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLastComponentAttach() <em>Last Component Attach</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastComponentAttach()
	 * @generated
	 * @ordered
	 */
	protected Object lastComponentAttach = LAST_COMPONENT_ATTACH_EDEFAULT;
	/**
	 * The default value of the '{@link #getLastComponentDetach() <em>Last Component Detach</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastComponentDetach()
	 * @generated
	 * @ordered
	 */
	protected static final Object LAST_COMPONENT_DETACH_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLastComponentDetach() <em>Last Component Detach</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastComponentDetach()
	 * @generated
	 * @ordered
	 */
	protected Object lastComponentDetach = LAST_COMPONENT_DETACH_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberColumns() <em>Number Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberColumns()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_COLUMNS_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getNumberColumns() <em>Number Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberColumns()
	 * @generated
	 * @ordered
	 */
	protected int numberColumns = NUMBER_COLUMNS_EDEFAULT;

	/**
	 * The default value of the '{@link #isSaveAndNew() <em>Save And New</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSaveAndNew()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAVE_AND_NEW_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isSaveAndNew() <em>Save And New</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSaveAndNew()
	 * @generated
	 * @ordered
	 */
	protected boolean saveAndNew = SAVE_AND_NEW_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected YLayoutImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoreModelPackage.Literals.YLAYOUT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInitialEditable() {
		return initialEditable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialEditable(boolean newInitialEditable) {
		boolean oldInitialEditable = initialEditable;
		initialEditable = newInitialEditable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__INITIAL_EDITABLE, oldInitialEditable, initialEditable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditable(boolean newEditable) {
		boolean oldEditable = editable;
		editable = newEditable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__EDITABLE, oldEditable, editable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInitialEnabled() {
		return initialEnabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialEnabled(boolean newInitialEnabled) {
		boolean oldInitialEnabled = initialEnabled;
		initialEnabled = newInitialEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__INITIAL_ENABLED, oldInitialEnabled, initialEnabled));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnabled(boolean newEnabled) {
		boolean oldEnabled = enabled;
		enabled = newEnabled;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__ENABLED, oldEnabled, enabled));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<YEmbeddable> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList.Resolving<YEmbeddable>(YEmbeddable.class, this, CoreModelPackage.YLAYOUT__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getLastComponentAttach() {
		return lastComponentAttach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastComponentAttach(Object newLastComponentAttach) {
		Object oldLastComponentAttach = lastComponentAttach;
		lastComponentAttach = newLastComponentAttach;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__LAST_COMPONENT_ATTACH, oldLastComponentAttach, lastComponentAttach));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getLastComponentDetach() {
		return lastComponentDetach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastComponentDetach(Object newLastComponentDetach) {
		Object oldLastComponentDetach = lastComponentDetach;
		lastComponentDetach = newLastComponentDetach;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__LAST_COMPONENT_DETACH, oldLastComponentDetach, lastComponentDetach));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberColumns() {
		return numberColumns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberColumns(int newNumberColumns) {
		int oldNumberColumns = numberColumns;
		numberColumns = newNumberColumns;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__NUMBER_COLUMNS, oldNumberColumns, numberColumns));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSaveAndNew() {
		return saveAndNew;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSaveAndNew(boolean newSaveAndNew) {
		boolean oldSaveAndNew = saveAndNew;
		saveAndNew = newSaveAndNew;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YLAYOUT__SAVE_AND_NEW, oldSaveAndNew, saveAndNew));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CoreModelPackage.YLAYOUT__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoreModelPackage.YLAYOUT__INITIAL_EDITABLE:
				return isInitialEditable();
			case CoreModelPackage.YLAYOUT__EDITABLE:
				return isEditable();
			case CoreModelPackage.YLAYOUT__INITIAL_ENABLED:
				return isInitialEnabled();
			case CoreModelPackage.YLAYOUT__ENABLED:
				return isEnabled();
			case CoreModelPackage.YLAYOUT__ELEMENTS:
				return getElements();
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_ATTACH:
				return getLastComponentAttach();
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_DETACH:
				return getLastComponentDetach();
			case CoreModelPackage.YLAYOUT__NUMBER_COLUMNS:
				return getNumberColumns();
			case CoreModelPackage.YLAYOUT__SAVE_AND_NEW:
				return isSaveAndNew();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoreModelPackage.YLAYOUT__INITIAL_EDITABLE:
				setInitialEditable((Boolean)newValue);
				return;
			case CoreModelPackage.YLAYOUT__EDITABLE:
				setEditable((Boolean)newValue);
				return;
			case CoreModelPackage.YLAYOUT__INITIAL_ENABLED:
				setInitialEnabled((Boolean)newValue);
				return;
			case CoreModelPackage.YLAYOUT__ENABLED:
				setEnabled((Boolean)newValue);
				return;
			case CoreModelPackage.YLAYOUT__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends YEmbeddable>)newValue);
				return;
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_ATTACH:
				setLastComponentAttach(newValue);
				return;
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_DETACH:
				setLastComponentDetach(newValue);
				return;
			case CoreModelPackage.YLAYOUT__NUMBER_COLUMNS:
				setNumberColumns((Integer)newValue);
				return;
			case CoreModelPackage.YLAYOUT__SAVE_AND_NEW:
				setSaveAndNew((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YLAYOUT__INITIAL_EDITABLE:
				setInitialEditable(INITIAL_EDITABLE_EDEFAULT);
				return;
			case CoreModelPackage.YLAYOUT__EDITABLE:
				setEditable(EDITABLE_EDEFAULT);
				return;
			case CoreModelPackage.YLAYOUT__INITIAL_ENABLED:
				setInitialEnabled(INITIAL_ENABLED_EDEFAULT);
				return;
			case CoreModelPackage.YLAYOUT__ENABLED:
				setEnabled(ENABLED_EDEFAULT);
				return;
			case CoreModelPackage.YLAYOUT__ELEMENTS:
				getElements().clear();
				return;
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_ATTACH:
				setLastComponentAttach(LAST_COMPONENT_ATTACH_EDEFAULT);
				return;
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_DETACH:
				setLastComponentDetach(LAST_COMPONENT_DETACH_EDEFAULT);
				return;
			case CoreModelPackage.YLAYOUT__NUMBER_COLUMNS:
				setNumberColumns(NUMBER_COLUMNS_EDEFAULT);
				return;
			case CoreModelPackage.YLAYOUT__SAVE_AND_NEW:
				setSaveAndNew(SAVE_AND_NEW_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YLAYOUT__INITIAL_EDITABLE:
				return initialEditable != INITIAL_EDITABLE_EDEFAULT;
			case CoreModelPackage.YLAYOUT__EDITABLE:
				return editable != EDITABLE_EDEFAULT;
			case CoreModelPackage.YLAYOUT__INITIAL_ENABLED:
				return initialEnabled != INITIAL_ENABLED_EDEFAULT;
			case CoreModelPackage.YLAYOUT__ENABLED:
				return enabled != ENABLED_EDEFAULT;
			case CoreModelPackage.YLAYOUT__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_ATTACH:
				return LAST_COMPONENT_ATTACH_EDEFAULT == null ? lastComponentAttach != null : !LAST_COMPONENT_ATTACH_EDEFAULT.equals(lastComponentAttach);
			case CoreModelPackage.YLAYOUT__LAST_COMPONENT_DETACH:
				return LAST_COMPONENT_DETACH_EDEFAULT == null ? lastComponentDetach != null : !LAST_COMPONENT_DETACH_EDEFAULT.equals(lastComponentDetach);
			case CoreModelPackage.YLAYOUT__NUMBER_COLUMNS:
				return numberColumns != NUMBER_COLUMNS_EDEFAULT;
			case CoreModelPackage.YLAYOUT__SAVE_AND_NEW:
				return saveAndNew != SAVE_AND_NEW_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == YEditable.class) {
			switch (derivedFeatureID) {
				case CoreModelPackage.YLAYOUT__INITIAL_EDITABLE: return CoreModelPackage.YEDITABLE__INITIAL_EDITABLE;
				case CoreModelPackage.YLAYOUT__EDITABLE: return CoreModelPackage.YEDITABLE__EDITABLE;
				default: return -1;
			}
		}
		if (baseClass == YEnable.class) {
			switch (derivedFeatureID) {
				case CoreModelPackage.YLAYOUT__INITIAL_ENABLED: return CoreModelPackage.YENABLE__INITIAL_ENABLED;
				case CoreModelPackage.YLAYOUT__ENABLED: return CoreModelPackage.YENABLE__ENABLED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == YEditable.class) {
			switch (baseFeatureID) {
				case CoreModelPackage.YEDITABLE__INITIAL_EDITABLE: return CoreModelPackage.YLAYOUT__INITIAL_EDITABLE;
				case CoreModelPackage.YEDITABLE__EDITABLE: return CoreModelPackage.YLAYOUT__EDITABLE;
				default: return -1;
			}
		}
		if (baseClass == YEnable.class) {
			switch (baseFeatureID) {
				case CoreModelPackage.YENABLE__INITIAL_ENABLED: return CoreModelPackage.YLAYOUT__INITIAL_ENABLED;
				case CoreModelPackage.YENABLE__ENABLED: return CoreModelPackage.YLAYOUT__ENABLED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (initialEditable: ");
		result.append(initialEditable);
		result.append(", editable: ");
		result.append(editable);
		result.append(", initialEnabled: ");
		result.append(initialEnabled);
		result.append(", enabled: ");
		result.append(enabled);
		result.append(", lastComponentAttach: ");
		result.append(lastComponentAttach);
		result.append(", lastComponentDetach: ");
		result.append(lastComponentDetach);
		result.append(", numberColumns: ");
		result.append(numberColumns);
		result.append(", saveAndNew: ");
		result.append(saveAndNew);
		result.append(')');
		return result.toString();
	}

	@Override
	public boolean addElement(YEmbeddable element) {
		return getElements().add(element);
	}

	@Override
	public void insertElement(int index, YEmbeddable element) {
		getElements().add(Math.min(getElements().size(), index), element);
	}

	@Override
	public void moveElement(int newPosition, YEmbeddable element) {
		getElements().move(newPosition, element);
	}

	@Override
	public boolean removeElement(YEmbeddable element) {
		return getElements().remove(element);
	}

	@Override
	public int getIndex(YEmbeddable element) {
		return getElements().indexOf(element);
	}

	@Override
	public YEmbeddable getElement(int index) {
		return getElements().get(index);
	}

	@Override
	public void setLabel(String label) {
		// nothing to do
		
	}

	@Override
	public void setLabelI18nKey(String i18nKey) {
		// nothing to do
		
	}
	
	@Override
	public String getLabel() {
		return "";
	}

	@Override
	public String getLabelI18nKey() {
		return "";
	}

} // YUiLayoutImpl
