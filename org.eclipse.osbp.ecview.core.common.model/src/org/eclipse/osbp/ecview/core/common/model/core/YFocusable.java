/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YFocusable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YFocusable#getLayoutIdx <em>Layout Idx</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YFocusable#getLayoutColumns <em>Layout Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YFocusable#getTabIndex <em>Tab Index</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYFocusable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YFocusable extends EObject {
	/**
	 * Returns the value of the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout Idx</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout Idx</em>' attribute.
	 * @see #setLayoutIdx(int)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYFocusable_LayoutIdx()
	 * @model
	 * @generated
	 */
	int getLayoutIdx();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YFocusable#getLayoutIdx <em>Layout Idx</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout Idx</em>' attribute.
	 * @see #getLayoutIdx()
	 * @generated
	 */
	void setLayoutIdx(int value);

	/**
	 * Returns the value of the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout Columns</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout Columns</em>' attribute.
	 * @see #setLayoutColumns(int)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYFocusable_LayoutColumns()
	 * @model
	 * @generated
	 */
	int getLayoutColumns();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YFocusable#getLayoutColumns <em>Layout Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout Columns</em>' attribute.
	 * @see #getLayoutColumns()
	 * @generated
	 */
	void setLayoutColumns(int value);

	/**
	 * Returns the value of the '<em><b>Tab Index</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tab Index</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tab Index</em>' attribute.
	 * @see #setTabIndex(int)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYFocusable_TabIndex()
	 * @model default="-1"
	 * @generated
	 */
	int getTabIndex();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YFocusable#getTabIndex <em>Tab Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tab Index</em>' attribute.
	 * @see #getTabIndex()
	 * @generated
	 */
	void setTabIndex(int value);

} // YFocusable
