/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.authorization;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationFactory
 * @model kind="package"
 * @generated
 */
public interface AuthorizationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "authorization";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.eclipse.org/ecview/v1/core/view/authorization";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "authorization";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AuthorizationPackage eINSTANCE = org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.AuthorizationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YAuthorizationStoreImpl <em>YAuthorization Store</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YAuthorizationStoreImpl
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.AuthorizationPackageImpl#getYAuthorizationStore()
	 * @generated
	 */
	int YAUTHORIZATION_STORE = 0;

	/**
	 * The feature id for the '<em><b>Authorizations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_STORE__AUTHORIZATIONS = 0;

	/**
	 * The number of structural features of the '<em>YAuthorization Store</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_STORE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization <em>YAuthorization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.AuthorizationPackageImpl#getYAuthorization()
	 * @generated
	 */
	int YAUTHORIZATION = 1;

	/**
	 * The number of structural features of the '<em>YAuthorization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YFieldAuthorizationImpl <em>YField Authorization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YFieldAuthorizationImpl
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.AuthorizationPackageImpl#getYFieldAuthorization()
	 * @generated
	 */
	int YFIELD_AUTHORIZATION = 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFIELD_AUTHORIZATION__TARGET = YAUTHORIZATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFIELD_AUTHORIZATION__ROLES = YAUTHORIZATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFIELD_AUTHORIZATION__ACTIONS = YAUTHORIZATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>YField Authorization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YFIELD_AUTHORIZATION_FEATURE_COUNT = YAUTHORIZATION_FEATURE_COUNT + 3;


	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorizationStore <em>YAuthorization Store</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YAuthorization Store</em>'.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorizationStore
	 * @generated
	 */
	EClass getYAuthorizationStore();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorizationStore#getAuthorizations <em>Authorizations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Authorizations</em>'.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorizationStore#getAuthorizations()
	 * @see #getYAuthorizationStore()
	 * @generated
	 */
	EReference getYAuthorizationStore_Authorizations();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization <em>YAuthorization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YAuthorization</em>'.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization
	 * @generated
	 */
	EClass getYAuthorization();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization <em>YField Authorization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YField Authorization</em>'.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization
	 * @generated
	 */
	EClass getYFieldAuthorization();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization#getTarget()
	 * @see #getYFieldAuthorization()
	 * @generated
	 */
	EReference getYFieldAuthorization_Target();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Roles</em>'.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization#getRoles()
	 * @see #getYFieldAuthorization()
	 * @generated
	 */
	EAttribute getYFieldAuthorization_Roles();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Actions</em>'.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization#getActions()
	 * @see #getYFieldAuthorization()
	 * @generated
	 */
	EAttribute getYFieldAuthorization_Actions();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AuthorizationFactory getAuthorizationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YAuthorizationStoreImpl <em>YAuthorization Store</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YAuthorizationStoreImpl
		 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.AuthorizationPackageImpl#getYAuthorizationStore()
		 * @generated
		 */
		EClass YAUTHORIZATION_STORE = eINSTANCE.getYAuthorizationStore();

		/**
		 * The meta object literal for the '<em><b>Authorizations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YAUTHORIZATION_STORE__AUTHORIZATIONS = eINSTANCE.getYAuthorizationStore_Authorizations();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization <em>YAuthorization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization
		 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.AuthorizationPackageImpl#getYAuthorization()
		 * @generated
		 */
		EClass YAUTHORIZATION = eINSTANCE.getYAuthorization();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YFieldAuthorizationImpl <em>YField Authorization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YFieldAuthorizationImpl
		 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.AuthorizationPackageImpl#getYFieldAuthorization()
		 * @generated
		 */
		EClass YFIELD_AUTHORIZATION = eINSTANCE.getYFieldAuthorization();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YFIELD_AUTHORIZATION__TARGET = eINSTANCE.getYFieldAuthorization_Target();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YFIELD_AUTHORIZATION__ROLES = eINSTANCE.getYFieldAuthorization_Roles();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YFIELD_AUTHORIZATION__ACTIONS = eINSTANCE.getYFieldAuthorization_Actions();

	}

} //AuthorizationPackage
