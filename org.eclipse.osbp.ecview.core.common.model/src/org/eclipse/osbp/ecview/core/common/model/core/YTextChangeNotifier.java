/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YText Change Notifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YTextChangeNotifier#getLastTextChange <em>Last Text Change</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYTextChangeNotifier()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YTextChangeNotifier extends EObject {
	/**
	 * Returns the value of the '<em><b>Last Text Change</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Text Change</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Text Change</em>' attribute.
	 * @see #setLastTextChange(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYTextChangeNotifier_LastTextChange()
	 * @model transient="true"
	 * @generated
	 */
	String getLastTextChange();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YTextChangeNotifier#getLastTextChange <em>Last Text Change</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Text Change</em>' attribute.
	 * @see #getLastTextChange()
	 * @generated
	 */
	void setLastTextChange(String value);

} // YTextChangeNotifier
