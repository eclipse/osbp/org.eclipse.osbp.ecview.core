/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.validation.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig;
import org.eclipse.osbp.ecview.core.common.model.validation.YValidationConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YUnique Attribute Validator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.impl.YUniqueAttributeValidatorImpl#getErrorCode <em>Error Code</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.impl.YUniqueAttributeValidatorImpl#getDefaultErrorMessage <em>Default Error Message</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.impl.YUniqueAttributeValidatorImpl#getValType <em>Val Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.impl.YUniqueAttributeValidatorImpl#getValTypeFullyQualifiedName <em>Val Type Fully Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.impl.YUniqueAttributeValidatorImpl#getPropertyPath <em>Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.validation.impl.YUniqueAttributeValidatorImpl#getContainerValueBindingEndpoint <em>Container Value Binding Endpoint</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YUniqueAttributeValidatorImpl extends YValidatorImpl implements YUniqueAttributeValidator {
	/**
	 * The default value of the '{@link #getErrorCode() <em>Error Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorCode()
	 * @generated
	 * @ordered
	 */
	protected static final String ERROR_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getErrorCode() <em>Error Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorCode()
	 * @generated
	 * @ordered
	 */
	protected String errorCode = ERROR_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultErrorMessage() <em>Default Error Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultErrorMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFAULT_ERROR_MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefaultErrorMessage() <em>Default Error Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultErrorMessage()
	 * @generated
	 * @ordered
	 */
	protected String defaultErrorMessage = DEFAULT_ERROR_MESSAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValType() <em>Val Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> valType;

	/**
	 * The default value of the '{@link #getValTypeFullyQualifiedName() <em>Val Type Fully Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValTypeFullyQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String VAL_TYPE_FULLY_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValTypeFullyQualifiedName() <em>Val Type Fully Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValTypeFullyQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String valTypeFullyQualifiedName = VAL_TYPE_FULLY_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPropertyPath() <em>Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected static final String PROPERTY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPropertyPath() <em>Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected String propertyPath = PROPERTY_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainerValueBindingEndpoint() <em>Container Value Binding Endpoint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainerValueBindingEndpoint()
	 * @generated
	 * @ordered
	 */
	protected YValueBindingEndpoint containerValueBindingEndpoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YUniqueAttributeValidatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErrorCode(String newErrorCode) {
		String oldErrorCode = errorCode;
		errorCode = newErrorCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE, oldErrorCode, errorCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefaultErrorMessage() {
		return defaultErrorMessage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultErrorMessage(String newDefaultErrorMessage) {
		String oldDefaultErrorMessage = defaultErrorMessage;
		defaultErrorMessage = newDefaultErrorMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE, oldDefaultErrorMessage, defaultErrorMessage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getValType() {
		return valType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValType(Class<?> newValType) {
		Class<?> oldValType = valType;
		valType = newValType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE, oldValType, valType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValTypeFullyQualifiedName() {
		return valTypeFullyQualifiedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValTypeFullyQualifiedName(String newValTypeFullyQualifiedName) {
		String oldValTypeFullyQualifiedName = valTypeFullyQualifiedName;
		valTypeFullyQualifiedName = newValTypeFullyQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME, oldValTypeFullyQualifiedName, valTypeFullyQualifiedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPropertyPath() {
		return propertyPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyPath(String newPropertyPath) {
		String oldPropertyPath = propertyPath;
		propertyPath = newPropertyPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH, oldPropertyPath, propertyPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YValueBindingEndpoint getContainerValueBindingEndpoint() {
		if (containerValueBindingEndpoint != null && containerValueBindingEndpoint.eIsProxy()) {
			InternalEObject oldContainerValueBindingEndpoint = (InternalEObject)containerValueBindingEndpoint;
			containerValueBindingEndpoint = (YValueBindingEndpoint)eResolveProxy(oldContainerValueBindingEndpoint);
			if (containerValueBindingEndpoint != oldContainerValueBindingEndpoint) {
				InternalEObject newContainerValueBindingEndpoint = (InternalEObject)containerValueBindingEndpoint;
				NotificationChain msgs = oldContainerValueBindingEndpoint.eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT, null, null);
				if (newContainerValueBindingEndpoint.eInternalContainer() == null) {
					msgs = newContainerValueBindingEndpoint.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT, null, msgs);
				}
				if (msgs != null) msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT, oldContainerValueBindingEndpoint, containerValueBindingEndpoint));
			}
		}
		return containerValueBindingEndpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YValueBindingEndpoint basicGetContainerValueBindingEndpoint() {
		return containerValueBindingEndpoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainerValueBindingEndpoint(YValueBindingEndpoint newContainerValueBindingEndpoint, NotificationChain msgs) {
		YValueBindingEndpoint oldContainerValueBindingEndpoint = containerValueBindingEndpoint;
		containerValueBindingEndpoint = newContainerValueBindingEndpoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT, oldContainerValueBindingEndpoint, newContainerValueBindingEndpoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainerValueBindingEndpoint(YValueBindingEndpoint newContainerValueBindingEndpoint) {
		if (newContainerValueBindingEndpoint != containerValueBindingEndpoint) {
			NotificationChain msgs = null;
			if (containerValueBindingEndpoint != null)
				msgs = ((InternalEObject)containerValueBindingEndpoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT, null, msgs);
			if (newContainerValueBindingEndpoint != null)
				msgs = ((InternalEObject)newContainerValueBindingEndpoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT, null, msgs);
			msgs = basicSetContainerValueBindingEndpoint(newContainerValueBindingEndpoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT, newContainerValueBindingEndpoint, newContainerValueBindingEndpoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT:
				return basicSetContainerValueBindingEndpoint(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE:
				return getErrorCode();
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE:
				return getDefaultErrorMessage();
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE:
				return getValType();
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME:
				return getValTypeFullyQualifiedName();
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH:
				return getPropertyPath();
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT:
				if (resolve) return getContainerValueBindingEndpoint();
				return basicGetContainerValueBindingEndpoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE:
				setErrorCode((String)newValue);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE:
				setDefaultErrorMessage((String)newValue);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE:
				setValType((Class<?>)newValue);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME:
				setValTypeFullyQualifiedName((String)newValue);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH:
				setPropertyPath((String)newValue);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT:
				setContainerValueBindingEndpoint((YValueBindingEndpoint)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE:
				setErrorCode(ERROR_CODE_EDEFAULT);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE:
				setDefaultErrorMessage(DEFAULT_ERROR_MESSAGE_EDEFAULT);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE:
				setValType((Class<?>)null);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME:
				setValTypeFullyQualifiedName(VAL_TYPE_FULLY_QUALIFIED_NAME_EDEFAULT);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH:
				setPropertyPath(PROPERTY_PATH_EDEFAULT);
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT:
				setContainerValueBindingEndpoint((YValueBindingEndpoint)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE:
				return ERROR_CODE_EDEFAULT == null ? errorCode != null : !ERROR_CODE_EDEFAULT.equals(errorCode);
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE:
				return DEFAULT_ERROR_MESSAGE_EDEFAULT == null ? defaultErrorMessage != null : !DEFAULT_ERROR_MESSAGE_EDEFAULT.equals(defaultErrorMessage);
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE:
				return valType != null;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME:
				return VAL_TYPE_FULLY_QUALIFIED_NAME_EDEFAULT == null ? valTypeFullyQualifiedName != null : !VAL_TYPE_FULLY_QUALIFIED_NAME_EDEFAULT.equals(valTypeFullyQualifiedName);
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH:
				return PROPERTY_PATH_EDEFAULT == null ? propertyPath != null : !PROPERTY_PATH_EDEFAULT.equals(propertyPath);
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT:
				return containerValueBindingEndpoint != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == YValidationConfig.class) {
			switch (derivedFeatureID) {
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE: return ValidationPackage.YVALIDATION_CONFIG__ERROR_CODE;
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE: return ValidationPackage.YVALIDATION_CONFIG__DEFAULT_ERROR_MESSAGE;
				default: return -1;
			}
		}
		if (baseClass == YUniqueAttributeValidatorConfig.class) {
			switch (derivedFeatureID) {
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE;
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE_FULLY_QUALIFIED_NAME;
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__PROPERTY_PATH;
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == YValidationConfig.class) {
			switch (baseFeatureID) {
				case ValidationPackage.YVALIDATION_CONFIG__ERROR_CODE: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE;
				case ValidationPackage.YVALIDATION_CONFIG__DEFAULT_ERROR_MESSAGE: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE;
				default: return -1;
			}
		}
		if (baseClass == YUniqueAttributeValidatorConfig.class) {
			switch (baseFeatureID) {
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE;
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE_FULLY_QUALIFIED_NAME: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME;
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__PROPERTY_PATH: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH;
				case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT: return ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (errorCode: ");
		result.append(errorCode);
		result.append(", defaultErrorMessage: ");
		result.append(defaultErrorMessage);
		result.append(", valType: ");
		result.append(valType);
		result.append(", valTypeFullyQualifiedName: ");
		result.append(valTypeFullyQualifiedName);
		result.append(", propertyPath: ");
		result.append(propertyPath);
		result.append(')');
		return result.toString();
	}

} //YUniqueAttributeValidatorImpl
