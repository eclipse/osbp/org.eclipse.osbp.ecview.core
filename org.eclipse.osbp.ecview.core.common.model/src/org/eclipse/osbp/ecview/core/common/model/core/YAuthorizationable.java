/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;

import org.eclipse.emf.ecore.EObject;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YAuthorizationable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable#getAuthorizationGroup <em>Authorization Group</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable#getAuthorizationId <em>Authorization Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYAuthorizationable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YAuthorizationable extends EObject {
	/**
	 * Returns the value of the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorization Group</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorization Group</em>' attribute.
	 * @see #setAuthorizationGroup(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYAuthorizationable_AuthorizationGroup()
	 * @model
	 * @generated
	 */
	String getAuthorizationGroup();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable#getAuthorizationGroup <em>Authorization Group</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Authorization Group</em>' attribute.
	 * @see #getAuthorizationGroup()
	 * @generated
	 */
	void setAuthorizationGroup(String value);

	/**
	 * Returns the value of the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorization Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorization Id</em>' attribute.
	 * @see #setAuthorizationId(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYAuthorizationable_AuthorizationId()
	 * @model
	 * @generated
	 */
	String getAuthorizationId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable#getAuthorizationId <em>Authorization Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Authorization Id</em>' attribute.
	 * @see #getAuthorizationId()
	 * @generated
	 */
	void setAuthorizationId(String value);

} // YAuthorizationable
