/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.datatypes;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.validation.YValidator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YDatatype</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.datatypes.YDatatype#getValidators <em>Validators</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.datatypes.DatatypesPackage#getYDatatype()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YDatatype extends YDtBase {

	/**
	 * Returns the value of the '<em><b>Validators</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.validation.YValidator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validators</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validators</em>' reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.datatypes.DatatypesPackage#getYDatatype_Validators()
	 * @model
	 * @generated
	 */
	EList<YValidator> getValidators();
} // YDatatype
