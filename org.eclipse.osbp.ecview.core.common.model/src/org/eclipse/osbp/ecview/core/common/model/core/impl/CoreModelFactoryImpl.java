/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core.impl;

import java.net.URI;
import java.util.Map;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.ecview.core.common.model.core.*;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YAction;
import org.eclipse.osbp.ecview.core.common.model.core.YActivatedEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YAlignment;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YCommandSet;
import org.eclipse.osbp.ecview.core.common.model.core.YContextValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YDeviceType;
import org.eclipse.osbp.ecview.core.common.model.core.YDialog;
import org.eclipse.osbp.ecview.core.common.model.core.YDtWrapper;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableCollectionEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableMultiSelectionEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableSelectionEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableValueEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YExposedAction;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YFlatAlignment;
import org.eclipse.osbp.ecview.core.common.model.core.YKeyCode;
import org.eclipse.osbp.ecview.core.common.model.core.YKeyStrokeDefinition;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YModifierKey;
import org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand;
import org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand;
import org.eclipse.osbp.ecview.core.common.model.core.YUnit;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.YViewSet;
import org.eclipse.osbp.ecview.core.common.model.core.listeners.YValueChangeListener;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoreModelFactoryImpl extends EFactoryImpl implements CoreModelFactory {
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static CoreModelFactory init() {
		try {
			CoreModelFactory theCoreModelFactory = (CoreModelFactory)EPackage.Registry.INSTANCE.getEFactory(CoreModelPackage.eNS_URI);
			if (theCoreModelFactory != null) {
				return theCoreModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CoreModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoreModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CoreModelPackage.YSTRING_TO_STRING_MAP: return (EObject)createYStringToStringMap();
			case CoreModelPackage.YLAYOUT: return createYLayout();
			case CoreModelPackage.YHELPER_LAYOUT: return createYHelperLayout();
			case CoreModelPackage.YFIELD: return createYField();
			case CoreModelPackage.YVIEW: return createYView();
			case CoreModelPackage.YVIEW_SET: return createYViewSet();
			case CoreModelPackage.YBEAN_SLOT: return createYBeanSlot();
			case CoreModelPackage.YDETAIL_BEAN_SLOT: return createYDetailBeanSlot();
			case CoreModelPackage.YDIALOG: return createYDialog();
			case CoreModelPackage.YACTION: return createYAction();
			case CoreModelPackage.YCONTEXT_VALUE_BINDING_ENDPOINT: return createYContextValueBindingEndpoint();
			case CoreModelPackage.YBEAN_SLOT_VALUE_BINDING_ENDPOINT: return createYBeanSlotValueBindingEndpoint();
			case CoreModelPackage.YBEAN_SLOT_LIST_BINDING_ENDPOINT: return createYBeanSlotListBindingEndpoint();
			case CoreModelPackage.YEMBEDDABLE_VALUE_ENDPOINT: return createYEmbeddableValueEndpoint();
			case CoreModelPackage.YEMBEDDABLE_SELECTION_ENDPOINT: return createYEmbeddableSelectionEndpoint();
			case CoreModelPackage.YEMBEDDABLE_MULTI_SELECTION_ENDPOINT: return createYEmbeddableMultiSelectionEndpoint();
			case CoreModelPackage.YEMBEDDABLE_COLLECTION_ENDPOINT: return createYEmbeddableCollectionEndpoint();
			case CoreModelPackage.YACTIVATED_ENDPOINT: return createYActivatedEndpoint();
			case CoreModelPackage.YDT_WRAPPER: return createYDtWrapper();
			case CoreModelPackage.YCOMMAND_SET: return createYCommandSet();
			case CoreModelPackage.YOPEN_DIALOG_COMMAND: return createYOpenDialogCommand();
			case CoreModelPackage.YEXPOSED_ACTION: return createYExposedAction();
			case CoreModelPackage.YSEND_EVENT_COMMAND: return createYSendEventCommand();
			case CoreModelPackage.YKEY_STROKE_DEFINITION: return createYKeyStrokeDefinition();
			case CoreModelPackage.YDELEGATE_CONVERTER: return createYDelegateConverter();
			case CoreModelPackage.YEMBEDDABLE_EVENT: return createYEmbeddableEvent();
			case CoreModelPackage.YCONTEXT_CLICK_EVENT: return createYContextClickEvent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the object
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case CoreModelPackage.YDEVICE_TYPE:
				return createYDeviceTypeFromString(eDataType, initialValue);
			case CoreModelPackage.YALIGNMENT:
				return createYAlignmentFromString(eDataType, initialValue);
			case CoreModelPackage.YFLAT_ALIGNMENT:
				return createYFlatAlignmentFromString(eDataType, initialValue);
			case CoreModelPackage.YUNIT:
				return createYUnitFromString(eDataType, initialValue);
			case CoreModelPackage.YKEY_CODE:
				return createYKeyCodeFromString(eDataType, initialValue);
			case CoreModelPackage.YMODIFIER_KEY:
				return createYModifierKeyFromString(eDataType, initialValue);
			case CoreModelPackage.YORIENTATION:
				return createYOrientationFromString(eDataType, initialValue);
			case CoreModelPackage.YCOMPARE:
				return createYCompareFromString(eDataType, initialValue);
			case CoreModelPackage.YURI:
				return createYURIFromString(eDataType, initialValue);
			case CoreModelPackage.YVALUE_CHANGE_LISTENER:
				return createYValueChangeListenerFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case CoreModelPackage.YDEVICE_TYPE:
				return convertYDeviceTypeToString(eDataType, instanceValue);
			case CoreModelPackage.YALIGNMENT:
				return convertYAlignmentToString(eDataType, instanceValue);
			case CoreModelPackage.YFLAT_ALIGNMENT:
				return convertYFlatAlignmentToString(eDataType, instanceValue);
			case CoreModelPackage.YUNIT:
				return convertYUnitToString(eDataType, instanceValue);
			case CoreModelPackage.YKEY_CODE:
				return convertYKeyCodeToString(eDataType, instanceValue);
			case CoreModelPackage.YMODIFIER_KEY:
				return convertYModifierKeyToString(eDataType, instanceValue);
			case CoreModelPackage.YORIENTATION:
				return convertYOrientationToString(eDataType, instanceValue);
			case CoreModelPackage.YCOMPARE:
				return convertYCompareToString(eDataType, instanceValue);
			case CoreModelPackage.YURI:
				return convertYURIToString(eDataType, instanceValue);
			case CoreModelPackage.YVALUE_CHANGE_LISTENER:
				return convertYValueChangeListenerToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the map. entry
	 * @generated
	 */
	public Map.Entry<String, String> createYStringToStringMap() {
		YStringToStringMapImpl yStringToStringMap = new YStringToStringMapImpl();
		return yStringToStringMap;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layout
	 * @generated
	 */
	public YLayout createYLayout() {
		YLayoutImpl yLayout = new YLayoutImpl();
		return yLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y helper layout
	 * @generated
	 */
	public YHelperLayout createYHelperLayout() {
		YHelperLayoutImpl yHelperLayout = new YHelperLayoutImpl();
		return yHelperLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y field
	 * @generated
	 */
	public YField createYField() {
		YFieldImpl yField = new YFieldImpl();
		return yField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y view
	 * @generated
	 */
	public YView createYView() {
		YViewImpl yView = new YViewImpl();
		return yView;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y view set
	 * @generated
	 */
	public YViewSet createYViewSet() {
		YViewSetImpl yViewSet = new YViewSetImpl();
		return yViewSet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean slot
	 * @generated
	 */
	public YBeanSlot createYBeanSlot() {
		YBeanSlotImpl yBeanSlot = new YBeanSlotImpl();
		return yBeanSlot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDetailBeanSlot createYDetailBeanSlot() {
		YDetailBeanSlotImpl yDetailBeanSlot = new YDetailBeanSlotImpl();
		return yDetailBeanSlot;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y dialog
	 * @generated
	 */
	public YDialog createYDialog() {
		YDialogImpl yDialog = new YDialogImpl();
		return yDialog;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y action
	 * @generated
	 */
	public YAction createYAction() {
		YActionImpl yAction = new YActionImpl();
		return yAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y context value binding endpoint
	 * @generated
	 */
	public YContextValueBindingEndpoint createYContextValueBindingEndpoint() {
		YContextValueBindingEndpointImpl yContextValueBindingEndpoint = new YContextValueBindingEndpointImpl();
		return yContextValueBindingEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean slot value binding endpoint
	 * @generated
	 */
	public YBeanSlotValueBindingEndpoint createYBeanSlotValueBindingEndpoint() {
		YBeanSlotValueBindingEndpointImpl yBeanSlotValueBindingEndpoint = new YBeanSlotValueBindingEndpointImpl();
		return yBeanSlotValueBindingEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean slot list binding endpoint
	 * @generated
	 */
	public YBeanSlotListBindingEndpoint createYBeanSlotListBindingEndpoint() {
		YBeanSlotListBindingEndpointImpl yBeanSlotListBindingEndpoint = new YBeanSlotListBindingEndpointImpl();
		return yBeanSlotListBindingEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable value endpoint
	 * @generated
	 */
	public YEmbeddableValueEndpoint createYEmbeddableValueEndpoint() {
		YEmbeddableValueEndpointImpl yEmbeddableValueEndpoint = new YEmbeddableValueEndpointImpl();
		return yEmbeddableValueEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable selection endpoint
	 * @generated
	 */
	public YEmbeddableSelectionEndpoint createYEmbeddableSelectionEndpoint() {
		YEmbeddableSelectionEndpointImpl yEmbeddableSelectionEndpoint = new YEmbeddableSelectionEndpointImpl();
		return yEmbeddableSelectionEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable multi selection endpoint
	 * @generated
	 */
	public YEmbeddableMultiSelectionEndpoint createYEmbeddableMultiSelectionEndpoint() {
		YEmbeddableMultiSelectionEndpointImpl yEmbeddableMultiSelectionEndpoint = new YEmbeddableMultiSelectionEndpointImpl();
		return yEmbeddableMultiSelectionEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable collection endpoint
	 * @generated
	 */
	public YEmbeddableCollectionEndpoint createYEmbeddableCollectionEndpoint() {
		YEmbeddableCollectionEndpointImpl yEmbeddableCollectionEndpoint = new YEmbeddableCollectionEndpointImpl();
		return yEmbeddableCollectionEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y activated endpoint
	 * @generated
	 */
	public YActivatedEndpoint createYActivatedEndpoint() {
		YActivatedEndpointImpl yActivatedEndpoint = new YActivatedEndpointImpl();
		return yActivatedEndpoint;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y dt wrapper
	 * @generated
	 */
	public YDtWrapper createYDtWrapper() {
		YDtWrapperImpl yDtWrapper = new YDtWrapperImpl();
		return yDtWrapper;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y command set
	 * @generated
	 */
	public YCommandSet createYCommandSet() {
		YCommandSetImpl yCommandSet = new YCommandSetImpl();
		return yCommandSet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y open dialog command
	 * @generated
	 */
	public YOpenDialogCommand createYOpenDialogCommand() {
		YOpenDialogCommandImpl yOpenDialogCommand = new YOpenDialogCommandImpl();
		return yOpenDialogCommand;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y exposed action
	 * @generated
	 */
	public YExposedAction createYExposedAction() {
		YExposedActionImpl yExposedAction = new YExposedActionImpl();
		return yExposedAction;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y send event command
	 * @generated
	 */
	public YSendEventCommand createYSendEventCommand() {
		YSendEventCommandImpl ySendEventCommand = new YSendEventCommandImpl();
		return ySendEventCommand;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y key stroke definition
	 * @generated
	 */
	public YKeyStrokeDefinition createYKeyStrokeDefinition() {
		YKeyStrokeDefinitionImpl yKeyStrokeDefinition = new YKeyStrokeDefinitionImpl();
		return yKeyStrokeDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y delegate converter
	 * @generated
	 */
	public YDelegateConverter createYDelegateConverter() {
		YDelegateConverterImpl yDelegateConverter = new YDelegateConverterImpl();
		return yDelegateConverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddableEvent createYEmbeddableEvent() {
		YEmbeddableEventImpl yEmbeddableEvent = new YEmbeddableEventImpl();
		return yEmbeddableEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YContextClickEvent createYContextClickEvent() {
		YContextClickEventImpl yContextClickEvent = new YContextClickEventImpl();
		return yContextClickEvent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y device type
	 * @generated
	 */
	public YDeviceType createYDeviceTypeFromString(EDataType eDataType, String initialValue) {
		YDeviceType result = YDeviceType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYDeviceTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y alignment
	 * @generated
	 */
	public YAlignment createYAlignmentFromString(EDataType eDataType, String initialValue) {
		YAlignment result = YAlignment.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYAlignmentToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y flat alignment
	 * @generated
	 */
	public YFlatAlignment createYFlatAlignmentFromString(EDataType eDataType, String initialValue) {
		YFlatAlignment result = YFlatAlignment.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYFlatAlignmentToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y unit
	 * @generated
	 */
	public YUnit createYUnitFromString(EDataType eDataType, String initialValue) {
		YUnit result = YUnit.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYUnitToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y key code
	 * @generated
	 */
	public YKeyCode createYKeyCodeFromString(EDataType eDataType, String initialValue) {
		YKeyCode result = YKeyCode.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYKeyCodeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y modifier key
	 * @generated
	 */
	public YModifierKey createYModifierKeyFromString(EDataType eDataType, String initialValue) {
		YModifierKey result = YModifierKey.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYModifierKeyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y orientation
	 * @generated
	 */
	public YOrientation createYOrientationFromString(EDataType eDataType, String initialValue) {
		YOrientation result = YOrientation.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYOrientationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y compare
	 * @generated
	 */
	public YCompare createYCompareFromString(EDataType eDataType, String initialValue) {
		YCompare result = YCompare.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYCompareToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the uri
	 * @generated
	 */
	public URI createYURIFromString(EDataType eDataType, String initialValue) {
		return (URI)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYURIToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y value change listener
	 * @generated
	 */
	public YValueChangeListener createYValueChangeListenerFromString(EDataType eDataType, String initialValue) {
		return (YValueChangeListener)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYValueChangeListenerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the core model package
	 * @generated
	 */
	public CoreModelPackage getCoreModelPackage() {
		return (CoreModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CoreModelPackage getPackage() {
		return CoreModelPackage.eINSTANCE;
	}

} //CoreModelFactoryImpl
