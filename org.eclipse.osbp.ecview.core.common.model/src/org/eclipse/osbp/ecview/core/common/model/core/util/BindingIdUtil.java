/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.common.model.core.util;

public class BindingIdUtil {

	public static final String UI_ACTIVATED = "uiActivated/";
	public static final String UI_VISIBILITY_PROCESSOR = "uiVisibilityProcessor/";
	public static final String UI_VALUE = "uiValue/";
	public static final String UI_SELECTION = "uiSelection/";
	public static final String MODEL_BINDING = "modelBinding/";
	public static final String BEANVALUE = "beanvalue/";
	public static final String BEANSLOT = "beanslot/";

	public static String getBeanSlotId(String beanSlot, String propertyPath) {
		// returns a string of pattern "beanslot/{name}/{path}"
		StringBuilder b = new StringBuilder();
		b.append(BEANSLOT);
		b.append(beanSlot);
		b.append("/");
		b.append(propertyPath);
		return b.toString();
	}

	public static String getBeanValueId(String beanName, String propertyPath) {
		// returns a string of pattern "beanslot/{name}/{path}"
		StringBuilder b = new StringBuilder();
		b.append(BEANVALUE);
		b.append(beanName);
		b.append("/");
		b.append(propertyPath);
		return b.toString();
	}

	public static String getDetailValueId(String masterId, String propertyPath) {
		StringBuilder b = new StringBuilder();
		b.append(masterId);
		b.append("/");
		b.append(propertyPath);
		return b.toString();
	}

	public static String getECViewModelValueId(String elementId,
			String propertyPath) {
		StringBuilder b = new StringBuilder();
		b.append(MODEL_BINDING);
		b.append(elementId);
		b.append("/");
		b.append(propertyPath);
		return b.toString();
	}

	public static String getEmbeddableSelectionId(String elementId,
			String propertyPath) {
		StringBuilder b = new StringBuilder();
		b.append(UI_SELECTION);
		b.append(elementId);
		b.append("/");
		b.append(propertyPath);
		return b.toString();
	}

	public static String getEmbeddableValueId(String elementId) {
		StringBuilder b = new StringBuilder();
		b.append(UI_VALUE);
		b.append(elementId);
		return b.toString();
	}

	public static String getVisibilityProcessorId(String property) {
		StringBuilder b = new StringBuilder();
		b.append(UI_VISIBILITY_PROCESSOR);
		b.append(property);
		return b.toString();
	}

	public static String getActivatedId(String elementId) {
		StringBuilder b = new StringBuilder();
		b.append(UI_ACTIVATED);
		b.append(elementId);
		return b.toString();
	}
}
