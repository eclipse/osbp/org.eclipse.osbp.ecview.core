/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.validation;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage
 * @generated
 */
public interface ValidationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ValidationFactory eINSTANCE = org.eclipse.osbp.ecview.core.common.model.validation.impl.ValidationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>YMin Length Validator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YMin Length Validator</em>'.
	 * @generated
	 */
	YMinLengthValidator createYMinLengthValidator();

	/**
	 * Returns a new object of class '<em>YMax Length Validator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YMax Length Validator</em>'.
	 * @generated
	 */
	YMaxLengthValidator createYMaxLengthValidator();

	/**
	 * Returns a new object of class '<em>YRegexp Validator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YRegexp Validator</em>'.
	 * @generated
	 */
	YRegexpValidator createYRegexpValidator();

	/**
	 * Returns a new object of class '<em>YClass Delegate Validator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YClass Delegate Validator</em>'.
	 * @generated
	 */
	YClassDelegateValidator createYClassDelegateValidator();

	/**
	 * Returns a new object of class '<em>YBean Validation Validator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YBean Validation Validator</em>'.
	 * @generated
	 */
	YBeanValidationValidator createYBeanValidationValidator();

	/**
	 * Returns a new object of class '<em>YUnique Attribute Validator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YUnique Attribute Validator</em>'.
	 * @generated
	 */
	YUniqueAttributeValidator createYUniqueAttributeValidator();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ValidationPackage getValidationPackage();

} //ValidationFactory
