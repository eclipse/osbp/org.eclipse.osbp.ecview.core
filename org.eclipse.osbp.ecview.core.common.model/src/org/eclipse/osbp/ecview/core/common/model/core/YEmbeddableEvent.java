/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;

import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YEmbeddable Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent#getTime <em>Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent#getEmbeddable <em>Embeddable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent#getRawEvent <em>Raw Event</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddableEvent()
 * @model
 * @generated
 */
public interface YEmbeddableEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(Date)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddableEvent_Time()
	 * @model transient="true"
	 * @generated
	 */
	Date getTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(Date value);

	/**
	 * Returns the value of the '<em><b>Embeddable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Embeddable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Embeddable</em>' reference.
	 * @see #setEmbeddable(YEmbeddable)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddableEvent_Embeddable()
	 * @model transient="true"
	 * @generated
	 */
	YEmbeddable getEmbeddable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent#getEmbeddable <em>Embeddable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Embeddable</em>' reference.
	 * @see #getEmbeddable()
	 * @generated
	 */
	void setEmbeddable(YEmbeddable value);

	/**
	 * Returns the value of the '<em><b>Raw Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raw Event</em>' attribute.
	 * @see #setRawEvent(Object)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYEmbeddableEvent_RawEvent()
	 * @model transient="true"
	 * @generated
	 */
	Object getRawEvent();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent#getRawEvent <em>Raw Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Raw Event</em>' attribute.
	 * @see #getRawEvent()
	 * @generated
	 */
	void setRawEvent(Object value);

} // YEmbeddableEvent
