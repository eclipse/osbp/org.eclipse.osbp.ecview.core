/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;


// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YExposed Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getIcon <em>Icon</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getExternalClickTime <em>External Click Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getInternalClickTime <em>Internal Click Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getExecutedNotificationTime <em>Executed Notification Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getCanceledNotificationTime <em>Canceled Notification Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#isCheckDirty <em>Check Dirty</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getExternalCommandId <em>External Command Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction()
 * @model
 * @generated
 */
public interface YExposedAction extends YElement, YEnable {
	/**
	 * Returns the value of the '<em><b>Icon</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Icon</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Icon</em>' attribute.
	 * @see #setIcon(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_Icon()
	 * @model
	 * @generated
	 */
	String getIcon();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getIcon <em>Icon</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Icon</em>' attribute.
	 * @see #getIcon()
	 * @generated
	 */
	void setIcon(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #setLabelI18nKey(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_LabelI18nKey()
	 * @model
	 * @generated
	 */
	String getLabelI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getLabelI18nKey <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #getLabelI18nKey()
	 * @generated
	 */
	void setLabelI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>External Click Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Click Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Click Time</em>' attribute.
	 * @see #setExternalClickTime(long)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_ExternalClickTime()
	 * @model
	 * @generated
	 */
	long getExternalClickTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getExternalClickTime <em>External Click Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Click Time</em>' attribute.
	 * @see #getExternalClickTime()
	 * @generated
	 */
	void setExternalClickTime(long value);

	/**
	 * Returns the value of the '<em><b>Internal Click Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Click Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Click Time</em>' attribute.
	 * @see #setInternalClickTime(long)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_InternalClickTime()
	 * @model
	 * @generated
	 */
	long getInternalClickTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getInternalClickTime <em>Internal Click Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal Click Time</em>' attribute.
	 * @see #getInternalClickTime()
	 * @generated
	 */
	void setInternalClickTime(long value);

	/**
	 * Returns the value of the '<em><b>Executed Notification Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Executed Notification Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Executed Notification Time</em>' attribute.
	 * @see #setExecutedNotificationTime(long)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_ExecutedNotificationTime()
	 * @model
	 * @generated
	 */
	long getExecutedNotificationTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getExecutedNotificationTime <em>Executed Notification Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Executed Notification Time</em>' attribute.
	 * @see #getExecutedNotificationTime()
	 * @generated
	 */
	void setExecutedNotificationTime(long value);

	/**
	 * Returns the value of the '<em><b>Canceled Notification Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Canceled Notification Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Canceled Notification Time</em>' attribute.
	 * @see #setCanceledNotificationTime(long)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_CanceledNotificationTime()
	 * @model
	 * @generated
	 */
	long getCanceledNotificationTime();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getCanceledNotificationTime <em>Canceled Notification Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Canceled Notification Time</em>' attribute.
	 * @see #getCanceledNotificationTime()
	 * @generated
	 */
	void setCanceledNotificationTime(long value);

	/**
	 * Returns the value of the '<em><b>Check Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Check Dirty</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Check Dirty</em>' attribute.
	 * @see #setCheckDirty(boolean)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_CheckDirty()
	 * @model
	 * @generated
	 */
	boolean isCheckDirty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#isCheckDirty <em>Check Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Check Dirty</em>' attribute.
	 * @see #isCheckDirty()
	 * @generated
	 */
	void setCheckDirty(boolean value);

	/**
	 * Returns the value of the '<em><b>External Command Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Command Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Command Id</em>' attribute.
	 * @see #setExternalCommandId(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYExposedAction_ExternalCommandId()
	 * @model
	 * @generated
	 */
	String getExternalCommandId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YExposedAction#getExternalCommandId <em>External Command Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Command Id</em>' attribute.
	 * @see #getExternalCommandId()
	 * @generated
	 */
	void setExternalCommandId(String value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the view
	 * @model kind="operation"
	 * @generated
	 */
	YView getView();

} // YExposedAction
