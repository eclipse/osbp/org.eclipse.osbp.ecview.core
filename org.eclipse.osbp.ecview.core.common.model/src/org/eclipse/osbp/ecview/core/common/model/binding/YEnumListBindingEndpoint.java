/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.binding;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YEnum List Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YEnumListBindingEndpoint#getEnum <em>Enum</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYEnumListBindingEndpoint()
 * @model
 * @generated
 */
public interface YEnumListBindingEndpoint extends YListBindingEndpoint {
	/**
	 * Returns the value of the '<em><b>Enum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enum</em>' attribute.
	 * @see #setEnum(Class)
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYEnumListBindingEndpoint_Enum()
	 * @model required="true"
	 * @generated
	 */
	Class<?> getEnum();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YEnumListBindingEndpoint#getEnum <em>Enum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enum</em>' attribute.
	 * @see #getEnum()
	 * @generated
	 */
	void setEnum(Class<?> value);

} // YEnumListBindingEndpoint
