/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YHelperLayout;

/**
 * <!-- begin-user-doc --> 
 * 
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YHelperLayoutImpl#getVirtualElements <em>Virtual Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YHelperLayoutImpl extends YLayoutImpl implements YHelperLayout {
	/**
	 * The cached value of the '{@link #getVirtualElements() <em>Virtual Elements</em>}' reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getVirtualElements()
	 * @generated
	 * @ordered
	 */
	protected EList<YEmbeddable> virtualElements;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected YHelperLayoutImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoreModelPackage.Literals.YHELPER_LAYOUT;
	}

	/**
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public EList<YEmbeddable> getVirtualElements() {
		if (virtualElements == null) {
			virtualElements = new EObjectResolvingEList<YEmbeddable>(YEmbeddable.class, this, CoreModelPackage.YHELPER_LAYOUT__VIRTUAL_ELEMENTS);
		}
		return virtualElements;
	}

	/**
	 * This method is responsible to delegate its call to virtual elements. A
	 * virtual element is an element that is <b>NOT</b> contained by its parent.
	 * For instance see CxGrid#column#editorfield. See {@link YHelperLayoutImpl}
	 * .
	 */
	@Override
	public EList<YEmbeddable> getElements() {
		return getVirtualElements();
	}

	@Override
	public boolean addElement(YEmbeddable element) {
		return getVirtualElements().add(element);
	}

	@Override
	public void insertElement(int index, YEmbeddable element) {
		getVirtualElements().add(index, element);
	}

	@Override
	public void moveElement(int newPosition, YEmbeddable element) {
		getVirtualElements().move(newPosition, element);
	}

	@Override
	public boolean removeElement(YEmbeddable element) {
		return getVirtualElements().remove(element);
	}

	@Override
	public YEmbeddable getElement(int index) {
		return getVirtualElements().get(index);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoreModelPackage.YHELPER_LAYOUT__VIRTUAL_ELEMENTS:
				return getVirtualElements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoreModelPackage.YHELPER_LAYOUT__VIRTUAL_ELEMENTS:
				getVirtualElements().clear();
				getVirtualElements().addAll((Collection<? extends YEmbeddable>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YHELPER_LAYOUT__VIRTUAL_ELEMENTS:
				getVirtualElements().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YHELPER_LAYOUT__VIRTUAL_ELEMENTS:
				return virtualElements != null && !virtualElements.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // YHelperLayoutImpl
