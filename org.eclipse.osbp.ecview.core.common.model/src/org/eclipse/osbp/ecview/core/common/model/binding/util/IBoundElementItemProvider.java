/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.binding.util;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint;

// TODO: Auto-generated Javadoc
/**
 * ItemProvider which is used to determine information about existing bindings.
 */
public interface IBoundElementItemProvider {

	/**
	 * Returns the bound element for the given object. Or <code>null</code> if
	 * no EObject was bound.
	 *
	 * @param object
	 *            the object
	 * @return the bound element
	 */
	EObject getBoundElement(YBindingEndpoint object);

	/**
	 * Returns the first bound feature, or <code>null</code> if no EObject was
	 * bound.
	 * <p>
	 * If more then one features are used for a nested binding, then the first
	 * one will be returned.
	 *
	 * @param object
	 *            the object
	 * @return the first bound feature
	 */
	EStructuralFeature getFirstBoundFeature(YBindingEndpoint object);

	/**
	 * Returns the list of bound features, or an empty list if no EObject was
	 * bound.
	 * <p>
	 * The result is ordered in the way, the features are applied to the
	 * bindings.
	 *
	 * @param object
	 *            the object
	 * @return the bound feature list
	 */
	List<EStructuralFeature> getBoundFeatureList(YBindingEndpoint object);
}
