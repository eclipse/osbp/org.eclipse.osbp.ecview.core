/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.authorization;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YAuthorization</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationPackage#getYAuthorization()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YAuthorization extends EObject {
	/**
	 * Returns true, if the role may execute the action on the given embeddable.
	 * If embeddable == null is passed, subclasses need to decide what should
	 * happen.
	 */
	boolean isAllowed(String role, String action, YEmbeddable embeddable);

} // YAuthorization
