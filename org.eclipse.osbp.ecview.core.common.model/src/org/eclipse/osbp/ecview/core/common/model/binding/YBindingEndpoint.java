/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.binding;

import org.eclipse.osbp.ecview.core.common.model.core.YElement;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YBinding Endpoint</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#isActive <em>Active</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYBindingEndpoint()
 * @model abstract="true"
 * @generated
 */
public interface YBindingEndpoint extends YElement {
	
	/**
	 * Returns the value of the '<em><b>Active</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active</em>' attribute.
	 * @see #setActive(boolean)
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYBindingEndpoint_Active()
	 * @model default="true"
	 * @generated
	 */
	boolean isActive();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#isActive <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active</em>' attribute.
	 * @see #isActive()
	 * @generated
	 */
	void setActive(boolean value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the binding
	 * @model kind="operation"
	 * @generated
	 */
	YBinding getBinding();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param element
	 *            the element
	 * @return true, if is binds element
	 * @model elementRequired="true"
	 * @generated
	 */
	boolean isBindsElement(YElement element);

//	/**
//	 * Returns the type of bound feature. It is used to validate, whether two
//	 * endpoints are compatible.
//	 * 
//	 * @return
//	 */
//	Class<?> getType();
//
//	/**
//	 * Checks whether the endpoints are compatible to each other.
//	 * 
//	 * @param other
//	 * @return
//	 */
//	IStatus isCompatible(YBindingEndpoint other);

} // YBindingEndpoint
