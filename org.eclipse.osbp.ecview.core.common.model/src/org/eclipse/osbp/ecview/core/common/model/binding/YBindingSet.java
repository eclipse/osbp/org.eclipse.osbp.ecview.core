/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.binding;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YBinding Set</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#getBindings <em>Bindings</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#getTransientBindings <em>Transient Bindings</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYBindingSet()
 * @model
 * @generated
 */
public interface YBindingSet extends YElement {
	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Bindings</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYBindingSet_Bindings()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YBinding> getBindings();

	/**
	 * Returns the value of the '<em><b>Transient Bindings</b></em>' reference
	 * list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transient Bindings</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Transient Bindings</em>' reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYBindingSet_TransientBindings()
	 * @model transient="true"
	 * @generated
	 */
	EList<YBinding> getTransientBindings();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @return the y value binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 * @generated
	 */
	YValueBinding addBinding(YValueBindingEndpoint targetValue,
			YValueBindingEndpoint modelValue);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @return the y list binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 * @generated
	 */
	YListBinding addBinding(YListBindingEndpoint targetValue,
			YListBindingEndpoint modelValue);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @param targetToModelStrategy
	 *            the target to model strategy
	 * @param modelToTargetStrategy
	 *            the model to target strategy
	 * @return the y value binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 *        targetToModelStrategyRequired="true"
	 *        modelToTargetStrategyRequired="true"
	 * @generated
	 */
	YValueBinding addBinding(YValueBindingEndpoint targetValue,
			YValueBindingEndpoint modelValue,
			YBindingUpdateStrategy targetToModelStrategy,
			YBindingUpdateStrategy modelToTargetStrategy);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @param targetToModelStrategy
	 *            the target to model strategy
	 * @param modelToTargetStrategy
	 *            the model to target strategy
	 * @return the y list binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 *        targetToModelStrategyRequired="true"
	 *        modelToTargetStrategyRequired="true"
	 * @generated
	 */
	YListBinding addBinding(YListBindingEndpoint targetValue,
			YListBindingEndpoint modelValue,
			YBindingUpdateStrategy targetToModelStrategy,
			YBindingUpdateStrategy modelToTargetStrategy);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the view
	 * @model kind="operation"
	 * @generated
	 */
	YView getView();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param binding
	 *            the binding
	 * @model bindingRequired="true"
	 * @generated
	 */
	void addBinding(YBinding binding);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param binding
	 *            the binding
	 * @model bindingRequired="true"
	 * @generated
	 */
	void removeBinding(YBinding binding);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param binding
	 *            the binding
	 * @model bindingRequired="true"
	 */
	void addTransientBinding(YBinding binding);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @return the y list binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 */
	YListBinding addTransientBinding(YListBindingEndpoint targetValue,
			YListBindingEndpoint modelValue);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @param targetToModelStrategy
	 *            the target to model strategy
	 * @param modelToTargetStrategy
	 *            the model to target strategy
	 * @return the y list binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 *        targetToModelStrategyRequired="true"
	 *        modelToTargetStrategyRequired="true"
	 */
	YListBinding addTransientBinding(YListBindingEndpoint targetValue,
			YListBindingEndpoint modelValue,
			YBindingUpdateStrategy targetToModelStrategy,
			YBindingUpdateStrategy modelToTargetStrategy);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @return the y value binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 */
	YValueBinding addTransientBinding(YValueBindingEndpoint targetValue,
			YValueBindingEndpoint modelValue);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param targetValue
	 *            the target value
	 * @param modelValue
	 *            the model value
	 * @param targetToModelStrategy
	 *            the target to model strategy
	 * @param modelToTargetStrategy
	 *            the model to target strategy
	 * @return the y value binding
	 * @model targetValueRequired="true" modelValueRequired="true"
	 *        targetToModelStrategyRequired="true"
	 *        modelToTargetStrategyRequired="true"
	 */
	YValueBinding addTransientBinding(YValueBindingEndpoint targetValue,
			YValueBindingEndpoint modelValue,
			YBindingUpdateStrategy targetToModelStrategy,
			YBindingUpdateStrategy modelToTargetStrategy);

} // YBindingSet
