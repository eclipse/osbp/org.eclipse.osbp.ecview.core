/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;


// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YCollection Bindable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#getCollectionBindingEndpoint <em>Collection Binding Endpoint</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYCollectionBindable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface YCollectionBindable extends YBindable {
	/**
	 * Returns the value of the '<em><b>Collection Binding Endpoint</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableCollectionEndpoint#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection Binding Endpoint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection Binding Endpoint</em>' reference.
	 * @see #setCollectionBindingEndpoint(YEmbeddableCollectionEndpoint)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYCollectionBindable_CollectionBindingEndpoint()
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableCollectionEndpoint#getElement
	 * @model opposite="element"
	 * @generated
	 */
	YEmbeddableCollectionEndpoint getCollectionBindingEndpoint();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable#getCollectionBindingEndpoint <em>Collection Binding Endpoint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection Binding Endpoint</em>' reference.
	 * @see #getCollectionBindingEndpoint()
	 * @generated
	 */
	void setCollectionBindingEndpoint(YEmbeddableCollectionEndpoint value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable collection endpoint
	 * @model
	 * @generated
	 */
	YEmbeddableCollectionEndpoint createCollectionEndpoint();

} // YCollectionBindable
