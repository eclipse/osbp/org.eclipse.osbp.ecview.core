/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.common.model.core.util;

import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;

/**
 * This ItemProvider determines which features of a given YElement may become
 * binded.
 */
public interface IBindableFeaturesItemProvider {

	/**
	 * Returns the list of bindable features.
	 * 
	 * @param element
	 * @return
	 */
	List<EStructuralFeature> getBindableFeatures(YElement element);

	/**
	 * Returns the type of the given structural feature. Subclasses can override
	 * and return a different class. See <code>YTableItemProvider</code> for
	 * instance.
	 * 
	 * @param element
	 * @param feature
	 * @return
	 */
	Class<?> getFeatureType(YElement element, EStructuralFeature feature);
}
