/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core;

import org.eclipse.emf.common.util.EList;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YUi Layout</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#getElements <em>Elements</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#getLastComponentAttach <em>Last Component Attach</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#getLastComponentDetach <em>Last Component Detach</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#getNumberColumns <em>Number Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#isSaveAndNew <em>Save And New</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYLayout()
 * @model
 * @generated
 */
public interface YLayout extends YEmbeddable, YEditable, YEnable {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference
	 * list. The list contents are of type
	 * {@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYLayout_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<YEmbeddable> getElements();

	/**
	 * Returns the value of the '<em><b>Last Component Attach</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Component Attach</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Component Attach</em>' attribute.
	 * @see #setLastComponentAttach(Object)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYLayout_LastComponentAttach()
	 * @model transient="true"
	 * @generated
	 */
	Object getLastComponentAttach();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#getLastComponentAttach <em>Last Component Attach</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Component Attach</em>' attribute.
	 * @see #getLastComponentAttach()
	 * @generated
	 */
	void setLastComponentAttach(Object value);

	/**
	 * Returns the value of the '<em><b>Last Component Detach</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Component Detach</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Component Detach</em>' attribute.
	 * @see #setLastComponentDetach(Object)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYLayout_LastComponentDetach()
	 * @model transient="true"
	 * @generated
	 */
	Object getLastComponentDetach();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#getLastComponentDetach <em>Last Component Detach</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Component Detach</em>' attribute.
	 * @see #getLastComponentDetach()
	 * @generated
	 */
	void setLastComponentDetach(Object value);

	/**
	 * Returns the value of the '<em><b>Number Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Columns</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Columns</em>' attribute.
	 * @see #setNumberColumns(int)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYLayout_NumberColumns()
	 * @model
	 * @generated
	 */
	int getNumberColumns();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#getNumberColumns <em>Number Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Columns</em>' attribute.
	 * @see #getNumberColumns()
	 * @generated
	 */
	void setNumberColumns(int value);

	/**
	 * Returns the value of the '<em><b>Save And New</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Save And New</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Save And New</em>' attribute.
	 * @see #setSaveAndNew(boolean)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYLayout_SaveAndNew()
	 * @model
	 * @generated
	 */
	boolean isSaveAndNew();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YLayout#isSaveAndNew <em>Save And New</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Save And New</em>' attribute.
	 * @see #isSaveAndNew()
	 * @generated
	 */
	void setSaveAndNew(boolean value);

	/**
	 * Adds the given element to the list of elements.
	 *
	 * @param element
	 *            the element
	 * @return true, if the element was added
	 */
	boolean addElement(YEmbeddable element);

	/**
	 * Adds the given element to the list of elements at the given position. If
	 * position is greater than list size, the element will be added at the end
	 * of the list.
	 *
	 * @param index
	 *            the index
	 * @param element
	 *            the element
	 * @throws IllegalArgumentException
	 *             if the object is a duplicate.
	 */
	void insertElement(int index, YEmbeddable element);

	/**
	 * Moves the given element from its current position to the new position.
	 *
	 * @param newPosition
	 *            the new position
	 * @param element
	 *            the element
	 */
	void moveElement(int newPosition, YEmbeddable element);

	/**
	 * Removes the given element from the elements.
	 *
	 * @param element
	 *            the element
	 * @return true, if the element was removed
	 */
	boolean removeElement(YEmbeddable element);

	/**
	 * Returns the index of the given element. If element is not present then -1
	 * will be returned.
	 *
	 * @param element
	 *            the element
	 * @return the index of the element or -1 if not present.
	 */
	int getIndex(YEmbeddable element);

	/**
	 * Returns the element at the given index.
	 *
	 * @param index
	 *            the index
	 * @return the element at the given index.
	 * @throws IndexOutOfBoundsException
	 *             if the index is out of range (
	 *             <tt>index &lt; 0 || index &gt;= size()</tt>)
	 */
	YEmbeddable getElement(int index);

} // YUiLayout
