/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core.listeners;

import org.eclipse.osbp.ecview.core.common.model.core.YField;

public interface YValueChangeListener {

	/**
	 * Is called if the value of the field has changed.
	 * 
	 * @param event
	 */
	void valueChanged(Event event);

	public static class Event {
		private final YField yField;
		private final Object newValue;
		private final Object oldValue;

		public Event(YField yField, Object oldValue, Object newValue) {
			this.yField = yField;
			this.oldValue = oldValue;
			this.newValue = newValue;
		}

		/**
		 * Returns the field which value was changed.
		 * 
		 * @return the yField
		 */
		public YField getField() {
			return yField;
		}

		/**
		 * Returns the new value of the field.
		 * 
		 * @return the newValue
		 */
		public Object getNewValue() {
			return newValue;
		}

		/**
		 * Returns the old value of the field.
		 * 
		 * @return the oldValue
		 */
		protected Object getOldValue() {
			return oldValue;
		}

	}
}
