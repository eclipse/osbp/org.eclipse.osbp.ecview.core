/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.binding;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YValue Binding Endpoint</b></em>'. <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage#getYValueBindingEndpoint()
 * @model abstract="true"
 * @generated
 */
public interface YValueBindingEndpoint extends YBindingEndpoint {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y detail value binding endpoint
	 * @model
	 * @generated
	 */
	YDetailValueBindingEndpoint createDetailValueEndpoint();

	/**
	 * Returns a string, that is unique for the current binding. Each type of
	 * binding like BeanSlotBinding, YEmbeddableBinding,... will create a unique
	 * value. Also masterEndpoints are included to the value.
	 * <p>
	 * See subclasses for details.
	 *
	 * @return the binding id string
	 */
	String getBindingIdString();

} // YValueBindingEndpoint
