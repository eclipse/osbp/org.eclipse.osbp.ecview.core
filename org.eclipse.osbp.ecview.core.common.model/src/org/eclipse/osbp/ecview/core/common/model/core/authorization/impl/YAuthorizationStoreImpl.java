/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.authorization.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationPackage;
import org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization;
import org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorizationStore;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>YAuthorization Store</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.impl.YAuthorizationStoreImpl#getAuthorizations <em>Authorizations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YAuthorizationStoreImpl extends MinimalEObjectImpl.Container
		implements YAuthorizationStore {
	/**
	 * The cached value of the '{@link #getAuthorizations() <em>Authorizations</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getAuthorizations()
	 * @generated
	 * @ordered
	 */
	protected EList<YAuthorization> authorizations;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected YAuthorizationStoreImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AuthorizationPackage.Literals.YAUTHORIZATION_STORE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<YAuthorization> getAuthorizations() {
		if (authorizations == null) {
			authorizations = new EObjectContainmentEList.Resolving<YAuthorization>(YAuthorization.class, this, AuthorizationPackage.YAUTHORIZATION_STORE__AUTHORIZATIONS);
		}
		return authorizations;
	}

	public boolean isAllowed(String role, String action, YEmbeddable embeddable) {
		for (YAuthorization auth : getAuthorizations()) {
			if (auth.isAllowed(role, action, embeddable)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AuthorizationPackage.YAUTHORIZATION_STORE__AUTHORIZATIONS:
				return ((InternalEList<?>)getAuthorizations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AuthorizationPackage.YAUTHORIZATION_STORE__AUTHORIZATIONS:
				return getAuthorizations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AuthorizationPackage.YAUTHORIZATION_STORE__AUTHORIZATIONS:
				getAuthorizations().clear();
				getAuthorizations().addAll((Collection<? extends YAuthorization>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AuthorizationPackage.YAUTHORIZATION_STORE__AUTHORIZATIONS:
				getAuthorizations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AuthorizationPackage.YAUTHORIZATION_STORE__AUTHORIZATIONS:
				return authorizations != null && !authorizations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} // YAuthorizationStoreImpl
