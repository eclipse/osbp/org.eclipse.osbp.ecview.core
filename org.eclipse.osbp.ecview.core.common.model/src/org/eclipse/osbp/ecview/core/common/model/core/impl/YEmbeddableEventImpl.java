/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core.impl;

import java.util.Date;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableEvent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YEmbeddable Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YEmbeddableEventImpl#getTime <em>Time</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YEmbeddableEventImpl#getEmbeddable <em>Embeddable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YEmbeddableEventImpl#getRawEvent <em>Raw Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YEmbeddableEventImpl extends MinimalEObjectImpl.Container implements YEmbeddableEvent {
	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected Date time = TIME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEmbeddable() <em>Embeddable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmbeddable()
	 * @generated
	 * @ordered
	 */
	protected YEmbeddable embeddable;

	/**
	 * The default value of the '{@link #getRawEvent() <em>Raw Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRawEvent()
	 * @generated
	 * @ordered
	 */
	protected static final Object RAW_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRawEvent() <em>Raw Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRawEvent()
	 * @generated
	 * @ordered
	 */
	protected Object rawEvent = RAW_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEmbeddableEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoreModelPackage.Literals.YEMBEDDABLE_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(Date newTime) {
		Date oldTime = time;
		time = newTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YEMBEDDABLE_EVENT__TIME, oldTime, time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddable getEmbeddable() {
		if (embeddable != null && embeddable.eIsProxy()) {
			InternalEObject oldEmbeddable = (InternalEObject)embeddable;
			embeddable = (YEmbeddable)eResolveProxy(oldEmbeddable);
			if (embeddable != oldEmbeddable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CoreModelPackage.YEMBEDDABLE_EVENT__EMBEDDABLE, oldEmbeddable, embeddable));
			}
		}
		return embeddable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddable basicGetEmbeddable() {
		return embeddable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmbeddable(YEmbeddable newEmbeddable) {
		YEmbeddable oldEmbeddable = embeddable;
		embeddable = newEmbeddable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YEMBEDDABLE_EVENT__EMBEDDABLE, oldEmbeddable, embeddable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getRawEvent() {
		return rawEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRawEvent(Object newRawEvent) {
		Object oldRawEvent = rawEvent;
		rawEvent = newRawEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YEMBEDDABLE_EVENT__RAW_EVENT, oldRawEvent, rawEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoreModelPackage.YEMBEDDABLE_EVENT__TIME:
				return getTime();
			case CoreModelPackage.YEMBEDDABLE_EVENT__EMBEDDABLE:
				if (resolve) return getEmbeddable();
				return basicGetEmbeddable();
			case CoreModelPackage.YEMBEDDABLE_EVENT__RAW_EVENT:
				return getRawEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoreModelPackage.YEMBEDDABLE_EVENT__TIME:
				setTime((Date)newValue);
				return;
			case CoreModelPackage.YEMBEDDABLE_EVENT__EMBEDDABLE:
				setEmbeddable((YEmbeddable)newValue);
				return;
			case CoreModelPackage.YEMBEDDABLE_EVENT__RAW_EVENT:
				setRawEvent(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YEMBEDDABLE_EVENT__TIME:
				setTime(TIME_EDEFAULT);
				return;
			case CoreModelPackage.YEMBEDDABLE_EVENT__EMBEDDABLE:
				setEmbeddable((YEmbeddable)null);
				return;
			case CoreModelPackage.YEMBEDDABLE_EVENT__RAW_EVENT:
				setRawEvent(RAW_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YEMBEDDABLE_EVENT__TIME:
				return TIME_EDEFAULT == null ? time != null : !TIME_EDEFAULT.equals(time);
			case CoreModelPackage.YEMBEDDABLE_EVENT__EMBEDDABLE:
				return embeddable != null;
			case CoreModelPackage.YEMBEDDABLE_EVENT__RAW_EVENT:
				return RAW_EVENT_EDEFAULT == null ? rawEvent != null : !RAW_EVENT_EDEFAULT.equals(rawEvent);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (time: ");
		result.append(time);
		result.append(", rawEvent: ");
		result.append(rawEvent);
		result.append(')');
		return result.toString();
	}

} //YEmbeddableEventImpl
