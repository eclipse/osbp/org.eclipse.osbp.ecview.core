/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YDetail Bean Slot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YDetailBeanSlot#getMasterBeanSlot <em>Master Bean Slot</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YDetailBeanSlot#getPropertyPath <em>Property Path</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYDetailBeanSlot()
 * @model
 * @generated
 */
public interface YDetailBeanSlot extends YBeanSlot {
	/**
	 * Returns the value of the '<em><b>Master Bean Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Master Bean Slot</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Master Bean Slot</em>' reference.
	 * @see #setMasterBeanSlot(YBeanSlot)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYDetailBeanSlot_MasterBeanSlot()
	 * @model
	 * @generated
	 */
	YBeanSlot getMasterBeanSlot();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YDetailBeanSlot#getMasterBeanSlot <em>Master Bean Slot</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Master Bean Slot</em>' reference.
	 * @see #getMasterBeanSlot()
	 * @generated
	 */
	void setMasterBeanSlot(YBeanSlot value);

	/**
	 * Returns the value of the '<em><b>Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Path</em>' attribute.
	 * @see #setPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage#getYDetailBeanSlot_PropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.common.model.core.YDetailBeanSlot#getPropertyPath <em>Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Path</em>' attribute.
	 * @see #getPropertyPath()
	 * @generated
	 */
	void setPropertyPath(String value);

} // YDetailBeanSlot
