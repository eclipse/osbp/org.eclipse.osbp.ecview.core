/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>YBean Slot</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YBeanSlotImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YBeanSlotImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YBeanSlotImpl#getValueType <em>Value Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YBeanSlotImpl#getValueTypeQualifiedName <em>Value Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YBeanSlotImpl#getEventTopic <em>Event Topic</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.impl.YBeanSlotImpl#isRedirectEvents <em>Redirect Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YBeanSlotImpl extends MinimalEObjectImpl.Container implements
		YBeanSlot {
	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValueType() <em>Value Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getValueType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> valueType;

	/**
	 * The default value of the '{@link #getValueTypeQualifiedName() <em>Value Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_TYPE_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValueTypeQualifiedName() <em>Value Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String valueTypeQualifiedName = VALUE_TYPE_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEventTopic() <em>Event Topic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventTopic()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_TOPIC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEventTopic() <em>Event Topic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventTopic()
	 * @generated
	 * @ordered
	 */
	protected String eventTopic = EVENT_TOPIC_EDEFAULT;

	/**
	 * The default value of the '{@link #isRedirectEvents() <em>Redirect Events</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRedirectEvents()
	 * @generated
	 * @ordered
	 */
	protected static final boolean REDIRECT_EVENTS_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isRedirectEvents() <em>Redirect Events</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isRedirectEvents()
	 * @generated
	 * @ordered
	 */
	protected boolean redirectEvents = REDIRECT_EVENTS_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected YBeanSlotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoreModelPackage.Literals.YBEAN_SLOT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YBEAN_SLOT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getValueType() {
		return valueType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueType(Class<?> newValueType) {
		Class<?> oldValueType = valueType;
		valueType = newValueType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YBEAN_SLOT__VALUE_TYPE, oldValueType, valueType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValueTypeQualifiedName() {
		return valueTypeQualifiedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueTypeQualifiedName(String newValueTypeQualifiedName) {
		String oldValueTypeQualifiedName = valueTypeQualifiedName;
		valueTypeQualifiedName = newValueTypeQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YBEAN_SLOT__VALUE_TYPE_QUALIFIED_NAME, oldValueTypeQualifiedName, valueTypeQualifiedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEventTopic() {
		return eventTopic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventTopic(String newEventTopic) {
		String oldEventTopic = eventTopic;
		eventTopic = newEventTopic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YBEAN_SLOT__EVENT_TOPIC, oldEventTopic, eventTopic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRedirectEvents() {
		return redirectEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRedirectEvents(boolean newRedirectEvents) {
		boolean oldRedirectEvents = redirectEvents;
		redirectEvents = newRedirectEvents;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoreModelPackage.YBEAN_SLOT__REDIRECT_EVENTS, oldRedirectEvents, redirectEvents));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, CoreModelPackage.YBEAN_SLOT__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CoreModelPackage.YBEAN_SLOT__TAGS:
				return getTags();
			case CoreModelPackage.YBEAN_SLOT__NAME:
				return getName();
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE:
				return getValueType();
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE_QUALIFIED_NAME:
				return getValueTypeQualifiedName();
			case CoreModelPackage.YBEAN_SLOT__EVENT_TOPIC:
				return getEventTopic();
			case CoreModelPackage.YBEAN_SLOT__REDIRECT_EVENTS:
				return isRedirectEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CoreModelPackage.YBEAN_SLOT__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case CoreModelPackage.YBEAN_SLOT__NAME:
				setName((String)newValue);
				return;
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE:
				setValueType((Class<?>)newValue);
				return;
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE_QUALIFIED_NAME:
				setValueTypeQualifiedName((String)newValue);
				return;
			case CoreModelPackage.YBEAN_SLOT__EVENT_TOPIC:
				setEventTopic((String)newValue);
				return;
			case CoreModelPackage.YBEAN_SLOT__REDIRECT_EVENTS:
				setRedirectEvents((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YBEAN_SLOT__TAGS:
				getTags().clear();
				return;
			case CoreModelPackage.YBEAN_SLOT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE:
				setValueType((Class<?>)null);
				return;
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE_QUALIFIED_NAME:
				setValueTypeQualifiedName(VALUE_TYPE_QUALIFIED_NAME_EDEFAULT);
				return;
			case CoreModelPackage.YBEAN_SLOT__EVENT_TOPIC:
				setEventTopic(EVENT_TOPIC_EDEFAULT);
				return;
			case CoreModelPackage.YBEAN_SLOT__REDIRECT_EVENTS:
				setRedirectEvents(REDIRECT_EVENTS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CoreModelPackage.YBEAN_SLOT__TAGS:
				return tags != null && !tags.isEmpty();
			case CoreModelPackage.YBEAN_SLOT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE:
				return valueType != null;
			case CoreModelPackage.YBEAN_SLOT__VALUE_TYPE_QUALIFIED_NAME:
				return VALUE_TYPE_QUALIFIED_NAME_EDEFAULT == null ? valueTypeQualifiedName != null : !VALUE_TYPE_QUALIFIED_NAME_EDEFAULT.equals(valueTypeQualifiedName);
			case CoreModelPackage.YBEAN_SLOT__EVENT_TOPIC:
				return EVENT_TOPIC_EDEFAULT == null ? eventTopic != null : !EVENT_TOPIC_EDEFAULT.equals(eventTopic);
			case CoreModelPackage.YBEAN_SLOT__REDIRECT_EVENTS:
				return redirectEvents != REDIRECT_EVENTS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == YTaggable.class) {
			switch (derivedFeatureID) {
				case CoreModelPackage.YBEAN_SLOT__TAGS: return CoreModelPackage.YTAGGABLE__TAGS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == YTaggable.class) {
			switch (baseFeatureID) {
				case CoreModelPackage.YTAGGABLE__TAGS: return CoreModelPackage.YBEAN_SLOT__TAGS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", name: ");
		result.append(name);
		result.append(", valueType: ");
		result.append(valueType);
		result.append(", valueTypeQualifiedName: ");
		result.append(valueTypeQualifiedName);
		result.append(", eventTopic: ");
		result.append(eventTopic);
		result.append(", redirectEvents: ");
		result.append(redirectEvents);
		result.append(')');
		return result.toString();
	}

	@Override
	public YBeanSlotValueBindingEndpoint createBindingEndpoint(String path) {
		YBeanSlotValueBindingEndpoint yEndpoint = CoreModelFactory.eINSTANCE
				.createYBeanSlotValueBindingEndpoint();
		yEndpoint.setBeanSlot(this);
		yEndpoint.setAttributePath(path);
		return yEndpoint;
	}

	@Override
	public YBeanSlotListBindingEndpoint createListBindingEndpoint(String path,
			Class<?> collectionType) {
		YBeanSlotListBindingEndpoint yEndpoint = CoreModelFactory.eINSTANCE
				.createYBeanSlotListBindingEndpoint();
		yEndpoint.setBeanSlot(this);
		yEndpoint.setCollectionType(collectionType);
		yEndpoint.setAttributePath(path);
		return yEndpoint;
	}

} // YBeanSlotImpl
