/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.core.authorization;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YAuthorization Store</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorizationStore#getAuthorizations <em>Authorizations</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationPackage#getYAuthorizationStore()
 * @model
 * @generated
 */
public interface YAuthorizationStore extends EObject {
	/**
	 * Returns the value of the '<em><b>Authorizations</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Authorizations</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Authorizations</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationPackage#getYAuthorizationStore_Authorizations()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YAuthorization> getAuthorizations();

	/**
	 * <!-- begin-user-doc --> Returns true, if the role may execute the action
	 * on the given embeddable. If embeddable == null is passed, the
	 * {@link YAuthorization authorization implementation} is required to check.
	 * <!-- end-user-doc -->
	 *
	 * @param role
	 *            the role
	 * @param action
	 *            the action
	 * @param embeddable
	 *            the embeddable
	 * @return true, if is allowed
	 */
	boolean isAllowed(String role, String action, YEmbeddable embeddable);

} // YAuthorizationStore
