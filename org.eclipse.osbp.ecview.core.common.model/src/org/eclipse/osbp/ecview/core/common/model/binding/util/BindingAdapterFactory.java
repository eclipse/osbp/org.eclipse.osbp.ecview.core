/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.binding.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.binding.YBeanValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet;
import org.eclipse.osbp.ecview.core.common.model.binding.YDetailValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YEnumListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YListBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YNoOpListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YNoOpValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage
 * @generated
 */
public class BindingAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BindingPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BindingAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = BindingPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BindingSwitch<Adapter> modelSwitch =
		new BindingSwitch<Adapter>() {
			@Override
			public Adapter caseYBindingSet(YBindingSet object) {
				return createYBindingSetAdapter();
			}
			@Override
			public Adapter caseYBindingEndpoint(YBindingEndpoint object) {
				return createYBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYValueBindingEndpoint(YValueBindingEndpoint object) {
				return createYValueBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYBeanValueBindingEndpoint(YBeanValueBindingEndpoint object) {
				return createYBeanValueBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYDetailValueBindingEndpoint(YDetailValueBindingEndpoint object) {
				return createYDetailValueBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYListBindingEndpoint(YListBindingEndpoint object) {
				return createYListBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYBinding(YBinding object) {
				return createYBindingAdapter();
			}
			@Override
			public Adapter caseYValueBinding(YValueBinding object) {
				return createYValueBindingAdapter();
			}
			@Override
			public Adapter caseYListBinding(YListBinding object) {
				return createYListBindingAdapter();
			}
			@Override
			public Adapter caseYEnumListBindingEndpoint(YEnumListBindingEndpoint object) {
				return createYEnumListBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYECViewModelValueBindingEndpoint(YECViewModelValueBindingEndpoint object) {
				return createYECViewModelValueBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYECViewModelListBindingEndpoint(YECViewModelListBindingEndpoint object) {
				return createYECViewModelListBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYVisibilityProcessorValueBindingEndpoint(YVisibilityProcessorValueBindingEndpoint object) {
				return createYVisibilityProcessorValueBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYNoOpValueBindingEndpoint(YNoOpValueBindingEndpoint object) {
				return createYNoOpValueBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYNoOpListBindingEndpoint(YNoOpListBindingEndpoint object) {
				return createYNoOpListBindingEndpointAdapter();
			}
			@Override
			public Adapter caseYTaggable(YTaggable object) {
				return createYTaggableAdapter();
			}
			@Override
			public Adapter caseYElement(YElement object) {
				return createYElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet <em>YBinding Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet
	 * @generated
	 */
	public Adapter createYBindingSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint <em>YBinding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint
	 * @generated
	 */
	public Adapter createYBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint <em>YValue Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint
	 * @generated
	 */
	public Adapter createYValueBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBeanValueBindingEndpoint <em>YBean Value Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBeanValueBindingEndpoint
	 * @generated
	 */
	public Adapter createYBeanValueBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint <em>YList Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint
	 * @generated
	 */
	public Adapter createYListBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding <em>YBinding</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBinding
	 * @generated
	 */
	public Adapter createYBindingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding <em>YValue Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding
	 * @generated
	 */
	public Adapter createYValueBindingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YListBinding <em>YList Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YListBinding
	 * @generated
	 */
	public Adapter createYListBindingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YEnumListBindingEndpoint <em>YEnum List Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YEnumListBindingEndpoint
	 * @generated
	 */
	public Adapter createYEnumListBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint <em>YEC View Model Value Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint
	 * @generated
	 */
	public Adapter createYECViewModelValueBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelListBindingEndpoint <em>YEC View Model List Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelListBindingEndpoint
	 * @generated
	 */
	public Adapter createYECViewModelListBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint <em>YVisibility Processor Value Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint
	 * @generated
	 */
	public Adapter createYVisibilityProcessorValueBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YNoOpValueBindingEndpoint <em>YNo Op Value Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YNoOpValueBindingEndpoint
	 * @generated
	 */
	public Adapter createYNoOpValueBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YNoOpListBindingEndpoint <em>YNo Op List Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YNoOpListBindingEndpoint
	 * @generated
	 */
	public Adapter createYNoOpListBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.core.YTaggable <em>YTaggable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YTaggable
	 * @generated
	 */
	public Adapter createYTaggableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.binding.YDetailValueBindingEndpoint <em>YDetail Value Binding Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YDetailValueBindingEndpoint
	 * @generated
	 */
	public Adapter createYDetailValueBindingEndpointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.core.YElement <em>YElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YElement
	 * @generated
	 */
	public Adapter createYElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //BindingAdapterFactory
