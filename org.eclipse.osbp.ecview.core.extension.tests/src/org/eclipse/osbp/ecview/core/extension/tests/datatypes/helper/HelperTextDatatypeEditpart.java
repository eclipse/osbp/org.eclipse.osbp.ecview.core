/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.core.extension.tests.datatypes.helper;

import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TextDatatypeEditpart;

@SuppressWarnings("restriction")
public class HelperTextDatatypeEditpart extends TextDatatypeEditpart {
	
	@Override
	public ValidatorDelta internalGetValidatorsDelta(DatatypeBridge bridge,
			Notification notification) {
		return super.internalGetValidatorsDelta(bridge, notification);
	}

	@Override
	public Set<DatatypeBridge> getBridges() {
		return super.getBridges();
	}

}