/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.core.extension.tests.datatypes.helper;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.EditpartManager;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.TextAreaEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.BrowserDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.CheckBoxDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.ComboBoxDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.DateTimeDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.DecimalDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.ListDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.NumericDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.OptionsGroupDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.ProgressBarDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TabSheetDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TableDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TreeDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YBrowserDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YCheckBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YComboBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YListDataType;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YNumericDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YOptionsGroupDataType;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YProgressBarDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTabSheetDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTableDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextAreaDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTreeDatatype;

/**
 * Hooks up the helper editparts to test internal behaviour.
 */
@SuppressWarnings("restriction")
public class HelperEditpartManager extends EditpartManager {

	/**
	 * Creates a new instance of the edit part.
	 * 
	 * @param <A>
	 *            an instance of {@link IElementEditpart}
	 * @param yElement
	 *            the model element
	 * @return editpart
	 */
	@SuppressWarnings("unchecked")
	public <A extends IElementEditpart> A getEditpart(IViewContext context,
			Object yElement) {
		// asserts that no editpart was created already for the given element
		assertOneEditpartForModelelement(yElement);

		ElementEditpart<YElement> result = null;
		if (yElement instanceof YBrowserDatatype) {
			result = createNewInstance(BrowserDatatypeEditpart.class);
		} else if (yElement instanceof YCheckBoxDatatype) {
			result = createNewInstance(CheckBoxDatatypeEditpart.class);
		} else if (yElement instanceof YComboBoxDatatype) {
			result = createNewInstance(ComboBoxDatatypeEditpart.class);
		} else if (yElement instanceof YDateTimeDatatype) {
			result = createNewInstance(DateTimeDatatypeEditpart.class);
		} else if (yElement instanceof YDecimalDatatype) {
			result = createNewInstance(DecimalDatatypeEditpart.class);
		} else if (yElement instanceof YListDataType) {
			result = createNewInstance(ListDatatypeEditpart.class);
		} else if (yElement instanceof YNumericDatatype) {
			result = createNewInstance(NumericDatatypeEditpart.class);
		} else if (yElement instanceof YOptionsGroupDataType) {
			result = createNewInstance(OptionsGroupDatatypeEditpart.class);
		} else if (yElement instanceof YProgressBarDatatype) {
			result = createNewInstance(ProgressBarDatatypeEditpart.class);
		} else if (yElement instanceof YTableDatatype) {
			result = createNewInstance(TableDatatypeEditpart.class);
		} else if (yElement instanceof YTabSheetDatatype) {
			result = createNewInstance(TabSheetDatatypeEditpart.class);
		} else if (yElement instanceof YTextAreaDatatype) {
			result = createNewInstance(TextAreaEditpart.class);
		} else if (yElement instanceof YTextDatatype) {
			result = createNewInstance(HelperTextDatatypeEditpart.class);
		} else if (yElement instanceof YTreeDatatype) {
			result = createNewInstance(TreeDatatypeEditpart.class);
		} else {
			return super.getEditpart(context, yElement);
		}

		if (result != null) {
			result.initialize(context, (YElement) yElement);
		}

		return (A) result;
	}

	/**
	 * Returns a new instance of the type.
	 * 
	 * @param type
	 *            The type of the editpart for which an instance should be
	 *            created.
	 * @return editPart
	 * @throws InstantiationException
	 *             e
	 * @throws IllegalAccessException
	 *             e
	 */
	protected IElementEditpart newInstance(
			Class<? extends IElementEditpart> type)
			throws InstantiationException, IllegalAccessException {
		return type.newInstance();
	}

}
