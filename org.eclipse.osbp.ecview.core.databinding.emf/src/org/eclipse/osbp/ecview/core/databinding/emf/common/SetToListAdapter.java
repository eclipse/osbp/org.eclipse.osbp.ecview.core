/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.core.databinding.emf.common;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.list.ObservableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.ISetChangeListener;
import org.eclipse.core.databinding.observable.set.SetChangeEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class SetToListAdapter.
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class SetToListAdapter extends ObservableList {

	/** The set. */
	private final IObservableSet set;

	/** The listener. */
	private ISetChangeListener listener = new ISetChangeListener() {

		public void handleSetChange(SetChangeEvent event) {
			List originalList = new ArrayList(wrappedList);
			for (Object addedElement : event.diff.getAdditions()) {
				if (!wrappedList.contains(addedElement)) {
					wrappedList.add(addedElement);
				}
			}
			for (Object removedElement : event.diff.getRemovals()) {
				wrappedList.remove(removedElement);
			}
			fireListChange(Diffs.computeListDiff(originalList, wrappedList));
		}
	};

	/**
	 * Instantiates a new sets the to list adapter.
	 *
	 * @param set
	 *            the set
	 */
	public SetToListAdapter(IObservableSet set) {
		super(set.getRealm(), new ArrayList(), set.getElementType());
		this.set = set;
		wrappedList.addAll(set);
		this.set.addSetChangeListener(listener);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.databinding.observable.list.ObservableList#dispose()
	 */
	public synchronized void dispose() {
		super.dispose();
		if (set != null && listener != null) {
			set.removeSetChangeListener(listener);
			listener = null;
		}
	}

}
