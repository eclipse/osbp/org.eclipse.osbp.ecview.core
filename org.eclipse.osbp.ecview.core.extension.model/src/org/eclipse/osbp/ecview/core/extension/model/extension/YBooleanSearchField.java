/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension;

import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatadescription;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YBoolean Search Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField#getDatadescription <em>Datadescription</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField#getPropertyPath <em>Property Path</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYBooleanSearchField()
 * @model
 * @generated
 */
public interface YBooleanSearchField extends YSearchField {
	/**
	 * Returns the value of the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datadescription</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datadescription</em>' reference.
	 * @see #setDatadescription(YDatadescription)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYBooleanSearchField_Datadescription()
	 * @model
	 * @generated
	 */
	YDatadescription getDatadescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField#getDatadescription <em>Datadescription</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datadescription</em>' reference.
	 * @see #getDatadescription()
	 * @generated
	 */
	void setDatadescription(YDatadescription value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchOption
	 * @see #setValue(YBooleanSearchOption)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYBooleanSearchField_Value()
	 * @model transient="true"
	 * @generated
	 */
	YBooleanSearchOption getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchOption
	 * @see #getValue()
	 * @generated
	 */
	void setValue(YBooleanSearchOption value);

	/**
	 * Returns the value of the '<em><b>Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Path</em>' attribute.
	 * @see #setPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYBooleanSearchField_PropertyPath()
	 * @model
	 * @generated
	 */
	String getPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField#getPropertyPath <em>Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Path</em>' attribute.
	 * @see #getPropertyPath()
	 * @generated
	 */
	void setPropertyPath(String value);

} // YBooleanSearchField
