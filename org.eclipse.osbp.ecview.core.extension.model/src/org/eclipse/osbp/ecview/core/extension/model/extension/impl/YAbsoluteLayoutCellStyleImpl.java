/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.core.extension.model.extension.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayoutCellStyle;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YAbsolute Layout Cell Style</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YAbsoluteLayoutCellStyleImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YAbsoluteLayoutCellStyleImpl#getTop <em>Top</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YAbsoluteLayoutCellStyleImpl#getBottom <em>Bottom</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YAbsoluteLayoutCellStyleImpl#getLeft <em>Left</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YAbsoluteLayoutCellStyleImpl#getRight <em>Right</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YAbsoluteLayoutCellStyleImpl#getZIndex <em>ZIndex</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YAbsoluteLayoutCellStyleImpl extends MinimalEObjectImpl.Container implements YAbsoluteLayoutCellStyle {
	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected YEmbeddable target;

	/**
	 * The default value of the '{@link #getTop() <em>Top</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTop()
	 * @generated
	 * @ordered
	 */
	protected static final int TOP_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getTop() <em>Top</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTop()
	 * @generated
	 * @ordered
	 */
	protected int top = TOP_EDEFAULT;

	/**
	 * The default value of the '{@link #getBottom() <em>Bottom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBottom()
	 * @generated
	 * @ordered
	 */
	protected static final int BOTTOM_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getBottom() <em>Bottom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBottom()
	 * @generated
	 * @ordered
	 */
	protected int bottom = BOTTOM_EDEFAULT;

	/**
	 * The default value of the '{@link #getLeft() <em>Left</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeft()
	 * @generated
	 * @ordered
	 */
	protected static final int LEFT_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getLeft() <em>Left</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeft()
	 * @generated
	 * @ordered
	 */
	protected int left = LEFT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRight() <em>Right</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRight()
	 * @generated
	 * @ordered
	 */
	protected static final int RIGHT_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getRight() <em>Right</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRight()
	 * @generated
	 * @ordered
	 */
	protected int right = RIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getZIndex() <em>ZIndex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int ZINDEX_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getZIndex() <em>ZIndex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getZIndex()
	 * @generated
	 * @ordered
	 */
	protected int zIndex = ZINDEX_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YAbsoluteLayoutCellStyleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExtensionModelPackage.Literals.YABSOLUTE_LAYOUT_CELL_STYLE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTarget() <em>Target</em>}'
	 *         reference
	 * @generated
	 */
	public YEmbeddable getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (YEmbeddable)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable
	 * @generated
	 */
	public YEmbeddable basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTarget
	 *            the new cached value of the '{@link #getTarget()
	 *            <em>Target</em>}' reference
	 * @generated
	 */
	public void setTarget(YEmbeddable newTarget) {
		YEmbeddable oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTop() <em>Top</em>}'
	 *         attribute
	 * @generated
	 */
	public int getTop() {
		return top;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTop
	 *            the new cached value of the '{@link #getTop() <em>Top</em>}'
	 *            attribute
	 * @generated
	 */
	public void setTop(int newTop) {
		int oldTop = top;
		top = newTop;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TOP, oldTop, top));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getBottom() <em>Bottom</em>}'
	 *         attribute
	 * @generated
	 */
	public int getBottom() {
		return bottom;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newBottom
	 *            the new cached value of the '{@link #getBottom()
	 *            <em>Bottom</em>}' attribute
	 * @generated
	 */
	public void setBottom(int newBottom) {
		int oldBottom = bottom;
		bottom = newBottom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__BOTTOM, oldBottom, bottom));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getLeft() <em>Left</em>}'
	 *         attribute
	 * @generated
	 */
	public int getLeft() {
		return left;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLeft
	 *            the new cached value of the '{@link #getLeft() <em>Left</em>}'
	 *            attribute
	 * @generated
	 */
	public void setLeft(int newLeft) {
		int oldLeft = left;
		left = newLeft;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__LEFT, oldLeft, left));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getRight() <em>Right</em>}'
	 *         attribute
	 * @generated
	 */
	public int getRight() {
		return right;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newRight
	 *            the new cached value of the '{@link #getRight()
	 *            <em>Right</em>}' attribute
	 * @generated
	 */
	public void setRight(int newRight) {
		int oldRight = right;
		right = newRight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__RIGHT, oldRight, right));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getZIndex() <em>ZIndex</em>}'
	 *         attribute
	 * @generated
	 */
	public int getZIndex() {
		return zIndex;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newZIndex
	 *            the new cached value of the '{@link #getZIndex()
	 *            <em>ZIndex</em>}' attribute
	 * @generated
	 */
	public void setZIndex(int newZIndex) {
		int oldZIndex = zIndex;
		zIndex = newZIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__ZINDEX, oldZIndex, zIndex));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TOP:
				return getTop();
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__BOTTOM:
				return getBottom();
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__LEFT:
				return getLeft();
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__RIGHT:
				return getRight();
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__ZINDEX:
				return getZIndex();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TARGET:
				setTarget((YEmbeddable)newValue);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TOP:
				setTop((Integer)newValue);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__BOTTOM:
				setBottom((Integer)newValue);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__LEFT:
				setLeft((Integer)newValue);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__RIGHT:
				setRight((Integer)newValue);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__ZINDEX:
				setZIndex((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TARGET:
				setTarget((YEmbeddable)null);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TOP:
				setTop(TOP_EDEFAULT);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__BOTTOM:
				setBottom(BOTTOM_EDEFAULT);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__LEFT:
				setLeft(LEFT_EDEFAULT);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__RIGHT:
				setRight(RIGHT_EDEFAULT);
				return;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__ZINDEX:
				setZIndex(ZINDEX_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TARGET:
				return target != null;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__TOP:
				return top != TOP_EDEFAULT;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__BOTTOM:
				return bottom != BOTTOM_EDEFAULT;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__LEFT:
				return left != LEFT_EDEFAULT;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__RIGHT:
				return right != RIGHT_EDEFAULT;
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE__ZINDEX:
				return zIndex != ZINDEX_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (top: ");
		result.append(top);
		result.append(", bottom: ");
		result.append(bottom);
		result.append(", left: ");
		result.append(left);
		result.append(", right: ");
		result.append(right);
		result.append(", zIndex: ");
		result.append(zIndex);
		result.append(')');
		return result.toString();
	}

} //YAbsoluteLayoutCellStyleImpl
