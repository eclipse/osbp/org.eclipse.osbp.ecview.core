/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>YSuggest Text Field Events</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYSuggestTextFieldEvents()
 * @model
 * @generated
 */
public enum YSuggestTextFieldEvents implements Enumerator {
	/**
	 * The '<em><b>OPEN POPUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPEN_POPUP_VALUE
	 * @generated
	 * @ordered
	 */
	OPEN_POPUP(0, "OPEN_POPUP", "OPEN_POPUP"),

	/**
	 * The '<em><b>CLOSE POPUP</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLOSE_POPUP_VALUE
	 * @generated
	 * @ordered
	 */
	CLOSE_POPUP(1, "CLOSE_POPUP", "CLOSE_POPUP"),

	/**
	 * The '<em><b>NAVIGATE NEXT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NAVIGATE_NEXT_VALUE
	 * @generated
	 * @ordered
	 */
	NAVIGATE_NEXT(2, "NAVIGATE_NEXT", "NAVIGATE_NEXT"),

	/**
	 * The '<em><b>NAVIGATE PREV</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NAVIGATE_PREV_VALUE
	 * @generated
	 * @ordered
	 */
	NAVIGATE_PREV(3, "NAVIGATE_PREV", "NAVIGATE_PREV"),

	/**
	 * The '<em><b>SELECT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECT_VALUE
	 * @generated
	 * @ordered
	 */
	SELECT(4, "SELECT", "SELECT"), /**
	 * The '<em><b>CLEAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLEAR_VALUE
	 * @generated
	 * @ordered
	 */
	CLEAR(5, "CLEAR", "CLEAR"), /**
	 * The '<em><b>SELECTED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECTED_VALUE
	 * @generated
	 * @ordered
	 */
	SELECTED(6, "SELECTED", "SELECTED");

	/**
	 * The '<em><b>OPEN POPUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OPEN POPUP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPEN_POPUP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OPEN_POPUP_VALUE = 0;

	/**
	 * The '<em><b>CLOSE POPUP</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CLOSE POPUP</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLOSE_POPUP
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CLOSE_POPUP_VALUE = 1;

	/**
	 * The '<em><b>NAVIGATE NEXT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NAVIGATE NEXT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NAVIGATE_NEXT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NAVIGATE_NEXT_VALUE = 2;

	/**
	 * The '<em><b>NAVIGATE PREV</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NAVIGATE PREV</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NAVIGATE_PREV
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NAVIGATE_PREV_VALUE = 3;

	/**
	 * The '<em><b>SELECT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SELECT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SELECT_VALUE = 4;

	/**
	 * The '<em><b>CLEAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CLEAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLEAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CLEAR_VALUE = 5;

	/**
	 * The '<em><b>SELECTED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SELECTED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECTED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SELECTED_VALUE = 6;

	/**
	 * An array of all the '<em><b>YSuggest Text Field Events</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final YSuggestTextFieldEvents[] VALUES_ARRAY =
		new YSuggestTextFieldEvents[] {
			OPEN_POPUP,
			CLOSE_POPUP,
			NAVIGATE_NEXT,
			NAVIGATE_PREV,
			SELECT,
			CLEAR,
			SELECTED,
		};

	/**
	 * A public read-only list of all the '<em><b>YSuggest Text Field Events</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<YSuggestTextFieldEvents> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>YSuggest Text Field Events</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static YSuggestTextFieldEvents get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			YSuggestTextFieldEvents result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>YSuggest Text Field Events</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static YSuggestTextFieldEvents getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			YSuggestTextFieldEvents result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>YSuggest Text Field Events</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static YSuggestTextFieldEvents get(int value) {
		switch (value) {
			case OPEN_POPUP_VALUE: return OPEN_POPUP;
			case CLOSE_POPUP_VALUE: return CLOSE_POPUP;
			case NAVIGATE_NEXT_VALUE: return NAVIGATE_NEXT;
			case NAVIGATE_PREV_VALUE: return NAVIGATE_PREV;
			case SELECT_VALUE: return SELECT;
			case CLEAR_VALUE: return CLEAR;
			case SELECTED_VALUE: return SELECTED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private YSuggestTextFieldEvents(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //YSuggestTextFieldEvents
