/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YLayoutImpl;
import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatadescription;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YPanel;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>YPanel</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YPanelImpl#getLayoutIdx <em>Layout Idx</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YPanelImpl#getLayoutColumns <em>Layout Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YPanelImpl#getTabIndex <em>Tab Index</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YPanelImpl#getDatadescription <em>Datadescription</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YPanelImpl#getFirstContent <em>First Content</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YPanelImpl#getSecondContent <em>Second Content</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YPanelImpl extends YLayoutImpl implements YPanel {
	/**
	 * The default value of the '{@link #getLayoutIdx() <em>Layout Idx</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayoutIdx()
	 * @generated
	 * @ordered
	 */
	protected static final int LAYOUT_IDX_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLayoutIdx() <em>Layout Idx</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayoutIdx()
	 * @generated
	 * @ordered
	 */
	protected int layoutIdx = LAYOUT_IDX_EDEFAULT;

	/**
	 * The default value of the '{@link #getLayoutColumns() <em>Layout Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayoutColumns()
	 * @generated
	 * @ordered
	 */
	protected static final int LAYOUT_COLUMNS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLayoutColumns() <em>Layout Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayoutColumns()
	 * @generated
	 * @ordered
	 */
	protected int layoutColumns = LAYOUT_COLUMNS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTabIndex() <em>Tab Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTabIndex()
	 * @generated
	 * @ordered
	 */
	protected static final int TAB_INDEX_EDEFAULT = -1;

	/**
	 * The cached value of the '{@link #getTabIndex() <em>Tab Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTabIndex()
	 * @generated
	 * @ordered
	 */
	protected int tabIndex = TAB_INDEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDatadescription() <em>Datadescription</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getDatadescription()
	 * @generated
	 * @ordered
	 */
	protected YDatadescription datadescription;

	/**
	 * The cached value of the '{@link #getFirstContent() <em>First Content</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getFirstContent()
	 * @generated
	 * @ordered
	 */
	protected YEmbeddable firstContent;

	/**
	 * The cached value of the '{@link #getSecondContent() <em>Second Content</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getSecondContent()
	 * @generated
	 * @ordered
	 */
	protected YEmbeddable secondContent;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YPanelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExtensionModelPackage.Literals.YPANEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLayoutIdx() {
		return layoutIdx;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayoutIdx(int newLayoutIdx) {
		int oldLayoutIdx = layoutIdx;
		layoutIdx = newLayoutIdx;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YPANEL__LAYOUT_IDX, oldLayoutIdx, layoutIdx));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLayoutColumns() {
		return layoutColumns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayoutColumns(int newLayoutColumns) {
		int oldLayoutColumns = layoutColumns;
		layoutColumns = newLayoutColumns;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YPANEL__LAYOUT_COLUMNS, oldLayoutColumns, layoutColumns));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTabIndex()
	 *         <em>Tab Index</em>}' attribute
	 * @generated
	 */
	public int getTabIndex() {
		return tabIndex;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTabIndex
	 *            the new cached value of the '{@link #getTabIndex()
	 *            <em>Tab Index</em>}' attribute
	 * @generated
	 */
	public void setTabIndex(int newTabIndex) {
		int oldTabIndex = tabIndex;
		tabIndex = newTabIndex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YPANEL__TAB_INDEX, oldTabIndex, tabIndex));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getDatadescription()
	 *         <em>Datadescription</em>}' reference
	 * @generated
	 */
	public YDatadescription getDatadescription() {
		if (datadescription != null && datadescription.eIsProxy()) {
			InternalEObject oldDatadescription = (InternalEObject)datadescription;
			datadescription = (YDatadescription)eResolveProxy(oldDatadescription);
			if (datadescription != oldDatadescription) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExtensionModelPackage.YPANEL__DATADESCRIPTION, oldDatadescription, datadescription));
			}
		}
		return datadescription;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y datadescription
	 * @generated
	 */
	public YDatadescription basicGetDatadescription() {
		return datadescription;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newDatadescription
	 *            the new cached value of the '{@link #getDatadescription()
	 *            <em>Datadescription</em>}' reference
	 * @generated
	 */
	public void setDatadescription(YDatadescription newDatadescription) {
		YDatadescription oldDatadescription = datadescription;
		datadescription = newDatadescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YPANEL__DATADESCRIPTION, oldDatadescription, datadescription));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getFirstContent()
	 *         <em>First Content</em>}' reference
	 * @generated
	 */
	public YEmbeddable getFirstContent() {
		if (firstContent != null && firstContent.eIsProxy()) {
			InternalEObject oldFirstContent = (InternalEObject)firstContent;
			firstContent = (YEmbeddable)eResolveProxy(oldFirstContent);
			if (firstContent != oldFirstContent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExtensionModelPackage.YPANEL__FIRST_CONTENT, oldFirstContent, firstContent));
			}
		}
		return firstContent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable
	 * @generated
	 */
	public YEmbeddable basicGetFirstContent() {
		return firstContent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newFirstContent
	 *            the new cached value of the '{@link #getFirstContent()
	 *            <em>First Content</em>}' reference
	 * @generated
	 */
	public void setFirstContent(YEmbeddable newFirstContent) {
		YEmbeddable oldFirstContent = firstContent;
		firstContent = newFirstContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YPANEL__FIRST_CONTENT, oldFirstContent, firstContent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getSecondContent()
	 *         <em>Second Content</em>}' reference
	 * @generated
	 */
	public YEmbeddable getSecondContent() {
		if (secondContent != null && secondContent.eIsProxy()) {
			InternalEObject oldSecondContent = (InternalEObject)secondContent;
			secondContent = (YEmbeddable)eResolveProxy(oldSecondContent);
			if (secondContent != oldSecondContent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExtensionModelPackage.YPANEL__SECOND_CONTENT, oldSecondContent, secondContent));
			}
		}
		return secondContent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable
	 * @generated
	 */
	public YEmbeddable basicGetSecondContent() {
		return secondContent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newSecondContent
	 *            the new cached value of the '{@link #getSecondContent()
	 *            <em>Second Content</em>}' reference
	 * @generated
	 */
	public void setSecondContent(YEmbeddable newSecondContent) {
		YEmbeddable oldSecondContent = secondContent;
		secondContent = newSecondContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YPANEL__SECOND_CONTENT, oldSecondContent, secondContent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param element
	 *            the element
	 * @return the cell style
	 * @generated
	 */
	public YHorizontalLayoutCellStyle getCellStyle(YEmbeddable element) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExtensionModelPackage.YPANEL__LAYOUT_IDX:
				return getLayoutIdx();
			case ExtensionModelPackage.YPANEL__LAYOUT_COLUMNS:
				return getLayoutColumns();
			case ExtensionModelPackage.YPANEL__TAB_INDEX:
				return getTabIndex();
			case ExtensionModelPackage.YPANEL__DATADESCRIPTION:
				if (resolve) return getDatadescription();
				return basicGetDatadescription();
			case ExtensionModelPackage.YPANEL__FIRST_CONTENT:
				if (resolve) return getFirstContent();
				return basicGetFirstContent();
			case ExtensionModelPackage.YPANEL__SECOND_CONTENT:
				if (resolve) return getSecondContent();
				return basicGetSecondContent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExtensionModelPackage.YPANEL__LAYOUT_IDX:
				setLayoutIdx((Integer)newValue);
				return;
			case ExtensionModelPackage.YPANEL__LAYOUT_COLUMNS:
				setLayoutColumns((Integer)newValue);
				return;
			case ExtensionModelPackage.YPANEL__TAB_INDEX:
				setTabIndex((Integer)newValue);
				return;
			case ExtensionModelPackage.YPANEL__DATADESCRIPTION:
				setDatadescription((YDatadescription)newValue);
				return;
			case ExtensionModelPackage.YPANEL__FIRST_CONTENT:
				setFirstContent((YEmbeddable)newValue);
				return;
			case ExtensionModelPackage.YPANEL__SECOND_CONTENT:
				setSecondContent((YEmbeddable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExtensionModelPackage.YPANEL__LAYOUT_IDX:
				setLayoutIdx(LAYOUT_IDX_EDEFAULT);
				return;
			case ExtensionModelPackage.YPANEL__LAYOUT_COLUMNS:
				setLayoutColumns(LAYOUT_COLUMNS_EDEFAULT);
				return;
			case ExtensionModelPackage.YPANEL__TAB_INDEX:
				setTabIndex(TAB_INDEX_EDEFAULT);
				return;
			case ExtensionModelPackage.YPANEL__DATADESCRIPTION:
				setDatadescription((YDatadescription)null);
				return;
			case ExtensionModelPackage.YPANEL__FIRST_CONTENT:
				setFirstContent((YEmbeddable)null);
				return;
			case ExtensionModelPackage.YPANEL__SECOND_CONTENT:
				setSecondContent((YEmbeddable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExtensionModelPackage.YPANEL__LAYOUT_IDX:
				return layoutIdx != LAYOUT_IDX_EDEFAULT;
			case ExtensionModelPackage.YPANEL__LAYOUT_COLUMNS:
				return layoutColumns != LAYOUT_COLUMNS_EDEFAULT;
			case ExtensionModelPackage.YPANEL__TAB_INDEX:
				return tabIndex != TAB_INDEX_EDEFAULT;
			case ExtensionModelPackage.YPANEL__DATADESCRIPTION:
				return datadescription != null;
			case ExtensionModelPackage.YPANEL__FIRST_CONTENT:
				return firstContent != null;
			case ExtensionModelPackage.YPANEL__SECOND_CONTENT:
				return secondContent != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param derivedFeatureID
	 *            the derived feature id
	 * @param baseClass
	 *            the base class
	 * @return the int
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == YFocusable.class) {
			switch (derivedFeatureID) {
				case ExtensionModelPackage.YPANEL__LAYOUT_IDX: return CoreModelPackage.YFOCUSABLE__LAYOUT_IDX;
				case ExtensionModelPackage.YPANEL__LAYOUT_COLUMNS: return CoreModelPackage.YFOCUSABLE__LAYOUT_COLUMNS;
				case ExtensionModelPackage.YPANEL__TAB_INDEX: return CoreModelPackage.YFOCUSABLE__TAB_INDEX;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param baseFeatureID
	 *            the base feature id
	 * @param baseClass
	 *            the base class
	 * @return the int
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == YFocusable.class) {
			switch (baseFeatureID) {
				case CoreModelPackage.YFOCUSABLE__LAYOUT_IDX: return ExtensionModelPackage.YPANEL__LAYOUT_IDX;
				case CoreModelPackage.YFOCUSABLE__LAYOUT_COLUMNS: return ExtensionModelPackage.YPANEL__LAYOUT_COLUMNS;
				case CoreModelPackage.YFOCUSABLE__TAB_INDEX: return ExtensionModelPackage.YPANEL__TAB_INDEX;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (layoutIdx: ");
		result.append(layoutIdx);
		result.append(", layoutColumns: ");
		result.append(layoutColumns);
		result.append(", tabIndex: ");
		result.append(tabIndex);
		result.append(')');
		return result.toString();
	}

	/**
	 * Sets the label by creating a new datadescription.
	 *
	 * @param label
	 *            the new label
	 */
	@Override
	public void setLabel(String label) {
		YDatadescription ds = getDatadescription();
		if (ds == null) {
			setDatadescription(createDatadescription(label));
			getOrphanDatadescriptions().add(getDatadescription());
		} else {
			ds.setLabel(label);
		}
	}

	/**
	 * Sets the label i18nKey by creating a new datadescription.
	 *
	 * @param i18nKey
	 *            the new label i18n key
	 */
	@Override
	public void setLabelI18nKey(String i18nKey) {
		YDatadescription ds = getDatadescription();
		if (ds == null) {
			setDatadescription(createDatadescriptionForI18n(i18nKey));
			getOrphanDatadescriptions().add(getDatadescription());
		} else {
			ds.setLabelI18nKey(i18nKey);
		}
	}

} // YPanelImpl
