/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YMarginable;
import org.eclipse.osbp.ecview.core.common.model.core.YSpacingable;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>YFiltering Component</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getEmfNsURI <em>Emf Ns URI</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getApplyFilter <em>Apply Filter</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getResetFilter <em>Reset Filter</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getFilterDescriptors <em>Filter Descriptors</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getTableDescriptors <em>Table Descriptors</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getSelectionBeanSlotName <em>Selection Bean Slot Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getFilterCols <em>Filter Cols</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#isHideGrid <em>Hide Grid</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent()
 * @model
 * @generated
 */
public interface YFilteringComponent extends YEmbeddable, YSpacingable, YMarginable {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(Class)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_Type()
	 * @model
	 * @generated
	 */
	Class<?> getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Emf Ns URI</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emf Ns URI</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #setEmfNsURI(String)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_EmfNsURI()
	 * @model
	 * @generated
	 */
	String getEmfNsURI();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getEmfNsURI <em>Emf Ns URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #getEmfNsURI()
	 * @generated
	 */
	void setEmfNsURI(String value);

	/**
	 * Returns the value of the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Qualified Name</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #setTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_TypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getTypeQualifiedName <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #getTypeQualifiedName()
	 * @generated
	 */
	void setTypeQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Apply Filter</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Apply Filter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Apply Filter</em>' attribute.
	 * @see #setApplyFilter(Object)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_ApplyFilter()
	 * @model transient="true"
	 * @generated
	 */
	Object getApplyFilter();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getApplyFilter <em>Apply Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Apply Filter</em>' attribute.
	 * @see #getApplyFilter()
	 * @generated
	 */
	void setApplyFilter(Object value);

	/**
	 * Returns the value of the '<em><b>Reset Filter</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reset Filter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Reset Filter</em>' attribute.
	 * @see #setResetFilter(Object)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_ResetFilter()
	 * @model transient="true"
	 * @generated
	 */
	Object getResetFilter();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getResetFilter <em>Reset Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Reset Filter</em>' attribute.
	 * @see #getResetFilter()
	 * @generated
	 */
	void setResetFilter(Object value);

	/**
	 * Returns the value of the '<em><b>Filter</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Filter</em>' attribute.
	 * @see #setFilter(Object)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_Filter()
	 * @model transient="true"
	 * @generated
	 */
	Object getFilter();

	/**
	 * Sets the value of the
	 * '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getFilter
	 * <em>Filter</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Filter</em>' attribute.
	 * @see #getFilter()
	 * @generated
	 */
	void setFilter(Object value);

	/**
	 * Returns the value of the '<em><b>Filter Descriptors</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilterDescriptor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Descriptors</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Descriptors</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_FilterDescriptors()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YFilterDescriptor> getFilterDescriptors();

	/**
	 * Returns the value of the '<em><b>Table Descriptors</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilterTableDescriptor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table Descriptors</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table Descriptors</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_TableDescriptors()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YFilterTableDescriptor> getTableDescriptors();

	/**
	 * Returns the value of the '<em><b>Selection Bean Slot Name</b></em>' attribute.
	 * The default value is <code>"$$intern_searchDialogSelection"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection Bean Slot Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection Bean Slot Name</em>' attribute.
	 * @see #setSelectionBeanSlotName(String)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_SelectionBeanSlotName()
	 * @model default="$$intern_searchDialogSelection"
	 * @generated
	 */
	String getSelectionBeanSlotName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getSelectionBeanSlotName <em>Selection Bean Slot Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection Bean Slot Name</em>' attribute.
	 * @see #getSelectionBeanSlotName()
	 * @generated
	 */
	void setSelectionBeanSlotName(String value);

	/**
	 * Returns the value of the '<em><b>Filter Cols</b></em>' attribute.
	 * The default value is <code>"2"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Cols</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Cols</em>' attribute.
	 * @see #setFilterCols(int)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_FilterCols()
	 * @model default="2"
	 * @generated
	 */
	int getFilterCols();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#getFilterCols <em>Filter Cols</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Cols</em>' attribute.
	 * @see #getFilterCols()
	 * @generated
	 */
	void setFilterCols(int value);

	/**
	 * Returns the value of the '<em><b>Hide Grid</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide Grid</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide Grid</em>' attribute.
	 * @see #setHideGrid(boolean)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYFilteringComponent_HideGrid()
	 * @model
	 * @generated
	 */
	boolean isHideGrid();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent#isHideGrid <em>Hide Grid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hide Grid</em>' attribute.
	 * @see #isHideGrid()
	 * @generated
	 */
	void setHideGrid(boolean value);

	/**
	 * Adds a new descriptor.
	 * 
	 * @param desc
	 */
	void addFilterDescriptor(YFilterDescriptor desc);

	/**
	 * Adds a new descriptor.
	 * 
	 * @param type
	 * @param propertyPath
	 */
	void addFilterDescriptor(YFilteringType type, String propertyPath);

	/**
	 * Adds a new table descriptor.
	 * 
	 * @param desc
	 */
	void addTableDescriptor(YFilterTableDescriptor desc);

	/**
	 * Adds a new table descriptor.
	 * 
	 * @param propertyPath
	 */
	void addTableDescriptor(String propertyPath);

} // YFilteringComponent
