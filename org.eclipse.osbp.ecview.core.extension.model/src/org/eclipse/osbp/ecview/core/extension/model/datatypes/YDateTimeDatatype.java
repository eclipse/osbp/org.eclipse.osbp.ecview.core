/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes;

import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatatype;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YDate Time Datatype</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype#getFormat <em>Format</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype#getResolution <em>Resolution</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage#getYDateTimeDatatype()
 * @model
 * @generated
 */
public interface YDateTimeDatatype extends YDatatype {
	
	public static final String SHOWWEEKNUMBERS = "showweeknumbers";
	
	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * The default value is <code>"DATE"</code>.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeFormat}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeFormat
	 * @see #setFormat(YDateTimeFormat)
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage#getYDateTimeDatatype_Format()
	 * @model default="DATE"
	 * @generated
	 */
	YDateTimeFormat getFormat();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeFormat
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(YDateTimeFormat value);

	/**
	 * Returns the value of the '<em><b>Resolution</b></em>' attribute.
	 * The default value is <code>"UNDEFINED"</code>.
	 * The literals are from the enumeration {@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeResolution}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resolution</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resolution</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeResolution
	 * @see #setResolution(YDateTimeResolution)
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage#getYDateTimeDatatype_Resolution()
	 * @model default="UNDEFINED"
	 * @generated
	 */
	YDateTimeResolution getResolution();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype#getResolution <em>Resolution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resolution</em>' attribute.
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeResolution
	 * @see #getResolution()
	 * @generated
	 */
	void setResolution(YDateTimeResolution value);

	
	void setShowISOWeeknumbers(boolean value);
	boolean isShowISOWeekNumbersActive();
	
} // YDateTimeDatatype
