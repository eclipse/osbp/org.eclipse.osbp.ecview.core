/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YAlignmentContainer;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YMarginable;
import org.eclipse.osbp.ecview.core.common.model.core.YSpacingable;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YUi Vertical Layout</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayout#getCellStyles <em>Cell Styles</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYVerticalLayout()
 * @model
 * @generated
 */
public interface YVerticalLayout extends YLayout, YSpacingable, YMarginable, YAlignmentContainer {
	/**
	 * Returns the value of the '<em><b>Cell Styles</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayoutCellStyle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Styles</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Styles</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYVerticalLayout_CellStyles()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YVerticalLayoutCellStyle> getCellStyles();

	/**
	 * Returns the cell style for the given element.
	 *
	 * @param element
	 *            the element
	 * @return the cell style
	 */
	YVerticalLayoutCellStyle getCellStyle(YEmbeddable element);

	/**
	 * Adds a new cell style to the internal list of cell styles.
	 *
	 * @param element
	 *            the element
	 * @return the y vertical layout cell style
	 */
	YVerticalLayoutCellStyle addCellStyle(YEmbeddable element);

} // YUiVerticalLayout
