/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable;
import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatadescription;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YMasterDetailDatatype;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YMaster Detail</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getDatadescription <em>Datadescription</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getSelection <em>Selection</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getCollection <em>Collection</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getEmfNsURI <em>Emf Ns URI</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getMasterElement <em>Master Element</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getDetailElement <em>Detail Element</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail()
 * @model
 * @generated
 */
public interface YMasterDetail extends YInput, YCollectionBindable, YSelectionBindable {
	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' reference.
	 * @see #setDatatype(YMasterDetailDatatype)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_Datatype()
	 * @model
	 * @generated
	 */
	YMasterDetailDatatype getDatatype();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getDatatype <em>Datatype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(YMasterDetailDatatype value);

	/**
	 * Returns the value of the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datadescription</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datadescription</em>' reference.
	 * @see #setDatadescription(YDatadescription)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_Datadescription()
	 * @model
	 * @generated
	 */
	YDatadescription getDatadescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getDatadescription <em>Datadescription</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datadescription</em>' reference.
	 * @see #getDatadescription()
	 * @generated
	 */
	void setDatadescription(YDatadescription value);

	/**
	 * Returns the value of the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection</em>' attribute.
	 * @see #setSelection(Object)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_Selection()
	 * @model transient="true"
	 * @generated
	 */
	Object getSelection();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getSelection <em>Selection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection</em>' attribute.
	 * @see #getSelection()
	 * @generated
	 */
	void setSelection(Object value);

	/**
	 * Returns the value of the '<em><b>Collection</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Object}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' attribute list.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_Collection()
	 * @model transient="true"
	 * @generated
	 */
	EList<Object> getCollection();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(Class)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_Type()
	 * @model
	 * @generated
	 */
	Class<?> getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Emf Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emf Ns URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #setEmfNsURI(String)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_EmfNsURI()
	 * @model
	 * @generated
	 */
	String getEmfNsURI();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getEmfNsURI <em>Emf Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #getEmfNsURI()
	 * @generated
	 */
	void setEmfNsURI(String value);

	/**
	 * Returns the value of the '<em><b>Master Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Master Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Master Element</em>' containment reference.
	 * @see #setMasterElement(YEmbeddable)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_MasterElement()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	YEmbeddable getMasterElement();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getMasterElement <em>Master Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Master Element</em>' containment reference.
	 * @see #getMasterElement()
	 * @generated
	 */
	void setMasterElement(YEmbeddable value);

	/**
	 * Returns the value of the '<em><b>Detail Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detail Element</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detail Element</em>' containment reference.
	 * @see #setDetailElement(YEmbeddable)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_DetailElement()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	YEmbeddable getDetailElement();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getDetailElement <em>Detail Element</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Detail Element</em>' containment reference.
	 * @see #getDetailElement()
	 * @generated
	 */
	void setDetailElement(YEmbeddable value);

	/**
	 * Returns the value of the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #setTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYMasterDetail_TypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail#getTypeQualifiedName <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #getTypeQualifiedName()
	 * @generated
	 */
	void setTypeQualifiedName(String value);

} // YMasterDetail
