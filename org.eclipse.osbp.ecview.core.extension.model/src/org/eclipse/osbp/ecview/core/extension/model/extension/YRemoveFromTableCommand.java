/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension;

import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YCommand;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>YRemove From Table Command</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YRemoveFromTableCommand#getTable <em>Table</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YRemoveFromTableCommand#getTrigger <em>Trigger</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYRemoveFromTableCommand()
 * @model
 * @generated
 */
public interface YRemoveFromTableCommand extends YCommand {
	/**
	 * Returns the value of the '<em><b>Table</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Table</em>' reference.
	 * @see #setTable(YTable)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYRemoveFromTableCommand_Table()
	 * @model
	 * @generated
	 */
	YTable getTable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YRemoveFromTableCommand#getTable <em>Table</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Table</em>' reference.
	 * @see #getTable()
	 * @generated
	 */
	void setTable(YTable value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigger</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see #setTrigger(Object)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYRemoveFromTableCommand_Trigger()
	 * @model
	 * @generated
	 */
	Object getTrigger();

	/**
	 * Sets the value of the '
	 * {@link org.eclipse.osbp.ecview.core.extension.model.extension.YRemoveFromTableCommand#getTrigger
	 * <em>Trigger</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Trigger</em>' attribute.
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(Object value);
	
	public YECViewModelValueBindingEndpoint createTriggerEndpoint();

} // YRemoveFromTableCommand
