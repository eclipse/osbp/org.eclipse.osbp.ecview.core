/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YMarginable;
import org.eclipse.osbp.ecview.core.common.model.core.YSpacingable;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YEmbeddableImpl;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilterDescriptor;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilterTableDescriptor;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringType;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>YFiltering Component</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#isSpacing <em>Spacing</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#isMargin <em>Margin</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getEmfNsURI <em>Emf Ns URI</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getApplyFilter <em>Apply Filter</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getResetFilter <em>Reset Filter</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getFilter <em>Filter</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getFilterDescriptors <em>Filter Descriptors</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getTableDescriptors <em>Table Descriptors</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getSelectionBeanSlotName <em>Selection Bean Slot Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#getFilterCols <em>Filter Cols</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.impl.YFilteringComponentImpl#isHideGrid <em>Hide Grid</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YFilteringComponentImpl extends YEmbeddableImpl implements YFilteringComponent {
	/**
	 * The default value of the '{@link #isSpacing() <em>Spacing</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSpacing()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SPACING_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isSpacing() <em>Spacing</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSpacing()
	 * @generated
	 * @ordered
	 */
	protected boolean spacing = SPACING_EDEFAULT;

	/**
	 * The default value of the '{@link #isMargin() <em>Margin</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isMargin()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MARGIN_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isMargin() <em>Margin</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isMargin()
	 * @generated
	 * @ordered
	 */
	protected boolean margin = MARGIN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> type;

	/**
	 * The default value of the '{@link #getEmfNsURI() <em>Emf Ns URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEmfNsURI()
	 * @generated
	 * @ordered
	 */
	protected static final String EMF_NS_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEmfNsURI() <em>Emf Ns URI</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEmfNsURI()
	 * @generated
	 * @ordered
	 */
	protected String emfNsURI = EMF_NS_URI_EDEFAULT;

	/**
	 * The default value of the '{@link #getTypeQualifiedName() <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeQualifiedName() <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String typeQualifiedName = TYPE_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getApplyFilter() <em>Apply Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getApplyFilter()
	 * @generated
	 * @ordered
	 */
	protected static final Object APPLY_FILTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getApplyFilter() <em>Apply Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getApplyFilter()
	 * @generated
	 * @ordered
	 */
	protected Object applyFilter = APPLY_FILTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getResetFilter() <em>Reset Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getResetFilter()
	 * @generated
	 * @ordered
	 */
	protected static final Object RESET_FILTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResetFilter() <em>Reset Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getResetFilter()
	 * @generated
	 * @ordered
	 */
	protected Object resetFilter = RESET_FILTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected static final Object FILTER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected Object filter = FILTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFilterDescriptors() <em>Filter Descriptors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterDescriptors()
	 * @generated
	 * @ordered
	 */
	protected EList<YFilterDescriptor> filterDescriptors;

	/**
	 * The cached value of the '{@link #getTableDescriptors() <em>Table Descriptors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTableDescriptors()
	 * @generated
	 * @ordered
	 */
	protected EList<YFilterTableDescriptor> tableDescriptors;

	/**
	 * The default value of the '{@link #getSelectionBeanSlotName() <em>Selection Bean Slot Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionBeanSlotName()
	 * @generated
	 * @ordered
	 */
	protected static final String SELECTION_BEAN_SLOT_NAME_EDEFAULT = "$$intern_searchDialogSelection";

	/**
	 * The cached value of the '{@link #getSelectionBeanSlotName() <em>Selection Bean Slot Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionBeanSlotName()
	 * @generated
	 * @ordered
	 */
	protected String selectionBeanSlotName = SELECTION_BEAN_SLOT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getFilterCols() <em>Filter Cols</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterCols()
	 * @generated
	 * @ordered
	 */
	protected static final int FILTER_COLS_EDEFAULT = 2;

	/**
	 * The cached value of the '{@link #getFilterCols() <em>Filter Cols</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterCols()
	 * @generated
	 * @ordered
	 */
	protected int filterCols = FILTER_COLS_EDEFAULT;

	/**
	 * The default value of the '{@link #isHideGrid() <em>Hide Grid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideGrid()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HIDE_GRID_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHideGrid() <em>Hide Grid</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHideGrid()
	 * @generated
	 * @ordered
	 */
	protected boolean hideGrid = HIDE_GRID_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected YFilteringComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExtensionModelPackage.Literals.YFILTERING_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSpacing() {
		return spacing;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpacing(boolean newSpacing) {
		boolean oldSpacing = spacing;
		spacing = newSpacing;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__SPACING, oldSpacing, spacing));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMargin() {
		return margin;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setMargin(boolean newMargin) {
		boolean oldMargin = margin;
		margin = newMargin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__MARGIN, oldMargin, margin));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Class<?> newType) {
		Class<?> oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getEmfNsURI() {
		return emfNsURI;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmfNsURI(String newEmfNsURI) {
		String oldEmfNsURI = emfNsURI;
		emfNsURI = newEmfNsURI;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__EMF_NS_URI, oldEmfNsURI, emfNsURI));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getTypeQualifiedName() {
		return typeQualifiedName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypeQualifiedName(String newTypeQualifiedName) {
		String oldTypeQualifiedName = typeQualifiedName;
		typeQualifiedName = newTypeQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__TYPE_QUALIFIED_NAME, oldTypeQualifiedName, typeQualifiedName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Object getApplyFilter() {
		return applyFilter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplyFilter(Object newApplyFilter) {
		Object oldApplyFilter = applyFilter;
		applyFilter = newApplyFilter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__APPLY_FILTER, oldApplyFilter, applyFilter));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Object getResetFilter() {
		return resetFilter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setResetFilter(Object newResetFilter) {
		Object oldResetFilter = resetFilter;
		resetFilter = newResetFilter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__RESET_FILTER, oldResetFilter, resetFilter));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Object getFilter() {
		return filter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilter(Object newFilter) {
		Object oldFilter = filter;
		filter = newFilter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__FILTER, oldFilter, filter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<YFilterDescriptor> getFilterDescriptors() {
		if (filterDescriptors == null) {
			filterDescriptors = new EObjectContainmentEList.Resolving<YFilterDescriptor>(YFilterDescriptor.class, this, ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_DESCRIPTORS);
		}
		return filterDescriptors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<YFilterTableDescriptor> getTableDescriptors() {
		if (tableDescriptors == null) {
			tableDescriptors = new EObjectContainmentEList.Resolving<YFilterTableDescriptor>(YFilterTableDescriptor.class, this, ExtensionModelPackage.YFILTERING_COMPONENT__TABLE_DESCRIPTORS);
		}
		return tableDescriptors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSelectionBeanSlotName() {
		return selectionBeanSlotName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionBeanSlotName(String newSelectionBeanSlotName) {
		String oldSelectionBeanSlotName = selectionBeanSlotName;
		selectionBeanSlotName = newSelectionBeanSlotName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__SELECTION_BEAN_SLOT_NAME, oldSelectionBeanSlotName, selectionBeanSlotName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFilterCols() {
		return filterCols;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterCols(int newFilterCols) {
		int oldFilterCols = filterCols;
		filterCols = newFilterCols;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_COLS, oldFilterCols, filterCols));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isHideGrid() {
		return hideGrid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHideGrid(boolean newHideGrid) {
		boolean oldHideGrid = hideGrid;
		hideGrid = newHideGrid;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExtensionModelPackage.YFILTERING_COMPONENT__HIDE_GRID, oldHideGrid, hideGrid));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_DESCRIPTORS:
				return ((InternalEList<?>)getFilterDescriptors()).basicRemove(otherEnd, msgs);
			case ExtensionModelPackage.YFILTERING_COMPONENT__TABLE_DESCRIPTORS:
				return ((InternalEList<?>)getTableDescriptors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExtensionModelPackage.YFILTERING_COMPONENT__SPACING:
				return isSpacing();
			case ExtensionModelPackage.YFILTERING_COMPONENT__MARGIN:
				return isMargin();
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE:
				return getType();
			case ExtensionModelPackage.YFILTERING_COMPONENT__EMF_NS_URI:
				return getEmfNsURI();
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE_QUALIFIED_NAME:
				return getTypeQualifiedName();
			case ExtensionModelPackage.YFILTERING_COMPONENT__APPLY_FILTER:
				return getApplyFilter();
			case ExtensionModelPackage.YFILTERING_COMPONENT__RESET_FILTER:
				return getResetFilter();
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER:
				return getFilter();
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_DESCRIPTORS:
				return getFilterDescriptors();
			case ExtensionModelPackage.YFILTERING_COMPONENT__TABLE_DESCRIPTORS:
				return getTableDescriptors();
			case ExtensionModelPackage.YFILTERING_COMPONENT__SELECTION_BEAN_SLOT_NAME:
				return getSelectionBeanSlotName();
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_COLS:
				return getFilterCols();
			case ExtensionModelPackage.YFILTERING_COMPONENT__HIDE_GRID:
				return isHideGrid();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExtensionModelPackage.YFILTERING_COMPONENT__SPACING:
				setSpacing((Boolean)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__MARGIN:
				setMargin((Boolean)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE:
				setType((Class<?>)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__EMF_NS_URI:
				setEmfNsURI((String)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE_QUALIFIED_NAME:
				setTypeQualifiedName((String)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__APPLY_FILTER:
				setApplyFilter(newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__RESET_FILTER:
				setResetFilter(newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER:
				setFilter(newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_DESCRIPTORS:
				getFilterDescriptors().clear();
				getFilterDescriptors().addAll((Collection<? extends YFilterDescriptor>)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__TABLE_DESCRIPTORS:
				getTableDescriptors().clear();
				getTableDescriptors().addAll((Collection<? extends YFilterTableDescriptor>)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__SELECTION_BEAN_SLOT_NAME:
				setSelectionBeanSlotName((String)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_COLS:
				setFilterCols((Integer)newValue);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__HIDE_GRID:
				setHideGrid((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExtensionModelPackage.YFILTERING_COMPONENT__SPACING:
				setSpacing(SPACING_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__MARGIN:
				setMargin(MARGIN_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE:
				setType((Class<?>)null);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__EMF_NS_URI:
				setEmfNsURI(EMF_NS_URI_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE_QUALIFIED_NAME:
				setTypeQualifiedName(TYPE_QUALIFIED_NAME_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__APPLY_FILTER:
				setApplyFilter(APPLY_FILTER_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__RESET_FILTER:
				setResetFilter(RESET_FILTER_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER:
				setFilter(FILTER_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_DESCRIPTORS:
				getFilterDescriptors().clear();
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__TABLE_DESCRIPTORS:
				getTableDescriptors().clear();
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__SELECTION_BEAN_SLOT_NAME:
				setSelectionBeanSlotName(SELECTION_BEAN_SLOT_NAME_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_COLS:
				setFilterCols(FILTER_COLS_EDEFAULT);
				return;
			case ExtensionModelPackage.YFILTERING_COMPONENT__HIDE_GRID:
				setHideGrid(HIDE_GRID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExtensionModelPackage.YFILTERING_COMPONENT__SPACING:
				return spacing != SPACING_EDEFAULT;
			case ExtensionModelPackage.YFILTERING_COMPONENT__MARGIN:
				return margin != MARGIN_EDEFAULT;
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE:
				return type != null;
			case ExtensionModelPackage.YFILTERING_COMPONENT__EMF_NS_URI:
				return EMF_NS_URI_EDEFAULT == null ? emfNsURI != null : !EMF_NS_URI_EDEFAULT.equals(emfNsURI);
			case ExtensionModelPackage.YFILTERING_COMPONENT__TYPE_QUALIFIED_NAME:
				return TYPE_QUALIFIED_NAME_EDEFAULT == null ? typeQualifiedName != null : !TYPE_QUALIFIED_NAME_EDEFAULT.equals(typeQualifiedName);
			case ExtensionModelPackage.YFILTERING_COMPONENT__APPLY_FILTER:
				return APPLY_FILTER_EDEFAULT == null ? applyFilter != null : !APPLY_FILTER_EDEFAULT.equals(applyFilter);
			case ExtensionModelPackage.YFILTERING_COMPONENT__RESET_FILTER:
				return RESET_FILTER_EDEFAULT == null ? resetFilter != null : !RESET_FILTER_EDEFAULT.equals(resetFilter);
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER:
				return FILTER_EDEFAULT == null ? filter != null : !FILTER_EDEFAULT.equals(filter);
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_DESCRIPTORS:
				return filterDescriptors != null && !filterDescriptors.isEmpty();
			case ExtensionModelPackage.YFILTERING_COMPONENT__TABLE_DESCRIPTORS:
				return tableDescriptors != null && !tableDescriptors.isEmpty();
			case ExtensionModelPackage.YFILTERING_COMPONENT__SELECTION_BEAN_SLOT_NAME:
				return SELECTION_BEAN_SLOT_NAME_EDEFAULT == null ? selectionBeanSlotName != null : !SELECTION_BEAN_SLOT_NAME_EDEFAULT.equals(selectionBeanSlotName);
			case ExtensionModelPackage.YFILTERING_COMPONENT__FILTER_COLS:
				return filterCols != FILTER_COLS_EDEFAULT;
			case ExtensionModelPackage.YFILTERING_COMPONENT__HIDE_GRID:
				return hideGrid != HIDE_GRID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == YSpacingable.class) {
			switch (derivedFeatureID) {
				case ExtensionModelPackage.YFILTERING_COMPONENT__SPACING: return CoreModelPackage.YSPACINGABLE__SPACING;
				default: return -1;
			}
		}
		if (baseClass == YMarginable.class) {
			switch (derivedFeatureID) {
				case ExtensionModelPackage.YFILTERING_COMPONENT__MARGIN: return CoreModelPackage.YMARGINABLE__MARGIN;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == YSpacingable.class) {
			switch (baseFeatureID) {
				case CoreModelPackage.YSPACINGABLE__SPACING: return ExtensionModelPackage.YFILTERING_COMPONENT__SPACING;
				default: return -1;
			}
		}
		if (baseClass == YMarginable.class) {
			switch (baseFeatureID) {
				case CoreModelPackage.YMARGINABLE__MARGIN: return ExtensionModelPackage.YFILTERING_COMPONENT__MARGIN;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (spacing: ");
		result.append(spacing);
		result.append(", margin: ");
		result.append(margin);
		result.append(", type: ");
		result.append(type);
		result.append(", emfNsURI: ");
		result.append(emfNsURI);
		result.append(", typeQualifiedName: ");
		result.append(typeQualifiedName);
		result.append(", applyFilter: ");
		result.append(applyFilter);
		result.append(", resetFilter: ");
		result.append(resetFilter);
		result.append(", filter: ");
		result.append(filter);
		result.append(", selectionBeanSlotName: ");
		result.append(selectionBeanSlotName);
		result.append(", filterCols: ");
		result.append(filterCols);
		result.append(", hideGrid: ");
		result.append(hideGrid);
		result.append(')');
		return result.toString();
	}

	@Override
	public void addFilterDescriptor(YFilterDescriptor desc) {
		getFilterDescriptors().add(desc);
	}

	@Override
	public void addFilterDescriptor(YFilteringType type, String propertyPath) {
		YFilterDescriptor desc = ExtensionModelFactory.eINSTANCE.createYFilterDescriptor();
		desc.setType(type);
		desc.setPropertyPath(propertyPath);
		addFilterDescriptor(desc);
	}
	
	@Override
	public void addTableDescriptor(YFilterTableDescriptor desc) {
		getTableDescriptors().add(desc);
	}

	@Override
	public void addTableDescriptor(String propertyPath) {
		YFilterTableDescriptor desc = ExtensionModelFactory.eINSTANCE.createYFilterTableDescriptor();
		desc.setPropertyPath(propertyPath);
		addTableDescriptor(desc);
	}

} // YFilteringComponentImpl
