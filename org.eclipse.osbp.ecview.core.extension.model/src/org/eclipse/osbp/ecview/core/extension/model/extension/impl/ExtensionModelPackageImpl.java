/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.impl;

import java.io.InputStream;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.datatypes.DatatypesPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.impl.ExtDatatypesPackageImpl;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAddToTableCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanReferenceField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanServiceConsumer;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchOption;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBrowser;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBrowserStreamInput;
import org.eclipse.osbp.ecview.core.extension.model.extension.YButton;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCheckBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YColumn;
import org.eclipse.osbp.ecview.core.extension.model.extension.YComboBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCssLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCssLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDateTime;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDecimalField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDialogComponent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumComboBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumList;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumOptionsGroup;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilter;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilterDescriptor;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilterTableDescriptor;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringType;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YImage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanban;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanbanEvent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanbanVisibilityProcessor;
import org.eclipse.osbp.ecview.core.extension.model.extension.YLabel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YList;
import org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YOptionsGroup;
import org.eclipse.osbp.ecview.core.extension.model.extension.YPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YPasswordField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YProgressBar;
import org.eclipse.osbp.ecview.core.extension.model.extension.YReferenceSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YRemoveFromTableCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchWildcards;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSetNewBeanInstanceCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSlider;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSortColumn;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSpanInfo;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextFieldEvents;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTab;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTabSheet;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextArea;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YToggleButton;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTree;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class ExtensionModelPackageImpl extends EPackageImpl implements
		ExtensionModelPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTextFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yBeanReferenceFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yGridLayoutEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yGridLayoutCellStyleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yHorizontalLayoutEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yHorizontalLayoutCellStyleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yVerticalLayoutEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yVerticalLayoutCellStyleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySpanInfoEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTableEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yColumnEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ySortColumnEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yLabelEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yImageEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTextAreaEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yCheckBoxEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yBrowserEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yDateTimeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yInputEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yDecimalFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yNumericFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yComboBoxEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yListEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yButtonEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySliderEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yToggleButtonEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yProgressBarEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTabSheetEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTabEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yMasterDetailEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yFormLayoutEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yFormLayoutCellStyleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySearchFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTextSearchFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yBooleanSearchFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yNumericSearchFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yReferenceSearchFieldEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yPanelEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySplitPanelEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySearchPanelEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yEnumOptionsGroupEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yEnumListEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yEnumComboBoxEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yBeanServiceConsumerEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yAddToTableCommandEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yRemoveFromTableCommandEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yBrowserStreamInputEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySetNewBeanInstanceCommandEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yCssLayoutEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yCssLayoutCellStyleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yFilterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yAbsoluteLayoutEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yAbsoluteLayoutCellStyleEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySuggestTextFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yPasswordFieldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yFilteringComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yFilterDescriptorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yFilterTableDescriptorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yKanbanEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yKanbanVisibilityProcessorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yDialogComponentEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yTreeEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yOptionsGroupEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EEnum ySelectionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EEnum yBooleanSearchOptionEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EEnum ySearchWildcardsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum ySuggestTextFieldEventsEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum yFilteringTypeEEnum = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EDataType yButtonClickListenerEDataType = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EDataType yInputStreamEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType yKanbanEventEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
	 * package package URI value.
	 * <p>
	 * Note: the correct way to create the package is via the static factory
	 * method {@link #init init()}, which also performs initialization of the
	 * package, or returns the registered package, if one already exists. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ExtensionModelPackageImpl() {
		super(eNS_URI, ExtensionModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link ExtensionModelPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the extension model package
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ExtensionModelPackage init() {
		if (isInited) return (ExtensionModelPackage)EPackage.Registry.INSTANCE.getEPackage(ExtensionModelPackage.eNS_URI);

		// Obtain or create and register package
		ExtensionModelPackageImpl theExtensionModelPackage = (ExtensionModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ExtensionModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ExtensionModelPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		BindingPackage.eINSTANCE.eClass();
		CoreModelPackage.eINSTANCE.eClass();
		DatatypesPackage.eINSTANCE.eClass();
		ValidationPackage.eINSTANCE.eClass();
		VisibilityPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ExtDatatypesPackageImpl theExtDatatypesPackage = (ExtDatatypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExtDatatypesPackage.eNS_URI) instanceof ExtDatatypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExtDatatypesPackage.eNS_URI) : ExtDatatypesPackage.eINSTANCE);

		// Create package meta-data objects
		theExtensionModelPackage.createPackageContents();
		theExtDatatypesPackage.createPackageContents();

		// Initialize created meta-data
		theExtensionModelPackage.initializePackageContents();
		theExtDatatypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theExtensionModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ExtensionModelPackage.eNS_URI, theExtensionModelPackage);
		return theExtensionModelPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text field
	 * @generated
	 */
	public EClass getYTextField() {
		return yTextFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text field_ datatype
	 * @generated
	 */
	public EReference getYTextField_Datatype() {
		return (EReference)yTextFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text field_ datadescription
	 * @generated
	 */
	public EReference getYTextField_Datadescription() {
		return (EReference)yTextFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text field_ value
	 * @generated
	 */
	public EAttribute getYTextField_Value() {
		return (EAttribute)yTextFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field
	 * @generated
	 */
	public EClass getYBeanReferenceField() {
		return yBeanReferenceFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ datadescription
	 * @generated
	 */
	public EReference getYBeanReferenceField_Datadescription() {
		return (EReference)yBeanReferenceFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ value
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_Value() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ type
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_Type() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ emf ns uri
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_EmfNsURI() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ type qualified name
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_TypeQualifiedName() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ in memory bean provider
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_InMemoryBeanProvider() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ in memory bean provider qualified
	 *         name
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_InMemoryBeanProviderQualifiedName() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ caption property path
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_CaptionPropertyPath() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ image property path
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_ImagePropertyPath() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ description property
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_DescriptionProperty() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ description
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_Description() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ reference source type
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_ReferenceSourceType() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ reference source type qualified name
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_ReferenceSourceTypeQualifiedName() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field_ reference source type property
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_ReferenceSourceTypeProperty() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYBeanReferenceField_Required() {
		return (EAttribute)yBeanReferenceFieldEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYBeanReferenceField_FilteringComponent() {
		return (EReference)yBeanReferenceFieldEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYBeanReferenceField_DialogComponent() {
		return (EReference)yBeanReferenceFieldEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout
	 * @generated
	 */
	public EClass getYGridLayout() {
		return yGridLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout_ cell styles
	 * @generated
	 */
	public EReference getYGridLayout_CellStyles() {
		return (EReference)yGridLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout_ columns
	 * @generated
	 */
	public EAttribute getYGridLayout_Columns() {
		return (EAttribute)yGridLayoutEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout cell style
	 * @generated
	 */
	public EClass getYGridLayoutCellStyle() {
		return yGridLayoutCellStyleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout cell style_ target
	 * @generated
	 */
	public EReference getYGridLayoutCellStyle_Target() {
		return (EReference)yGridLayoutCellStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout cell style_ alignment
	 * @generated
	 */
	public EAttribute getYGridLayoutCellStyle_Alignment() {
		return (EAttribute)yGridLayoutCellStyleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout cell style_ span info
	 * @generated
	 */
	public EReference getYGridLayoutCellStyle_SpanInfo() {
		return (EReference)yGridLayoutCellStyleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y horizontal layout
	 * @generated
	 */
	public EClass getYHorizontalLayout() {
		return yHorizontalLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y horizontal layout_ cell styles
	 * @generated
	 */
	public EReference getYHorizontalLayout_CellStyles() {
		return (EReference)yHorizontalLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y horizontal layout cell style
	 * @generated
	 */
	public EClass getYHorizontalLayoutCellStyle() {
		return yHorizontalLayoutCellStyleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y horizontal layout cell style_ target
	 * @generated
	 */
	public EReference getYHorizontalLayoutCellStyle_Target() {
		return (EReference)yHorizontalLayoutCellStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y horizontal layout cell style_ alignment
	 * @generated
	 */
	public EAttribute getYHorizontalLayoutCellStyle_Alignment() {
		return (EAttribute)yHorizontalLayoutCellStyleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y vertical layout
	 * @generated
	 */
	public EClass getYVerticalLayout() {
		return yVerticalLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y vertical layout_ cell styles
	 * @generated
	 */
	public EReference getYVerticalLayout_CellStyles() {
		return (EReference)yVerticalLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y vertical layout cell style
	 * @generated
	 */
	public EClass getYVerticalLayoutCellStyle() {
		return yVerticalLayoutCellStyleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y vertical layout cell style_ target
	 * @generated
	 */
	public EReference getYVerticalLayoutCellStyle_Target() {
		return (EReference)yVerticalLayoutCellStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y vertical layout cell style_ alignment
	 * @generated
	 */
	public EAttribute getYVerticalLayoutCellStyle_Alignment() {
		return (EAttribute)yVerticalLayoutCellStyleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y span info
	 * @generated
	 */
	public EClass getYSpanInfo() {
		return ySpanInfoEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y span info_ column from
	 * @generated
	 */
	public EAttribute getYSpanInfo_ColumnFrom() {
		return (EAttribute)ySpanInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y span info_ row from
	 * @generated
	 */
	public EAttribute getYSpanInfo_RowFrom() {
		return (EAttribute)ySpanInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y span info_ column to
	 * @generated
	 */
	public EAttribute getYSpanInfo_ColumnTo() {
		return (EAttribute)ySpanInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y span info_ row to
	 * @generated
	 */
	public EAttribute getYSpanInfo_RowTo() {
		return (EAttribute)ySpanInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table
	 * @generated
	 */
	public EClass getYTable() {
		return yTableEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ datatype
	 * @generated
	 */
	public EReference getYTable_Datatype() {
		return (EReference)yTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ datadescription
	 * @generated
	 */
	public EReference getYTable_Datadescription() {
		return (EReference)yTableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ selection type
	 * @generated
	 */
	public EAttribute getYTable_SelectionType() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ selection
	 * @generated
	 */
	public EAttribute getYTable_Selection() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ multi selection
	 * @generated
	 */
	public EAttribute getYTable_MultiSelection() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ collection
	 * @generated
	 */
	public EAttribute getYTable_Collection() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ type
	 * @generated
	 */
	public EAttribute getYTable_Type() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ emf ns uri
	 * @generated
	 */
	public EAttribute getYTable_EmfNsURI() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ type qualified name
	 * @generated
	 */
	public EAttribute getYTable_TypeQualifiedName() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ columns
	 * @generated
	 */
	public EReference getYTable_Columns() {
		return (EReference)yTableEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ item image property
	 * @generated
	 */
	public EAttribute getYTable_ItemImageProperty() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ filter
	 * @generated
	 */
	public EAttribute getYTable_Filter() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table_ refresh
	 * @generated
	 */
	public EAttribute getYTable_Refresh() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYTable_SortOrder() {
		return (EReference)yTableEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYTable_DoSort() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYTable_ScrollToBottom() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYTable_PageLength() {
		return (EAttribute)yTableEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column
	 * @generated
	 */
	public EClass getYColumn() {
		return yColumnEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ icon
	 * @generated
	 */
	public EAttribute getYColumn_Icon() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ visible
	 * @generated
	 */
	public EAttribute getYColumn_Visible() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ datadescription
	 * @generated
	 */
	public EReference getYColumn_Datadescription() {
		return (EReference)yColumnEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ orphan datadescriptions
	 * @generated
	 */
	public EReference getYColumn_OrphanDatadescriptions() {
		return (EReference)yColumnEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ orderable
	 * @generated
	 */
	public EAttribute getYColumn_Orderable() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ collapsed
	 * @generated
	 */
	public EAttribute getYColumn_Collapsed() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ collapsible
	 * @generated
	 */
	public EAttribute getYColumn_Collapsible() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ alignment
	 * @generated
	 */
	public EAttribute getYColumn_Alignment() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ expand ratio
	 * @generated
	 */
	public EAttribute getYColumn_ExpandRatio() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ property path
	 * @generated
	 */
	public EAttribute getYColumn_PropertyPath() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ converter
	 * @generated
	 */
	public EReference getYColumn_Converter() {
		return (EReference)yColumnEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ type
	 * @generated
	 */
	public EAttribute getYColumn_Type() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column_ type qualified name
	 * @generated
	 */
	public EAttribute getYColumn_TypeQualifiedName() {
		return (EAttribute)yColumnEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYSortColumn() {
		return ySortColumnEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSortColumn_PropertyPath() {
		return (EAttribute)ySortColumnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSortColumn_Type() {
		return (EAttribute)ySortColumnEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSortColumn_TypeQualifiedName() {
		return (EAttribute)ySortColumnEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSortColumn_Asc() {
		return (EAttribute)ySortColumnEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y label
	 * @generated
	 */
	public EClass getYLabel() {
		return yLabelEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y label_ datadescription
	 * @generated
	 */
	public EReference getYLabel_Datadescription() {
		return (EReference)yLabelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y label_ value
	 * @generated
	 */
	public EAttribute getYLabel_Value() {
		return (EAttribute)yLabelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y image
	 * @generated
	 */
	public EClass getYImage() {
		return yImageEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y image_ datadescription
	 * @generated
	 */
	public EReference getYImage_Datadescription() {
		return (EReference)yImageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y image_ value
	 * @generated
	 */
	public EAttribute getYImage_Value() {
		return (EAttribute)yImageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYImage_Resource() {
		return (EAttribute)yImageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area
	 * @generated
	 */
	public EClass getYTextArea() {
		return yTextAreaEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area_ datadescription
	 * @generated
	 */
	public EReference getYTextArea_Datadescription() {
		return (EReference)yTextAreaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area_ datatype
	 * @generated
	 */
	public EReference getYTextArea_Datatype() {
		return (EReference)yTextAreaEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area_ value
	 * @generated
	 */
	public EAttribute getYTextArea_Value() {
		return (EAttribute)yTextAreaEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area_ word wrap
	 * @generated
	 */
	public EAttribute getYTextArea_WordWrap() {
		return (EAttribute)yTextAreaEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area_ rows
	 * @generated
	 */
	public EAttribute getYTextArea_Rows() {
		return (EAttribute)yTextAreaEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y check box
	 * @generated
	 */
	public EClass getYCheckBox() {
		return yCheckBoxEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y check box_ datadescription
	 * @generated
	 */
	public EReference getYCheckBox_Datadescription() {
		return (EReference)yCheckBoxEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y check box_ datatype
	 * @generated
	 */
	public EReference getYCheckBox_Datatype() {
		return (EReference)yCheckBoxEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y check box_ value
	 * @generated
	 */
	public EAttribute getYCheckBox_Value() {
		return (EAttribute)yCheckBoxEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser
	 * @generated
	 */
	public EClass getYBrowser() {
		return yBrowserEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser_ datatype
	 * @generated
	 */
	public EReference getYBrowser_Datatype() {
		return (EReference)yBrowserEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser_ datadescription
	 * @generated
	 */
	public EReference getYBrowser_Datadescription() {
		return (EReference)yBrowserEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser_ value
	 * @generated
	 */
	public EAttribute getYBrowser_Value() {
		return (EAttribute)yBrowserEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y date time
	 * @generated
	 */
	public EClass getYDateTime() {
		return yDateTimeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y date time_ datatype
	 * @generated
	 */
	public EReference getYDateTime_Datatype() {
		return (EReference)yDateTimeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y date time_ datadescription
	 * @generated
	 */
	public EReference getYDateTime_Datadescription() {
		return (EReference)yDateTimeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y date time_ value
	 * @generated
	 */
	public EAttribute getYDateTime_Value() {
		return (EAttribute)yDateTimeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y input
	 * @generated
	 */
	public EClass getYInput() {
		return yInputEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal field
	 * @generated
	 */
	public EClass getYDecimalField() {
		return yDecimalFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal field_ datatype
	 * @generated
	 */
	public EReference getYDecimalField_Datatype() {
		return (EReference)yDecimalFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal field_ datadescription
	 * @generated
	 */
	public EReference getYDecimalField_Datadescription() {
		return (EReference)yDecimalFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal field_ value
	 * @generated
	 */
	public EAttribute getYDecimalField_Value() {
		return (EAttribute)yDecimalFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric field
	 * @generated
	 */
	public EClass getYNumericField() {
		return yNumericFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric field_ datatype
	 * @generated
	 */
	public EReference getYNumericField_Datatype() {
		return (EReference)yNumericFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric field_ datadescription
	 * @generated
	 */
	public EReference getYNumericField_Datadescription() {
		return (EReference)yNumericFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric field_ value
	 * @generated
	 */
	public EAttribute getYNumericField_Value() {
		return (EAttribute)yNumericFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box
	 * @generated
	 */
	public EClass getYComboBox() {
		return yComboBoxEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ datadescription
	 * @generated
	 */
	public EReference getYComboBox_Datadescription() {
		return (EReference)yComboBoxEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ datatype
	 * @generated
	 */
	public EReference getYComboBox_Datatype() {
		return (EReference)yComboBoxEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ selection
	 * @generated
	 */
	public EAttribute getYComboBox_Selection() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ collection
	 * @generated
	 */
	public EAttribute getYComboBox_Collection() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ type
	 * @generated
	 */
	public EAttribute getYComboBox_Type() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ emf ns uri
	 * @generated
	 */
	public EAttribute getYComboBox_EmfNsURI() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ type qualified name
	 * @generated
	 */
	public EAttribute getYComboBox_TypeQualifiedName() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ caption property
	 * @generated
	 */
	public EAttribute getYComboBox_CaptionProperty() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ image property
	 * @generated
	 */
	public EAttribute getYComboBox_ImageProperty() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ description property
	 * @generated
	 */
	public EAttribute getYComboBox_DescriptionProperty() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box_ description
	 * @generated
	 */
	public EAttribute getYComboBox_Description() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYComboBox_ModelSelectionType() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYComboBox_ModelSelectionTypeQualifiedName() {
		return (EAttribute)yComboBoxEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list
	 * @generated
	 */
	public EClass getYList() {
		return yListEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ datadescription
	 * @generated
	 */
	public EReference getYList_Datadescription() {
		return (EReference)yListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ datatype
	 * @generated
	 */
	public EReference getYList_Datatype() {
		return (EReference)yListEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ selection type
	 * @generated
	 */
	public EAttribute getYList_SelectionType() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ selection
	 * @generated
	 */
	public EAttribute getYList_Selection() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ multi selection
	 * @generated
	 */
	public EAttribute getYList_MultiSelection() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ collection
	 * @generated
	 */
	public EAttribute getYList_Collection() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ type
	 * @generated
	 */
	public EAttribute getYList_Type() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ emf ns uri
	 * @generated
	 */
	public EAttribute getYList_EmfNsURI() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ type qualified name
	 * @generated
	 */
	public EAttribute getYList_TypeQualifiedName() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ caption property
	 * @generated
	 */
	public EAttribute getYList_CaptionProperty() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ image property
	 * @generated
	 */
	public EAttribute getYList_ImageProperty() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ description property
	 * @generated
	 */
	public EAttribute getYList_DescriptionProperty() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list_ description
	 * @generated
	 */
	public EAttribute getYList_Description() {
		return (EAttribute)yListEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y button
	 * @generated
	 */
	public EClass getYButton() {
		return yButtonEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y button_ datadescription
	 * @generated
	 */
	public EReference getYButton_Datadescription() {
		return (EReference)yButtonEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y button_ click listeners
	 * @generated
	 */
	public EAttribute getYButton_ClickListeners() {
		return (EAttribute)yButtonEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y button_ last click time
	 * @generated
	 */
	public EAttribute getYButton_LastClickTime() {
		return (EAttribute)yButtonEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYButton_Image() {
		return (EAttribute)yButtonEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider
	 * @generated
	 */
	public EClass getYSlider() {
		return ySliderEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider_ datadescription
	 * @generated
	 */
	public EReference getYSlider_Datadescription() {
		return (EReference)ySliderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider_ value
	 * @generated
	 */
	public EAttribute getYSlider_Value() {
		return (EAttribute)ySliderEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider_ max value
	 * @generated
	 */
	public EAttribute getYSlider_MaxValue() {
		return (EAttribute)ySliderEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider_ min value
	 * @generated
	 */
	public EAttribute getYSlider_MinValue() {
		return (EAttribute)ySliderEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider_ resolution
	 * @generated
	 */
	public EAttribute getYSlider_Resolution() {
		return (EAttribute)ySliderEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider_ orientation
	 * @generated
	 */
	public EAttribute getYSlider_Orientation() {
		return (EAttribute)ySliderEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y toggle button
	 * @generated
	 */
	public EClass getYToggleButton() {
		return yToggleButtonEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y toggle button_ datadescription
	 * @generated
	 */
	public EReference getYToggleButton_Datadescription() {
		return (EReference)yToggleButtonEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y progress bar
	 * @generated
	 */
	public EClass getYProgressBar() {
		return yProgressBarEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y progress bar_ datatype
	 * @generated
	 */
	public EReference getYProgressBar_Datatype() {
		return (EReference)yProgressBarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y progress bar_ datadescription
	 * @generated
	 */
	public EReference getYProgressBar_Datadescription() {
		return (EReference)yProgressBarEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y progress bar_ value
	 * @generated
	 */
	public EAttribute getYProgressBar_Value() {
		return (EAttribute)yProgressBarEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab sheet
	 * @generated
	 */
	public EClass getYTabSheet() {
		return yTabSheetEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab sheet_ tabs
	 * @generated
	 */
	public EReference getYTabSheet_Tabs() {
		return (EReference)yTabSheetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab
	 * @generated
	 */
	public EClass getYTab() {
		return yTabEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab_ parent
	 * @generated
	 */
	public EReference getYTab_Parent() {
		return (EReference)yTabEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab_ embeddable
	 * @generated
	 */
	public EReference getYTab_Embeddable() {
		return (EReference)yTabEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab_ datadescription
	 * @generated
	 */
	public EReference getYTab_Datadescription() {
		return (EReference)yTabEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab_ orphan datadescriptions
	 * @generated
	 */
	public EReference getYTab_OrphanDatadescriptions() {
		return (EReference)yTabEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail
	 * @generated
	 */
	public EClass getYMasterDetail() {
		return yMasterDetailEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ datatype
	 * @generated
	 */
	public EReference getYMasterDetail_Datatype() {
		return (EReference)yMasterDetailEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ datadescription
	 * @generated
	 */
	public EReference getYMasterDetail_Datadescription() {
		return (EReference)yMasterDetailEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ selection
	 * @generated
	 */
	public EAttribute getYMasterDetail_Selection() {
		return (EAttribute)yMasterDetailEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ collection
	 * @generated
	 */
	public EAttribute getYMasterDetail_Collection() {
		return (EAttribute)yMasterDetailEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ type
	 * @generated
	 */
	public EAttribute getYMasterDetail_Type() {
		return (EAttribute)yMasterDetailEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ emf ns uri
	 * @generated
	 */
	public EAttribute getYMasterDetail_EmfNsURI() {
		return (EAttribute)yMasterDetailEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ master element
	 * @generated
	 */
	public EReference getYMasterDetail_MasterElement() {
		return (EReference)yMasterDetailEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ detail element
	 * @generated
	 */
	public EReference getYMasterDetail_DetailElement() {
		return (EReference)yMasterDetailEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail_ type qualified name
	 * @generated
	 */
	public EAttribute getYMasterDetail_TypeQualifiedName() {
		return (EAttribute)yMasterDetailEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y form layout
	 * @generated
	 */
	public EClass getYFormLayout() {
		return yFormLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y form layout_ cell styles
	 * @generated
	 */
	public EReference getYFormLayout_CellStyles() {
		return (EReference)yFormLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y form layout cell style
	 * @generated
	 */
	public EClass getYFormLayoutCellStyle() {
		return yFormLayoutCellStyleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y form layout cell style_ target
	 * @generated
	 */
	public EReference getYFormLayoutCellStyle_Target() {
		return (EReference)yFormLayoutCellStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y form layout cell style_ alignment
	 * @generated
	 */
	public EAttribute getYFormLayoutCellStyle_Alignment() {
		return (EAttribute)yFormLayoutCellStyleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search field
	 * @generated
	 */
	public EClass getYSearchField() {
		return ySearchFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text search field
	 * @generated
	 */
	public EClass getYTextSearchField() {
		return yTextSearchFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text search field_ datadescription
	 * @generated
	 */
	public EReference getYTextSearchField_Datadescription() {
		return (EReference)yTextSearchFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text search field_ value
	 * @generated
	 */
	public EAttribute getYTextSearchField_Value() {
		return (EAttribute)yTextSearchFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text search field_ wildcard
	 * @generated
	 */
	public EAttribute getYTextSearchField_Wildcard() {
		return (EAttribute)yTextSearchFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text search field_ property path
	 * @generated
	 */
	public EAttribute getYTextSearchField_PropertyPath() {
		return (EAttribute)yTextSearchFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y boolean search field
	 * @generated
	 */
	public EClass getYBooleanSearchField() {
		return yBooleanSearchFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y boolean search field_ datadescription
	 * @generated
	 */
	public EReference getYBooleanSearchField_Datadescription() {
		return (EReference)yBooleanSearchFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y boolean search field_ value
	 * @generated
	 */
	public EAttribute getYBooleanSearchField_Value() {
		return (EAttribute)yBooleanSearchFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y boolean search field_ property path
	 * @generated
	 */
	public EAttribute getYBooleanSearchField_PropertyPath() {
		return (EAttribute)yBooleanSearchFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field
	 * @generated
	 */
	public EClass getYNumericSearchField() {
		return yNumericSearchFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field_ datadescription
	 * @generated
	 */
	public EReference getYNumericSearchField_Datadescription() {
		return (EReference)yNumericSearchFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field_ value
	 * @generated
	 */
	public EAttribute getYNumericSearchField_Value() {
		return (EAttribute)yNumericSearchFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field_ wildcard
	 * @generated
	 */
	public EAttribute getYNumericSearchField_Wildcard() {
		return (EAttribute)yNumericSearchFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field_ property path
	 * @generated
	 */
	public EAttribute getYNumericSearchField_PropertyPath() {
		return (EAttribute)yNumericSearchFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field_ type
	 * @generated
	 */
	public EAttribute getYNumericSearchField_Type() {
		return (EAttribute)yNumericSearchFieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field_ type qualified name
	 * @generated
	 */
	public EAttribute getYNumericSearchField_TypeQualifiedName() {
		return (EAttribute)yNumericSearchFieldEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field
	 * @generated
	 */
	public EClass getYReferenceSearchField() {
		return yReferenceSearchFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field_ datadescription
	 * @generated
	 */
	public EReference getYReferenceSearchField_Datadescription() {
		return (EReference)yReferenceSearchFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field_ value
	 * @generated
	 */
	public EAttribute getYReferenceSearchField_Value() {
		return (EAttribute)yReferenceSearchFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field_ wildcard
	 * @generated
	 */
	public EAttribute getYReferenceSearchField_Wildcard() {
		return (EAttribute)yReferenceSearchFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field_ property path
	 * @generated
	 */
	public EAttribute getYReferenceSearchField_PropertyPath() {
		return (EAttribute)yReferenceSearchFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field_ type
	 * @generated
	 */
	public EAttribute getYReferenceSearchField_Type() {
		return (EAttribute)yReferenceSearchFieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field_ emf ns uri
	 * @generated
	 */
	public EAttribute getYReferenceSearchField_EmfNsURI() {
		return (EAttribute)yReferenceSearchFieldEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field_ type qualified name
	 * @generated
	 */
	public EAttribute getYReferenceSearchField_TypeQualifiedName() {
		return (EAttribute)yReferenceSearchFieldEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y panel
	 * @generated
	 */
	public EClass getYPanel() {
		return yPanelEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y panel_ datadescription
	 * @generated
	 */
	public EReference getYPanel_Datadescription() {
		return (EReference)yPanelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y panel_ first content
	 * @generated
	 */
	public EReference getYPanel_FirstContent() {
		return (EReference)yPanelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y panel_ second content
	 * @generated
	 */
	public EReference getYPanel_SecondContent() {
		return (EReference)yPanelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y split panel
	 * @generated
	 */
	public EClass getYSplitPanel() {
		return ySplitPanelEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y split panel_ datadescription
	 * @generated
	 */
	public EReference getYSplitPanel_Datadescription() {
		return (EReference)ySplitPanelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y split panel_ cell styles
	 * @generated
	 */
	public EReference getYSplitPanel_CellStyles() {
		return (EReference)ySplitPanelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y split panel_ fill horizontal
	 * @generated
	 */
	public EAttribute getYSplitPanel_FillHorizontal() {
		return (EAttribute)ySplitPanelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y split panel_ split position
	 * @generated
	 */
	public EAttribute getYSplitPanel_SplitPosition() {
		return (EAttribute)ySplitPanelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y split panel_ vertical
	 * @generated
	 */
	public EAttribute getYSplitPanel_Vertical() {
		return (EAttribute)ySplitPanelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search panel
	 * @generated
	 */
	public EClass getYSearchPanel() {
		return ySearchPanelEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search panel_ type
	 * @generated
	 */
	public EAttribute getYSearchPanel_Type() {
		return (EAttribute)ySearchPanelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search panel_ emf ns uri
	 * @generated
	 */
	public EAttribute getYSearchPanel_EmfNsURI() {
		return (EAttribute)ySearchPanelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search panel_ type qualified name
	 * @generated
	 */
	public EAttribute getYSearchPanel_TypeQualifiedName() {
		return (EAttribute)ySearchPanelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search panel_ apply filter
	 * @generated
	 */
	public EAttribute getYSearchPanel_ApplyFilter() {
		return (EAttribute)ySearchPanelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search panel_ filter
	 * @generated
	 */
	public EAttribute getYSearchPanel_Filter() {
		return (EAttribute)ySearchPanelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group
	 * @generated
	 */
	public EClass getYEnumOptionsGroup() {
		return yEnumOptionsGroupEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ datadescription
	 * @generated
	 */
	public EReference getYEnumOptionsGroup_Datadescription() {
		return (EReference)yEnumOptionsGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ datatype
	 * @generated
	 */
	public EReference getYEnumOptionsGroup_Datatype() {
		return (EReference)yEnumOptionsGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ selection type
	 * @generated
	 */
	public EAttribute getYEnumOptionsGroup_SelectionType() {
		return (EAttribute)yEnumOptionsGroupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ selection
	 * @generated
	 */
	public EAttribute getYEnumOptionsGroup_Selection() {
		return (EAttribute)yEnumOptionsGroupEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ multi selection
	 * @generated
	 */
	public EAttribute getYEnumOptionsGroup_MultiSelection() {
		return (EAttribute)yEnumOptionsGroupEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ type
	 * @generated
	 */
	public EAttribute getYEnumOptionsGroup_Type() {
		return (EAttribute)yEnumOptionsGroupEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ emf ns uri
	 * @generated
	 */
	public EAttribute getYEnumOptionsGroup_EmfNsURI() {
		return (EAttribute)yEnumOptionsGroupEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group_ type qualified name
	 * @generated
	 */
	public EAttribute getYEnumOptionsGroup_TypeQualifiedName() {
		return (EAttribute)yEnumOptionsGroupEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list
	 * @generated
	 */
	public EClass getYEnumList() {
		return yEnumListEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ datadescription
	 * @generated
	 */
	public EReference getYEnumList_Datadescription() {
		return (EReference)yEnumListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ datatype
	 * @generated
	 */
	public EReference getYEnumList_Datatype() {
		return (EReference)yEnumListEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ selection type
	 * @generated
	 */
	public EAttribute getYEnumList_SelectionType() {
		return (EAttribute)yEnumListEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ selection
	 * @generated
	 */
	public EAttribute getYEnumList_Selection() {
		return (EAttribute)yEnumListEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ multi selection
	 * @generated
	 */
	public EAttribute getYEnumList_MultiSelection() {
		return (EAttribute)yEnumListEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ type
	 * @generated
	 */
	public EAttribute getYEnumList_Type() {
		return (EAttribute)yEnumListEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ emf ns uri
	 * @generated
	 */
	public EAttribute getYEnumList_EmfNsURI() {
		return (EAttribute)yEnumListEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list_ type qualified name
	 * @generated
	 */
	public EAttribute getYEnumList_TypeQualifiedName() {
		return (EAttribute)yEnumListEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box
	 * @generated
	 */
	public EClass getYEnumComboBox() {
		return yEnumComboBoxEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box_ datadescription
	 * @generated
	 */
	public EReference getYEnumComboBox_Datadescription() {
		return (EReference)yEnumComboBoxEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box_ datatype
	 * @generated
	 */
	public EReference getYEnumComboBox_Datatype() {
		return (EReference)yEnumComboBoxEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box_ selection
	 * @generated
	 */
	public EAttribute getYEnumComboBox_Selection() {
		return (EAttribute)yEnumComboBoxEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box_ type
	 * @generated
	 */
	public EAttribute getYEnumComboBox_Type() {
		return (EAttribute)yEnumComboBoxEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box_ emf ns uri
	 * @generated
	 */
	public EAttribute getYEnumComboBox_EmfNsURI() {
		return (EAttribute)yEnumComboBoxEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box_ type qualified name
	 * @generated
	 */
	public EAttribute getYEnumComboBox_TypeQualifiedName() {
		return (EAttribute)yEnumComboBoxEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYEnumComboBox_DefaultLiteral() {
		return (EAttribute)yEnumComboBoxEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean service consumer
	 * @generated
	 */
	public EClass getYBeanServiceConsumer() {
		return yBeanServiceConsumerEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean service consumer_ use bean service
	 * @generated
	 */
	public EAttribute getYBeanServiceConsumer_UseBeanService() {
		return (EAttribute)yBeanServiceConsumerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y add to table command
	 * @generated
	 */
	public EClass getYAddToTableCommand() {
		return yAddToTableCommandEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y add to table command_ table
	 * @generated
	 */
	public EReference getYAddToTableCommand_Table() {
		return (EReference)yAddToTableCommandEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y add to table command_ trigger
	 * @generated
	 */
	public EAttribute getYAddToTableCommand_Trigger() {
		return (EAttribute)yAddToTableCommandEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y remove from table command
	 * @generated
	 */
	public EClass getYRemoveFromTableCommand() {
		return yRemoveFromTableCommandEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y remove from table command_ table
	 * @generated
	 */
	public EReference getYRemoveFromTableCommand_Table() {
		return (EReference)yRemoveFromTableCommandEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y remove from table command_ trigger
	 * @generated
	 */
	public EAttribute getYRemoveFromTableCommand_Trigger() {
		return (EAttribute)yRemoveFromTableCommandEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser stream input
	 * @generated
	 */
	public EClass getYBrowserStreamInput() {
		return yBrowserStreamInputEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser stream input_ filename
	 * @generated
	 */
	public EAttribute getYBrowserStreamInput_Filename() {
		return (EAttribute)yBrowserStreamInputEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser stream input_ input stream
	 * @generated
	 */
	public EAttribute getYBrowserStreamInput_InputStream() {
		return (EAttribute)yBrowserStreamInputEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser stream input_ mime type
	 * @generated
	 */
	public EAttribute getYBrowserStreamInput_MimeType() {
		return (EAttribute)yBrowserStreamInputEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y set new bean instance command
	 * @generated
	 */
	public EClass getYSetNewBeanInstanceCommand() {
		return ySetNewBeanInstanceCommandEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y set new bean instance command_ target
	 * @generated
	 */
	public EReference getYSetNewBeanInstanceCommand_Target() {
		return (EReference)ySetNewBeanInstanceCommandEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y set new bean instance command_ trigger
	 * @generated
	 */
	public EAttribute getYSetNewBeanInstanceCommand_Trigger() {
		return (EAttribute)ySetNewBeanInstanceCommandEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y set new bean instance command_ type
	 * @generated
	 */
	public EAttribute getYSetNewBeanInstanceCommand_Type() {
		return (EAttribute)ySetNewBeanInstanceCommandEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y set new bean instance command_ emf ns uri
	 * @generated
	 */
	public EAttribute getYSetNewBeanInstanceCommand_EmfNsURI() {
		return (EAttribute)ySetNewBeanInstanceCommandEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y set new bean instance command_ type qualified name
	 * @generated
	 */
	public EAttribute getYSetNewBeanInstanceCommand_TypeQualifiedName() {
		return (EAttribute)ySetNewBeanInstanceCommandEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y css layout
	 * @generated
	 */
	public EClass getYCssLayout() {
		return yCssLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y css layout_ cell styles
	 * @generated
	 */
	public EReference getYCssLayout_CellStyles() {
		return (EReference)yCssLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y css layout cell style
	 * @generated
	 */
	public EClass getYCssLayoutCellStyle() {
		return yCssLayoutCellStyleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y css layout cell style_ target
	 * @generated
	 */
	public EReference getYCssLayoutCellStyle_Target() {
		return (EReference)yCssLayoutCellStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y css layout cell style_ alignment
	 * @generated
	 */
	public EAttribute getYCssLayoutCellStyle_Alignment() {
		return (EAttribute)yCssLayoutCellStyleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y filter
	 * @generated
	 */
	public EClass getYFilter() {
		return yFilterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y filter_ property path
	 * @generated
	 */
	public EAttribute getYFilter_PropertyPath() {
		return (EAttribute)yFilterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y filter_ filter value
	 * @generated
	 */
	public EAttribute getYFilter_FilterValue() {
		return (EAttribute)yFilterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout
	 * @generated
	 */
	public EClass getYAbsoluteLayout() {
		return yAbsoluteLayoutEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout_ cell styles
	 * @generated
	 */
	public EReference getYAbsoluteLayout_CellStyles() {
		return (EReference)yAbsoluteLayoutEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout_ child resize enabled
	 * @generated
	 */
	public EAttribute getYAbsoluteLayout_ChildResizeEnabled() {
		return (EAttribute)yAbsoluteLayoutEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout_ child move enabled
	 * @generated
	 */
	public EAttribute getYAbsoluteLayout_ChildMoveEnabled() {
		return (EAttribute)yAbsoluteLayoutEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style
	 * @generated
	 */
	public EClass getYAbsoluteLayoutCellStyle() {
		return yAbsoluteLayoutCellStyleEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style_ target
	 * @generated
	 */
	public EReference getYAbsoluteLayoutCellStyle_Target() {
		return (EReference)yAbsoluteLayoutCellStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style_ top
	 * @generated
	 */
	public EAttribute getYAbsoluteLayoutCellStyle_Top() {
		return (EAttribute)yAbsoluteLayoutCellStyleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style_ bottom
	 * @generated
	 */
	public EAttribute getYAbsoluteLayoutCellStyle_Bottom() {
		return (EAttribute)yAbsoluteLayoutCellStyleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style_ left
	 * @generated
	 */
	public EAttribute getYAbsoluteLayoutCellStyle_Left() {
		return (EAttribute)yAbsoluteLayoutCellStyleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style_ right
	 * @generated
	 */
	public EAttribute getYAbsoluteLayoutCellStyle_Right() {
		return (EAttribute)yAbsoluteLayoutCellStyleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style_ z index
	 * @generated
	 */
	public EAttribute getYAbsoluteLayoutCellStyle_ZIndex() {
		return (EAttribute)yAbsoluteLayoutCellStyleEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field
	 * @generated
	 */
	public EClass getYSuggestTextField() {
		return ySuggestTextFieldEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ datatype
	 * @generated
	 */
	public EReference getYSuggestTextField_Datatype() {
		return (EReference)ySuggestTextFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ datadescription
	 * @generated
	 */
	public EReference getYSuggestTextField_Datadescription() {
		return (EReference)ySuggestTextFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ value
	 * @generated
	 */
	public EAttribute getYSuggestTextField_Value() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSuggestTextField_Keys() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ use suggestions
	 * @generated
	 */
	public EAttribute getYSuggestTextField_UseSuggestions() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSuggestTextField_AutoHidePopup() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ last suggestion
	 * @generated
	 */
	public EAttribute getYSuggestTextField_LastSuggestion() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ type
	 * @generated
	 */
	public EAttribute getYSuggestTextField_Type() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ emf ns uri
	 * @generated
	 */
	public EAttribute getYSuggestTextField_EmfNsURI() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ type qualified name
	 * @generated
	 */
	public EAttribute getYSuggestTextField_TypeQualifiedName() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ item caption property
	 * @generated
	 */
	public EAttribute getYSuggestTextField_ItemCaptionProperty() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ item filter property
	 * @generated
	 */
	public EAttribute getYSuggestTextField_ItemFilterProperty() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field_ item uuid property
	 * @generated
	 */
	public EAttribute getYSuggestTextField_ItemUUIDProperty() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSuggestTextField_CurrentValueDTO() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYSuggestTextField_Event() {
		return (EAttribute)ySuggestTextFieldEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYPasswordField() {
		return yPasswordFieldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYPasswordField_Datadescription() {
		return (EReference)yPasswordFieldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYPasswordField_Value() {
		return (EAttribute)yPasswordFieldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYFilteringComponent() {
		return yFilteringComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_Type() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_EmfNsURI() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_TypeQualifiedName() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_ApplyFilter() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_ResetFilter() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_Filter() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYFilteringComponent_FilterDescriptors() {
		return (EReference)yFilteringComponentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYFilteringComponent_TableDescriptors() {
		return (EReference)yFilteringComponentEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_SelectionBeanSlotName() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_FilterCols() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilteringComponent_HideGrid() {
		return (EAttribute)yFilteringComponentEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYFilterDescriptor() {
		return yFilterDescriptorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilterDescriptor_PropertyPath() {
		return (EAttribute)yFilterDescriptorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilterDescriptor_Type() {
		return (EAttribute)yFilterDescriptorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYFilterTableDescriptor() {
		return yFilterTableDescriptorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYFilterTableDescriptor_PropertyPath() {
		return (EAttribute)yFilterTableDescriptorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYKanban() {
		return yKanbanEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getYKanban_Datadescription() {
		return (EReference)yKanbanEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_SelectionType() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_Selection() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_Type() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_EmfNsURI() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_TypeQualifiedName() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_CardECViewId() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_EditDialogId() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_DoubleClicked() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_StateChanged() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYKanban_ToRefresh() {
		return (EAttribute)yKanbanEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYKanbanVisibilityProcessor() {
		return yKanbanVisibilityProcessorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYDialogComponent() {
		return yDialogComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYDialogComponent_ViewContextCallback() {
		return (EAttribute)yDialogComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYDialogComponent_Type() {
		return (EAttribute)yDialogComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getYDialogComponent_UpdateCallback() {
		return (EAttribute)yDialogComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree
	 * @generated
	 */
	public EClass getYTree() {
		return yTreeEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ datatype
	 * @generated
	 */
	public EReference getYTree_Datatype() {
		return (EReference)yTreeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ datadescription
	 * @generated
	 */
	public EReference getYTree_Datadescription() {
		return (EReference)yTreeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ selection type
	 * @generated
	 */
	public EAttribute getYTree_SelectionType() {
		return (EAttribute)yTreeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ selection
	 * @generated
	 */
	public EAttribute getYTree_Selection() {
		return (EAttribute)yTreeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ multi selection
	 * @generated
	 */
	public EAttribute getYTree_MultiSelection() {
		return (EAttribute)yTreeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ collection
	 * @generated
	 */
	public EAttribute getYTree_Collection() {
		return (EAttribute)yTreeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ type
	 * @generated
	 */
	public EAttribute getYTree_Type() {
		return (EAttribute)yTreeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ emf ns uri
	 * @generated
	 */
	public EAttribute getYTree_EmfNsURI() {
		return (EAttribute)yTreeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree_ type qualified name
	 * @generated
	 */
	public EAttribute getYTree_TypeQualifiedName() {
		return (EAttribute)yTreeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group
	 * @generated
	 */
	public EClass getYOptionsGroup() {
		return yOptionsGroupEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ datadescription
	 * @generated
	 */
	public EReference getYOptionsGroup_Datadescription() {
		return (EReference)yOptionsGroupEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ datatype
	 * @generated
	 */
	public EReference getYOptionsGroup_Datatype() {
		return (EReference)yOptionsGroupEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ selection type
	 * @generated
	 */
	public EAttribute getYOptionsGroup_SelectionType() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ selection
	 * @generated
	 */
	public EAttribute getYOptionsGroup_Selection() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ multi selection
	 * @generated
	 */
	public EAttribute getYOptionsGroup_MultiSelection() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ collection
	 * @generated
	 */
	public EAttribute getYOptionsGroup_Collection() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ type
	 * @generated
	 */
	public EAttribute getYOptionsGroup_Type() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ emf ns uri
	 * @generated
	 */
	public EAttribute getYOptionsGroup_EmfNsURI() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ type qualified name
	 * @generated
	 */
	public EAttribute getYOptionsGroup_TypeQualifiedName() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ caption property
	 * @generated
	 */
	public EAttribute getYOptionsGroup_CaptionProperty() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ image property
	 * @generated
	 */
	public EAttribute getYOptionsGroup_ImageProperty() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ description property
	 * @generated
	 */
	public EAttribute getYOptionsGroup_DescriptionProperty() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group_ description
	 * @generated
	 */
	public EAttribute getYOptionsGroup_Description() {
		return (EAttribute)yOptionsGroupEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y button click listener
	 * @generated
	 */
	public EDataType getYButtonClickListener() {
		return yButtonClickListenerEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y input stream
	 * @generated
	 */
	public EDataType getYInputStream() {
		return yInputStreamEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getYKanbanEvent() {
		return yKanbanEventEDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y selection type
	 * @generated
	 */
	public EEnum getYSelectionType() {
		return ySelectionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y boolean search option
	 * @generated
	 */
	public EEnum getYBooleanSearchOption() {
		return yBooleanSearchOptionEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search wildcards
	 * @generated
	 */
	public EEnum getYSearchWildcards() {
		return ySearchWildcardsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getYSuggestTextFieldEvents() {
		return ySuggestTextFieldEventsEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getYFilteringType() {
		return yFilteringTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the extension model factory
	 * @generated
	 */
	public ExtensionModelFactory getExtensionModelFactory() {
		return (ExtensionModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		yInputEClass = createEClass(YINPUT);

		yGridLayoutEClass = createEClass(YGRID_LAYOUT);
		createEReference(yGridLayoutEClass, YGRID_LAYOUT__CELL_STYLES);
		createEAttribute(yGridLayoutEClass, YGRID_LAYOUT__COLUMNS);

		yGridLayoutCellStyleEClass = createEClass(YGRID_LAYOUT_CELL_STYLE);
		createEReference(yGridLayoutCellStyleEClass, YGRID_LAYOUT_CELL_STYLE__TARGET);
		createEAttribute(yGridLayoutCellStyleEClass, YGRID_LAYOUT_CELL_STYLE__ALIGNMENT);
		createEReference(yGridLayoutCellStyleEClass, YGRID_LAYOUT_CELL_STYLE__SPAN_INFO);

		yHorizontalLayoutEClass = createEClass(YHORIZONTAL_LAYOUT);
		createEReference(yHorizontalLayoutEClass, YHORIZONTAL_LAYOUT__CELL_STYLES);

		yHorizontalLayoutCellStyleEClass = createEClass(YHORIZONTAL_LAYOUT_CELL_STYLE);
		createEReference(yHorizontalLayoutCellStyleEClass, YHORIZONTAL_LAYOUT_CELL_STYLE__TARGET);
		createEAttribute(yHorizontalLayoutCellStyleEClass, YHORIZONTAL_LAYOUT_CELL_STYLE__ALIGNMENT);

		yVerticalLayoutEClass = createEClass(YVERTICAL_LAYOUT);
		createEReference(yVerticalLayoutEClass, YVERTICAL_LAYOUT__CELL_STYLES);

		yVerticalLayoutCellStyleEClass = createEClass(YVERTICAL_LAYOUT_CELL_STYLE);
		createEReference(yVerticalLayoutCellStyleEClass, YVERTICAL_LAYOUT_CELL_STYLE__TARGET);
		createEAttribute(yVerticalLayoutCellStyleEClass, YVERTICAL_LAYOUT_CELL_STYLE__ALIGNMENT);

		ySpanInfoEClass = createEClass(YSPAN_INFO);
		createEAttribute(ySpanInfoEClass, YSPAN_INFO__COLUMN_FROM);
		createEAttribute(ySpanInfoEClass, YSPAN_INFO__ROW_FROM);
		createEAttribute(ySpanInfoEClass, YSPAN_INFO__COLUMN_TO);
		createEAttribute(ySpanInfoEClass, YSPAN_INFO__ROW_TO);

		yTableEClass = createEClass(YTABLE);
		createEReference(yTableEClass, YTABLE__DATATYPE);
		createEReference(yTableEClass, YTABLE__DATADESCRIPTION);
		createEAttribute(yTableEClass, YTABLE__SELECTION_TYPE);
		createEAttribute(yTableEClass, YTABLE__SELECTION);
		createEAttribute(yTableEClass, YTABLE__MULTI_SELECTION);
		createEAttribute(yTableEClass, YTABLE__COLLECTION);
		createEAttribute(yTableEClass, YTABLE__TYPE);
		createEAttribute(yTableEClass, YTABLE__EMF_NS_URI);
		createEAttribute(yTableEClass, YTABLE__TYPE_QUALIFIED_NAME);
		createEReference(yTableEClass, YTABLE__COLUMNS);
		createEAttribute(yTableEClass, YTABLE__ITEM_IMAGE_PROPERTY);
		createEAttribute(yTableEClass, YTABLE__FILTER);
		createEAttribute(yTableEClass, YTABLE__REFRESH);
		createEReference(yTableEClass, YTABLE__SORT_ORDER);
		createEAttribute(yTableEClass, YTABLE__DO_SORT);
		createEAttribute(yTableEClass, YTABLE__SCROLL_TO_BOTTOM);
		createEAttribute(yTableEClass, YTABLE__PAGE_LENGTH);

		yColumnEClass = createEClass(YCOLUMN);
		createEAttribute(yColumnEClass, YCOLUMN__ICON);
		createEAttribute(yColumnEClass, YCOLUMN__VISIBLE);
		createEReference(yColumnEClass, YCOLUMN__DATADESCRIPTION);
		createEReference(yColumnEClass, YCOLUMN__ORPHAN_DATADESCRIPTIONS);
		createEAttribute(yColumnEClass, YCOLUMN__ORDERABLE);
		createEAttribute(yColumnEClass, YCOLUMN__COLLAPSED);
		createEAttribute(yColumnEClass, YCOLUMN__COLLAPSIBLE);
		createEAttribute(yColumnEClass, YCOLUMN__ALIGNMENT);
		createEAttribute(yColumnEClass, YCOLUMN__EXPAND_RATIO);
		createEAttribute(yColumnEClass, YCOLUMN__PROPERTY_PATH);
		createEReference(yColumnEClass, YCOLUMN__CONVERTER);
		createEAttribute(yColumnEClass, YCOLUMN__TYPE);
		createEAttribute(yColumnEClass, YCOLUMN__TYPE_QUALIFIED_NAME);

		ySortColumnEClass = createEClass(YSORT_COLUMN);
		createEAttribute(ySortColumnEClass, YSORT_COLUMN__PROPERTY_PATH);
		createEAttribute(ySortColumnEClass, YSORT_COLUMN__TYPE);
		createEAttribute(ySortColumnEClass, YSORT_COLUMN__TYPE_QUALIFIED_NAME);
		createEAttribute(ySortColumnEClass, YSORT_COLUMN__ASC);

		yTreeEClass = createEClass(YTREE);
		createEReference(yTreeEClass, YTREE__DATATYPE);
		createEReference(yTreeEClass, YTREE__DATADESCRIPTION);
		createEAttribute(yTreeEClass, YTREE__SELECTION_TYPE);
		createEAttribute(yTreeEClass, YTREE__SELECTION);
		createEAttribute(yTreeEClass, YTREE__MULTI_SELECTION);
		createEAttribute(yTreeEClass, YTREE__COLLECTION);
		createEAttribute(yTreeEClass, YTREE__TYPE);
		createEAttribute(yTreeEClass, YTREE__EMF_NS_URI);
		createEAttribute(yTreeEClass, YTREE__TYPE_QUALIFIED_NAME);

		yOptionsGroupEClass = createEClass(YOPTIONS_GROUP);
		createEReference(yOptionsGroupEClass, YOPTIONS_GROUP__DATADESCRIPTION);
		createEReference(yOptionsGroupEClass, YOPTIONS_GROUP__DATATYPE);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__SELECTION_TYPE);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__SELECTION);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__MULTI_SELECTION);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__COLLECTION);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__TYPE);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__EMF_NS_URI);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__TYPE_QUALIFIED_NAME);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__CAPTION_PROPERTY);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__IMAGE_PROPERTY);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__DESCRIPTION_PROPERTY);
		createEAttribute(yOptionsGroupEClass, YOPTIONS_GROUP__DESCRIPTION);

		yListEClass = createEClass(YLIST);
		createEReference(yListEClass, YLIST__DATADESCRIPTION);
		createEReference(yListEClass, YLIST__DATATYPE);
		createEAttribute(yListEClass, YLIST__SELECTION_TYPE);
		createEAttribute(yListEClass, YLIST__SELECTION);
		createEAttribute(yListEClass, YLIST__MULTI_SELECTION);
		createEAttribute(yListEClass, YLIST__COLLECTION);
		createEAttribute(yListEClass, YLIST__TYPE);
		createEAttribute(yListEClass, YLIST__EMF_NS_URI);
		createEAttribute(yListEClass, YLIST__TYPE_QUALIFIED_NAME);
		createEAttribute(yListEClass, YLIST__CAPTION_PROPERTY);
		createEAttribute(yListEClass, YLIST__IMAGE_PROPERTY);
		createEAttribute(yListEClass, YLIST__DESCRIPTION_PROPERTY);
		createEAttribute(yListEClass, YLIST__DESCRIPTION);

		yLabelEClass = createEClass(YLABEL);
		createEReference(yLabelEClass, YLABEL__DATADESCRIPTION);
		createEAttribute(yLabelEClass, YLABEL__VALUE);

		yImageEClass = createEClass(YIMAGE);
		createEReference(yImageEClass, YIMAGE__DATADESCRIPTION);
		createEAttribute(yImageEClass, YIMAGE__VALUE);
		createEAttribute(yImageEClass, YIMAGE__RESOURCE);

		yTextFieldEClass = createEClass(YTEXT_FIELD);
		createEReference(yTextFieldEClass, YTEXT_FIELD__DATATYPE);
		createEReference(yTextFieldEClass, YTEXT_FIELD__DATADESCRIPTION);
		createEAttribute(yTextFieldEClass, YTEXT_FIELD__VALUE);

		yBeanReferenceFieldEClass = createEClass(YBEAN_REFERENCE_FIELD);
		createEReference(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__DATADESCRIPTION);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__VALUE);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__TYPE);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__EMF_NS_URI);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__TYPE_QUALIFIED_NAME);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__IN_MEMORY_BEAN_PROVIDER);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__IN_MEMORY_BEAN_PROVIDER_QUALIFIED_NAME);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__CAPTION_PROPERTY_PATH);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__IMAGE_PROPERTY_PATH);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__DESCRIPTION_PROPERTY);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__DESCRIPTION);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE_QUALIFIED_NAME);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE_PROPERTY);
		createEAttribute(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__REQUIRED);
		createEReference(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__FILTERING_COMPONENT);
		createEReference(yBeanReferenceFieldEClass, YBEAN_REFERENCE_FIELD__DIALOG_COMPONENT);

		yTextAreaEClass = createEClass(YTEXT_AREA);
		createEReference(yTextAreaEClass, YTEXT_AREA__DATADESCRIPTION);
		createEReference(yTextAreaEClass, YTEXT_AREA__DATATYPE);
		createEAttribute(yTextAreaEClass, YTEXT_AREA__VALUE);
		createEAttribute(yTextAreaEClass, YTEXT_AREA__WORD_WRAP);
		createEAttribute(yTextAreaEClass, YTEXT_AREA__ROWS);

		yCheckBoxEClass = createEClass(YCHECK_BOX);
		createEReference(yCheckBoxEClass, YCHECK_BOX__DATADESCRIPTION);
		createEReference(yCheckBoxEClass, YCHECK_BOX__DATATYPE);
		createEAttribute(yCheckBoxEClass, YCHECK_BOX__VALUE);

		yBrowserEClass = createEClass(YBROWSER);
		createEReference(yBrowserEClass, YBROWSER__DATATYPE);
		createEReference(yBrowserEClass, YBROWSER__DATADESCRIPTION);
		createEAttribute(yBrowserEClass, YBROWSER__VALUE);

		yDateTimeEClass = createEClass(YDATE_TIME);
		createEReference(yDateTimeEClass, YDATE_TIME__DATATYPE);
		createEReference(yDateTimeEClass, YDATE_TIME__DATADESCRIPTION);
		createEAttribute(yDateTimeEClass, YDATE_TIME__VALUE);

		yDecimalFieldEClass = createEClass(YDECIMAL_FIELD);
		createEReference(yDecimalFieldEClass, YDECIMAL_FIELD__DATATYPE);
		createEReference(yDecimalFieldEClass, YDECIMAL_FIELD__DATADESCRIPTION);
		createEAttribute(yDecimalFieldEClass, YDECIMAL_FIELD__VALUE);

		yNumericFieldEClass = createEClass(YNUMERIC_FIELD);
		createEReference(yNumericFieldEClass, YNUMERIC_FIELD__DATATYPE);
		createEReference(yNumericFieldEClass, YNUMERIC_FIELD__DATADESCRIPTION);
		createEAttribute(yNumericFieldEClass, YNUMERIC_FIELD__VALUE);

		yComboBoxEClass = createEClass(YCOMBO_BOX);
		createEReference(yComboBoxEClass, YCOMBO_BOX__DATADESCRIPTION);
		createEReference(yComboBoxEClass, YCOMBO_BOX__DATATYPE);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__SELECTION);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__COLLECTION);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__TYPE);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__EMF_NS_URI);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__TYPE_QUALIFIED_NAME);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__CAPTION_PROPERTY);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__IMAGE_PROPERTY);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__DESCRIPTION_PROPERTY);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__DESCRIPTION);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__MODEL_SELECTION_TYPE);
		createEAttribute(yComboBoxEClass, YCOMBO_BOX__MODEL_SELECTION_TYPE_QUALIFIED_NAME);

		yButtonEClass = createEClass(YBUTTON);
		createEReference(yButtonEClass, YBUTTON__DATADESCRIPTION);
		createEAttribute(yButtonEClass, YBUTTON__CLICK_LISTENERS);
		createEAttribute(yButtonEClass, YBUTTON__LAST_CLICK_TIME);
		createEAttribute(yButtonEClass, YBUTTON__IMAGE);

		ySliderEClass = createEClass(YSLIDER);
		createEReference(ySliderEClass, YSLIDER__DATADESCRIPTION);
		createEAttribute(ySliderEClass, YSLIDER__VALUE);
		createEAttribute(ySliderEClass, YSLIDER__MAX_VALUE);
		createEAttribute(ySliderEClass, YSLIDER__MIN_VALUE);
		createEAttribute(ySliderEClass, YSLIDER__RESOLUTION);
		createEAttribute(ySliderEClass, YSLIDER__ORIENTATION);

		yToggleButtonEClass = createEClass(YTOGGLE_BUTTON);
		createEReference(yToggleButtonEClass, YTOGGLE_BUTTON__DATADESCRIPTION);

		yProgressBarEClass = createEClass(YPROGRESS_BAR);
		createEReference(yProgressBarEClass, YPROGRESS_BAR__DATATYPE);
		createEReference(yProgressBarEClass, YPROGRESS_BAR__DATADESCRIPTION);
		createEAttribute(yProgressBarEClass, YPROGRESS_BAR__VALUE);

		yTabSheetEClass = createEClass(YTAB_SHEET);
		createEReference(yTabSheetEClass, YTAB_SHEET__TABS);

		yTabEClass = createEClass(YTAB);
		createEReference(yTabEClass, YTAB__PARENT);
		createEReference(yTabEClass, YTAB__EMBEDDABLE);
		createEReference(yTabEClass, YTAB__DATADESCRIPTION);
		createEReference(yTabEClass, YTAB__ORPHAN_DATADESCRIPTIONS);

		yMasterDetailEClass = createEClass(YMASTER_DETAIL);
		createEReference(yMasterDetailEClass, YMASTER_DETAIL__DATATYPE);
		createEReference(yMasterDetailEClass, YMASTER_DETAIL__DATADESCRIPTION);
		createEAttribute(yMasterDetailEClass, YMASTER_DETAIL__SELECTION);
		createEAttribute(yMasterDetailEClass, YMASTER_DETAIL__COLLECTION);
		createEAttribute(yMasterDetailEClass, YMASTER_DETAIL__TYPE);
		createEAttribute(yMasterDetailEClass, YMASTER_DETAIL__EMF_NS_URI);
		createEReference(yMasterDetailEClass, YMASTER_DETAIL__MASTER_ELEMENT);
		createEReference(yMasterDetailEClass, YMASTER_DETAIL__DETAIL_ELEMENT);
		createEAttribute(yMasterDetailEClass, YMASTER_DETAIL__TYPE_QUALIFIED_NAME);

		yFormLayoutEClass = createEClass(YFORM_LAYOUT);
		createEReference(yFormLayoutEClass, YFORM_LAYOUT__CELL_STYLES);

		yFormLayoutCellStyleEClass = createEClass(YFORM_LAYOUT_CELL_STYLE);
		createEReference(yFormLayoutCellStyleEClass, YFORM_LAYOUT_CELL_STYLE__TARGET);
		createEAttribute(yFormLayoutCellStyleEClass, YFORM_LAYOUT_CELL_STYLE__ALIGNMENT);

		ySearchFieldEClass = createEClass(YSEARCH_FIELD);

		yTextSearchFieldEClass = createEClass(YTEXT_SEARCH_FIELD);
		createEReference(yTextSearchFieldEClass, YTEXT_SEARCH_FIELD__DATADESCRIPTION);
		createEAttribute(yTextSearchFieldEClass, YTEXT_SEARCH_FIELD__VALUE);
		createEAttribute(yTextSearchFieldEClass, YTEXT_SEARCH_FIELD__WILDCARD);
		createEAttribute(yTextSearchFieldEClass, YTEXT_SEARCH_FIELD__PROPERTY_PATH);

		yBooleanSearchFieldEClass = createEClass(YBOOLEAN_SEARCH_FIELD);
		createEReference(yBooleanSearchFieldEClass, YBOOLEAN_SEARCH_FIELD__DATADESCRIPTION);
		createEAttribute(yBooleanSearchFieldEClass, YBOOLEAN_SEARCH_FIELD__VALUE);
		createEAttribute(yBooleanSearchFieldEClass, YBOOLEAN_SEARCH_FIELD__PROPERTY_PATH);

		yNumericSearchFieldEClass = createEClass(YNUMERIC_SEARCH_FIELD);
		createEReference(yNumericSearchFieldEClass, YNUMERIC_SEARCH_FIELD__DATADESCRIPTION);
		createEAttribute(yNumericSearchFieldEClass, YNUMERIC_SEARCH_FIELD__VALUE);
		createEAttribute(yNumericSearchFieldEClass, YNUMERIC_SEARCH_FIELD__WILDCARD);
		createEAttribute(yNumericSearchFieldEClass, YNUMERIC_SEARCH_FIELD__PROPERTY_PATH);
		createEAttribute(yNumericSearchFieldEClass, YNUMERIC_SEARCH_FIELD__TYPE);
		createEAttribute(yNumericSearchFieldEClass, YNUMERIC_SEARCH_FIELD__TYPE_QUALIFIED_NAME);

		yReferenceSearchFieldEClass = createEClass(YREFERENCE_SEARCH_FIELD);
		createEReference(yReferenceSearchFieldEClass, YREFERENCE_SEARCH_FIELD__DATADESCRIPTION);
		createEAttribute(yReferenceSearchFieldEClass, YREFERENCE_SEARCH_FIELD__VALUE);
		createEAttribute(yReferenceSearchFieldEClass, YREFERENCE_SEARCH_FIELD__WILDCARD);
		createEAttribute(yReferenceSearchFieldEClass, YREFERENCE_SEARCH_FIELD__PROPERTY_PATH);
		createEAttribute(yReferenceSearchFieldEClass, YREFERENCE_SEARCH_FIELD__TYPE);
		createEAttribute(yReferenceSearchFieldEClass, YREFERENCE_SEARCH_FIELD__EMF_NS_URI);
		createEAttribute(yReferenceSearchFieldEClass, YREFERENCE_SEARCH_FIELD__TYPE_QUALIFIED_NAME);

		yPanelEClass = createEClass(YPANEL);
		createEReference(yPanelEClass, YPANEL__DATADESCRIPTION);
		createEReference(yPanelEClass, YPANEL__FIRST_CONTENT);
		createEReference(yPanelEClass, YPANEL__SECOND_CONTENT);

		ySplitPanelEClass = createEClass(YSPLIT_PANEL);
		createEReference(ySplitPanelEClass, YSPLIT_PANEL__DATADESCRIPTION);
		createEReference(ySplitPanelEClass, YSPLIT_PANEL__CELL_STYLES);
		createEAttribute(ySplitPanelEClass, YSPLIT_PANEL__FILL_HORIZONTAL);
		createEAttribute(ySplitPanelEClass, YSPLIT_PANEL__SPLIT_POSITION);
		createEAttribute(ySplitPanelEClass, YSPLIT_PANEL__VERTICAL);

		ySearchPanelEClass = createEClass(YSEARCH_PANEL);
		createEAttribute(ySearchPanelEClass, YSEARCH_PANEL__TYPE);
		createEAttribute(ySearchPanelEClass, YSEARCH_PANEL__EMF_NS_URI);
		createEAttribute(ySearchPanelEClass, YSEARCH_PANEL__TYPE_QUALIFIED_NAME);
		createEAttribute(ySearchPanelEClass, YSEARCH_PANEL__APPLY_FILTER);
		createEAttribute(ySearchPanelEClass, YSEARCH_PANEL__FILTER);

		yEnumOptionsGroupEClass = createEClass(YENUM_OPTIONS_GROUP);
		createEReference(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__DATADESCRIPTION);
		createEReference(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__DATATYPE);
		createEAttribute(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__SELECTION_TYPE);
		createEAttribute(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__SELECTION);
		createEAttribute(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__MULTI_SELECTION);
		createEAttribute(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__TYPE);
		createEAttribute(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__EMF_NS_URI);
		createEAttribute(yEnumOptionsGroupEClass, YENUM_OPTIONS_GROUP__TYPE_QUALIFIED_NAME);

		yEnumComboBoxEClass = createEClass(YENUM_COMBO_BOX);
		createEReference(yEnumComboBoxEClass, YENUM_COMBO_BOX__DATADESCRIPTION);
		createEReference(yEnumComboBoxEClass, YENUM_COMBO_BOX__DATATYPE);
		createEAttribute(yEnumComboBoxEClass, YENUM_COMBO_BOX__SELECTION);
		createEAttribute(yEnumComboBoxEClass, YENUM_COMBO_BOX__TYPE);
		createEAttribute(yEnumComboBoxEClass, YENUM_COMBO_BOX__EMF_NS_URI);
		createEAttribute(yEnumComboBoxEClass, YENUM_COMBO_BOX__TYPE_QUALIFIED_NAME);
		createEAttribute(yEnumComboBoxEClass, YENUM_COMBO_BOX__DEFAULT_LITERAL);

		yEnumListEClass = createEClass(YENUM_LIST);
		createEReference(yEnumListEClass, YENUM_LIST__DATADESCRIPTION);
		createEReference(yEnumListEClass, YENUM_LIST__DATATYPE);
		createEAttribute(yEnumListEClass, YENUM_LIST__SELECTION_TYPE);
		createEAttribute(yEnumListEClass, YENUM_LIST__SELECTION);
		createEAttribute(yEnumListEClass, YENUM_LIST__MULTI_SELECTION);
		createEAttribute(yEnumListEClass, YENUM_LIST__TYPE);
		createEAttribute(yEnumListEClass, YENUM_LIST__EMF_NS_URI);
		createEAttribute(yEnumListEClass, YENUM_LIST__TYPE_QUALIFIED_NAME);

		yBeanServiceConsumerEClass = createEClass(YBEAN_SERVICE_CONSUMER);
		createEAttribute(yBeanServiceConsumerEClass, YBEAN_SERVICE_CONSUMER__USE_BEAN_SERVICE);

		yAddToTableCommandEClass = createEClass(YADD_TO_TABLE_COMMAND);
		createEReference(yAddToTableCommandEClass, YADD_TO_TABLE_COMMAND__TABLE);
		createEAttribute(yAddToTableCommandEClass, YADD_TO_TABLE_COMMAND__TRIGGER);

		yRemoveFromTableCommandEClass = createEClass(YREMOVE_FROM_TABLE_COMMAND);
		createEReference(yRemoveFromTableCommandEClass, YREMOVE_FROM_TABLE_COMMAND__TABLE);
		createEAttribute(yRemoveFromTableCommandEClass, YREMOVE_FROM_TABLE_COMMAND__TRIGGER);

		yBrowserStreamInputEClass = createEClass(YBROWSER_STREAM_INPUT);
		createEAttribute(yBrowserStreamInputEClass, YBROWSER_STREAM_INPUT__FILENAME);
		createEAttribute(yBrowserStreamInputEClass, YBROWSER_STREAM_INPUT__INPUT_STREAM);
		createEAttribute(yBrowserStreamInputEClass, YBROWSER_STREAM_INPUT__MIME_TYPE);

		ySetNewBeanInstanceCommandEClass = createEClass(YSET_NEW_BEAN_INSTANCE_COMMAND);
		createEReference(ySetNewBeanInstanceCommandEClass, YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET);
		createEAttribute(ySetNewBeanInstanceCommandEClass, YSET_NEW_BEAN_INSTANCE_COMMAND__TRIGGER);
		createEAttribute(ySetNewBeanInstanceCommandEClass, YSET_NEW_BEAN_INSTANCE_COMMAND__TYPE);
		createEAttribute(ySetNewBeanInstanceCommandEClass, YSET_NEW_BEAN_INSTANCE_COMMAND__EMF_NS_URI);
		createEAttribute(ySetNewBeanInstanceCommandEClass, YSET_NEW_BEAN_INSTANCE_COMMAND__TYPE_QUALIFIED_NAME);

		yCssLayoutEClass = createEClass(YCSS_LAYOUT);
		createEReference(yCssLayoutEClass, YCSS_LAYOUT__CELL_STYLES);

		yCssLayoutCellStyleEClass = createEClass(YCSS_LAYOUT_CELL_STYLE);
		createEReference(yCssLayoutCellStyleEClass, YCSS_LAYOUT_CELL_STYLE__TARGET);
		createEAttribute(yCssLayoutCellStyleEClass, YCSS_LAYOUT_CELL_STYLE__ALIGNMENT);

		yFilterEClass = createEClass(YFILTER);
		createEAttribute(yFilterEClass, YFILTER__PROPERTY_PATH);
		createEAttribute(yFilterEClass, YFILTER__FILTER_VALUE);

		yAbsoluteLayoutEClass = createEClass(YABSOLUTE_LAYOUT);
		createEReference(yAbsoluteLayoutEClass, YABSOLUTE_LAYOUT__CELL_STYLES);
		createEAttribute(yAbsoluteLayoutEClass, YABSOLUTE_LAYOUT__CHILD_RESIZE_ENABLED);
		createEAttribute(yAbsoluteLayoutEClass, YABSOLUTE_LAYOUT__CHILD_MOVE_ENABLED);

		yAbsoluteLayoutCellStyleEClass = createEClass(YABSOLUTE_LAYOUT_CELL_STYLE);
		createEReference(yAbsoluteLayoutCellStyleEClass, YABSOLUTE_LAYOUT_CELL_STYLE__TARGET);
		createEAttribute(yAbsoluteLayoutCellStyleEClass, YABSOLUTE_LAYOUT_CELL_STYLE__TOP);
		createEAttribute(yAbsoluteLayoutCellStyleEClass, YABSOLUTE_LAYOUT_CELL_STYLE__BOTTOM);
		createEAttribute(yAbsoluteLayoutCellStyleEClass, YABSOLUTE_LAYOUT_CELL_STYLE__LEFT);
		createEAttribute(yAbsoluteLayoutCellStyleEClass, YABSOLUTE_LAYOUT_CELL_STYLE__RIGHT);
		createEAttribute(yAbsoluteLayoutCellStyleEClass, YABSOLUTE_LAYOUT_CELL_STYLE__ZINDEX);

		ySuggestTextFieldEClass = createEClass(YSUGGEST_TEXT_FIELD);
		createEReference(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__DATATYPE);
		createEReference(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__DATADESCRIPTION);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__VALUE);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__KEYS);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__USE_SUGGESTIONS);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__AUTO_HIDE_POPUP);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__LAST_SUGGESTION);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__TYPE);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__EMF_NS_URI);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__TYPE_QUALIFIED_NAME);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__EVENT);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__ITEM_CAPTION_PROPERTY);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__ITEM_FILTER_PROPERTY);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__ITEM_UUID_PROPERTY);
		createEAttribute(ySuggestTextFieldEClass, YSUGGEST_TEXT_FIELD__CURRENT_VALUE_DTO);

		yPasswordFieldEClass = createEClass(YPASSWORD_FIELD);
		createEReference(yPasswordFieldEClass, YPASSWORD_FIELD__DATADESCRIPTION);
		createEAttribute(yPasswordFieldEClass, YPASSWORD_FIELD__VALUE);

		yFilteringComponentEClass = createEClass(YFILTERING_COMPONENT);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__TYPE);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__EMF_NS_URI);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__TYPE_QUALIFIED_NAME);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__APPLY_FILTER);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__RESET_FILTER);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__FILTER);
		createEReference(yFilteringComponentEClass, YFILTERING_COMPONENT__FILTER_DESCRIPTORS);
		createEReference(yFilteringComponentEClass, YFILTERING_COMPONENT__TABLE_DESCRIPTORS);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__SELECTION_BEAN_SLOT_NAME);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__FILTER_COLS);
		createEAttribute(yFilteringComponentEClass, YFILTERING_COMPONENT__HIDE_GRID);

		yFilterDescriptorEClass = createEClass(YFILTER_DESCRIPTOR);
		createEAttribute(yFilterDescriptorEClass, YFILTER_DESCRIPTOR__PROPERTY_PATH);
		createEAttribute(yFilterDescriptorEClass, YFILTER_DESCRIPTOR__TYPE);

		yFilterTableDescriptorEClass = createEClass(YFILTER_TABLE_DESCRIPTOR);
		createEAttribute(yFilterTableDescriptorEClass, YFILTER_TABLE_DESCRIPTOR__PROPERTY_PATH);

		yKanbanEClass = createEClass(YKANBAN);
		createEReference(yKanbanEClass, YKANBAN__DATADESCRIPTION);
		createEAttribute(yKanbanEClass, YKANBAN__SELECTION_TYPE);
		createEAttribute(yKanbanEClass, YKANBAN__SELECTION);
		createEAttribute(yKanbanEClass, YKANBAN__TYPE);
		createEAttribute(yKanbanEClass, YKANBAN__EMF_NS_URI);
		createEAttribute(yKanbanEClass, YKANBAN__TYPE_QUALIFIED_NAME);
		createEAttribute(yKanbanEClass, YKANBAN__CARD_EC_VIEW_ID);
		createEAttribute(yKanbanEClass, YKANBAN__EDIT_DIALOG_ID);
		createEAttribute(yKanbanEClass, YKANBAN__DOUBLE_CLICKED);
		createEAttribute(yKanbanEClass, YKANBAN__STATE_CHANGED);
		createEAttribute(yKanbanEClass, YKANBAN__TO_REFRESH);

		yKanbanVisibilityProcessorEClass = createEClass(YKANBAN_VISIBILITY_PROCESSOR);

		yDialogComponentEClass = createEClass(YDIALOG_COMPONENT);
		createEAttribute(yDialogComponentEClass, YDIALOG_COMPONENT__VIEW_CONTEXT_CALLBACK);
		createEAttribute(yDialogComponentEClass, YDIALOG_COMPONENT__TYPE);
		createEAttribute(yDialogComponentEClass, YDIALOG_COMPONENT__UPDATE_CALLBACK);

		// Create enums
		ySelectionTypeEEnum = createEEnum(YSELECTION_TYPE);
		yBooleanSearchOptionEEnum = createEEnum(YBOOLEAN_SEARCH_OPTION);
		ySearchWildcardsEEnum = createEEnum(YSEARCH_WILDCARDS);
		ySuggestTextFieldEventsEEnum = createEEnum(YSUGGEST_TEXT_FIELD_EVENTS);
		yFilteringTypeEEnum = createEEnum(YFILTERING_TYPE);

		// Create data types
		yButtonClickListenerEDataType = createEDataType(YBUTTON_CLICK_LISTENER);
		yInputStreamEDataType = createEDataType(YINPUT_STREAM);
		yKanbanEventEDataType = createEDataType(YKANBAN_EVENT);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model. This
	 * method is guarded to have no affect on any invocation but its first. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CoreModelPackage theCoreModelPackage = (CoreModelPackage)EPackage.Registry.INSTANCE.getEPackage(CoreModelPackage.eNS_URI);
		ExtDatatypesPackage theExtDatatypesPackage = (ExtDatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(ExtDatatypesPackage.eNS_URI);
		DatatypesPackage theDatatypesPackage = (DatatypesPackage)EPackage.Registry.INSTANCE.getEPackage(DatatypesPackage.eNS_URI);
		BindingPackage theBindingPackage = (BindingPackage)EPackage.Registry.INSTANCE.getEPackage(BindingPackage.eNS_URI);
		VisibilityPackage theVisibilityPackage = (VisibilityPackage)EPackage.Registry.INSTANCE.getEPackage(VisibilityPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		yInputEClass.getESuperTypes().add(theCoreModelPackage.getYField());
		yGridLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		yGridLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		yGridLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yGridLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYAlignmentContainer());
		yHorizontalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		yHorizontalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		yHorizontalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yHorizontalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYAlignmentContainer());
		yVerticalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		yVerticalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		yVerticalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yVerticalLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYAlignmentContainer());
		yTableEClass.getESuperTypes().add(this.getYInput());
		yTableEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yTableEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yTableEClass.getESuperTypes().add(theCoreModelPackage.getYMultiSelectionBindable());
		yTableEClass.getESuperTypes().add(this.getYBeanServiceConsumer());
		yColumnEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		ySortColumnEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yTreeEClass.getESuperTypes().add(this.getYInput());
		yTreeEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yTreeEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yTreeEClass.getESuperTypes().add(theCoreModelPackage.getYMultiSelectionBindable());
		yTreeEClass.getESuperTypes().add(this.getYBeanServiceConsumer());
		yOptionsGroupEClass.getESuperTypes().add(this.getYInput());
		yOptionsGroupEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yOptionsGroupEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yOptionsGroupEClass.getESuperTypes().add(theCoreModelPackage.getYMultiSelectionBindable());
		yOptionsGroupEClass.getESuperTypes().add(this.getYBeanServiceConsumer());
		yListEClass.getESuperTypes().add(this.getYInput());
		yListEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yListEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yListEClass.getESuperTypes().add(theCoreModelPackage.getYMultiSelectionBindable());
		yListEClass.getESuperTypes().add(this.getYBeanServiceConsumer());
		yLabelEClass.getESuperTypes().add(theCoreModelPackage.getYField());
		yLabelEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yImageEClass.getESuperTypes().add(theCoreModelPackage.getYField());
		yImageEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yTextFieldEClass.getESuperTypes().add(this.getYInput());
		yTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYTextChangeNotifier());
		yBeanReferenceFieldEClass.getESuperTypes().add(this.getYInput());
		yBeanReferenceFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yBeanReferenceFieldEClass.getESuperTypes().add(this.getYBeanServiceConsumer());
		yBeanReferenceFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusable());
		yBeanReferenceFieldEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yBeanReferenceFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yTextAreaEClass.getESuperTypes().add(this.getYInput());
		yTextAreaEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yTextAreaEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yTextAreaEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yTextAreaEClass.getESuperTypes().add(theCoreModelPackage.getYTextChangeNotifier());
		yCheckBoxEClass.getESuperTypes().add(this.getYInput());
		yCheckBoxEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yCheckBoxEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yCheckBoxEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yBrowserEClass.getESuperTypes().add(this.getYInput());
		yBrowserEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yDateTimeEClass.getESuperTypes().add(this.getYInput());
		yDateTimeEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yDateTimeEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yDateTimeEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yDecimalFieldEClass.getESuperTypes().add(this.getYInput());
		yDecimalFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yDecimalFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yDecimalFieldEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yNumericFieldEClass.getESuperTypes().add(this.getYInput());
		yNumericFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yNumericFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yNumericFieldEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yComboBoxEClass.getESuperTypes().add(this.getYInput());
		yComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yComboBoxEClass.getESuperTypes().add(this.getYBeanServiceConsumer());
		yButtonEClass.getESuperTypes().add(theCoreModelPackage.getYAction());
		yButtonEClass.getESuperTypes().add(theCoreModelPackage.getYVisibleable());
		yButtonEClass.getESuperTypes().add(theCoreModelPackage.getYEditable());
		yButtonEClass.getESuperTypes().add(theCoreModelPackage.getYEnable());
		yButtonEClass.getESuperTypes().add(theCoreModelPackage.getYFocusable());
		yButtonEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yButtonEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		ySliderEClass.getESuperTypes().add(this.getYInput());
		ySliderEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yToggleButtonEClass.getESuperTypes().add(theCoreModelPackage.getYAction());
		yToggleButtonEClass.getESuperTypes().add(theCoreModelPackage.getYActivateable());
		yToggleButtonEClass.getESuperTypes().add(theCoreModelPackage.getYFocusable());
		yProgressBarEClass.getESuperTypes().add(this.getYInput());
		yProgressBarEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yTabSheetEClass.getESuperTypes().add(theCoreModelPackage.getYEmbeddable());
		yTabSheetEClass.getESuperTypes().add(theCoreModelPackage.getYFocusable());
		yTabEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yTabEClass.getESuperTypes().add(theCoreModelPackage.getYCssAble());
		yMasterDetailEClass.getESuperTypes().add(this.getYInput());
		yMasterDetailEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yMasterDetailEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yFormLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		yFormLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		yFormLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yFormLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYAlignmentContainer());
		ySearchFieldEClass.getESuperTypes().add(this.getYInput());
		ySearchFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yTextSearchFieldEClass.getESuperTypes().add(this.getYSearchField());
		yBooleanSearchFieldEClass.getESuperTypes().add(this.getYSearchField());
		yNumericSearchFieldEClass.getESuperTypes().add(this.getYSearchField());
		yReferenceSearchFieldEClass.getESuperTypes().add(this.getYSearchField());
		yPanelEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		yPanelEClass.getESuperTypes().add(theCoreModelPackage.getYFocusable());
		ySplitPanelEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		ySearchPanelEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		ySearchPanelEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		ySearchPanelEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yEnumOptionsGroupEClass.getESuperTypes().add(this.getYInput());
		yEnumOptionsGroupEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yEnumOptionsGroupEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yEnumOptionsGroupEClass.getESuperTypes().add(theCoreModelPackage.getYMultiSelectionBindable());
		yEnumComboBoxEClass.getESuperTypes().add(this.getYInput());
		yEnumComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yEnumComboBoxEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yEnumListEClass.getESuperTypes().add(this.getYInput());
		yEnumListEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		yEnumListEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yEnumListEClass.getESuperTypes().add(theCoreModelPackage.getYMultiSelectionBindable());
		yAddToTableCommandEClass.getESuperTypes().add(theCoreModelPackage.getYCommand());
		yRemoveFromTableCommandEClass.getESuperTypes().add(theCoreModelPackage.getYCommand());
		ySetNewBeanInstanceCommandEClass.getESuperTypes().add(theCoreModelPackage.getYCommand());
		yCssLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		yCssLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		yCssLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yCssLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYAlignmentContainer());
		yFilterEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yAbsoluteLayoutEClass.getESuperTypes().add(theCoreModelPackage.getYLayout());
		ySuggestTextFieldEClass.getESuperTypes().add(this.getYInput());
		ySuggestTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		ySuggestTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusable());
		ySuggestTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		ySuggestTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		ySuggestTextFieldEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yPasswordFieldEClass.getESuperTypes().add(this.getYInput());
		yPasswordFieldEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yPasswordFieldEClass.getESuperTypes().add(theCoreModelPackage.getYFocusNotifier());
		yPasswordFieldEClass.getESuperTypes().add(theCoreModelPackage.getYBlurNotifier());
		yPasswordFieldEClass.getESuperTypes().add(theCoreModelPackage.getYTextChangeNotifier());
		yFilteringComponentEClass.getESuperTypes().add(theCoreModelPackage.getYEmbeddable());
		yFilteringComponentEClass.getESuperTypes().add(theCoreModelPackage.getYSpacingable());
		yFilteringComponentEClass.getESuperTypes().add(theCoreModelPackage.getYMarginable());
		yFilterDescriptorEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yFilterDescriptorEClass.getESuperTypes().add(theCoreModelPackage.getYCssAble());
		yFilterTableDescriptorEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yFilterTableDescriptorEClass.getESuperTypes().add(theCoreModelPackage.getYCssAble());
		yKanbanEClass.getESuperTypes().add(this.getYInput());
		yKanbanEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		yKanbanEClass.getESuperTypes().add(this.getYBeanServiceConsumer());
		yKanbanVisibilityProcessorEClass.getESuperTypes().add(theVisibilityPackage.getYVisibilityProcessor());
		yDialogComponentEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		yDialogComponentEClass.getESuperTypes().add(theCoreModelPackage.getYValueBindable());
		yDialogComponentEClass.getESuperTypes().add(theCoreModelPackage.getYCssAble());

		// Initialize classes and features; add operations and parameters
		initEClass(yInputEClass, YInput.class, "YInput", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yGridLayoutEClass, YGridLayout.class, "YGridLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYGridLayout_CellStyles(), this.getYGridLayoutCellStyle(), null, "cellStyles", null, 0, -1, YGridLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYGridLayout_Columns(), ecorePackage.getEInt(), "columns", null, 0, 1, YGridLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(yGridLayoutEClass, this.getYGridLayoutCellStyle(), "addGridLayoutCellStyle", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCoreModelPackage.getYEmbeddable(), "element", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(yGridLayoutCellStyleEClass, YGridLayoutCellStyle.class, "YGridLayoutCellStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYGridLayoutCellStyle_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 1, 1, YGridLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYGridLayoutCellStyle_Alignment(), theCoreModelPackage.getYAlignment(), "alignment", "UNDEFINED", 0, 1, YGridLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYGridLayoutCellStyle_SpanInfo(), this.getYSpanInfo(), null, "spanInfo", null, 0, 1, YGridLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(yGridLayoutCellStyleEClass, this.getYSpanInfo(), "addSpanInfo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "col1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "row1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "col2", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "row2", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(yHorizontalLayoutEClass, YHorizontalLayout.class, "YHorizontalLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYHorizontalLayout_CellStyles(), this.getYHorizontalLayoutCellStyle(), null, "cellStyles", null, 0, -1, YHorizontalLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(yHorizontalLayoutEClass, this.getYHorizontalLayoutCellStyle(), "getCellStyle", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCoreModelPackage.getYEmbeddable(), "element", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(yHorizontalLayoutCellStyleEClass, YHorizontalLayoutCellStyle.class, "YHorizontalLayoutCellStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYHorizontalLayoutCellStyle_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 1, 1, YHorizontalLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYHorizontalLayoutCellStyle_Alignment(), theCoreModelPackage.getYAlignment(), "alignment", "UNDEFINED", 0, 1, YHorizontalLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yVerticalLayoutEClass, YVerticalLayout.class, "YVerticalLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYVerticalLayout_CellStyles(), this.getYVerticalLayoutCellStyle(), null, "cellStyles", null, 0, -1, YVerticalLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yVerticalLayoutCellStyleEClass, YVerticalLayoutCellStyle.class, "YVerticalLayoutCellStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYVerticalLayoutCellStyle_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 1, 1, YVerticalLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYVerticalLayoutCellStyle_Alignment(), theCoreModelPackage.getYAlignment(), "alignment", "UNDEFINED", 0, 1, YVerticalLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySpanInfoEClass, YSpanInfo.class, "YSpanInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYSpanInfo_ColumnFrom(), ecorePackage.getEInt(), "columnFrom", null, 0, 1, YSpanInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSpanInfo_RowFrom(), ecorePackage.getEInt(), "rowFrom", null, 0, 1, YSpanInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSpanInfo_ColumnTo(), ecorePackage.getEInt(), "columnTo", null, 0, 1, YSpanInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSpanInfo_RowTo(), ecorePackage.getEInt(), "rowTo", null, 0, 1, YSpanInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTableEClass, YTable.class, "YTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTable_Datatype(), theExtDatatypesPackage.getYTableDatatype(), null, "datatype", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTable_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_SelectionType(), this.getYSelectionType(), "selectionType", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YTable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_MultiSelection(), ecorePackage.getEJavaObject(), "multiSelection", null, 0, -1, YTable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, YTable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYTable_Type(), g1, "type", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTable_Columns(), this.getYColumn(), null, "columns", null, 0, -1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_ItemImageProperty(), ecorePackage.getEString(), "itemImageProperty", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_Filter(), ecorePackage.getEJavaObject(), "filter", null, 0, 1, YTable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_Refresh(), ecorePackage.getEJavaObject(), "refresh", null, 0, 1, YTable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTable_SortOrder(), this.getYSortColumn(), null, "sortOrder", null, 0, -1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_DoSort(), ecorePackage.getEJavaObject(), "doSort", null, 0, 1, YTable.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_ScrollToBottom(), ecorePackage.getEBoolean(), "scrollToBottom", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTable_PageLength(), ecorePackage.getEInt(), "pageLength", null, 0, 1, YTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yColumnEClass, YColumn.class, "YColumn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYColumn_Icon(), ecorePackage.getEString(), "icon", null, 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_Visible(), ecorePackage.getEBoolean(), "visible", "true", 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYColumn_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYColumn_OrphanDatadescriptions(), theDatatypesPackage.getYDatadescription(), null, "orphanDatadescriptions", null, 0, -1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_Orderable(), ecorePackage.getEBoolean(), "orderable", "true", 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_Collapsed(), ecorePackage.getEBoolean(), "collapsed", "false", 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_Collapsible(), ecorePackage.getEBoolean(), "collapsible", "true", 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_Alignment(), theCoreModelPackage.getYFlatAlignment(), "alignment", "LEFT", 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_ExpandRatio(), ecorePackage.getEFloat(), "expandRatio", "-1.0f", 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYColumn_Converter(), theCoreModelPackage.getYConverter(), null, "converter", null, 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYColumn_Type(), g1, "type", null, 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYColumn_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySortColumnEClass, YSortColumn.class, "YSortColumn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYSortColumn_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YSortColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYSortColumn_Type(), g1, "type", null, 0, 1, YSortColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSortColumn_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YSortColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSortColumn_Asc(), ecorePackage.getEBoolean(), "asc", "true", 0, 1, YSortColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTreeEClass, YTree.class, "YTree", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTree_Datatype(), theExtDatatypesPackage.getYTreeDatatype(), null, "datatype", null, 0, 1, YTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTree_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTree_SelectionType(), this.getYSelectionType(), "selectionType", null, 0, 1, YTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTree_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YTree.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTree_MultiSelection(), ecorePackage.getEJavaObject(), "multiSelection", null, 0, -1, YTree.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTree_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, YTree.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYTree_Type(), g1, "type", null, 0, 1, YTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTree_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTree_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yOptionsGroupEClass, YOptionsGroup.class, "YOptionsGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYOptionsGroup_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYOptionsGroup_Datatype(), theExtDatatypesPackage.getYOptionsGroupDataType(), null, "datatype", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_SelectionType(), this.getYSelectionType(), "selectionType", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YOptionsGroup.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_MultiSelection(), ecorePackage.getEJavaObject(), "multiSelection", null, 0, -1, YOptionsGroup.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, YOptionsGroup.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYOptionsGroup_Type(), g1, "type", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_CaptionProperty(), ecorePackage.getEString(), "captionProperty", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_ImageProperty(), ecorePackage.getEString(), "imageProperty", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_DescriptionProperty(), ecorePackage.getEString(), "descriptionProperty", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYOptionsGroup_Description(), ecorePackage.getEString(), "description", null, 0, 1, YOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yListEClass, YList.class, "YList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYList_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYList_Datatype(), theExtDatatypesPackage.getYListDataType(), null, "datatype", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_SelectionType(), this.getYSelectionType(), "selectionType", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YList.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_MultiSelection(), ecorePackage.getEJavaObject(), "multiSelection", null, 0, -1, YList.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, YList.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYList_Type(), g1, "type", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_CaptionProperty(), ecorePackage.getEString(), "captionProperty", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_ImageProperty(), ecorePackage.getEString(), "imageProperty", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_DescriptionProperty(), ecorePackage.getEString(), "descriptionProperty", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYList_Description(), ecorePackage.getEString(), "description", null, 0, 1, YList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yLabelEClass, YLabel.class, "YLabel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYLabel_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YLabel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYLabel_Value(), ecorePackage.getEString(), "value", null, 0, 1, YLabel.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yImageEClass, YImage.class, "YImage", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYImage_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YImage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYImage_Value(), ecorePackage.getEString(), "value", null, 0, 1, YImage.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYImage_Resource(), ecorePackage.getEJavaObject(), "resource", null, 0, 1, YImage.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTextFieldEClass, YTextField.class, "YTextField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTextField_Datatype(), theExtDatatypesPackage.getYTextDatatype(), null, "datatype", null, 0, 1, YTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTextField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTextField_Value(), ecorePackage.getEString(), "value", null, 0, 1, YTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yBeanReferenceFieldEClass, YBeanReferenceField.class, "YBeanReferenceField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYBeanReferenceField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_Value(), ecorePackage.getEJavaObject(), "value", null, 0, 1, YBeanReferenceField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYBeanReferenceField_Type(), g1, "type", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYBeanReferenceField_InMemoryBeanProvider(), g1, "inMemoryBeanProvider", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_InMemoryBeanProviderQualifiedName(), ecorePackage.getEString(), "inMemoryBeanProviderQualifiedName", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_CaptionPropertyPath(), ecorePackage.getEString(), "captionPropertyPath", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_ImagePropertyPath(), ecorePackage.getEString(), "imagePropertyPath", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_DescriptionProperty(), ecorePackage.getEString(), "descriptionProperty", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_Description(), ecorePackage.getEString(), "description", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYBeanReferenceField_ReferenceSourceType(), g1, "referenceSourceType", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_ReferenceSourceTypeQualifiedName(), ecorePackage.getEString(), "referenceSourceTypeQualifiedName", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_ReferenceSourceTypeProperty(), ecorePackage.getEString(), "referenceSourceTypeProperty", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBeanReferenceField_Required(), ecorePackage.getEBoolean(), "required", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYBeanReferenceField_FilteringComponent(), this.getYFilteringComponent(), null, "filteringComponent", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYBeanReferenceField_DialogComponent(), this.getYDialogComponent(), null, "dialogComponent", null, 0, 1, YBeanReferenceField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTextAreaEClass, YTextArea.class, "YTextArea", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTextArea_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YTextArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTextArea_Datatype(), theExtDatatypesPackage.getYTextAreaDatatype(), null, "datatype", null, 0, 1, YTextArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTextArea_Value(), ecorePackage.getEString(), "value", null, 0, 1, YTextArea.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTextArea_WordWrap(), ecorePackage.getEBoolean(), "wordWrap", "true", 0, 1, YTextArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTextArea_Rows(), ecorePackage.getEInt(), "rows", "3", 0, 1, YTextArea.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yCheckBoxEClass, YCheckBox.class, "YCheckBox", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYCheckBox_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YCheckBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYCheckBox_Datatype(), theExtDatatypesPackage.getYCheckBoxDatatype(), null, "datatype", null, 0, 1, YCheckBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYCheckBox_Value(), ecorePackage.getEBoolean(), "value", null, 0, 1, YCheckBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yBrowserEClass, YBrowser.class, "YBrowser", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYBrowser_Datatype(), theExtDatatypesPackage.getYBrowserDatatype(), null, "datatype", null, 0, 1, YBrowser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYBrowser_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YBrowser.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBrowser_Value(), ecorePackage.getEJavaObject(), "value", null, 0, 1, YBrowser.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yDateTimeEClass, YDateTime.class, "YDateTime", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYDateTime_Datatype(), theExtDatatypesPackage.getYDateTimeDatatype(), null, "datatype", null, 0, 1, YDateTime.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYDateTime_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YDateTime.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYDateTime_Value(), ecorePackage.getEDate(), "value", null, 0, 1, YDateTime.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yDecimalFieldEClass, YDecimalField.class, "YDecimalField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYDecimalField_Datatype(), theExtDatatypesPackage.getYDecimalDatatype(), null, "datatype", null, 0, 1, YDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYDecimalField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YDecimalField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYDecimalField_Value(), ecorePackage.getEDouble(), "value", null, 0, 1, YDecimalField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yNumericFieldEClass, YNumericField.class, "YNumericField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYNumericField_Datatype(), theExtDatatypesPackage.getYNumericDatatype(), null, "datatype", null, 0, 1, YNumericField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYNumericField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YNumericField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYNumericField_Value(), ecorePackage.getELong(), "value", null, 0, 1, YNumericField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yComboBoxEClass, YComboBox.class, "YComboBox", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYComboBox_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYComboBox_Datatype(), theExtDatatypesPackage.getYComboBoxDatatype(), null, "datatype", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, YComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYComboBox_Type(), g1, "type", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_CaptionProperty(), ecorePackage.getEString(), "captionProperty", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_ImageProperty(), ecorePackage.getEString(), "imageProperty", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_DescriptionProperty(), ecorePackage.getEString(), "descriptionProperty", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_Description(), ecorePackage.getEString(), "description", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYComboBox_ModelSelectionType(), g1, "modelSelectionType", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYComboBox_ModelSelectionTypeQualifiedName(), ecorePackage.getEString(), "modelSelectionTypeQualifiedName", null, 0, 1, YComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yButtonEClass, YButton.class, "YButton", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYButton_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYButton_ClickListeners(), this.getYButtonClickListener(), "clickListeners", null, 0, -1, YButton.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYButton_LastClickTime(), ecorePackage.getELong(), "lastClickTime", null, 0, 1, YButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYButton_Image(), ecorePackage.getEJavaObject(), "image", null, 0, 1, YButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(yButtonEClass, null, "addClickListener", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getYButtonClickListener(), "listener", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = addEOperation(yButtonEClass, null, "removeClickListener", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getYButtonClickListener(), "listener", 0, 1, IS_UNIQUE, IS_ORDERED);

		addEOperation(yButtonEClass, theBindingPackage.getYECViewModelValueBindingEndpoint(), "createClickEndpoint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ySliderEClass, YSlider.class, "YSlider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYSlider_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YSlider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSlider_Value(), ecorePackage.getEDouble(), "value", null, 0, 1, YSlider.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSlider_MaxValue(), ecorePackage.getEDouble(), "maxValue", "100", 0, 1, YSlider.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSlider_MinValue(), ecorePackage.getEDouble(), "minValue", "0", 0, 1, YSlider.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSlider_Resolution(), ecorePackage.getEInt(), "resolution", "10", 0, 1, YSlider.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSlider_Orientation(), theCoreModelPackage.getYOrientation(), "orientation", null, 0, 1, YSlider.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yToggleButtonEClass, YToggleButton.class, "YToggleButton", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYToggleButton_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YToggleButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yProgressBarEClass, YProgressBar.class, "YProgressBar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYProgressBar_Datatype(), theExtDatatypesPackage.getYProgressBarDatatype(), null, "datatype", null, 0, 1, YProgressBar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYProgressBar_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YProgressBar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYProgressBar_Value(), ecorePackage.getEFloat(), "value", null, 0, 1, YProgressBar.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTabSheetEClass, YTabSheet.class, "YTabSheet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTabSheet_Tabs(), this.getYTab(), this.getYTab_Parent(), "tabs", null, 0, -1, YTabSheet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yTabEClass, YTab.class, "YTab", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTab_Parent(), this.getYTabSheet(), this.getYTabSheet_Tabs(), "parent", null, 1, 1, YTab.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTab_Embeddable(), theCoreModelPackage.getYEmbeddable(), null, "embeddable", null, 1, 1, YTab.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTab_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YTab.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYTab_OrphanDatadescriptions(), theDatatypesPackage.getYDatadescription(), null, "orphanDatadescriptions", null, 0, -1, YTab.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(yTabEClass, theCoreModelPackage.getYView(), "getView", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(yMasterDetailEClass, YMasterDetail.class, "YMasterDetail", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYMasterDetail_Datatype(), theExtDatatypesPackage.getYMasterDetailDatatype(), null, "datatype", null, 0, 1, YMasterDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYMasterDetail_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YMasterDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMasterDetail_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YMasterDetail.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMasterDetail_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, YMasterDetail.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYMasterDetail_Type(), g1, "type", null, 0, 1, YMasterDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMasterDetail_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YMasterDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYMasterDetail_MasterElement(), theCoreModelPackage.getYEmbeddable(), null, "masterElement", null, 0, 1, YMasterDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYMasterDetail_DetailElement(), theCoreModelPackage.getYEmbeddable(), null, "detailElement", null, 0, 1, YMasterDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYMasterDetail_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YMasterDetail.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yFormLayoutEClass, YFormLayout.class, "YFormLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYFormLayout_CellStyles(), this.getYFormLayoutCellStyle(), null, "cellStyles", null, 0, -1, YFormLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yFormLayoutCellStyleEClass, YFormLayoutCellStyle.class, "YFormLayoutCellStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYFormLayoutCellStyle_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 1, 1, YFormLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFormLayoutCellStyle_Alignment(), theCoreModelPackage.getYAlignment(), "alignment", "UNDEFINED", 0, 1, YFormLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySearchFieldEClass, YSearchField.class, "YSearchField", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yTextSearchFieldEClass, YTextSearchField.class, "YTextSearchField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYTextSearchField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YTextSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTextSearchField_Value(), ecorePackage.getEString(), "value", null, 0, 1, YTextSearchField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTextSearchField_Wildcard(), this.getYSearchWildcards(), "wildcard", null, 0, 1, YTextSearchField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYTextSearchField_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YTextSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yBooleanSearchFieldEClass, YBooleanSearchField.class, "YBooleanSearchField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYBooleanSearchField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YBooleanSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBooleanSearchField_Value(), this.getYBooleanSearchOption(), "value", null, 0, 1, YBooleanSearchField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBooleanSearchField_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YBooleanSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yNumericSearchFieldEClass, YNumericSearchField.class, "YNumericSearchField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYNumericSearchField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YNumericSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYNumericSearchField_Value(), ecorePackage.getEString(), "value", null, 0, 1, YNumericSearchField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYNumericSearchField_Wildcard(), this.getYSearchWildcards(), "wildcard", null, 0, 1, YNumericSearchField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYNumericSearchField_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YNumericSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYNumericSearchField_Type(), g1, "type", null, 0, 1, YNumericSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYNumericSearchField_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YNumericSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yReferenceSearchFieldEClass, YReferenceSearchField.class, "YReferenceSearchField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYReferenceSearchField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YReferenceSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYReferenceSearchField_Value(), ecorePackage.getEJavaObject(), "value", null, 0, 1, YReferenceSearchField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYReferenceSearchField_Wildcard(), this.getYSearchWildcards(), "wildcard", null, 0, 1, YReferenceSearchField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYReferenceSearchField_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YReferenceSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYReferenceSearchField_Type(), g1, "type", null, 0, 1, YReferenceSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYReferenceSearchField_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YReferenceSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYReferenceSearchField_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YReferenceSearchField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yPanelEClass, YPanel.class, "YPanel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYPanel_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYPanel_FirstContent(), theCoreModelPackage.getYEmbeddable(), null, "firstContent", null, 0, 1, YPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYPanel_SecondContent(), theCoreModelPackage.getYEmbeddable(), null, "secondContent", null, 0, 1, YPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(yPanelEClass, this.getYHorizontalLayoutCellStyle(), "getCellStyle", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCoreModelPackage.getYEmbeddable(), "element", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ySplitPanelEClass, YSplitPanel.class, "YSplitPanel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYSplitPanel_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YSplitPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSplitPanel_CellStyles(), this.getYHorizontalLayoutCellStyle(), null, "cellStyles", null, 0, -1, YSplitPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSplitPanel_FillHorizontal(), ecorePackage.getEBoolean(), "fillHorizontal", "true", 0, 1, YSplitPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSplitPanel_SplitPosition(), ecorePackage.getEInt(), "splitPosition", "50", 0, 1, YSplitPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSplitPanel_Vertical(), ecorePackage.getEBoolean(), "vertical", "true", 0, 1, YSplitPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(ySplitPanelEClass, this.getYHorizontalLayoutCellStyle(), "getCellStyle", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theCoreModelPackage.getYEmbeddable(), "element", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(ySearchPanelEClass, YSearchPanel.class, "YSearchPanel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYSearchPanel_Type(), g1, "type", null, 0, 1, YSearchPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSearchPanel_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YSearchPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSearchPanel_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YSearchPanel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSearchPanel_ApplyFilter(), ecorePackage.getEJavaObject(), "applyFilter", null, 0, 1, YSearchPanel.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSearchPanel_Filter(), ecorePackage.getEJavaObject(), "filter", null, 0, 1, YSearchPanel.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yEnumOptionsGroupEClass, YEnumOptionsGroup.class, "YEnumOptionsGroup", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYEnumOptionsGroup_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YEnumOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYEnumOptionsGroup_Datatype(), theExtDatatypesPackage.getYOptionsGroupDataType(), null, "datatype", null, 0, 1, YEnumOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumOptionsGroup_SelectionType(), this.getYSelectionType(), "selectionType", null, 0, 1, YEnumOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumOptionsGroup_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YEnumOptionsGroup.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumOptionsGroup_MultiSelection(), ecorePackage.getEJavaObject(), "multiSelection", null, 0, -1, YEnumOptionsGroup.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYEnumOptionsGroup_Type(), g1, "type", null, 0, 1, YEnumOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumOptionsGroup_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YEnumOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumOptionsGroup_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YEnumOptionsGroup.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yEnumComboBoxEClass, YEnumComboBox.class, "YEnumComboBox", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYEnumComboBox_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YEnumComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYEnumComboBox_Datatype(), theExtDatatypesPackage.getYComboBoxDatatype(), null, "datatype", null, 0, 1, YEnumComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumComboBox_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YEnumComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYEnumComboBox_Type(), g1, "type", null, 0, 1, YEnumComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumComboBox_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YEnumComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumComboBox_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YEnumComboBox.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumComboBox_DefaultLiteral(), ecorePackage.getEJavaObject(), "defaultLiteral", null, 0, 1, YEnumComboBox.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yEnumListEClass, YEnumList.class, "YEnumList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYEnumList_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YEnumList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYEnumList_Datatype(), theExtDatatypesPackage.getYListDataType(), null, "datatype", null, 0, 1, YEnumList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumList_SelectionType(), this.getYSelectionType(), "selectionType", null, 0, 1, YEnumList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumList_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YEnumList.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumList_MultiSelection(), ecorePackage.getEJavaObject(), "multiSelection", null, 0, -1, YEnumList.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYEnumList_Type(), g1, "type", null, 0, 1, YEnumList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumList_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YEnumList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYEnumList_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YEnumList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yBeanServiceConsumerEClass, YBeanServiceConsumer.class, "YBeanServiceConsumer", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYBeanServiceConsumer_UseBeanService(), ecorePackage.getEBoolean(), "useBeanService", null, 0, 1, YBeanServiceConsumer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yAddToTableCommandEClass, YAddToTableCommand.class, "YAddToTableCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYAddToTableCommand_Table(), this.getYTable(), null, "table", null, 0, 1, YAddToTableCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAddToTableCommand_Trigger(), ecorePackage.getEJavaObject(), "trigger", null, 0, 1, YAddToTableCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yRemoveFromTableCommandEClass, YRemoveFromTableCommand.class, "YRemoveFromTableCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYRemoveFromTableCommand_Table(), this.getYTable(), null, "table", null, 0, 1, YRemoveFromTableCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYRemoveFromTableCommand_Trigger(), ecorePackage.getEJavaObject(), "trigger", null, 0, 1, YRemoveFromTableCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yBrowserStreamInputEClass, YBrowserStreamInput.class, "YBrowserStreamInput", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYBrowserStreamInput_Filename(), ecorePackage.getEString(), "filename", null, 1, 1, YBrowserStreamInput.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBrowserStreamInput_InputStream(), this.getYInputStream(), "inputStream", null, 1, 1, YBrowserStreamInput.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYBrowserStreamInput_MimeType(), ecorePackage.getEString(), "mimeType", null, 1, 1, YBrowserStreamInput.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySetNewBeanInstanceCommandEClass, YSetNewBeanInstanceCommand.class, "YSetNewBeanInstanceCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYSetNewBeanInstanceCommand_Target(), theBindingPackage.getYValueBindingEndpoint(), null, "target", null, 0, 1, YSetNewBeanInstanceCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSetNewBeanInstanceCommand_Trigger(), ecorePackage.getEJavaObject(), "trigger", null, 0, 1, YSetNewBeanInstanceCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYSetNewBeanInstanceCommand_Type(), g1, "type", null, 0, 1, YSetNewBeanInstanceCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSetNewBeanInstanceCommand_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YSetNewBeanInstanceCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSetNewBeanInstanceCommand_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YSetNewBeanInstanceCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yCssLayoutEClass, YCssLayout.class, "YCssLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYCssLayout_CellStyles(), this.getYCssLayoutCellStyle(), null, "cellStyles", null, 0, -1, YCssLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yCssLayoutCellStyleEClass, YCssLayoutCellStyle.class, "YCssLayoutCellStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYCssLayoutCellStyle_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 1, 1, YCssLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYCssLayoutCellStyle_Alignment(), theCoreModelPackage.getYAlignment(), "alignment", "UNDEFINED", 0, 1, YCssLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yFilterEClass, YFilter.class, "YFilter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYFilter_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilter_FilterValue(), ecorePackage.getEJavaObject(), "filterValue", null, 0, 1, YFilter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yAbsoluteLayoutEClass, YAbsoluteLayout.class, "YAbsoluteLayout", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYAbsoluteLayout_CellStyles(), this.getYAbsoluteLayoutCellStyle(), null, "cellStyles", null, 0, -1, YAbsoluteLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAbsoluteLayout_ChildResizeEnabled(), ecorePackage.getEBoolean(), "childResizeEnabled", null, 0, 1, YAbsoluteLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAbsoluteLayout_ChildMoveEnabled(), ecorePackage.getEBoolean(), "childMoveEnabled", null, 0, 1, YAbsoluteLayout.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yAbsoluteLayoutCellStyleEClass, YAbsoluteLayoutCellStyle.class, "YAbsoluteLayoutCellStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYAbsoluteLayoutCellStyle_Target(), theCoreModelPackage.getYEmbeddable(), null, "target", null, 1, 1, YAbsoluteLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAbsoluteLayoutCellStyle_Top(), ecorePackage.getEInt(), "top", "-1", 0, 1, YAbsoluteLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAbsoluteLayoutCellStyle_Bottom(), ecorePackage.getEInt(), "bottom", "-1", 0, 1, YAbsoluteLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAbsoluteLayoutCellStyle_Left(), ecorePackage.getEInt(), "left", "-1", 0, 1, YAbsoluteLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAbsoluteLayoutCellStyle_Right(), ecorePackage.getEInt(), "right", "-1", 0, 1, YAbsoluteLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYAbsoluteLayoutCellStyle_ZIndex(), ecorePackage.getEInt(), "zIndex", "-1", 0, 1, YAbsoluteLayoutCellStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ySuggestTextFieldEClass, YSuggestTextField.class, "YSuggestTextField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYSuggestTextField_Datatype(), theExtDatatypesPackage.getYTextDatatype(), null, "datatype", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYSuggestTextField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_Value(), ecorePackage.getEString(), "value", null, 0, 1, YSuggestTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_Keys(), ecorePackage.getEString(), "keys", null, 0, 1, YSuggestTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_UseSuggestions(), ecorePackage.getEBoolean(), "useSuggestions", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_AutoHidePopup(), ecorePackage.getEBoolean(), "autoHidePopup", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_LastSuggestion(), ecorePackage.getEJavaObject(), "lastSuggestion", null, 0, 1, YSuggestTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYSuggestTextField_Type(), g1, "type", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_Event(), this.getYSuggestTextFieldEvents(), "event", null, 0, 1, YSuggestTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_ItemCaptionProperty(), ecorePackage.getEString(), "itemCaptionProperty", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_ItemFilterProperty(), ecorePackage.getEString(), "itemFilterProperty", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_ItemUUIDProperty(), ecorePackage.getEString(), "itemUUIDProperty", null, 0, 1, YSuggestTextField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYSuggestTextField_CurrentValueDTO(), ecorePackage.getEJavaObject(), "currentValueDTO", null, 0, 1, YSuggestTextField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yPasswordFieldEClass, YPasswordField.class, "YPasswordField", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYPasswordField_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YPasswordField.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPasswordField_Value(), ecorePackage.getEString(), "value", null, 0, 1, YPasswordField.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yFilteringComponentEClass, YFilteringComponent.class, "YFilteringComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYFilteringComponent_Type(), g1, "type", null, 0, 1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_ApplyFilter(), ecorePackage.getEJavaObject(), "applyFilter", null, 0, 1, YFilteringComponent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_ResetFilter(), ecorePackage.getEJavaObject(), "resetFilter", null, 0, 1, YFilteringComponent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_Filter(), ecorePackage.getEJavaObject(), "filter", null, 0, 1, YFilteringComponent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYFilteringComponent_FilterDescriptors(), this.getYFilterDescriptor(), null, "filterDescriptors", null, 0, -1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getYFilteringComponent_TableDescriptors(), this.getYFilterTableDescriptor(), null, "tableDescriptors", null, 0, -1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_SelectionBeanSlotName(), ecorePackage.getEString(), "selectionBeanSlotName", "$$intern_searchDialogSelection", 0, 1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_FilterCols(), ecorePackage.getEInt(), "filterCols", "2", 0, 1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilteringComponent_HideGrid(), ecorePackage.getEBoolean(), "hideGrid", null, 0, 1, YFilteringComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yFilterDescriptorEClass, YFilterDescriptor.class, "YFilterDescriptor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYFilterDescriptor_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YFilterDescriptor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYFilterDescriptor_Type(), this.getYFilteringType(), "type", null, 0, 1, YFilterDescriptor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yFilterTableDescriptorEClass, YFilterTableDescriptor.class, "YFilterTableDescriptor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYFilterTableDescriptor_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, YFilterTableDescriptor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yKanbanEClass, YKanban.class, "YKanban", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYKanban_Datadescription(), theDatatypesPackage.getYDatadescription(), null, "datadescription", null, 0, 1, YKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_SelectionType(), this.getYSelectionType(), "selectionType", null, 0, 1, YKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, YKanban.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYKanban_Type(), g1, "type", null, 0, 1, YKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, YKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_CardECViewId(), ecorePackage.getEString(), "cardECViewId", null, 0, 1, YKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_EditDialogId(), ecorePackage.getEString(), "editDialogId", null, 0, 1, YKanban.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_DoubleClicked(), this.getYKanbanEvent(), "doubleClicked", null, 0, 1, YKanban.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_StateChanged(), this.getYKanbanEvent(), "stateChanged", null, 0, 1, YKanban.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYKanban_ToRefresh(), this.getYKanbanEvent(), "toRefresh", null, 0, 1, YKanban.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yKanbanVisibilityProcessorEClass, YKanbanVisibilityProcessor.class, "YKanbanVisibilityProcessor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yDialogComponentEClass, YDialogComponent.class, "YDialogComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYDialogComponent_ViewContextCallback(), ecorePackage.getEJavaObject(), "viewContextCallback", null, 0, 1, YDialogComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYDialogComponent_Type(), g1, "type", null, 0, 1, YDialogComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYDialogComponent_UpdateCallback(), ecorePackage.getEJavaObject(), "updateCallback", null, 0, 1, YDialogComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(ySelectionTypeEEnum, YSelectionType.class, "YSelectionType");
		addEEnumLiteral(ySelectionTypeEEnum, YSelectionType.SINGLE);
		addEEnumLiteral(ySelectionTypeEEnum, YSelectionType.MULTI);

		initEEnum(yBooleanSearchOptionEEnum, YBooleanSearchOption.class, "YBooleanSearchOption");
		addEEnumLiteral(yBooleanSearchOptionEEnum, YBooleanSearchOption.TRUE);
		addEEnumLiteral(yBooleanSearchOptionEEnum, YBooleanSearchOption.FALSE);
		addEEnumLiteral(yBooleanSearchOptionEEnum, YBooleanSearchOption.IGNORE);

		initEEnum(ySearchWildcardsEEnum, YSearchWildcards.class, "YSearchWildcards");
		addEEnumLiteral(ySearchWildcardsEEnum, YSearchWildcards.GT);
		addEEnumLiteral(ySearchWildcardsEEnum, YSearchWildcards.GE);
		addEEnumLiteral(ySearchWildcardsEEnum, YSearchWildcards.LT);
		addEEnumLiteral(ySearchWildcardsEEnum, YSearchWildcards.LE);
		addEEnumLiteral(ySearchWildcardsEEnum, YSearchWildcards.EQ);
		addEEnumLiteral(ySearchWildcardsEEnum, YSearchWildcards.NE);
		addEEnumLiteral(ySearchWildcardsEEnum, YSearchWildcards.ANY);

		initEEnum(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.class, "YSuggestTextFieldEvents");
		addEEnumLiteral(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.OPEN_POPUP);
		addEEnumLiteral(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.CLOSE_POPUP);
		addEEnumLiteral(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.NAVIGATE_NEXT);
		addEEnumLiteral(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.NAVIGATE_PREV);
		addEEnumLiteral(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.SELECT);
		addEEnumLiteral(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.CLEAR);
		addEEnumLiteral(ySuggestTextFieldEventsEEnum, YSuggestTextFieldEvents.SELECTED);

		initEEnum(yFilteringTypeEEnum, YFilteringType.class, "YFilteringType");
		addEEnumLiteral(yFilteringTypeEEnum, YFilteringType.COMPARE);
		addEEnumLiteral(yFilteringTypeEEnum, YFilteringType.RANGE);

		// Initialize data types
		initEDataType(yButtonClickListenerEDataType, YButtonClickListener.class, "YButtonClickListener", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(yInputStreamEDataType, InputStream.class, "YInputStream", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(yKanbanEventEDataType, YKanbanEvent.class, "YKanbanEvent", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} // ExtensionModelPackageImpl
