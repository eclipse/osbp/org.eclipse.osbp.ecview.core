/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YAbsolute Layout</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayout#getCellStyles <em>Cell Styles</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayout#isChildResizeEnabled <em>Child Resize Enabled</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayout#isChildMoveEnabled <em>Child Move Enabled</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYAbsoluteLayout()
 * @model
 * @generated
 */
public interface YAbsoluteLayout extends YLayout {
	/**
	 * Returns the value of the '<em><b>Cell Styles</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayoutCellStyle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Styles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Styles</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYAbsoluteLayout_CellStyles()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<YAbsoluteLayoutCellStyle> getCellStyles();

	/**
	 * Returns the value of the '<em><b>Child Resize Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Resize Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Resize Enabled</em>' attribute.
	 * @see #setChildResizeEnabled(boolean)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYAbsoluteLayout_ChildResizeEnabled()
	 * @model
	 * @generated
	 */
	boolean isChildResizeEnabled();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayout#isChildResizeEnabled <em>Child Resize Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child Resize Enabled</em>' attribute.
	 * @see #isChildResizeEnabled()
	 * @generated
	 */
	void setChildResizeEnabled(boolean value);

	/**
	 * Returns the value of the '<em><b>Child Move Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Move Enabled</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Move Enabled</em>' attribute.
	 * @see #setChildMoveEnabled(boolean)
	 * @see org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage#getYAbsoluteLayout_ChildMoveEnabled()
	 * @model
	 * @generated
	 */
	boolean isChildMoveEnabled();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayout#isChildMoveEnabled <em>Child Move Enabled</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child Move Enabled</em>' attribute.
	 * @see #isChildMoveEnabled()
	 * @generated
	 */
	void setChildMoveEnabled(boolean value);

	YAbsoluteLayoutCellStyle getCellStyle(YEmbeddable element);

} // YAbsoluteLayout
