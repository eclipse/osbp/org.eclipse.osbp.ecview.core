/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.impl;

import java.io.InputStream;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.ecview.core.extension.model.extension.*;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAddToTableCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanReferenceField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchOption;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBrowser;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBrowserStreamInput;
import org.eclipse.osbp.ecview.core.extension.model.extension.YButton;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCheckBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YColumn;
import org.eclipse.osbp.ecview.core.extension.model.extension.YComboBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDateTime;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDecimalField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumComboBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumList;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumOptionsGroup;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YImage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YLabel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YList;
import org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YOptionsGroup;
import org.eclipse.osbp.ecview.core.extension.model.extension.YPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YProgressBar;
import org.eclipse.osbp.ecview.core.extension.model.extension.YReferenceSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YRemoveFromTableCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchWildcards;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSelectionType;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSetNewBeanInstanceCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSpanInfo;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTab;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTabSheet;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextArea;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YToggleButton;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTree;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.listener.YButtonClickListener;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExtensionModelFactoryImpl extends EFactoryImpl implements ExtensionModelFactory {
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static ExtensionModelFactory init() {
		try {
			ExtensionModelFactory theExtensionModelFactory = (ExtensionModelFactory)EPackage.Registry.INSTANCE.getEFactory(ExtensionModelPackage.eNS_URI);
			if (theExtensionModelFactory != null) {
				return theExtensionModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ExtensionModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ExtensionModelPackage.YGRID_LAYOUT: return createYGridLayout();
			case ExtensionModelPackage.YGRID_LAYOUT_CELL_STYLE: return createYGridLayoutCellStyle();
			case ExtensionModelPackage.YHORIZONTAL_LAYOUT: return createYHorizontalLayout();
			case ExtensionModelPackage.YHORIZONTAL_LAYOUT_CELL_STYLE: return createYHorizontalLayoutCellStyle();
			case ExtensionModelPackage.YVERTICAL_LAYOUT: return createYVerticalLayout();
			case ExtensionModelPackage.YVERTICAL_LAYOUT_CELL_STYLE: return createYVerticalLayoutCellStyle();
			case ExtensionModelPackage.YSPAN_INFO: return createYSpanInfo();
			case ExtensionModelPackage.YTABLE: return createYTable();
			case ExtensionModelPackage.YCOLUMN: return createYColumn();
			case ExtensionModelPackage.YSORT_COLUMN: return createYSortColumn();
			case ExtensionModelPackage.YTREE: return createYTree();
			case ExtensionModelPackage.YOPTIONS_GROUP: return createYOptionsGroup();
			case ExtensionModelPackage.YLIST: return createYList();
			case ExtensionModelPackage.YLABEL: return createYLabel();
			case ExtensionModelPackage.YIMAGE: return createYImage();
			case ExtensionModelPackage.YTEXT_FIELD: return createYTextField();
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD: return createYBeanReferenceField();
			case ExtensionModelPackage.YTEXT_AREA: return createYTextArea();
			case ExtensionModelPackage.YCHECK_BOX: return createYCheckBox();
			case ExtensionModelPackage.YBROWSER: return createYBrowser();
			case ExtensionModelPackage.YDATE_TIME: return createYDateTime();
			case ExtensionModelPackage.YDECIMAL_FIELD: return createYDecimalField();
			case ExtensionModelPackage.YNUMERIC_FIELD: return createYNumericField();
			case ExtensionModelPackage.YCOMBO_BOX: return createYComboBox();
			case ExtensionModelPackage.YBUTTON: return createYButton();
			case ExtensionModelPackage.YSLIDER: return createYSlider();
			case ExtensionModelPackage.YTOGGLE_BUTTON: return createYToggleButton();
			case ExtensionModelPackage.YPROGRESS_BAR: return createYProgressBar();
			case ExtensionModelPackage.YTAB_SHEET: return createYTabSheet();
			case ExtensionModelPackage.YTAB: return createYTab();
			case ExtensionModelPackage.YMASTER_DETAIL: return createYMasterDetail();
			case ExtensionModelPackage.YFORM_LAYOUT: return createYFormLayout();
			case ExtensionModelPackage.YFORM_LAYOUT_CELL_STYLE: return createYFormLayoutCellStyle();
			case ExtensionModelPackage.YTEXT_SEARCH_FIELD: return createYTextSearchField();
			case ExtensionModelPackage.YBOOLEAN_SEARCH_FIELD: return createYBooleanSearchField();
			case ExtensionModelPackage.YNUMERIC_SEARCH_FIELD: return createYNumericSearchField();
			case ExtensionModelPackage.YREFERENCE_SEARCH_FIELD: return createYReferenceSearchField();
			case ExtensionModelPackage.YPANEL: return createYPanel();
			case ExtensionModelPackage.YSPLIT_PANEL: return createYSplitPanel();
			case ExtensionModelPackage.YSEARCH_PANEL: return createYSearchPanel();
			case ExtensionModelPackage.YENUM_OPTIONS_GROUP: return createYEnumOptionsGroup();
			case ExtensionModelPackage.YENUM_COMBO_BOX: return createYEnumComboBox();
			case ExtensionModelPackage.YENUM_LIST: return createYEnumList();
			case ExtensionModelPackage.YADD_TO_TABLE_COMMAND: return createYAddToTableCommand();
			case ExtensionModelPackage.YREMOVE_FROM_TABLE_COMMAND: return createYRemoveFromTableCommand();
			case ExtensionModelPackage.YBROWSER_STREAM_INPUT: return createYBrowserStreamInput();
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND: return createYSetNewBeanInstanceCommand();
			case ExtensionModelPackage.YCSS_LAYOUT: return createYCssLayout();
			case ExtensionModelPackage.YCSS_LAYOUT_CELL_STYLE: return createYCssLayoutCellStyle();
			case ExtensionModelPackage.YFILTER: return createYFilter();
			case ExtensionModelPackage.YABSOLUTE_LAYOUT: return createYAbsoluteLayout();
			case ExtensionModelPackage.YABSOLUTE_LAYOUT_CELL_STYLE: return createYAbsoluteLayoutCellStyle();
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD: return createYSuggestTextField();
			case ExtensionModelPackage.YPASSWORD_FIELD: return createYPasswordField();
			case ExtensionModelPackage.YFILTERING_COMPONENT: return createYFilteringComponent();
			case ExtensionModelPackage.YFILTER_DESCRIPTOR: return createYFilterDescriptor();
			case ExtensionModelPackage.YFILTER_TABLE_DESCRIPTOR: return createYFilterTableDescriptor();
			case ExtensionModelPackage.YKANBAN: return createYKanban();
			case ExtensionModelPackage.YKANBAN_VISIBILITY_PROCESSOR: return createYKanbanVisibilityProcessor();
			case ExtensionModelPackage.YDIALOG_COMPONENT: return createYDialogComponent();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the object
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ExtensionModelPackage.YSELECTION_TYPE:
				return createYSelectionTypeFromString(eDataType, initialValue);
			case ExtensionModelPackage.YBOOLEAN_SEARCH_OPTION:
				return createYBooleanSearchOptionFromString(eDataType, initialValue);
			case ExtensionModelPackage.YSEARCH_WILDCARDS:
				return createYSearchWildcardsFromString(eDataType, initialValue);
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD_EVENTS:
				return createYSuggestTextFieldEventsFromString(eDataType, initialValue);
			case ExtensionModelPackage.YFILTERING_TYPE:
				return createYFilteringTypeFromString(eDataType, initialValue);
			case ExtensionModelPackage.YBUTTON_CLICK_LISTENER:
				return createYButtonClickListenerFromString(eDataType, initialValue);
			case ExtensionModelPackage.YINPUT_STREAM:
				return createYInputStreamFromString(eDataType, initialValue);
			case ExtensionModelPackage.YKANBAN_EVENT:
				return createYKanbanEventFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ExtensionModelPackage.YSELECTION_TYPE:
				return convertYSelectionTypeToString(eDataType, instanceValue);
			case ExtensionModelPackage.YBOOLEAN_SEARCH_OPTION:
				return convertYBooleanSearchOptionToString(eDataType, instanceValue);
			case ExtensionModelPackage.YSEARCH_WILDCARDS:
				return convertYSearchWildcardsToString(eDataType, instanceValue);
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD_EVENTS:
				return convertYSuggestTextFieldEventsToString(eDataType, instanceValue);
			case ExtensionModelPackage.YFILTERING_TYPE:
				return convertYFilteringTypeToString(eDataType, instanceValue);
			case ExtensionModelPackage.YBUTTON_CLICK_LISTENER:
				return convertYButtonClickListenerToString(eDataType, instanceValue);
			case ExtensionModelPackage.YINPUT_STREAM:
				return convertYInputStreamToString(eDataType, instanceValue);
			case ExtensionModelPackage.YKANBAN_EVENT:
				return convertYKanbanEventToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text field
	 * @generated
	 */
	public YTextField createYTextField() {
		YTextFieldImpl yTextField = new YTextFieldImpl();
		return yTextField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y bean reference field
	 * @generated
	 */
	public YBeanReferenceField createYBeanReferenceField() {
		YBeanReferenceFieldImpl yBeanReferenceField = new YBeanReferenceFieldImpl();
		return yBeanReferenceField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout
	 * @generated
	 */
	public YGridLayout createYGridLayout() {
		YGridLayoutImpl yGridLayout = new YGridLayoutImpl();
		return yGridLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y grid layout cell style
	 * @generated
	 */
	public YGridLayoutCellStyle createYGridLayoutCellStyle() {
		YGridLayoutCellStyleImpl yGridLayoutCellStyle = new YGridLayoutCellStyleImpl();
		return yGridLayoutCellStyle;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y horizontal layout
	 * @generated
	 */
	public YHorizontalLayout createYHorizontalLayout() {
		YHorizontalLayoutImpl yHorizontalLayout = new YHorizontalLayoutImpl();
		return yHorizontalLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y horizontal layout cell style
	 * @generated
	 */
	public YHorizontalLayoutCellStyle createYHorizontalLayoutCellStyle() {
		YHorizontalLayoutCellStyleImpl yHorizontalLayoutCellStyle = new YHorizontalLayoutCellStyleImpl();
		return yHorizontalLayoutCellStyle;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y vertical layout
	 * @generated
	 */
	public YVerticalLayout createYVerticalLayout() {
		YVerticalLayoutImpl yVerticalLayout = new YVerticalLayoutImpl();
		return yVerticalLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y vertical layout cell style
	 * @generated
	 */
	public YVerticalLayoutCellStyle createYVerticalLayoutCellStyle() {
		YVerticalLayoutCellStyleImpl yVerticalLayoutCellStyle = new YVerticalLayoutCellStyleImpl();
		return yVerticalLayoutCellStyle;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y span info
	 * @generated
	 */
	public YSpanInfo createYSpanInfo() {
		YSpanInfoImpl ySpanInfo = new YSpanInfoImpl();
		return ySpanInfo;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table
	 * @generated
	 */
	public YTable createYTable() {
		YTableImpl yTable = new YTableImpl();
		return yTable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column
	 * @generated
	 */
	public YColumn createYColumn() {
		YColumnImpl yColumn = new YColumnImpl();
		return yColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSortColumn createYSortColumn() {
		YSortColumnImpl ySortColumn = new YSortColumnImpl();
		return ySortColumn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y label
	 * @generated
	 */
	public YLabel createYLabel() {
		YLabelImpl yLabel = new YLabelImpl();
		return yLabel;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y image
	 * @generated
	 */
	public YImage createYImage() {
		YImageImpl yImage = new YImageImpl();
		return yImage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area
	 * @generated
	 */
	public YTextArea createYTextArea() {
		YTextAreaImpl yTextArea = new YTextAreaImpl();
		return yTextArea;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y check box
	 * @generated
	 */
	public YCheckBox createYCheckBox() {
		YCheckBoxImpl yCheckBox = new YCheckBoxImpl();
		return yCheckBox;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser
	 * @generated
	 */
	public YBrowser createYBrowser() {
		YBrowserImpl yBrowser = new YBrowserImpl();
		return yBrowser;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y date time
	 * @generated
	 */
	public YDateTime createYDateTime() {
		YDateTimeImpl yDateTime = new YDateTimeImpl();
		return yDateTime;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal field
	 * @generated
	 */
	public YDecimalField createYDecimalField() {
		YDecimalFieldImpl yDecimalField = new YDecimalFieldImpl();
		return yDecimalField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric field
	 * @generated
	 */
	public YNumericField createYNumericField() {
		YNumericFieldImpl yNumericField = new YNumericFieldImpl();
		return yNumericField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box
	 * @generated
	 */
	public YComboBox createYComboBox() {
		YComboBoxImpl yComboBox = new YComboBoxImpl();
		return yComboBox;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list
	 * @generated
	 */
	public YList createYList() {
		YListImpl yList = new YListImpl();
		return yList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y button
	 * @generated
	 */
	public YButton createYButton() {
		YButtonImpl yButton = new YButtonImpl();
		return yButton;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y slider
	 * @generated
	 */
	public YSlider createYSlider() {
		YSliderImpl ySlider = new YSliderImpl();
		return ySlider;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y toggle button
	 * @generated
	 */
	public YToggleButton createYToggleButton() {
		YToggleButtonImpl yToggleButton = new YToggleButtonImpl();
		return yToggleButton;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y progress bar
	 * @generated
	 */
	public YProgressBar createYProgressBar() {
		YProgressBarImpl yProgressBar = new YProgressBarImpl();
		return yProgressBar;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab sheet
	 * @generated
	 */
	public YTabSheet createYTabSheet() {
		YTabSheetImpl yTabSheet = new YTabSheetImpl();
		return yTabSheet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab
	 * @generated
	 */
	public YTab createYTab() {
		YTabImpl yTab = new YTabImpl();
		return yTab;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail
	 * @generated
	 */
	public YMasterDetail createYMasterDetail() {
		YMasterDetailImpl yMasterDetail = new YMasterDetailImpl();
		return yMasterDetail;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y form layout
	 * @generated
	 */
	public YFormLayout createYFormLayout() {
		YFormLayoutImpl yFormLayout = new YFormLayoutImpl();
		return yFormLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y form layout cell style
	 * @generated
	 */
	public YFormLayoutCellStyle createYFormLayoutCellStyle() {
		YFormLayoutCellStyleImpl yFormLayoutCellStyle = new YFormLayoutCellStyleImpl();
		return yFormLayoutCellStyle;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text search field
	 * @generated
	 */
	public YTextSearchField createYTextSearchField() {
		YTextSearchFieldImpl yTextSearchField = new YTextSearchFieldImpl();
		return yTextSearchField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y boolean search field
	 * @generated
	 */
	public YBooleanSearchField createYBooleanSearchField() {
		YBooleanSearchFieldImpl yBooleanSearchField = new YBooleanSearchFieldImpl();
		return yBooleanSearchField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric search field
	 * @generated
	 */
	public YNumericSearchField createYNumericSearchField() {
		YNumericSearchFieldImpl yNumericSearchField = new YNumericSearchFieldImpl();
		return yNumericSearchField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y reference search field
	 * @generated
	 */
	public YReferenceSearchField createYReferenceSearchField() {
		YReferenceSearchFieldImpl yReferenceSearchField = new YReferenceSearchFieldImpl();
		return yReferenceSearchField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y panel
	 * @generated
	 */
	public YPanel createYPanel() {
		YPanelImpl yPanel = new YPanelImpl();
		return yPanel;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y split panel
	 * @generated
	 */
	public YSplitPanel createYSplitPanel() {
		YSplitPanelImpl ySplitPanel = new YSplitPanelImpl();
		return ySplitPanel;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y search panel
	 * @generated
	 */
	public YSearchPanel createYSearchPanel() {
		YSearchPanelImpl ySearchPanel = new YSearchPanelImpl();
		return ySearchPanel;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum options group
	 * @generated
	 */
	public YEnumOptionsGroup createYEnumOptionsGroup() {
		YEnumOptionsGroupImpl yEnumOptionsGroup = new YEnumOptionsGroupImpl();
		return yEnumOptionsGroup;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum list
	 * @generated
	 */
	public YEnumList createYEnumList() {
		YEnumListImpl yEnumList = new YEnumListImpl();
		return yEnumList;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y enum combo box
	 * @generated
	 */
	public YEnumComboBox createYEnumComboBox() {
		YEnumComboBoxImpl yEnumComboBox = new YEnumComboBoxImpl();
		return yEnumComboBox;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y add to table command
	 * @generated
	 */
	public YAddToTableCommand createYAddToTableCommand() {
		YAddToTableCommandImpl yAddToTableCommand = new YAddToTableCommandImpl();
		return yAddToTableCommand;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y remove from table command
	 * @generated
	 */
	public YRemoveFromTableCommand createYRemoveFromTableCommand() {
		YRemoveFromTableCommandImpl yRemoveFromTableCommand = new YRemoveFromTableCommandImpl();
		return yRemoveFromTableCommand;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser stream input
	 * @generated
	 */
	public YBrowserStreamInput createYBrowserStreamInput() {
		YBrowserStreamInputImpl yBrowserStreamInput = new YBrowserStreamInputImpl();
		return yBrowserStreamInput;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y set new bean instance command
	 * @generated
	 */
	public YSetNewBeanInstanceCommand createYSetNewBeanInstanceCommand() {
		YSetNewBeanInstanceCommandImpl ySetNewBeanInstanceCommand = new YSetNewBeanInstanceCommandImpl();
		return ySetNewBeanInstanceCommand;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y css layout
	 * @generated
	 */
	public YCssLayout createYCssLayout() {
		YCssLayoutImpl yCssLayout = new YCssLayoutImpl();
		return yCssLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y css layout cell style
	 * @generated
	 */
	public YCssLayoutCellStyle createYCssLayoutCellStyle() {
		YCssLayoutCellStyleImpl yCssLayoutCellStyle = new YCssLayoutCellStyleImpl();
		return yCssLayoutCellStyle;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y filter
	 * @generated
	 */
	public YFilter createYFilter() {
		YFilterImpl yFilter = new YFilterImpl();
		return yFilter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout
	 * @generated
	 */
	public YAbsoluteLayout createYAbsoluteLayout() {
		YAbsoluteLayoutImpl yAbsoluteLayout = new YAbsoluteLayoutImpl();
		return yAbsoluteLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y absolute layout cell style
	 * @generated
	 */
	public YAbsoluteLayoutCellStyle createYAbsoluteLayoutCellStyle() {
		YAbsoluteLayoutCellStyleImpl yAbsoluteLayoutCellStyle = new YAbsoluteLayoutCellStyleImpl();
		return yAbsoluteLayoutCellStyle;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suggest text field
	 * @generated
	 */
	public YSuggestTextField createYSuggestTextField() {
		YSuggestTextFieldImpl ySuggestTextField = new YSuggestTextFieldImpl();
		return ySuggestTextField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YPasswordField createYPasswordField() {
		YPasswordFieldImpl yPasswordField = new YPasswordFieldImpl();
		return yPasswordField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFilteringComponent createYFilteringComponent() {
		YFilteringComponentImpl yFilteringComponent = new YFilteringComponentImpl();
		return yFilteringComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFilterDescriptor createYFilterDescriptor() {
		YFilterDescriptorImpl yFilterDescriptor = new YFilterDescriptorImpl();
		return yFilterDescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFilterTableDescriptor createYFilterTableDescriptor() {
		YFilterTableDescriptorImpl yFilterTableDescriptor = new YFilterTableDescriptorImpl();
		return yFilterTableDescriptor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YKanban createYKanban() {
		YKanbanImpl yKanban = new YKanbanImpl();
		return yKanban;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YKanbanVisibilityProcessor createYKanbanVisibilityProcessor() {
		YKanbanVisibilityProcessorImpl yKanbanVisibilityProcessor = new YKanbanVisibilityProcessorImpl();
		return yKanbanVisibilityProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDialogComponent createYDialogComponent() {
		YDialogComponentImpl yDialogComponent = new YDialogComponentImpl();
		return yDialogComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree
	 * @generated
	 */
	public YTree createYTree() {
		YTreeImpl yTree = new YTreeImpl();
		return yTree;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group
	 * @generated
	 */
	public YOptionsGroup createYOptionsGroup() {
		YOptionsGroupImpl yOptionsGroup = new YOptionsGroupImpl();
		return yOptionsGroup;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y selection type
	 * @generated
	 */
	public YSelectionType createYSelectionTypeFromString(EDataType eDataType, String initialValue) {
		YSelectionType result = YSelectionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYSelectionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y boolean search option
	 * @generated
	 */
	public YBooleanSearchOption createYBooleanSearchOptionFromString(EDataType eDataType, String initialValue) {
		YBooleanSearchOption result = YBooleanSearchOption.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYBooleanSearchOptionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y search wildcards
	 * @generated
	 */
	public YSearchWildcards createYSearchWildcardsFromString(EDataType eDataType, String initialValue) {
		YSearchWildcards result = YSearchWildcards.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYSearchWildcardsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSuggestTextFieldEvents createYSuggestTextFieldEventsFromString(EDataType eDataType, String initialValue) {
		YSuggestTextFieldEvents result = YSuggestTextFieldEvents.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertYSuggestTextFieldEventsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFilteringType createYFilteringTypeFromString(EDataType eDataType, String initialValue) {
		YFilteringType result = YFilteringType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertYFilteringTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y button click listener
	 * @generated
	 */
	public YButtonClickListener createYButtonClickListenerFromString(EDataType eDataType, String initialValue) {
		return (YButtonClickListener)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYButtonClickListenerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the input stream
	 * @generated
	 */
	public InputStream createYInputStreamFromString(EDataType eDataType, String initialValue) {
		return (InputStream)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYInputStreamToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YKanbanEvent createYKanbanEventFromString(EDataType eDataType, String initialValue) {
		return (YKanbanEvent)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertYKanbanEventToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the extension model package
	 * @generated
	 */
	public ExtensionModelPackage getExtensionModelPackage() {
		return (ExtensionModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ExtensionModelPackage getPackage() {
		return ExtensionModelPackage.eINSTANCE;
	}

} //ExtensionModelFactoryImpl
