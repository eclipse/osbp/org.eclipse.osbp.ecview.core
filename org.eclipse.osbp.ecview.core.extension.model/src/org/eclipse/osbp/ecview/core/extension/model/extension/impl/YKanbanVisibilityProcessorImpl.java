/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.osbp.ecview.core.common.model.visibility.impl.YVisibilityProcessorImpl;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanbanVisibilityProcessor;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YKanban Visibility Processor</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class YKanbanVisibilityProcessorImpl extends YVisibilityProcessorImpl implements YKanbanVisibilityProcessor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YKanbanVisibilityProcessorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExtensionModelPackage.Literals.YKANBAN_VISIBILITY_PROCESSOR;
	}

} //YKanbanVisibilityProcessorImpl
