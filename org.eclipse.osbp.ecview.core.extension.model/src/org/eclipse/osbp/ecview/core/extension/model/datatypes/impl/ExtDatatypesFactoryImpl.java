/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YBrowserDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YCheckBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YComboBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeFormat;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeResolution;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YListDataType;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YMasterDetailDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YNumericDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YOptionsGroupDataType;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YProgressBarDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTabSheetDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTableDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextAreaDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTreeDatatype;

// TODO: Auto-generated Javadoc
/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExtDatatypesFactoryImpl extends EFactoryImpl implements ExtDatatypesFactory {
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static ExtDatatypesFactory init() {
		try {
			ExtDatatypesFactory theExtDatatypesFactory = (ExtDatatypesFactory)EPackage.Registry.INSTANCE.getEFactory(ExtDatatypesPackage.eNS_URI);
			if (theExtDatatypesFactory != null) {
				return theExtDatatypesFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ExtDatatypesFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtDatatypesFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ExtDatatypesPackage.YTEXT_DATATYPE: return createYTextDatatype();
			case ExtDatatypesPackage.YTEXT_AREA_DATATYPE: return createYTextAreaDatatype();
			case ExtDatatypesPackage.YNUMERIC_DATATYPE: return createYNumericDatatype();
			case ExtDatatypesPackage.YDECIMAL_DATATYPE: return createYDecimalDatatype();
			case ExtDatatypesPackage.YTABLE_DATATYPE: return createYTableDatatype();
			case ExtDatatypesPackage.YCHECK_BOX_DATATYPE: return createYCheckBoxDatatype();
			case ExtDatatypesPackage.YCOMBO_BOX_DATATYPE: return createYComboBoxDatatype();
			case ExtDatatypesPackage.YLIST_DATA_TYPE: return createYListDataType();
			case ExtDatatypesPackage.YOPTIONS_GROUP_DATA_TYPE: return createYOptionsGroupDataType();
			case ExtDatatypesPackage.YBROWSER_DATATYPE: return createYBrowserDatatype();
			case ExtDatatypesPackage.YDATE_TIME_DATATYPE: return createYDateTimeDatatype();
			case ExtDatatypesPackage.YTREE_DATATYPE: return createYTreeDatatype();
			case ExtDatatypesPackage.YPROGRESS_BAR_DATATYPE: return createYProgressBarDatatype();
			case ExtDatatypesPackage.YTAB_SHEET_DATATYPE: return createYTabSheetDatatype();
			case ExtDatatypesPackage.YMASTER_DETAIL_DATATYPE: return createYMasterDetailDatatype();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the object
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ExtDatatypesPackage.YDATE_TIME_FORMAT:
				return createYDateTimeFormatFromString(eDataType, initialValue);
			case ExtDatatypesPackage.YDATE_TIME_RESOLUTION:
				return createYDateTimeResolutionFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ExtDatatypesPackage.YDATE_TIME_FORMAT:
				return convertYDateTimeFormatToString(eDataType, instanceValue);
			case ExtDatatypesPackage.YDATE_TIME_RESOLUTION:
				return convertYDateTimeResolutionToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text datatype
	 * @generated
	 */
	public YTextDatatype createYTextDatatype() {
		YTextDatatypeImpl yTextDatatype = new YTextDatatypeImpl();
		return yTextDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y text area datatype
	 * @generated
	 */
	public YTextAreaDatatype createYTextAreaDatatype() {
		YTextAreaDatatypeImpl yTextAreaDatatype = new YTextAreaDatatypeImpl();
		return yTextAreaDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric datatype
	 * @generated
	 */
	public YNumericDatatype createYNumericDatatype() {
		YNumericDatatypeImpl yNumericDatatype = new YNumericDatatypeImpl();
		return yNumericDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal datatype
	 * @generated
	 */
	public YDecimalDatatype createYDecimalDatatype() {
		YDecimalDatatypeImpl yDecimalDatatype = new YDecimalDatatypeImpl();
		return yDecimalDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y table datatype
	 * @generated
	 */
	public YTableDatatype createYTableDatatype() {
		YTableDatatypeImpl yTableDatatype = new YTableDatatypeImpl();
		return yTableDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y check box datatype
	 * @generated
	 */
	public YCheckBoxDatatype createYCheckBoxDatatype() {
		YCheckBoxDatatypeImpl yCheckBoxDatatype = new YCheckBoxDatatypeImpl();
		return yCheckBoxDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y combo box datatype
	 * @generated
	 */
	public YComboBoxDatatype createYComboBoxDatatype() {
		YComboBoxDatatypeImpl yComboBoxDatatype = new YComboBoxDatatypeImpl();
		return yComboBoxDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y list data type
	 * @generated
	 */
	public YListDataType createYListDataType() {
		YListDataTypeImpl yListDataType = new YListDataTypeImpl();
		return yListDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y options group data type
	 * @generated
	 */
	public YOptionsGroupDataType createYOptionsGroupDataType() {
		YOptionsGroupDataTypeImpl yOptionsGroupDataType = new YOptionsGroupDataTypeImpl();
		return yOptionsGroupDataType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y browser datatype
	 * @generated
	 */
	public YBrowserDatatype createYBrowserDatatype() {
		YBrowserDatatypeImpl yBrowserDatatype = new YBrowserDatatypeImpl();
		return yBrowserDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y date time datatype
	 * @generated
	 */
	public YDateTimeDatatype createYDateTimeDatatype() {
		YDateTimeDatatypeImpl yDateTimeDatatype = new YDateTimeDatatypeImpl();
		return yDateTimeDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tree datatype
	 * @generated
	 */
	public YTreeDatatype createYTreeDatatype() {
		YTreeDatatypeImpl yTreeDatatype = new YTreeDatatypeImpl();
		return yTreeDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y progress bar datatype
	 * @generated
	 */
	public YProgressBarDatatype createYProgressBarDatatype() {
		YProgressBarDatatypeImpl yProgressBarDatatype = new YProgressBarDatatypeImpl();
		return yProgressBarDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y tab sheet datatype
	 * @generated
	 */
	public YTabSheetDatatype createYTabSheetDatatype() {
		YTabSheetDatatypeImpl yTabSheetDatatype = new YTabSheetDatatypeImpl();
		return yTabSheetDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y master detail datatype
	 * @generated
	 */
	public YMasterDetailDatatype createYMasterDetailDatatype() {
		YMasterDetailDatatypeImpl yMasterDetailDatatype = new YMasterDetailDatatypeImpl();
		return yMasterDetailDatatype;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y date time format
	 * @generated
	 */
	public YDateTimeFormat createYDateTimeFormatFromString(EDataType eDataType, String initialValue) {
		YDateTimeFormat result = YDateTimeFormat.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYDateTimeFormatToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param initialValue
	 *            the initial value
	 * @return the y date time resolution
	 * @generated
	 */
	public YDateTimeResolution createYDateTimeResolutionFromString(EDataType eDataType, String initialValue) {
		YDateTimeResolution result = YDateTimeResolution.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eDataType
	 *            the e data type
	 * @param instanceValue
	 *            the instance value
	 * @return the string
	 * @generated
	 */
	public String convertYDateTimeResolutionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the ext datatypes package
	 * @generated
	 */
	public ExtDatatypesPackage getExtDatatypesPackage() {
		return (ExtDatatypesPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ExtDatatypesPackage getPackage() {
		return ExtDatatypesPackage.eINSTANCE;
	}

} //ExtDatatypesFactoryImpl
