/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.datatypes;

import org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidationConfig;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YDecimal Datatype</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype#getPrecision <em>Precision</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage#getYDecimalDatatype()
 * @model
 * @generated
 */
public interface YDecimalDatatype extends YNumericDatatype, YRegexpValidationConfig {
	/**
	 * Returns the value of the '<em><b>Precision</b></em>' attribute.
	 * The default value is <code>"2"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precision</em>' attribute.
	 * @see #setPrecision(int)
	 * @see org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage#getYDecimalDatatype_Precision()
	 * @model default="2"
	 * @generated
	 */
	int getPrecision();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype#getPrecision <em>Precision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Precision</em>' attribute.
	 * @see #getPrecision()
	 * @generated
	 */
	void setPrecision(int value);

} // YDecimalDatatype
