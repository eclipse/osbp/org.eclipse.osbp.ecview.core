/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.extension.model.extension.listener;

import org.eclipse.osbp.ecview.core.extension.model.extension.YButton;

// TODO: check reference
/**
 * The listener interface for receiving YButtonClick events. The class that is
 * interested in processing a YButtonClick event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addYButtonClickListener</code> method. When
 * the YButtonClick event occurs, that object's appropriate
 * method is invoked.
 *
 * see YButtonClickEvent
 */
public interface YButtonClickListener {

	/**
	 * Is called if the button was clicked.
	 *
	 * @param yButton
	 *            the y button
	 */
	void clicked(YButton yButton);

}
