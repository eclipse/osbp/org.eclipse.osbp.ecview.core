/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.util.IBindableFeaturesItemProvider;
import org.eclipse.osbp.ecview.core.common.model.datatypes.DatatypesFactory;

/**
 * This is the item provider adapter for a
 * {@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 */
public class YEmbeddableItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource,
		IBindableFeaturesItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public YEmbeddableItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addCssClassPropertyDescriptor(object);
			addCssIDPropertyDescriptor(object);
			addInitialVisiblePropertyDescriptor(object);
			addVisiblePropertyDescriptor(object);
			addAuthorizationGroupPropertyDescriptor(object);
			addAuthorizationIdPropertyDescriptor(object);
			addMementoEnabledPropertyDescriptor(object);
			addMementoIdPropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
			addLabelI18nKeyPropertyDescriptor(object);
			addLastContextClickPropertyDescriptor(object);
			addReadonlyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_id_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_name_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YTaggable_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YTaggable_tags_feature", "_UI_YTaggable_type"),
				 CoreModelPackage.Literals.YTAGGABLE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Css Class feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addCssClassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCssAble_cssClass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCssAble_cssClass_feature", "_UI_YCssAble_type"),
				 CoreModelPackage.Literals.YCSS_ABLE__CSS_CLASS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Css ID feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addCssIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCssAble_cssID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCssAble_cssID_feature", "_UI_YCssAble_type"),
				 CoreModelPackage.Literals.YCSS_ABLE__CSS_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Initial Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addInitialVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YVisibleable_initialVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YVisibleable_initialVisible_feature", "_UI_YVisibleable_type"),
				 CoreModelPackage.Literals.YVISIBLEABLE__INITIAL_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YVisibleable_visible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YVisibleable_visible_feature", "_UI_YVisibleable_type"),
				 CoreModelPackage.Literals.YVISIBLEABLE__VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Authorization Group feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAuthorizationGroupPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YAuthorizationable_authorizationGroup_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YAuthorizationable_authorizationGroup_feature", "_UI_YAuthorizationable_type"),
				 CoreModelPackage.Literals.YAUTHORIZATIONABLE__AUTHORIZATION_GROUP,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Authorization Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAuthorizationIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YAuthorizationable_authorizationId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YAuthorizationable_authorizationId_feature", "_UI_YAuthorizationable_type"),
				 CoreModelPackage.Literals.YAUTHORIZATIONABLE__AUTHORIZATION_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Memento Enabled feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMementoEnabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEmbeddable_mementoEnabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEmbeddable_mementoEnabled_feature", "_UI_YEmbeddable_type"),
				 CoreModelPackage.Literals.YEMBEDDABLE__MEMENTO_ENABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Memento Id feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addMementoIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEmbeddable_mementoId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEmbeddable_mementoId_feature", "_UI_YEmbeddable_type"),
				 CoreModelPackage.Literals.YEMBEDDABLE__MEMENTO_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEmbeddable_label_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEmbeddable_label_feature", "_UI_YEmbeddable_type"),
				 CoreModelPackage.Literals.YEMBEDDABLE__LABEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label I1 8n Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLabelI18nKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEmbeddable_labelI18nKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEmbeddable_labelI18nKey_feature", "_UI_YEmbeddable_type"),
				 CoreModelPackage.Literals.YEMBEDDABLE__LABEL_I1_8N_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Context Click feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastContextClickPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEmbeddable_lastContextClick_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEmbeddable_lastContextClick_feature", "_UI_YEmbeddable_type"),
				 CoreModelPackage.Literals.YEMBEDDABLE__LAST_CONTEXT_CLICK,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Readonly feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReadonlyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEmbeddable_readonly_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEmbeddable_readonly_feature", "_UI_YEmbeddable_type"),
				 CoreModelPackage.Literals.YEMBEDDABLE__READONLY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YELEMENT__PROPERTIES);
			childrenFeatures.add(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES);
			childrenFeatures.add(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATADESCRIPTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YEmbeddable)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YEmbeddable_type") :
			getString("_UI_YEmbeddable_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YEmbeddable.class)) {
			case CoreModelPackage.YEMBEDDABLE__TAGS:
			case CoreModelPackage.YEMBEDDABLE__ID:
			case CoreModelPackage.YEMBEDDABLE__NAME:
			case CoreModelPackage.YEMBEDDABLE__CSS_CLASS:
			case CoreModelPackage.YEMBEDDABLE__CSS_ID:
			case CoreModelPackage.YEMBEDDABLE__INITIAL_VISIBLE:
			case CoreModelPackage.YEMBEDDABLE__VISIBLE:
			case CoreModelPackage.YEMBEDDABLE__AUTHORIZATION_GROUP:
			case CoreModelPackage.YEMBEDDABLE__AUTHORIZATION_ID:
			case CoreModelPackage.YEMBEDDABLE__MEMENTO_ENABLED:
			case CoreModelPackage.YEMBEDDABLE__MEMENTO_ID:
			case CoreModelPackage.YEMBEDDABLE__LABEL:
			case CoreModelPackage.YEMBEDDABLE__LABEL_I1_8N_KEY:
			case CoreModelPackage.YEMBEDDABLE__READONLY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case CoreModelPackage.YEMBEDDABLE__PROPERTIES:
			case CoreModelPackage.YEMBEDDABLE__ORPHAN_DATATYPES:
			case CoreModelPackage.YEMBEDDABLE__ORPHAN_DATADESCRIPTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YELEMENT__PROPERTIES,
				 CoreModelFactory.eINSTANCE.create(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 CoreModelFactory.eINSTANCE.createYDtWrapper()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATADESCRIPTIONS,
				 DatatypesFactory.eINSTANCE.createYDatadescription()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

	@Override
	public List<EStructuralFeature> getBindableFeatures(YElement element) {
		List<EStructuralFeature> features = doGetBindableFeatures(element);
		return features;
	}

	/**
	 * Returns all attributes and features ordered by their name.
	 * 
	 * @param element
	 * @return
	 */
	protected List<EStructuralFeature> doGetBindableFeatures(YElement element) {
		EClass eClass = element.eClass();
		List<EStructuralFeature> features = new ArrayList<EStructuralFeature>();
		features.addAll(eClass.getEAllAttributes());
		features.addAll(eClass.getEAllReferences());

		Collections.sort(features, new Comparator<EStructuralFeature>() {
			@Override
			public int compare(EStructuralFeature o1, EStructuralFeature o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		return features;
	}

	@Override
	public Class<?> getFeatureType(YElement element, EStructuralFeature feature) {
		// a hardcoded default implementation
		// override to change.
		if (feature.getName().equals("selection")
				|| feature.getName().equals("multiSelection")
				|| feature.getName().equals("collection")) {
			EClass eClass = element.eClass();
			EStructuralFeature typeFeature = eClass
					.getEStructuralFeature("type");
			if (typeFeature != null) {
				if (typeFeature instanceof EAttribute) {
					EAttribute typeAttribute = (EAttribute) typeFeature;
					if (typeAttribute.getEAttributeType() == EcorePackage.Literals.EJAVA_CLASS) {
						return (Class<?>) element.eGet(typeFeature);
					}
				}
			}
		}
		return feature.getEType().getInstanceClass();
	}
}
