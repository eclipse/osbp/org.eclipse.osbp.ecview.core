/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.common.model.core.YField} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YFieldItemProvider extends YEmbeddableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFieldItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addInitialEditablePropertyDescriptor(object);
			addEditablePropertyDescriptor(object);
			addInitialEnabledPropertyDescriptor(object);
			addEnabledPropertyDescriptor(object);
			addLayoutIdxPropertyDescriptor(object);
			addLayoutColumnsPropertyDescriptor(object);
			addTabIndexPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Initial Editable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitialEditablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEditable_initialEditable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEditable_initialEditable_feature", "_UI_YEditable_type"),
				 CoreModelPackage.Literals.YEDITABLE__INITIAL_EDITABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEditablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEditable_editable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEditable_editable_feature", "_UI_YEditable_type"),
				 CoreModelPackage.Literals.YEDITABLE__EDITABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Initial Enabled feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitialEnabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEnable_initialEnabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEnable_initialEnabled_feature", "_UI_YEnable_type"),
				 CoreModelPackage.Literals.YENABLE__INITIAL_ENABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Enabled feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YEnable_enabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YEnable_enabled_feature", "_UI_YEnable_type"),
				 CoreModelPackage.Literals.YENABLE__ENABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Layout Idx feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLayoutIdxPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YFocusable_layoutIdx_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YFocusable_layoutIdx_feature", "_UI_YFocusable_type"),
				 CoreModelPackage.Literals.YFOCUSABLE__LAYOUT_IDX,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Layout Columns feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLayoutColumnsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YFocusable_layoutColumns_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YFocusable_layoutColumns_feature", "_UI_YFocusable_type"),
				 CoreModelPackage.Literals.YFOCUSABLE__LAYOUT_COLUMNS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tab Index feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTabIndexPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YFocusable_tabIndex_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YFocusable_tabIndex_feature", "_UI_YFocusable_type"),
				 CoreModelPackage.Literals.YFOCUSABLE__TAB_INDEX,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YFIELD__VALIDATORS);
			childrenFeatures.add(CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS);
			childrenFeatures.add(CoreModelPackage.Literals.YFIELD__CONVERTER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YField.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YField"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YField)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YField_type") :
			getString("_UI_YField_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YField.class)) {
			case CoreModelPackage.YFIELD__INITIAL_EDITABLE:
			case CoreModelPackage.YFIELD__EDITABLE:
			case CoreModelPackage.YFIELD__INITIAL_ENABLED:
			case CoreModelPackage.YFIELD__ENABLED:
			case CoreModelPackage.YFIELD__LAYOUT_IDX:
			case CoreModelPackage.YFIELD__LAYOUT_COLUMNS:
			case CoreModelPackage.YFIELD__TAB_INDEX:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case CoreModelPackage.YFIELD__VALIDATORS:
			case CoreModelPackage.YFIELD__INTERNAL_VALIDATORS:
			case CoreModelPackage.YFIELD__CONVERTER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__VALIDATORS,
				 ValidationFactory.eINSTANCE.createYMinLengthValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__VALIDATORS,
				 ValidationFactory.eINSTANCE.createYMaxLengthValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__VALIDATORS,
				 ValidationFactory.eINSTANCE.createYRegexpValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__VALIDATORS,
				 ValidationFactory.eINSTANCE.createYClassDelegateValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__VALIDATORS,
				 ValidationFactory.eINSTANCE.createYBeanValidationValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__VALIDATORS,
				 ValidationFactory.eINSTANCE.createYUniqueAttributeValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS,
				 ValidationFactory.eINSTANCE.createYMinLengthValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS,
				 ValidationFactory.eINSTANCE.createYMaxLengthValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS,
				 ValidationFactory.eINSTANCE.createYRegexpValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS,
				 ValidationFactory.eINSTANCE.createYClassDelegateValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS,
				 ValidationFactory.eINSTANCE.createYBeanValidationValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS,
				 ValidationFactory.eINSTANCE.createYUniqueAttributeValidator()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 CoreModelFactory.eINSTANCE.createYDelegateConverter()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
