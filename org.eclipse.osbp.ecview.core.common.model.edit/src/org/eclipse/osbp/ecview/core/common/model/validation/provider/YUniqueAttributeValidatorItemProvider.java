/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.model.validation.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;

import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidator;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidator} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YUniqueAttributeValidatorItemProvider extends YValidatorItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YUniqueAttributeValidatorItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addErrorCodePropertyDescriptor(object);
			addDefaultErrorMessagePropertyDescriptor(object);
			addValTypePropertyDescriptor(object);
			addValTypeFullyQualifiedNamePropertyDescriptor(object);
			addPropertyPathPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Error Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addErrorCodePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YValidationConfig_errorCode_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YValidationConfig_errorCode_feature", "_UI_YValidationConfig_type"),
				 ValidationPackage.Literals.YVALIDATION_CONFIG__ERROR_CODE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Default Error Message feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDefaultErrorMessagePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YValidationConfig_defaultErrorMessage_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YValidationConfig_defaultErrorMessage_feature", "_UI_YValidationConfig_type"),
				 ValidationPackage.Literals.YVALIDATION_CONFIG__DEFAULT_ERROR_MESSAGE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Val Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YUniqueAttributeValidatorConfig_valType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YUniqueAttributeValidatorConfig_valType_feature", "_UI_YUniqueAttributeValidatorConfig_type"),
				 ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Val Type Fully Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValTypeFullyQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YUniqueAttributeValidatorConfig_valTypeFullyQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YUniqueAttributeValidatorConfig_valTypeFullyQualifiedName_feature", "_UI_YUniqueAttributeValidatorConfig_type"),
				 ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE_FULLY_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Property Path feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyPathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YUniqueAttributeValidatorConfig_propertyPath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YUniqueAttributeValidatorConfig_propertyPath_feature", "_UI_YUniqueAttributeValidatorConfig_type"),
				 ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__PROPERTY_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YUniqueAttributeValidator.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YUniqueAttributeValidator"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YUniqueAttributeValidator)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YUniqueAttributeValidator_type") :
			getString("_UI_YUniqueAttributeValidator_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YUniqueAttributeValidator.class)) {
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__ERROR_CODE:
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__DEFAULT_ERROR_MESSAGE:
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE:
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__VAL_TYPE_FULLY_QUALIFIED_NAME:
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__PROPERTY_PATH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ValidationPackage.YUNIQUE_ATTRIBUTE_VALIDATOR__CONTAINER_VALUE_BINDING_ENDPOINT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYBeanValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYDetailValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYECViewModelValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYVisibilityProcessorValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 BindingFactory.eINSTANCE.createYNoOpValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYContextValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYBeanSlotValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYEmbeddableValueEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYEmbeddableSelectionEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__CONTAINER_VALUE_BINDING_ENDPOINT,
				 CoreModelFactory.eINSTANCE.createYActivatedEndpoint()));
	}

}
