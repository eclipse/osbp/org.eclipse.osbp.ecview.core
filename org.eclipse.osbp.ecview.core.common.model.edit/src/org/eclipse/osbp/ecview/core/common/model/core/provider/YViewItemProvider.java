/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.provider;


import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationFactory;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.common.model.core.YView} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YViewItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YViewItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addCssClassPropertyDescriptor(object);
			addCssIDPropertyDescriptor(object);
			addMarginPropertyDescriptor(object);
			addViewNamePropertyDescriptor(object);
			addVersionPropertyDescriptor(object);
			addDeviceTypePropertyDescriptor(object);
			addContentAlignmentPropertyDescriptor(object);
			addTransientVisibilityProcessorsPropertyDescriptor(object);
			addSharedStateGroupPropertyDescriptor(object);
			addCategoryPropertyDescriptor(object);
			addInitialFocusPropertyDescriptor(object);
			addCurrentFocusPropertyDescriptor(object);
			addDisabledElementsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_id_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_name_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YTaggable_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YTaggable_tags_feature", "_UI_YTaggable_type"),
				 CoreModelPackage.Literals.YTAGGABLE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Css Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCssClassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCssAble_cssClass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCssAble_cssClass_feature", "_UI_YCssAble_type"),
				 CoreModelPackage.Literals.YCSS_ABLE__CSS_CLASS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Css ID feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCssIDPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCssAble_cssID_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCssAble_cssID_feature", "_UI_YCssAble_type"),
				 CoreModelPackage.Literals.YCSS_ABLE__CSS_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Margin feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMarginPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMarginable_margin_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMarginable_margin_feature", "_UI_YMarginable_type"),
				 CoreModelPackage.Literals.YMARGINABLE__MARGIN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the View Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addViewNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_viewName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_viewName_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__VIEW_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_version_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_version_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Device Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeviceTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_deviceType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_deviceType_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__DEVICE_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Content Alignment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContentAlignmentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_contentAlignment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_contentAlignment_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__CONTENT_ALIGNMENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Transient Visibility Processors feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTransientVisibilityProcessorsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_transientVisibilityProcessors_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_transientVisibilityProcessors_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__TRANSIENT_VISIBILITY_PROCESSORS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Shared State Group feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSharedStateGroupPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_sharedStateGroup_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_sharedStateGroup_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__SHARED_STATE_GROUP,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Category feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCategoryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_category_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_category_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__CATEGORY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Initial Focus feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitialFocusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_initialFocus_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_initialFocus_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__INITIAL_FOCUS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Current Focus feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCurrentFocusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_currentFocus_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_currentFocus_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__CURRENT_FOCUS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Disabled Elements feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisabledElementsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YView_disabledElements_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YView_disabledElements_feature", "_UI_YView_type"),
				 CoreModelPackage.Literals.YVIEW__DISABLED_ELEMENTS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YELEMENT__PROPERTIES);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__CONTENT);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__BINDING_SET);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__BEAN_SLOTS);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__COMMAND_SET);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__DIALOGS);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__VISIBILITY_PROCESSORS);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__EXPOSED_ACTIONS);
			childrenFeatures.add(CoreModelPackage.Literals.YVIEW__AUTHORIZATION_STORE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YView.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YView"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YView)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YView_type") :
			getString("_UI_YView_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YView.class)) {
			case CoreModelPackage.YVIEW__TAGS:
			case CoreModelPackage.YVIEW__ID:
			case CoreModelPackage.YVIEW__NAME:
			case CoreModelPackage.YVIEW__CSS_CLASS:
			case CoreModelPackage.YVIEW__CSS_ID:
			case CoreModelPackage.YVIEW__MARGIN:
			case CoreModelPackage.YVIEW__VIEW_NAME:
			case CoreModelPackage.YVIEW__VERSION:
			case CoreModelPackage.YVIEW__DEVICE_TYPE:
			case CoreModelPackage.YVIEW__CONTENT_ALIGNMENT:
			case CoreModelPackage.YVIEW__SHARED_STATE_GROUP:
			case CoreModelPackage.YVIEW__CATEGORY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case CoreModelPackage.YVIEW__PROPERTIES:
			case CoreModelPackage.YVIEW__CONTENT:
			case CoreModelPackage.YVIEW__BINDING_SET:
			case CoreModelPackage.YVIEW__BEAN_SLOTS:
			case CoreModelPackage.YVIEW__COMMAND_SET:
			case CoreModelPackage.YVIEW__DIALOGS:
			case CoreModelPackage.YVIEW__VISIBILITY_PROCESSORS:
			case CoreModelPackage.YVIEW__EXPOSED_ACTIONS:
			case CoreModelPackage.YVIEW__AUTHORIZATION_STORE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YELEMENT__PROPERTIES,
				 CoreModelFactory.eINSTANCE.create(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__CONTENT,
				 CoreModelFactory.eINSTANCE.createYLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__CONTENT,
				 CoreModelFactory.eINSTANCE.createYHelperLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__CONTENT,
				 CoreModelFactory.eINSTANCE.createYField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__CONTENT,
				 CoreModelFactory.eINSTANCE.createYAction()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__BINDING_SET,
				 BindingFactory.eINSTANCE.createYBindingSet()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__BEAN_SLOTS,
				 CoreModelFactory.eINSTANCE.createYBeanSlot()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__BEAN_SLOTS,
				 CoreModelFactory.eINSTANCE.createYDetailBeanSlot()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__COMMAND_SET,
				 CoreModelFactory.eINSTANCE.createYCommandSet()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__DIALOGS,
				 CoreModelFactory.eINSTANCE.createYDialog()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__VISIBILITY_PROCESSORS,
				 VisibilityFactory.eINSTANCE.createYVisibilityProcessor()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__EXPOSED_ACTIONS,
				 CoreModelFactory.eINSTANCE.createYExposedAction()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YVIEW__AUTHORIZATION_STORE,
				 AuthorizationFactory.eINSTANCE.createYAuthorizationStore()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
