/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.common.model.core.provider;

import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.provider.YValueBindingEndpointItemProvider;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class YBeanSlotValueBindingEndpointItemProvider extends
		YValueBindingEndpointItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public YBeanSlotValueBindingEndpointItemProvider(
			AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAttributePathPropertyDescriptor(object);
			addBeanSlotPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Attribute Path feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAttributePathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanSlotValueBindingEndpoint_attributePath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanSlotValueBindingEndpoint_attributePath_feature", "_UI_YBeanSlotValueBindingEndpoint_type"),
				 CoreModelPackage.Literals.YBEAN_SLOT_VALUE_BINDING_ENDPOINT__ATTRIBUTE_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Bean Slot feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addBeanSlotPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanSlotValueBindingEndpoint_beanSlot_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanSlotValueBindingEndpoint_beanSlot_feature", "_UI_YBeanSlotValueBindingEndpoint_type"),
				 CoreModelPackage.Literals.YBEAN_SLOT_VALUE_BINDING_ENDPOINT__BEAN_SLOT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns YBeanSlotValueBindingEndpoint.gif. <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator()
						.getImage("full/obj16/YBeanSlotValueBindingEndpoint.gif"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 */
	@Override
	public String getText(Object object) {
		YBeanSlotValueBindingEndpoint ep = (YBeanSlotValueBindingEndpoint) object;
		return "slot: " + ep.getBeanSlot().getName() + "/"
				+ ep.getAttributePath() + getTextPrefix((YValueBindingEndpoint) object);
	}
	
	@Override
	public EObject getBoundElement(YBindingEndpoint ep) {
		YBeanSlotValueBindingEndpoint temp = (YBeanSlotValueBindingEndpoint) ep;
		return temp.getBeanSlot();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YBeanSlotValueBindingEndpoint.class)) {
			case CoreModelPackage.YBEAN_SLOT_VALUE_BINDING_ENDPOINT__ATTRIBUTE_PATH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
