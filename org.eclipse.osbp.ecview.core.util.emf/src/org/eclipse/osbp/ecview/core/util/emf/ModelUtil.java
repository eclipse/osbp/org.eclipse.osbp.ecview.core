/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.core.util.emf;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.core.databinding.Binding;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableValueBindingEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IValueBindingEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet;
import org.eclipse.osbp.ecview.core.common.model.binding.YEnumListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableValueEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YValueBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleService;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.core.common.uri.URIHelper;

/**
 * The Class ModelUtil.
 */
public class ModelUtil {

	/**
	 * Returns the editpart for the given element. If no editpart was found, a
	 * new instance will be created.
	 * 
	 * @param context
	 *
	 * @param <A>
	 *            the generic type
	 * @param yElement
	 *            the y element
	 * @return editpart - the controller class of the element
	 */
	public static <A extends IElementEditpart> A getEditpart(
			ViewContext context, YElement yElement) {
		return DelegatingEditPartManager.getInstance().getEditpart(context,
				yElement);
	}

	/**
	 * Returns the editpart for the given element. If no existing could be
	 * found, a new one will be created.
	 *
	 * @param <A>
	 *            the generic type
	 * @param yElement
	 *            the y element
	 * @return editpart - the controller class of the element
	 */
	public static <A extends IElementEditpart> A findEditpart(YElement yElement) {
		return DelegatingEditPartManager.getInstance().findEditpart(yElement);
	}

	public static <A extends IElementEditpart> A getEditpart(
			IViewContext viewContext, YElement yElement) {
		return DelegatingEditPartManager.getInstance().getEditpart(viewContext,
				yElement);
	}

	/**
	 * Returns the presentation for the given embeddable.
	 *
	 * @param yElement
	 *            the y element
	 * @return Presentation - the class that renders the UI for the element
	 */
	public static IWidgetPresentation<?> getPresentation(YEmbeddable yElement) {
		IEmbeddableEditpart editpart = DelegatingEditPartManager.getInstance()
				.findEditpart(yElement);
		return editpart != null ? editpart.getPresentation() : null;
	}

	/**
	 * Returns all bindings for the given element.
	 *
	 * @param yElement
	 *            the y element
	 * @return the bindings
	 */
	public static List<YBinding> getBindings(YElement yElement) {
		if (yElement == null) {
			return Collections.emptyList();
		}

		YView yView = getView(yElement);
		if (yView == null) {
			return Collections.emptyList();
		}

		YBindingSet yBindingSet = yView.getBindingSet();
		if (yBindingSet == null) {
			return Collections.emptyList();
		}

		List<YBinding> result = new ArrayList<YBinding>();
		for (YBinding yBinding : yBindingSet.getBindings()) {
			if (yBinding.isBindsElement(yElement)) {
				result.add(yBinding);
			}
		}

		for (YBinding yBinding : yBindingSet.getTransientBindings()) {
			if (yBinding.isBindsElement(yElement)) {
				result.add(yBinding);
			}
		}

		return result;
	}

	/**
	 * Returns the value binding for the given bindable.
	 *
	 * @param bindable
	 *            the bindable
	 * @return the value binding
	 */
	public static Binding getValueBinding(YValueBindable bindable) {
		if (bindable == null || bindable.getValueBindingEndpoint() == null) {
			return null;
		}
		IValueBindingEditpart bindingEditpart = getValueBindingEditpart(bindable);
		if(bindingEditpart == null) {
			return null;
		}
		Binding binding = bindingEditpart.getBinding();
		return binding;
	}

	/**
	 * Returns the binding editpart for the given bindable.
	 *
	 * @param bindable
	 *            the bindable
	 * @return the value binding editpart
	 */
	public static IValueBindingEditpart getValueBindingEditpart(
			YValueBindable bindable) {
		if (bindable == null || bindable.getValueBindingEndpoint() == null) {
			return null;
		}

		YBinding yBinding = bindable.getValueBindingEndpoint().getBinding();
		IValueBindingEditpart bindingEditpart = findEditpart(yBinding);
		return bindingEditpart;
	}

	/**
	 * Returns the value endpoint editpart for the given bindable.
	 *
	 * @param bindable
	 *            the bindable
	 * @return the value endpoint editpart
	 */
	public static IEmbeddableValueBindingEndpointEditpart getValueEndpointEditpart(
			YValueBindable bindable) {
		if (bindable == null || bindable.getValueBindingEndpoint() == null) {
			return null;
		}

		YEmbeddableValueEndpoint yBinding = bindable.getValueBindingEndpoint();
		return getValueEndpointEditpart(yBinding);
	}

	/**
	 * Returns the value endpoint editpart for the given endpoint.
	 *
	 * @param endpoint
	 *            the endpoint
	 * @return the value endpoint editpart
	 */
	public static IEmbeddableValueBindingEndpointEditpart getValueEndpointEditpart(
			YEmbeddableValueEndpoint endpoint) {
		IEmbeddableValueBindingEndpointEditpart bindingEditpart = DelegatingEditPartManager
				.getInstance().findEditpart(endpoint);
		return bindingEditpart;
	}

	/**
	 * Returns all bindings that are attached to the UI widget. These bindings
	 * connect the ECView model to the widgets. For bindings from domain model
	 * to ECView model see {@link #getModelBindingEditparts(YEmbeddable)}. No
	 * bindingEditparts are available for that kind of binding.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 * @return the UI bindings
	 */
	public static Set<Binding> getUIBindings(YEmbeddable yEmbeddable) {
		IWidgetPresentation<?> presentation = getPresentation(yEmbeddable);
		return presentation.getUIBindings();
	}

	/**
	 * Returns all bindings that are wire the domain model with the ECView
	 * model. The ECView model is bound to the UI widgets by the UI bindings.
	 * See {@link #getUIBindings(YEmbeddable)}.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 * @return the model binding editparts
	 */
	@SuppressWarnings("unchecked")
	public static List<IValueBindingEditpart> getModelBindingEditparts(
			YEmbeddable yEmbeddable) {
		YBindingSet bindingset = yEmbeddable.getView().getBindingSet();
		if (bindingset == null) {
			return Collections.emptyList();
		}

		IBindingSetEditpart bindingSetEditpart = findEditpart(bindingset);
		return (List<IValueBindingEditpart>) (bindingSetEditpart != null ? bindingSetEditpart
				.findBindings(yEmbeddable) : Collections.emptyList());
	}

	/**
	 * Returns the widget that is associated with the given embeddable.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 * @return the widget
	 */
	public static Object getWidget(YEmbeddable yEmbeddable) {
		IWidgetPresentation<?> presentation = getPresentation(yEmbeddable);
		return presentation.getWidget();
	}

	/**
	 * Returns the view context for the given embeddable.
	 *
	 * @param yView
	 *            the y view
	 * @return the view context
	 */
	public static IViewContext getViewContext(YView yView) {
		IViewEditpart viewEditpart = findViewEditpart(yView);
		return viewEditpart != null ? viewEditpart.getContext() : null;
	}

	/**
	 * Returns the view editpart for the given yView.
	 *
	 * @param yView
	 *            the y view
	 * @return the view editpart
	 */
	public static IViewEditpart findViewEditpart(YView yView) {
		return findEditpart(yView);
	}

	/**
	 * Returns the view context for the given embeddable.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 * @return the view context
	 */
	public static IViewContext getViewContext(YEmbeddable yEmbeddable) {
		IViewEditpart viewEditpart = findViewEditpart(yEmbeddable);
		return viewEditpart != null ? viewEditpart.getContext() : null;
	}

	/**
	 * Returns the view editpart for the given embeddable.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 * @return the view editpart
	 */
	public static IViewEditpart findViewEditpart(YEmbeddable yEmbeddable) {
		YView view = getView(yEmbeddable);
		return (IViewEditpart) (view != null ? findEditpart(view) : null);
	}

	/**
	 * Returns the view for the given element.
	 *
	 * @param yElement
	 *            the y element
	 * @return the view
	 */
	public static YView getView(YElement yElement) {

		if (yElement == null) {
			return null;
		}

		if (yElement instanceof YView) {
			return (YView) yElement;
		} else {
			EObject container = yElement.eContainer();
			while (container != null) {
				if (container instanceof YView) {
					return (YView) container;
				}
				container = container.eContainer();
			}
		}
		return null;
	}

	/**
	 * Returns the view context for the given element.
	 *
	 * @param yElement
	 *            the y element
	 * @return the view context
	 */
	public static IViewContext getViewContext(YElement yElement) {

		if (yElement == null) {
			return null;
		}

		if (yElement instanceof YView) {
			return getViewContext((YView) yElement);
		} else {
			EObject container = yElement.eContainer();
			while (container != null) {
				if (container instanceof YView) {
					return getViewContext((YView) container);
				}
				container = container.eContainer();
			}
		}
		return null;
	}

	/**
	 * Returns the {@link ILifecycleService} registered with the context.
	 *
	 * @param context
	 *            the context
	 * @return the lifecylce service
	 */
	public static ILifecycleService getLifecylceService(IViewContext context) {
		return context.getService(ILifecycleService.class.getName());
	}

	/**
	 * Returns the {@link IECViewBindingManager} registered with the context.
	 *
	 * @param context
	 *            the context
	 * @return the binding manager
	 */
	public static IECViewBindingManager getBindingManager(IViewContext context) {
		return context.getService(IECViewBindingManager.class.getName());
	}

	/**
	 * Returns the URI for the given bean slot. The URI may be used with the
	 * {@link URIHelper} to access the bean slot.
	 *
	 * @param yBeanSlot
	 *            the y bean slot
	 * @return the uri
	 */
	public static URI getURI(YBeanSlot yBeanSlot) {
		EObject container = yBeanSlot.eContainer();
		URI uri = null;
		if (container instanceof YView) {
			uri = URIHelper.view().bean(yBeanSlot.getName()).toURI();
		} else {
			throw new RuntimeException(container + " is not a valid type!");
		}

		return uri;
	}

	/**
	 * Returns the URI for the given bean slot endpoint. The URI may be used
	 * with the {@link URIHelper} to access the bean the URI points to.
	 *
	 * @param yBeanSlotEndpoint
	 *            the y bean slot endpoint
	 * @return the uri
	 */
	public static URI getURI(YBeanSlotValueBindingEndpoint yBeanSlotEndpoint) {

		YBeanSlot beanSlot = yBeanSlotEndpoint.getBeanSlot();
		if (beanSlot == null) {
			throw new IllegalArgumentException("BeanSlot mus not be null!");
		}
		EObject container = beanSlot.eContainer();
		URI uri = null;
		if (container instanceof YView) {
			uri = URIHelper.view().bean(beanSlot.getName())
					.fragment(yBeanSlotEndpoint.getAttributePath()).toURI();
		} else {
			throw new RuntimeException(container + " is not a valid type!");
		}

		return uri;
	}

	/**
	 * Creates and {@link YEnumListBindingEndpoint} for the given enum.
	 *
	 * @param xEnum
	 *            the x enum
	 * @return the enum list binding
	 */
	public static YEnumListBindingEndpoint getEnumListBinding(
			Class<? extends Enum<?>> xEnum) {
		YEnumListBindingEndpoint yEndpoint = BindingFactory.eINSTANCE
				.createYEnumListBindingEndpoint();
		yEndpoint.setEnum(xEnum);
		return yEndpoint;
	}

}
