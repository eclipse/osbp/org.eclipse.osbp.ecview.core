/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YMasterDetailItemProvider extends YInputItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMasterDetailItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCollectionBindingEndpointPropertyDescriptor(object);
			addSelectionBindingEndpointPropertyDescriptor(object);
			addDatatypePropertyDescriptor(object);
			addDatadescriptionPropertyDescriptor(object);
			addSelectionPropertyDescriptor(object);
			addCollectionPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addEmfNsURIPropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Collection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCollectionBindable_collectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCollectionBindable_collectionBindingEndpoint_feature", "_UI_YCollectionBindable_type"),
				 CoreModelPackage.Literals.YCOLLECTION_BINDABLE__COLLECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSelectionBindable_selectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSelectionBindable_selectionBindingEndpoint_feature", "_UI_YSelectionBindable_type"),
				 CoreModelPackage.Literals.YSELECTION_BINDABLE__SELECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datatype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatatypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMasterDetail_datatype_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMasterDetail_datatype_feature", "_UI_YMasterDetail_type"),
				 ExtensionModelPackage.Literals.YMASTER_DETAIL__DATATYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datadescription feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatadescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMasterDetail_datadescription_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMasterDetail_datadescription_feature", "_UI_YMasterDetail_type"),
				 ExtensionModelPackage.Literals.YMASTER_DETAIL__DATADESCRIPTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMasterDetail_selection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMasterDetail_selection_feature", "_UI_YMasterDetail_type"),
				 ExtensionModelPackage.Literals.YMASTER_DETAIL__SELECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Collection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMasterDetail_collection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMasterDetail_collection_feature", "_UI_YMasterDetail_type"),
				 ExtensionModelPackage.Literals.YMASTER_DETAIL__COLLECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMasterDetail_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMasterDetail_type_feature", "_UI_YMasterDetail_type"),
				 ExtensionModelPackage.Literals.YMASTER_DETAIL__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Emf Ns URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEmfNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMasterDetail_emfNsURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMasterDetail_emfNsURI_feature", "_UI_YMasterDetail_type"),
				 ExtensionModelPackage.Literals.YMASTER_DETAIL__EMF_NS_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMasterDetail_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMasterDetail_typeQualifiedName_feature", "_UI_YMasterDetail_type"),
				 ExtensionModelPackage.Literals.YMASTER_DETAIL__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT);
			childrenFeatures.add(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YMasterDetail.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YMasterDetail"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YMasterDetail)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YMasterDetail_type") :
			getString("_UI_YMasterDetail_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YMasterDetail.class)) {
			case ExtensionModelPackage.YMASTER_DETAIL__SELECTION:
			case ExtensionModelPackage.YMASTER_DETAIL__COLLECTION:
			case ExtensionModelPackage.YMASTER_DETAIL__TYPE:
			case ExtensionModelPackage.YMASTER_DETAIL__EMF_NS_URI:
			case ExtensionModelPackage.YMASTER_DETAIL__TYPE_QUALIFIED_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ExtensionModelPackage.YMASTER_DETAIL__MASTER_ELEMENT:
			case ExtensionModelPackage.YMASTER_DETAIL__DETAIL_ELEMENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYGridLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYHorizontalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYVerticalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTable()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTree()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYList()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYLabel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYImage()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTextField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYBeanReferenceField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYCheckBox()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYBrowser()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYDateTime()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYButton()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSlider()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYToggleButton()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYProgressBar()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYMasterDetail()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYFormLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTextSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYBooleanSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYNumericSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYReferenceSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYPanel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSplitPanel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumList()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYCssLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYAbsoluteLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSuggestTextField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYPasswordField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYFilteringComponent()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYKanban()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYHelperLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYAction()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYGridLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYHorizontalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYVerticalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTable()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTree()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYList()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYLabel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYImage()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTextField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYBeanReferenceField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYCheckBox()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYBrowser()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYDateTime()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYButton()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSlider()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYToggleButton()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYProgressBar()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYMasterDetail()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYFormLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYTextSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYBooleanSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYNumericSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYReferenceSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYPanel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSplitPanel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumList()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYCssLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYAbsoluteLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYSuggestTextField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYPasswordField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYFilteringComponent()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 ExtensionModelFactory.eINSTANCE.createYKanban()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYHelperLayout()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYField()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT,
				 CoreModelFactory.eINSTANCE.createYAction()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS ||
			childFeature == ExtensionModelPackage.Literals.YMASTER_DETAIL__MASTER_ELEMENT ||
			childFeature == ExtensionModelPackage.Literals.YMASTER_DETAIL__DETAIL_ELEMENT;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
