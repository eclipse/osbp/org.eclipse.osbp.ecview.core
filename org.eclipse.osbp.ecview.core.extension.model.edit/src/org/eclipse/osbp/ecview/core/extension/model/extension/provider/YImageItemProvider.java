/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension.provider;


import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.provider.YFieldItemProvider;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YImage;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.extension.model.extension.YImage} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YImageItemProvider extends YFieldItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YImageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValueBindingEndpointPropertyDescriptor(object);
			addDatadescriptionPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addResourcePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Value Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValueBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YValueBindable_valueBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YValueBindable_valueBindingEndpoint_feature", "_UI_YValueBindable_type"),
				 CoreModelPackage.Literals.YVALUE_BINDABLE__VALUE_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datadescription feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatadescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YImage_datadescription_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YImage_datadescription_feature", "_UI_YImage_type"),
				 ExtensionModelPackage.Literals.YIMAGE__DATADESCRIPTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YImage_value_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YImage_value_feature", "_UI_YImage_type"),
				 ExtensionModelPackage.Literals.YIMAGE__VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Resource feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YImage_resource_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YImage_resource_feature", "_UI_YImage_type"),
				 ExtensionModelPackage.Literals.YIMAGE__RESOURCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns YImage.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YImage"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YImage)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YImage_type") :
			getString("_UI_YImage_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YImage.class)) {
			case ExtensionModelPackage.YIMAGE__VALUE:
			case ExtensionModelPackage.YIMAGE__RESOURCE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTextDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTextAreaDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYNumericDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYDecimalDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTableDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYCheckBoxDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYComboBoxDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYListDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYOptionsGroupDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYBrowserDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYDateTimeDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTreeDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYProgressBarDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTabSheetDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYMasterDetailDatatype()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
