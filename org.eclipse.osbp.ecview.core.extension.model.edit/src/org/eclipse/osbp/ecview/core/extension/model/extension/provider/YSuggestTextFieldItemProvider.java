/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextField;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextField} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YSuggestTextFieldItemProvider extends YInputItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSuggestTextFieldItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValueBindingEndpointPropertyDescriptor(object);
			addLastFocusEventPropertyDescriptor(object);
			addLastBlurEventPropertyDescriptor(object);
			addSelectionBindingEndpointPropertyDescriptor(object);
			addDatatypePropertyDescriptor(object);
			addDatadescriptionPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addKeysPropertyDescriptor(object);
			addUseSuggestionsPropertyDescriptor(object);
			addAutoHidePopupPropertyDescriptor(object);
			addLastSuggestionPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addEmfNsURIPropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
			addEventPropertyDescriptor(object);
			addItemCaptionPropertyPropertyDescriptor(object);
			addItemFilterPropertyPropertyDescriptor(object);
			addItemUUIDPropertyPropertyDescriptor(object);
			addCurrentValueDTOPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Value Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValueBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YValueBindable_valueBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YValueBindable_valueBindingEndpoint_feature", "_UI_YValueBindable_type"),
				 CoreModelPackage.Literals.YVALUE_BINDABLE__VALUE_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Focus Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastFocusEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YFocusNotifier_lastFocusEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YFocusNotifier_lastFocusEvent_feature", "_UI_YFocusNotifier_type"),
				 CoreModelPackage.Literals.YFOCUS_NOTIFIER__LAST_FOCUS_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Blur Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastBlurEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBlurNotifier_lastBlurEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBlurNotifier_lastBlurEvent_feature", "_UI_YBlurNotifier_type"),
				 CoreModelPackage.Literals.YBLUR_NOTIFIER__LAST_BLUR_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSelectionBindable_selectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSelectionBindable_selectionBindingEndpoint_feature", "_UI_YSelectionBindable_type"),
				 CoreModelPackage.Literals.YSELECTION_BINDABLE__SELECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datatype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatatypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_datatype_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_datatype_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__DATATYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datadescription feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatadescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_datadescription_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_datadescription_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__DATADESCRIPTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_value_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_value_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Keys feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeysPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_keys_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_keys_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__KEYS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Suggestions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseSuggestionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_useSuggestions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_useSuggestions_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__USE_SUGGESTIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Auto Hide Popup feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAutoHidePopupPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_autoHidePopup_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_autoHidePopup_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__AUTO_HIDE_POPUP,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Suggestion feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastSuggestionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_lastSuggestion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_lastSuggestion_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__LAST_SUGGESTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_type_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Emf Ns URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEmfNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_emfNsURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_emfNsURI_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__EMF_NS_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_typeQualifiedName_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Item Caption Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addItemCaptionPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_itemCaptionProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_itemCaptionProperty_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__ITEM_CAPTION_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Item Filter Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addItemFilterPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_itemFilterProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_itemFilterProperty_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__ITEM_FILTER_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Item UUID Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addItemUUIDPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_itemUUIDProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_itemUUIDProperty_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__ITEM_UUID_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Current Value DTO feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCurrentValueDTOPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_currentValueDTO_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_currentValueDTO_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__CURRENT_VALUE_DTO,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSuggestTextField_event_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSuggestTextField_event_feature", "_UI_YSuggestTextField_type"),
				 ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__EVENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns YSuggestTextField.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YSuggestTextField"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YSuggestTextField)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YSuggestTextField_type") :
			getString("_UI_YSuggestTextField_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YSuggestTextField.class)) {
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__VALUE:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__KEYS:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__USE_SUGGESTIONS:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__AUTO_HIDE_POPUP:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__LAST_SUGGESTION:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__TYPE:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__EMF_NS_URI:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__TYPE_QUALIFIED_NAME:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__EVENT:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__ITEM_CAPTION_PROPERTY:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__ITEM_FILTER_PROPERTY:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__ITEM_UUID_PROPERTY:
			case ExtensionModelPackage.YSUGGEST_TEXT_FIELD__CURRENT_VALUE_DTO:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
