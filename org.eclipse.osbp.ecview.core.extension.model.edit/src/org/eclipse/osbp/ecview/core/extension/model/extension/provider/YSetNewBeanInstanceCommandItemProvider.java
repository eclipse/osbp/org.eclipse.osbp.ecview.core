/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension.provider;


import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSetNewBeanInstanceCommand;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.extension.model.extension.YSetNewBeanInstanceCommand} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YSetNewBeanInstanceCommandItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSetNewBeanInstanceCommandItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addTriggerPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addEmfNsURIPropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_id_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_name_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YTaggable_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YTaggable_tags_feature", "_UI_YTaggable_type"),
				 CoreModelPackage.Literals.YTAGGABLE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Trigger feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTriggerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSetNewBeanInstanceCommand_trigger_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSetNewBeanInstanceCommand_trigger_feature", "_UI_YSetNewBeanInstanceCommand_type"),
				 ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TRIGGER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSetNewBeanInstanceCommand_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSetNewBeanInstanceCommand_type_feature", "_UI_YSetNewBeanInstanceCommand_type"),
				 ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Emf Ns URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEmfNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSetNewBeanInstanceCommand_emfNsURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSetNewBeanInstanceCommand_emfNsURI_feature", "_UI_YSetNewBeanInstanceCommand_type"),
				 ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__EMF_NS_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSetNewBeanInstanceCommand_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSetNewBeanInstanceCommand_typeQualifiedName_feature", "_UI_YSetNewBeanInstanceCommand_type"),
				 ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YELEMENT__PROPERTIES);
			childrenFeatures.add(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YSetNewBeanInstanceCommand.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YSetNewBeanInstanceCommand"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YSetNewBeanInstanceCommand)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YSetNewBeanInstanceCommand_type") :
			getString("_UI_YSetNewBeanInstanceCommand_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YSetNewBeanInstanceCommand.class)) {
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__TAGS:
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__ID:
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__NAME:
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__TRIGGER:
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__TYPE:
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__EMF_NS_URI:
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__TYPE_QUALIFIED_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__PROPERTIES:
			case ExtensionModelPackage.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YELEMENT__PROPERTIES,
				 CoreModelFactory.eINSTANCE.create(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 BindingFactory.eINSTANCE.createYBeanValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 BindingFactory.eINSTANCE.createYDetailValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 BindingFactory.eINSTANCE.createYECViewModelValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 BindingFactory.eINSTANCE.createYVisibilityProcessorValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 BindingFactory.eINSTANCE.createYNoOpValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 CoreModelFactory.eINSTANCE.createYContextValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 CoreModelFactory.eINSTANCE.createYBeanSlotValueBindingEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 CoreModelFactory.eINSTANCE.createYEmbeddableValueEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 CoreModelFactory.eINSTANCE.createYEmbeddableSelectionEndpoint()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YSET_NEW_BEAN_INSTANCE_COMMAND__TARGET,
				 CoreModelFactory.eINSTANCE.createYActivatedEndpoint()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
