/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YList;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.extension.model.extension.YList} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YListItemProvider extends YInputItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YListItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCollectionBindingEndpointPropertyDescriptor(object);
			addSelectionBindingEndpointPropertyDescriptor(object);
			addMultiSelectionBindingEndpointPropertyDescriptor(object);
			addUseBeanServicePropertyDescriptor(object);
			addDatadescriptionPropertyDescriptor(object);
			addDatatypePropertyDescriptor(object);
			addSelectionTypePropertyDescriptor(object);
			addSelectionPropertyDescriptor(object);
			addMultiSelectionPropertyDescriptor(object);
			addCollectionPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addEmfNsURIPropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
			addCaptionPropertyPropertyDescriptor(object);
			addImagePropertyPropertyDescriptor(object);
			addDescriptionPropertyPropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Collection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCollectionBindable_collectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCollectionBindable_collectionBindingEndpoint_feature", "_UI_YCollectionBindable_type"),
				 CoreModelPackage.Literals.YCOLLECTION_BINDABLE__COLLECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSelectionBindable_selectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSelectionBindable_selectionBindingEndpoint_feature", "_UI_YSelectionBindable_type"),
				 CoreModelPackage.Literals.YSELECTION_BINDABLE__SELECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Multi Selection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMultiSelectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMultiSelectionBindable_multiSelectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMultiSelectionBindable_multiSelectionBindingEndpoint_feature", "_UI_YMultiSelectionBindable_type"),
				 CoreModelPackage.Literals.YMULTI_SELECTION_BINDABLE__MULTI_SELECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Bean Service feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseBeanServicePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanServiceConsumer_useBeanService_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanServiceConsumer_useBeanService_feature", "_UI_YBeanServiceConsumer_type"),
				 ExtensionModelPackage.Literals.YBEAN_SERVICE_CONSUMER__USE_BEAN_SERVICE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datadescription feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatadescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_datadescription_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_datadescription_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__DATADESCRIPTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datatype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatatypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_datatype_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_datatype_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__DATATYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_selectionType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_selectionType_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__SELECTION_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_selection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_selection_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__SELECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Multi Selection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMultiSelectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_multiSelection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_multiSelection_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__MULTI_SELECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Collection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_collection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_collection_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__COLLECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_type_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Emf Ns URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEmfNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_emfNsURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_emfNsURI_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__EMF_NS_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_typeQualifiedName_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Caption Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCaptionPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_captionProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_captionProperty_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__CAPTION_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Image Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addImagePropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_imageProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_imageProperty_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__IMAGE_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_descriptionProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_descriptionProperty_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__DESCRIPTION_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YList_description_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YList_description_feature", "_UI_YList_type"),
				 ExtensionModelPackage.Literals.YLIST__DESCRIPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns YList.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YList"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YList)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YList_type") :
			getString("_UI_YList_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YList.class)) {
			case ExtensionModelPackage.YLIST__USE_BEAN_SERVICE:
			case ExtensionModelPackage.YLIST__SELECTION_TYPE:
			case ExtensionModelPackage.YLIST__SELECTION:
			case ExtensionModelPackage.YLIST__MULTI_SELECTION:
			case ExtensionModelPackage.YLIST__COLLECTION:
			case ExtensionModelPackage.YLIST__TYPE:
			case ExtensionModelPackage.YLIST__EMF_NS_URI:
			case ExtensionModelPackage.YLIST__TYPE_QUALIFIED_NAME:
			case ExtensionModelPackage.YLIST__CAPTION_PROPERTY:
			case ExtensionModelPackage.YLIST__IMAGE_PROPERTY:
			case ExtensionModelPackage.YLIST__DESCRIPTION_PROPERTY:
			case ExtensionModelPackage.YLIST__DESCRIPTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
