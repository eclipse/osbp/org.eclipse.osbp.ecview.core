/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.core.extension.model.extension.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanReferenceField;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.core.extension.model.extension.YBeanReferenceField} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YBeanReferenceFieldItemProvider extends YInputItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBeanReferenceFieldItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValueBindingEndpointPropertyDescriptor(object);
			addUseBeanServicePropertyDescriptor(object);
			addLastBlurEventPropertyDescriptor(object);
			addLastFocusEventPropertyDescriptor(object);
			addDatadescriptionPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addEmfNsURIPropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
			addInMemoryBeanProviderPropertyDescriptor(object);
			addInMemoryBeanProviderQualifiedNamePropertyDescriptor(object);
			addCaptionPropertyPathPropertyDescriptor(object);
			addImagePropertyPathPropertyDescriptor(object);
			addDescriptionPropertyPropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
			addReferenceSourceTypePropertyDescriptor(object);
			addReferenceSourceTypeQualifiedNamePropertyDescriptor(object);
			addReferenceSourceTypePropertyPropertyDescriptor(object);
			addRequiredPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Value Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValueBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YValueBindable_valueBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YValueBindable_valueBindingEndpoint_feature", "_UI_YValueBindable_type"),
				 CoreModelPackage.Literals.YVALUE_BINDABLE__VALUE_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Bean Service feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseBeanServicePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanServiceConsumer_useBeanService_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanServiceConsumer_useBeanService_feature", "_UI_YBeanServiceConsumer_type"),
				 ExtensionModelPackage.Literals.YBEAN_SERVICE_CONSUMER__USE_BEAN_SERVICE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Blur Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastBlurEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBlurNotifier_lastBlurEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBlurNotifier_lastBlurEvent_feature", "_UI_YBlurNotifier_type"),
				 CoreModelPackage.Literals.YBLUR_NOTIFIER__LAST_BLUR_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Last Focus Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastFocusEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YFocusNotifier_lastFocusEvent_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YFocusNotifier_lastFocusEvent_feature", "_UI_YFocusNotifier_type"),
				 CoreModelPackage.Literals.YFOCUS_NOTIFIER__LAST_FOCUS_EVENT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datadescription feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatadescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_datadescription_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_datadescription_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__DATADESCRIPTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_value_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_value_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_type_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Emf Ns URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEmfNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_emfNsURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_emfNsURI_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__EMF_NS_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_typeQualifiedName_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the In Memory Bean Provider feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInMemoryBeanProviderPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_inMemoryBeanProvider_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_inMemoryBeanProvider_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__IN_MEMORY_BEAN_PROVIDER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the In Memory Bean Provider Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInMemoryBeanProviderQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_inMemoryBeanProviderQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_inMemoryBeanProviderQualifiedName_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__IN_MEMORY_BEAN_PROVIDER_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Caption Property Path feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCaptionPropertyPathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_captionPropertyPath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_captionPropertyPath_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__CAPTION_PROPERTY_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Image Property Path feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addImagePropertyPathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_imagePropertyPath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_imagePropertyPath_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__IMAGE_PROPERTY_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_descriptionProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_descriptionProperty_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__DESCRIPTION_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_description_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_description_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__DESCRIPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Reference Source Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferenceSourceTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_referenceSourceType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_referenceSourceType_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Reference Source Type Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferenceSourceTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_referenceSourceTypeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_referenceSourceTypeQualifiedName_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Reference Source Type Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferenceSourceTypePropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_referenceSourceTypeProperty_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_referenceSourceTypeProperty_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE_PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Required feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRequiredPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanReferenceField_required_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanReferenceField_required_feature", "_UI_YBeanReferenceField_type"),
				 ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__REQUIRED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__FILTERING_COMPONENT);
			childrenFeatures.add(ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__DIALOG_COMPONENT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YBeanReferenceField.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YBeanReferenceField"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YBeanReferenceField)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YBeanReferenceField_type") :
			getString("_UI_YBeanReferenceField_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YBeanReferenceField.class)) {
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__USE_BEAN_SERVICE:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__VALUE:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__TYPE:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__EMF_NS_URI:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__TYPE_QUALIFIED_NAME:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__IN_MEMORY_BEAN_PROVIDER:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__IN_MEMORY_BEAN_PROVIDER_QUALIFIED_NAME:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__CAPTION_PROPERTY_PATH:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__IMAGE_PROPERTY_PATH:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__DESCRIPTION_PROPERTY:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__DESCRIPTION:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE_QUALIFIED_NAME:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__REFERENCE_SOURCE_TYPE_PROPERTY:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__REQUIRED:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__FILTERING_COMPONENT:
			case ExtensionModelPackage.YBEAN_REFERENCE_FIELD__DIALOG_COMPONENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__FILTERING_COMPONENT,
				 ExtensionModelFactory.eINSTANCE.createYFilteringComponent()));

		newChildDescriptors.add
			(createChildParameter
				(ExtensionModelPackage.Literals.YBEAN_REFERENCE_FIELD__DIALOG_COMPONENT,
				 ExtensionModelFactory.eINSTANCE.createYDialogComponent()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
