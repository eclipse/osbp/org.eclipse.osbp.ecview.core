/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.databinding.tests.emf.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>TList</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.databinding.tests.emf.model.TList#getTObject <em>TObject</em>}</li>
 * </ul>
 * 
 *
 * @see org.eclipse.osbp.ecview.core.databinding.tests.emf.model.TestmodelPackage#getTList()
 * @model
 * @generated
 */
public interface TList extends EObject {
	/**
	 * Returns the value of the '<em><b>TObject</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>TObject</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * 
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>TObject</em>' attribute.
	 * @see #setTObject(Object)
	 * @see org.eclipse.osbp.ecview.core.databinding.tests.emf.model.TestmodelPackage#getTList_TObject()
	 * @model
	 * @generated
	 */
	Object getTObject();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.core.databinding.tests.emf.model.TList#getTObject <em>TObject</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>TObject</em>' attribute.
	 * @see #getTObject()
	 * @generated
	 */
	void setTObject(Object value);

} // TList
