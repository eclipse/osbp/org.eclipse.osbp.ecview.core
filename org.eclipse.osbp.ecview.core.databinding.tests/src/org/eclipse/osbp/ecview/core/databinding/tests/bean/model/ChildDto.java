/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.databinding.tests.bean.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

@SuppressWarnings("all")
public class ChildDto implements Serializable, PropertyChangeListener {
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(
			this);

	private boolean disposed;

	private ParentDto parent;

	/**
	 * @return true, if the object is disposed. Disposed means, that it is
	 *         prepared for garbage collection and may not be used anymore.
	 *         Accessing objects that are already disposed will cause runtime
	 *         exceptions.
	 * 
	 */
	public boolean isDisposed() {
		return this.disposed;
	}

	/**
	 * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * @see PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final String propertyName,
			final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 */
	public void removePropertyChangeListener(
			final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	/**
	 * @see PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void removePropertyChangeListener(final String propertyName,
			final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName,
				listener);
	}

	/**
	 * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
	 */
	public void firePropertyChange(final String propertyName,
			final Object oldValue, final Object newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue,
				newValue);
	}

	/**
	 * Checks whether the object is disposed.
	 * 
	 * @throws RuntimeException
	 *             if the object is disposed.
	 */
	private void checkDisposed() {
		if (isDisposed()) {
			throw new RuntimeException("Object already disposed: " + this);
		}
	}

	/**
	 * Calling dispose will destroy that instance. The internal state will be
	 * set to 'disposed' and methods of that object must not be used anymore.
	 * Each call will result in runtime exceptions.<br/>
	 * If this object keeps composition containments, these will be disposed
	 * too. So the whole composition containment tree will be disposed on
	 * calling this method.
	 */
	public void dispose() {
		if (isDisposed()) {
			return;
		}
		firePropertyChange("disposed", this.disposed, this.disposed = true);
	}

	/**
	 * Installs lazy collection resolving for entity {@link } to the dto
	 * {@link ChildDto}.
	 * 
	 */
	protected void installLazyCollections() {

	}

	/**
	 * Returns the parent property or <code>null</code> if not present.
	 */
	public ParentDto getParent() {
		return this.parent;
	}

/**
   * Sets the <code>parent</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>ParentDto#
   * children</code> of the <code>parent</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link ParentDto#setChildren(ParentDto)
   * 
   * @param parent - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
	public void setParent(final ParentDto parent) {
		checkDisposed();
		if (this.parent != null) {
			this.parent.internalRemoveFromChildren(this);
		}

		internalSetParent(parent);

		if (this.parent != null) {
			this.parent.internalAddToChildren(this);
		}
	}

	/**
	 * For internal use only!
	 */
	void internalSetParent(final ParentDto parent) {
		firePropertyChange("parent", this.parent, this.parent = parent);
	}

	public ChildDto createDto() {
		return new ChildDto();
	}

	public void propertyChange(final java.beans.PropertyChangeEvent event) {
		Object source = event.getSource();

		// forward the event from embeddable beans to all listeners. So the
		// parent of the embeddable
		// bean will become notified and its dirty state can be handled properly
		{
			// no super class available to forward event
		}
	}
}
