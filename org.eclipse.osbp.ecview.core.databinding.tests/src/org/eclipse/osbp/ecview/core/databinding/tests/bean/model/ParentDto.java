/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.databinding.tests.bean.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("all")
public class ParentDto implements Serializable, PropertyChangeListener {
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

	private boolean disposed;

	private List<ChildDto> children;

	/**
	 * @return true, if the object is disposed. Disposed means, that it is
	 *         prepared for garbage collection and may not be used anymore.
	 *         Accessing objects that are already disposed will cause runtime
	 *         exceptions.
	 * 
	 */
	public boolean isDisposed() {
		return this.disposed;
	}

	/**
	 * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * @see PropertyChangeSupport#addPropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
	}

	/**
	 * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
	 */
	public void removePropertyChangeListener(final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	/**
	 * @see PropertyChangeSupport#removePropertyChangeListener(String,
	 *      PropertyChangeListener)
	 */
	public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
	}

	/**
	 * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
	 */
	public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
		propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}

	/**
	 * Checks whether the object is disposed.
	 * 
	 * @throws RuntimeException
	 *             if the object is disposed.
	 */
	private void checkDisposed() {
		if (isDisposed()) {
			throw new RuntimeException("Object already disposed: " + this);
		}
	}

	/**
	 * Calling dispose will destroy that instance. The internal state will be
	 * set to 'disposed' and methods of that object must not be used anymore.
	 * Each call will result in runtime exceptions.<br/>
	 * If this object keeps composition containments, these will be disposed
	 * too. So the whole composition containment tree will be disposed on
	 * calling this method.
	 */
	public void dispose() {
		if (isDisposed()) {
			return;
		}
		try {
			// Dispose all the composition references.
			if (this.children != null) {
				for (ChildDto childDto : this.children) {
					childDto.dispose();
				}
				this.children = null;
			}

		} finally {
			firePropertyChange("disposed", this.disposed, this.disposed = true);
		}

	}

	/**
	 * Installs lazy collection resolving for entity {@link } to the dto
	 * {@link ParentDto}.
	 * 
	 */
	protected void installLazyCollections() {

	}

	/**
	 * Returns an unmodifiable list of children.
	 */
	public List<ChildDto> getChildren() {
		return Collections.unmodifiableList(internalGetChildren());
	}

	/**
	 * Returns the list of <code>ChildDto</code>s thereby lazy initializing it.
	 * For internal use only!
	 * 
	 * @return list - the resulting list
	 * 
	 */
	private List<ChildDto> internalGetChildren() {
		if (this.children == null) {
			this.children = new java.util.ArrayList<ChildDto>();
		}
		return this.children;
	}

	/**
	 * Adds the given childDto to this object.
	 * <p>
	 * Since the reference is a composition reference, the opposite reference
	 * <code>ChildDto#parent</code> of the <code>childDto</code> will be handled
	 * automatically and no further coding is required to keep them in sync.
	 * <p>
	 * See {@link ChildDto#setParent(ChildDto)}.
	 * 
	 * @param childDto
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void addToChildren(final ChildDto childDto) {
		checkDisposed();

		childDto.setParent(this);
	}

	/**
	 * Removes the given childDto from this object.
	 * <p>
	 * Since the reference is a cascading reference, the opposite reference
	 * (ChildDto.parent) of the childDto will be handled automatically and no
	 * further coding is required to keep them in sync. See
	 * {@link ChildDto#setParent(ChildDto)}.
	 * 
	 * @param childDto
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void removeFromChildren(final ChildDto childDto) {
		checkDisposed();

		childDto.setParent(null);
	}

	/**
	 * For internal use only!
	 */
	void internalAddToChildren(final ChildDto childDto) {
		List<ChildDto> oldList = new ArrayList<>(internalGetChildren());
		internalGetChildren().add(childDto);
		
		propertyChangeSupport.firePropertyChange("children", oldList, internalGetChildren());
	}

	/**
	 * For internal use only!
	 */
	void internalRemoveFromChildren(final ChildDto childDto) {
		List<ChildDto> oldList = new ArrayList<>(internalGetChildren());
		internalGetChildren().remove(childDto);
		
		propertyChangeSupport.firePropertyChange("children", oldList, internalGetChildren());
	}

	/**
	 * Sets the <code>children</code> property to this instance. Since the
	 * reference has an opposite reference, the opposite <code>ChildDto#
	 * parent</code> of the <code>children</code> will be handled automatically
	 * and no further coding is required to keep them in sync.
	 * <p>
	 * See {@link ChildDto#setParent(ChildDto)
	 * 
	 * @param children
	 *            - the property
	 * @throws RuntimeException
	 *             if instance is <code>disposed</code>
	 * 
	 */
	public void setChildren(final List<ChildDto> children) {
		checkDisposed();
		for (ChildDto dto : internalGetChildren().toArray(new ChildDto[this.children.size()])) {
			removeFromChildren(dto);
		}

		if (children == null) {
			return;
		}

		for (ChildDto dto : children) {
			addToChildren(dto);
		}
	}

	public ParentDto createDto() {
		return new ParentDto();
	}

	public void propertyChange(final java.beans.PropertyChangeEvent event) {
		Object source = event.getSource();

		// forward the event from embeddable beans to all listeners. So the
		// parent of the embeddable
		// bean will become notified and its dirty state can be handled properly
		{
			// no super class available to forward event
		}
	}
}
