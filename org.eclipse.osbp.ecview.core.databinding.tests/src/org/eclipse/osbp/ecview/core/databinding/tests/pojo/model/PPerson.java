/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.databinding.tests.pojo.model;

public class PPerson {

	private PAddress address;

	/**
	 * @return the address
	 */
	public PAddress getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(PAddress address) {
		this.address = address;
	}

	/**
	 * Creates a new instance of person. All references are properly setup and
	 * the isoCode of the country is set to the given value.
	 * 
	 * @param isoCode
	 * @return
	 */
	public static PPerson newInstance(String isoCode) {
		PPerson person = new PPerson();
		PAddress address = new PAddress();
		person.setAddress(address);
		PCountry country = new PCountry();
		country.setIsoCode(isoCode);
		address.setCountry(country);
		return person;
	}

}
