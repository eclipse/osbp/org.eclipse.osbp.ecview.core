/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.databinding.tests.bean.model;

public class BPerson extends AbstractBean {

	private BAddress address;

	/**
	 * @return the address
	 */
	public BAddress getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            the address to set
	 */
	public void setAddress(BAddress address) {
		firePropertyChanged("address", this.address, this.address = address);
	}

	/**
	 * Creates a new instance of person. All references are properly setup and
	 * the isoCode of the country is set to the given value.
	 * 
	 * @param isoCode
	 * @return
	 */
	public static BPerson newInstance(String isoCode) {
		BPerson person = new BPerson();
		BAddress address = new BAddress();
		person.setAddress(address);
		BCountry country = new BCountry();
		country.setIsoCode(isoCode);
		address.setCountry(country);
		return person;
	}

}
