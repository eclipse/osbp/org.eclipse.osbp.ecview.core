/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBinding Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint) <em>Add Binding</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint) <em>Add Binding</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy) <em>Add Binding</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy) <em>Add Binding</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#getView() <em>Get View</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YBinding) <em>Add Binding</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#removeBinding(org.eclipse.osbp.ecview.core.common.model.binding.YBinding) <em>Remove Binding</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YBindingSetTest extends TestCase {

	/**
	 * The fixture for this YBinding Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBindingSet fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YBindingSetTest.class);
	}

	/**
	 * Constructs a new YBinding Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBindingSetTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBinding Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBindingSet fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBinding Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBindingSet getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(BindingFactory.eINSTANCE.createYBindingSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint) <em>Add Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint)
	 * @generated
	 */
	public void testAddBinding__YValueBindingEndpoint_YValueBindingEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint) <em>Add Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint)
	 * @generated
	 */
	public void testAddBinding__YListBindingEndpoint_YListBindingEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy) <em>Add Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy)
	 * @generated
	 */
	public void testAddBinding__YValueBindingEndpoint_YValueBindingEndpoint_YBindingUpdateStrategy_YBindingUpdateStrategy() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy) <em>Add Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy, org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy)
	 * @generated
	 */
	public void testAddBinding__YListBindingEndpoint_YListBindingEndpoint_YBindingUpdateStrategy_YBindingUpdateStrategy() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#getView() <em>Get View</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#getView()
	 * @generated
	 */
	public void testGetView() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YBinding) <em>Add Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#addBinding(org.eclipse.osbp.ecview.core.common.model.binding.YBinding)
	 * @generated
	 */
	public void testAddBinding__YBinding() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#removeBinding(org.eclipse.osbp.ecview.core.common.model.binding.YBinding) <em>Remove Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet#removeBinding(org.eclipse.osbp.ecview.core.common.model.binding.YBinding)
	 * @generated
	 */
	public void testRemoveBinding__YBinding() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YBindingSetTest
