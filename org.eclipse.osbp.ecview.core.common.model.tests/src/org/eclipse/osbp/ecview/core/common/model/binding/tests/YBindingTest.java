/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBinding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getBindingSet() <em>Get Binding Set</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#isBindsElement(org.eclipse.osbp.ecview.core.common.model.core.YElement) <em>Is Binds Element</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getTargetEndpoint() <em>Get Target Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getModelEndpoint() <em>Get Model Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YBindingTest extends TestCase {

	/**
	 * The fixture for this YBinding test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBinding fixture = null;

	/**
	 * Constructs a new YBinding test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBindingTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBinding test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBinding fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBinding test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBinding getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getBindingSet() <em>Get Binding Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getBindingSet()
	 * @generated
	 */
	public void testGetBindingSet() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#isBindsElement(org.eclipse.osbp.ecview.core.common.model.core.YElement) <em>Is Binds Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBinding#isBindsElement(org.eclipse.osbp.ecview.core.common.model.core.YElement)
	 * @generated
	 */
	public void testIsBindsElement__YElement() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getTargetEndpoint() <em>Get Target Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getTargetEndpoint()
	 * @generated
	 */
	public void testGetTargetEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getModelEndpoint() <em>Get Model Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBinding#getModelEndpoint()
	 * @generated
	 */
	public void testGetModelEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YBindingTest
