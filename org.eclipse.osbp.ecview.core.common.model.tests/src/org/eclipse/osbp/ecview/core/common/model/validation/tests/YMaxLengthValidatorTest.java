/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.YMaxLengthValidator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YMax Length Validator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YMaxLengthValidatorTest extends YValidatorTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YMaxLengthValidatorTest.class);
	}

	/**
	 * Constructs a new YMax Length Validator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMaxLengthValidatorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YMax Length Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YMaxLengthValidator getFixture() {
		return (YMaxLengthValidator)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ValidationFactory.eINSTANCE.createYMaxLengthValidator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YMaxLengthValidatorTest
