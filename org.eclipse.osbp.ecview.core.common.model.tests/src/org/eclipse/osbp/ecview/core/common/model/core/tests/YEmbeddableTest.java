/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEmbeddable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#isVisible() <em>Visible</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabel() <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabelI18nKey() <em>Label I1 8n Key</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getView() <em>Get View</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getParent() <em>Get Parent</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YEmbeddableTest extends TestCase {

	/**
	 * The fixture for this YEmbeddable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEmbeddable fixture = null;

	/**
	 * Constructs a new YEmbeddable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YEmbeddable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YEmbeddable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YEmbeddable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEmbeddable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#isVisible() <em>Visible</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#isVisible()
	 * @generated
	 */
	public void testIsVisible() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#setVisible(boolean) <em>Visible</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#setVisible(boolean)
	 * @generated
	 */
	public void testSetVisible() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabel() <em>Label</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabel()
	 * @generated
	 */
	public void testGetLabel() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#setLabel(java.lang.String) <em>Label</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#setLabel(java.lang.String)
	 * @generated
	 */
	public void testSetLabel() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabelI18nKey() <em>Label I1 8n Key</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getLabelI18nKey()
	 * @generated
	 */
	public void testGetLabelI18nKey() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#setLabelI18nKey(java.lang.String) <em>Label I1 8n Key</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#setLabelI18nKey(java.lang.String)
	 * @generated
	 */
	public void testSetLabelI18nKey() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getView() <em>Get View</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getView()
	 * @generated
	 */
	public void testGetView() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getParent() <em>Get Parent</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable#getParent()
	 * @generated
	 */
	public void testGetParent() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YEmbeddableTest
