/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.authorization.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationFactory;
import org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorizationStore;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YAuthorization Store</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YAuthorizationStoreTest extends TestCase {

	/**
	 * The fixture for this YAuthorization Store test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAuthorizationStore fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YAuthorizationStoreTest.class);
	}

	/**
	 * Constructs a new YAuthorization Store test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YAuthorizationStoreTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YAuthorization Store test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YAuthorizationStore fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YAuthorization Store test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAuthorizationStore getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(AuthorizationFactory.eINSTANCE.createYAuthorizationStore());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YAuthorizationStoreTest
