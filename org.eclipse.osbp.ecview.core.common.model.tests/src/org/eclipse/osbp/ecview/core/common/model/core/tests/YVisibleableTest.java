/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YVisibleable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YVisibleable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#isVisible() <em>Visible</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YVisibleableTest extends TestCase {

	/**
	 * The fixture for this YVisibleable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVisibleable fixture = null;

	/**
	 * Constructs a new YVisibleable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVisibleableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YVisibleable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YVisibleable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YVisibleable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVisibleable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#isVisible() <em>Visible</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#isVisible()
	 * @generated
	 */
	public void testIsVisible() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#setVisible(boolean) <em>Visible</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YVisibleable#setVisible(boolean)
	 * @generated
	 */
	public void testSetVisible() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YVisibleableTest
