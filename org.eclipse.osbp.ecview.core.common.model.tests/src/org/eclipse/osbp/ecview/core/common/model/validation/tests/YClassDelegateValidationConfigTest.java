/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidationConfig;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YClass Delegate Validation Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YClassDelegateValidationConfigTest extends TestCase {

	/**
	 * The fixture for this YClass Delegate Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YClassDelegateValidationConfig fixture = null;

	/**
	 * Constructs a new YClass Delegate Validation Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YClassDelegateValidationConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YClass Delegate Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YClassDelegateValidationConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YClass Delegate Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YClassDelegateValidationConfig getFixture() {
		return fixture;
	}

} //YClassDelegateValidationConfigTest
