/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YBeanValidationValidatorConfig;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBean Validation Validator Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YBeanValidationValidatorConfigTest extends TestCase {

	/**
	 * The fixture for this YBean Validation Validator Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBeanValidationValidatorConfig fixture = null;

	/**
	 * Constructs a new YBean Validation Validator Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBeanValidationValidatorConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBean Validation Validator Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBeanValidationValidatorConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBean Validation Validator Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBeanValidationValidatorConfig getFixture() {
		return fixture;
	}

} //YBeanValidationValidatorConfigTest
