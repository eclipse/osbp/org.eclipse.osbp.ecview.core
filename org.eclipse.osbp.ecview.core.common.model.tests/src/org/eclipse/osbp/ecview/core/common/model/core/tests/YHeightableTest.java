/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YHeightable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YHeightable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YHeightableTest extends TestCase {

	/**
	 * The fixture for this YHeightable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YHeightable fixture = null;

	/**
	 * Constructs a new YHeightable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YHeightableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YHeightable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YHeightable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YHeightable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YHeightable getFixture() {
		return fixture;
	}

} //YHeightableTest
