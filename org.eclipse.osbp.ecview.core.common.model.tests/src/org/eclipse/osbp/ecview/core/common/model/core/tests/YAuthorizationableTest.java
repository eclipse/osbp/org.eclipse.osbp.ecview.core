/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YAuthorizationable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YAuthorizationable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YAuthorizationableTest extends TestCase {

	/**
	 * The fixture for this YAuthorizationable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAuthorizationable fixture = null;

	/**
	 * Constructs a new YAuthorizationable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YAuthorizationableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YAuthorizationable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YAuthorizationable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YAuthorizationable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAuthorizationable getFixture() {
		return fixture;
	}

} //YAuthorizationableTest
