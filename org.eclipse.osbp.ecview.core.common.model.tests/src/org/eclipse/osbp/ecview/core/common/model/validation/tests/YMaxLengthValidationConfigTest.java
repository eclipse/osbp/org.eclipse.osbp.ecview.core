/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YMaxLengthValidationConfig;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YMax Length Validation Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YMaxLengthValidationConfigTest extends TestCase {

	/**
	 * The fixture for this YMax Length Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMaxLengthValidationConfig fixture = null;

	/**
	 * Constructs a new YMax Length Validation Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMaxLengthValidationConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YMax Length Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YMaxLengthValidationConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YMax Length Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMaxLengthValidationConfig getFixture() {
		return fixture;
	}

} //YMaxLengthValidationConfigTest
