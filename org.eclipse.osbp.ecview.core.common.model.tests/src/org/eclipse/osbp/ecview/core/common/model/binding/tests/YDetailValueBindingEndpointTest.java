/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YDetailValueBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YDetail Value Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YDetailValueBindingEndpointTest extends YValueBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YDetailValueBindingEndpointTest.class);
	}

	/**
	 * Constructs a new YDetail Value Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDetailValueBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YDetail Value Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YDetailValueBindingEndpoint getFixture() {
		return (YDetailValueBindingEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(BindingFactory.eINSTANCE.createYDetailValueBindingEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YDetailValueBindingEndpointTest
