/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>binding</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class BindingTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new BindingTests("binding Tests");
		suite.addTestSuite(YBindingSetTest.class);
		suite.addTestSuite(YBeanValueBindingEndpointTest.class);
		suite.addTestSuite(YDetailValueBindingEndpointTest.class);
		suite.addTestSuite(YValueBindingTest.class);
		suite.addTestSuite(YListBindingTest.class);
		suite.addTestSuite(YEnumListBindingEndpointTest.class);
		suite.addTestSuite(YECViewModelValueBindingEndpointTest.class);
		suite.addTestSuite(YECViewModelListBindingEndpointTest.class);
		suite.addTestSuite(YVisibilityProcessorValueBindingEndpointTest.class);
		suite.addTestSuite(YNoOpValueBindingEndpointTest.class);
		suite.addTestSuite(YNoOpListBindingEndpointTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BindingTests(String name) {
		super(name);
	}

} //BindingTests
