/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YElement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YElementTest extends TestCase {

	/**
	 * The fixture for this YElement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YElement fixture = null;

	/**
	 * Constructs a new YElement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YElement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YElement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YElement getFixture() {
		return fixture;
	}

} //YElementTest
