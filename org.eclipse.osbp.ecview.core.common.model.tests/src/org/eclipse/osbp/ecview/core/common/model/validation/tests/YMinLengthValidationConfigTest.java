/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidationConfig;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YMin Length Validation Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YMinLengthValidationConfigTest extends TestCase {

	/**
	 * The fixture for this YMin Length Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMinLengthValidationConfig fixture = null;

	/**
	 * Constructs a new YMin Length Validation Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMinLengthValidationConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YMin Length Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YMinLengthValidationConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YMin Length Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMinLengthValidationConfig getFixture() {
		return fixture;
	}

} //YMinLengthValidationConfigTest
