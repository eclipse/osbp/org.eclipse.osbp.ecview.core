/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YValue Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint#createDetailValueEndpoint() <em>Create Detail Value Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YValueBindingEndpointTest extends YBindingEndpointTest {

	/**
	 * Constructs a new YValue Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YValueBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YValue Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YValueBindingEndpoint getFixture() {
		return (YValueBindingEndpoint)fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint#createDetailValueEndpoint() <em>Create Detail Value Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint#createDetailValueEndpoint()
	 * @generated
	 */
	public void testCreateDetailValueEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YValueBindingEndpointTest
