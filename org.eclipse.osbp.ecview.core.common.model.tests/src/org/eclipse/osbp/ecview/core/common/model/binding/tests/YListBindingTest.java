/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YListBinding;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YList Binding</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YListBindingTest extends YBindingTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YListBindingTest.class);
	}

	/**
	 * Constructs a new YList Binding test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YListBindingTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YList Binding test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YListBinding getFixture() {
		return (YListBinding)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(BindingFactory.eINSTANCE.createYListBinding());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YListBindingTest
