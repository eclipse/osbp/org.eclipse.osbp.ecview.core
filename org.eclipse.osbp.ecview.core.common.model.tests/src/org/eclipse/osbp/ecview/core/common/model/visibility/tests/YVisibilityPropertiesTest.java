/**
 */
package org.eclipse.osbp.ecview.core.common.model.visibility.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityFactory;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProperties;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YVisibility Properties</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YVisibilityPropertiesTest extends TestCase {

	/**
	 * The fixture for this YVisibility Properties test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVisibilityProperties fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YVisibilityPropertiesTest.class);
	}

	/**
	 * Constructs a new YVisibility Properties test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVisibilityPropertiesTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YVisibility Properties test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YVisibilityProperties fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YVisibility Properties test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVisibilityProperties getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(VisibilityFactory.eINSTANCE.createYVisibilityProperties());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YVisibilityPropertiesTest
