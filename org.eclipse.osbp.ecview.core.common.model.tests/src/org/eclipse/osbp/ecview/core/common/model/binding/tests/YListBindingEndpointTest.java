/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import org.eclipse.osbp.ecview.core.common.model.binding.YListBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YList Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YListBindingEndpointTest extends YBindingEndpointTest {

	/**
	 * Constructs a new YList Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YListBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YList Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YListBindingEndpoint getFixture() {
		return (YListBindingEndpoint)fixture;
	}

} //YListBindingEndpointTest
