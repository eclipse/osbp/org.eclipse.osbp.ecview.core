/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBinding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#getBinding() <em>Get Binding</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#isBindsElement(org.eclipse.osbp.ecview.core.common.model.core.YElement) <em>Is Binds Element</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YBindingEndpointTest extends TestCase {

	/**
	 * The fixture for this YBinding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBindingEndpoint fixture = null;

	/**
	 * Constructs a new YBinding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBinding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBindingEndpoint fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBinding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBindingEndpoint getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#getBinding() <em>Get Binding</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#getBinding()
	 * @generated
	 */
	public void testGetBinding() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#isBindsElement(org.eclipse.osbp.ecview.core.common.model.core.YElement) <em>Is Binds Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint#isBindsElement(org.eclipse.osbp.ecview.core.common.model.core.YElement)
	 * @generated
	 */
	public void testIsBindsElement__YElement() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YBindingEndpointTest
