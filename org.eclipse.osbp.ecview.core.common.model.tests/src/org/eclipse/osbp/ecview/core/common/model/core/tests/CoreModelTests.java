/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>core</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class CoreModelTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new CoreModelTests("core Tests");
		suite.addTestSuite(YLayoutTest.class);
		suite.addTestSuite(YHelperLayoutTest.class);
		suite.addTestSuite(YFieldTest.class);
		suite.addTestSuite(YViewTest.class);
		suite.addTestSuite(YDialogTest.class);
		suite.addTestSuite(YActionTest.class);
		suite.addTestSuite(YContextValueBindingEndpointTest.class);
		suite.addTestSuite(YBeanSlotValueBindingEndpointTest.class);
		suite.addTestSuite(YBeanSlotListBindingEndpointTest.class);
		suite.addTestSuite(YEmbeddableValueEndpointTest.class);
		suite.addTestSuite(YEmbeddableSelectionEndpointTest.class);
		suite.addTestSuite(YEmbeddableMultiSelectionEndpointTest.class);
		suite.addTestSuite(YEmbeddableCollectionEndpointTest.class);
		suite.addTestSuite(YActivatedEndpointTest.class);
		suite.addTestSuite(YCommandSetTest.class);
		suite.addTestSuite(YOpenDialogCommandTest.class);
		suite.addTestSuite(YExposedActionTest.class);
		suite.addTestSuite(YSendEventCommandTest.class);
		suite.addTestSuite(YKeyStrokeDefinitionTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoreModelTests(String name) {
		super(name);
	}

} //CoreModelTests
