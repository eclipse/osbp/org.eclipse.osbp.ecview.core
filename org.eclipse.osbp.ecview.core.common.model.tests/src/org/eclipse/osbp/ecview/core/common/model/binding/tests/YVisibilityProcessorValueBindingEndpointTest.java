/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YVisibility Processor Value Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YVisibilityProcessorValueBindingEndpointTest extends YValueBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YVisibilityProcessorValueBindingEndpointTest.class);
	}

	/**
	 * Constructs a new YVisibility Processor Value Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVisibilityProcessorValueBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YVisibility Processor Value Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YVisibilityProcessorValueBindingEndpoint getFixture() {
		return (YVisibilityProcessorValueBindingEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(BindingFactory.eINSTANCE.createYVisibilityProcessorValueBindingEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YVisibilityProcessorValueBindingEndpointTest
