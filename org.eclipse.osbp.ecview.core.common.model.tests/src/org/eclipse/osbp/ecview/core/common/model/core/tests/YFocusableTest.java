/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YFocusable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YFocusableTest extends TestCase {

	/**
	 * The fixture for this YFocusable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFocusable fixture = null;

	/**
	 * Constructs a new YFocusable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFocusableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YFocusable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YFocusable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YFocusable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFocusable getFixture() {
		return fixture;
	}

} //YFocusableTest
