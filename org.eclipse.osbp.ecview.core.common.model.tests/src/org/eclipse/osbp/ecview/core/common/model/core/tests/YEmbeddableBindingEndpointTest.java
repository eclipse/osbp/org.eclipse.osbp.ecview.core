/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEmbeddable Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint#getElement() <em>Get Element</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YEmbeddableBindingEndpointTest extends TestCase {

	/**
	 * The fixture for this YEmbeddable Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEmbeddableBindingEndpoint fixture = null;

	/**
	 * Constructs a new YEmbeddable Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddableBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YEmbeddable Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YEmbeddableBindingEndpoint fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YEmbeddable Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEmbeddableBindingEndpoint getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint#getElement() <em>Get Element</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableBindingEndpoint#getElement()
	 * @generated
	 */
	public void testGetElement() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YEmbeddableBindingEndpointTest
