/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YHelperLayout;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YHelper Layout</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YHelperLayoutTest extends YLayoutTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YHelperLayoutTest.class);
	}

	/**
	 * Constructs a new YHelper Layout test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YHelperLayoutTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YHelper Layout test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YHelperLayout getFixture() {
		return (YHelperLayout)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYHelperLayout());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YHelperLayoutTest
