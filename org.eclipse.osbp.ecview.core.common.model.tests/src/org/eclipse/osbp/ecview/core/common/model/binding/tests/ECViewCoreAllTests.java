/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.tests.CoreModelTests;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>ECViewCore</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class ECViewCoreAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ECViewCoreAllTests("ECViewCore Tests");
		suite.addTest(BindingTests.suite());
		suite.addTest(CoreModelTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ECViewCoreAllTests(String name) {
		super(name);
	}

} //ECViewCoreAllTests
