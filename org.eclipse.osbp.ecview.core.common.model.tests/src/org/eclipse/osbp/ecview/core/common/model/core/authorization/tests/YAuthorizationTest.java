/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.authorization.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.authorization.YAuthorization;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YAuthorization</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YAuthorizationTest extends TestCase {

	/**
	 * The fixture for this YAuthorization test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAuthorization fixture = null;

	/**
	 * Constructs a new YAuthorization test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YAuthorizationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YAuthorization test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YAuthorization fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YAuthorization test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YAuthorization getFixture() {
		return fixture;
	}

} //YAuthorizationTest
