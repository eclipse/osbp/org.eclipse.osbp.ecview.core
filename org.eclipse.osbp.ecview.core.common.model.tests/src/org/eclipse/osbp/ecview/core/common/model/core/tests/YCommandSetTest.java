/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YCommandSet;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YCommand Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#getView() <em>Get View</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#addCommand(org.eclipse.osbp.ecview.core.common.model.core.YCommand) <em>Add Command</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#removeCommand(org.eclipse.osbp.ecview.core.common.model.core.YCommand) <em>Remove Command</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YCommandSetTest extends TestCase {

	/**
	 * The fixture for this YCommand Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YCommandSet fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YCommandSetTest.class);
	}

	/**
	 * Constructs a new YCommand Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YCommandSetTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YCommand Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YCommandSet fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YCommand Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YCommandSet getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYCommandSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#getView() <em>Get View</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#getView()
	 * @generated
	 */
	public void testGetView() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#addCommand(org.eclipse.osbp.ecview.core.common.model.core.YCommand) <em>Add Command</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#addCommand(org.eclipse.osbp.ecview.core.common.model.core.YCommand)
	 * @generated
	 */
	public void testAddCommand__YCommand() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#removeCommand(org.eclipse.osbp.ecview.core.common.model.core.YCommand) <em>Remove Command</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YCommandSet#removeCommand(org.eclipse.osbp.ecview.core.common.model.core.YCommand)
	 * @generated
	 */
	public void testRemoveCommand__YCommand() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YCommandSetTest
