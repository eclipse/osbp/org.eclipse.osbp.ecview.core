/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YUnique Attribute Validator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YUniqueAttributeValidatorTest extends YValidatorTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YUniqueAttributeValidatorTest.class);
	}

	/**
	 * Constructs a new YUnique Attribute Validator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YUniqueAttributeValidatorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YUnique Attribute Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YUniqueAttributeValidator getFixture() {
		return (YUniqueAttributeValidator)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ValidationFactory.eINSTANCE.createYUniqueAttributeValidator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YUniqueAttributeValidatorTest
