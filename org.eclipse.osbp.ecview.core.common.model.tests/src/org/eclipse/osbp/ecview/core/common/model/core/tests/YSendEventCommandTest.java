/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YSend Event Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#getView() <em>Get View</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createTriggerEndpoint() <em>Create Trigger Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createMessageEndpoint() <em>Create Message Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createAutoTriggerEndpoint() <em>Create Auto Trigger Endpoint</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createEventTopicEndpoint() <em>Create Event Topic Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YSendEventCommandTest extends TestCase {

	/**
	 * The fixture for this YSend Event Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSendEventCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YSendEventCommandTest.class);
	}

	/**
	 * Constructs a new YSend Event Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSendEventCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YSend Event Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YSendEventCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YSend Event Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSendEventCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYSendEventCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#getView() <em>Get View</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#getView()
	 * @generated
	 */
	public void testGetView() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createTriggerEndpoint() <em>Create Trigger Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createTriggerEndpoint()
	 * @generated
	 */
	public void testCreateTriggerEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createMessageEndpoint() <em>Create Message Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createMessageEndpoint()
	 * @generated
	 */
	public void testCreateMessageEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createAutoTriggerEndpoint() <em>Create Auto Trigger Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createAutoTriggerEndpoint()
	 * @generated
	 */
	public void testCreateAutoTriggerEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createEventTopicEndpoint() <em>Create Event Topic Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand#createEventTopicEndpoint()
	 * @generated
	 */
	public void testCreateEventTopicEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YSendEventCommandTest
