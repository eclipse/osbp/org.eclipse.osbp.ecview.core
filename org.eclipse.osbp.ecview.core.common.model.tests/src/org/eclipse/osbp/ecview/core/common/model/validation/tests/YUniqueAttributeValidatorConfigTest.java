/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YUnique Attribute Validator Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YUniqueAttributeValidatorConfigTest extends TestCase {

	/**
	 * The fixture for this YUnique Attribute Validator Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YUniqueAttributeValidatorConfig fixture = null;

	/**
	 * Constructs a new YUnique Attribute Validator Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YUniqueAttributeValidatorConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YUnique Attribute Validator Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YUniqueAttributeValidatorConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YUnique Attribute Validator Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YUniqueAttributeValidatorConfig getFixture() {
		return fixture;
	}

} //YUniqueAttributeValidatorConfigTest
