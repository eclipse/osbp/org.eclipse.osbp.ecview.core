/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.tests.YListBindingEndpointTest;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableMultiSelectionEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEmbeddable Multi Selection Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YEmbeddableMultiSelectionEndpointTest extends YListBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YEmbeddableMultiSelectionEndpointTest.class);
	}

	/**
	 * Constructs a new YEmbeddable Multi Selection Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddableMultiSelectionEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YEmbeddable Multi Selection Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YEmbeddableMultiSelectionEndpoint getFixture() {
		return (YEmbeddableMultiSelectionEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYEmbeddableMultiSelectionEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YEmbeddableMultiSelectionEndpointTest
