/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YActivateable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YActivateable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YActivateable#isActivated() <em>Activated</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YActivateableTest extends TestCase {

	/**
	 * The fixture for this YActivateable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YActivateable fixture = null;

	/**
	 * Constructs a new YActivateable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YActivateableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YActivateable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YActivateable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YActivateable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YActivateable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YActivateable#isActivated() <em>Activated</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YActivateable#isActivated()
	 * @generated
	 */
	public void testIsActivated() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YActivateable#setActivated(boolean) <em>Activated</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YActivateable#setActivated(boolean)
	 * @generated
	 */
	public void testSetActivated() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YActivateableTest
