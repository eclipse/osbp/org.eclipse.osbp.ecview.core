/**
 */
package org.eclipse.osbp.ecview.core.common.model.datatypes.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatatype;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YDatatype</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YDatatypeTest extends TestCase {

	/**
	 * The fixture for this YDatatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDatatype fixture = null;

	/**
	 * Constructs a new YDatatype test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDatatypeTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YDatatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YDatatype fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YDatatype test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDatatype getFixture() {
		return fixture;
	}

} //YDatatypeTest
