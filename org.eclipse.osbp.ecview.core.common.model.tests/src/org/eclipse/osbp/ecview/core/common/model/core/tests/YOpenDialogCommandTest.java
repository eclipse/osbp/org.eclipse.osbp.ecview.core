/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YOpen Dialog Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand#getView() <em>Get View</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand#createTriggerDialogEndpoint() <em>Create Trigger Dialog Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YOpenDialogCommandTest extends TestCase {

	/**
	 * The fixture for this YOpen Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YOpenDialogCommand fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YOpenDialogCommandTest.class);
	}

	/**
	 * Constructs a new YOpen Dialog Command test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YOpenDialogCommandTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YOpen Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YOpenDialogCommand fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YOpen Dialog Command test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YOpenDialogCommand getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYOpenDialogCommand());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand#getView() <em>Get View</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand#getView()
	 * @generated
	 */
	public void testGetView() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand#createTriggerDialogEndpoint() <em>Create Trigger Dialog Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand#createTriggerDialogEndpoint()
	 * @generated
	 */
	public void testCreateTriggerDialogEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YOpenDialogCommandTest
