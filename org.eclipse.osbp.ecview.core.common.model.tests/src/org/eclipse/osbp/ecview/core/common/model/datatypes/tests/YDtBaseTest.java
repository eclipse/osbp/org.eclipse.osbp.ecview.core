/**
 */
package org.eclipse.osbp.ecview.core.common.model.datatypes.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.datatypes.YDtBase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YDt Base</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YDtBaseTest extends TestCase {

	/**
	 * The fixture for this YDt Base test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDtBase fixture = null;

	/**
	 * Constructs a new YDt Base test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDtBaseTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YDt Base test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YDtBase fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YDt Base test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDtBase getFixture() {
		return fixture;
	}

} //YDtBaseTest
