/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidationConfig;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YRegexp Validation Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YRegexpValidationConfigTest extends TestCase {

	/**
	 * The fixture for this YRegexp Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YRegexpValidationConfig fixture = null;

	/**
	 * Constructs a new YRegexp Validation Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YRegexpValidationConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YRegexp Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YRegexpValidationConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YRegexp Validation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YRegexpValidationConfig getFixture() {
		return fixture;
	}

} //YRegexpValidationConfigTest
