/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YMemento;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YMemento</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YMementoTest extends TestCase {

	/**
	 * The fixture for this YMemento test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMemento fixture = null;

	/**
	 * Constructs a new YMemento test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMementoTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YMemento test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YMemento fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YMemento test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMemento getFixture() {
		return fixture;
	}

} //YMementoTest
