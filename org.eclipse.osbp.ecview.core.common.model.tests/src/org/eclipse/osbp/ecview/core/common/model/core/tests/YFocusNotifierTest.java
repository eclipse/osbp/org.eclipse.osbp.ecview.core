/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YFocusNotifier;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YFocus Notifier</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YFocusNotifierTest extends TestCase {

	/**
	 * The fixture for this YFocus Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFocusNotifier fixture = null;

	/**
	 * Constructs a new YFocus Notifier test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFocusNotifierTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YFocus Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YFocusNotifier fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YFocus Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFocusNotifier getFixture() {
		return fixture;
	}

} //YFocusNotifierTest
