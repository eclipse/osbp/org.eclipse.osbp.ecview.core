/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YEnable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEnable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled() <em>Enabled</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YEnableTest extends TestCase {

	/**
	 * The fixture for this YEnable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEnable fixture = null;

	/**
	 * Constructs a new YEnable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEnableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YEnable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YEnable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YEnable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEnable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled() <em>Enabled</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled()
	 * @generated
	 */
	public void testIsEnabled() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#setEnabled(boolean) <em>Enabled</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEnable#setEnabled(boolean)
	 * @generated
	 */
	public void testSetEnabled() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YEnableTest
