/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YTaggable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YTaggableTest extends TestCase {

	/**
	 * The fixture for this YTaggable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTaggable fixture = null;

	/**
	 * Constructs a new YTaggable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YTaggableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YTaggable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YTaggable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YTaggable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTaggable getFixture() {
		return fixture;
	}

} //YTaggableTest
