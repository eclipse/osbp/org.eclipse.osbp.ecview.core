/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YEditable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEditable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YEditableTest extends TestCase {

	/**
	 * The fixture for this YEditable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEditable fixture = null;

	/**
	 * Constructs a new YEditable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEditableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YEditable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YEditable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YEditable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YEditable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable()
	 * @generated
	 */
	public void testIsEditable() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean) <em>Editable</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean)
	 * @generated
	 */
	public void testSetEditable() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YEditableTest
