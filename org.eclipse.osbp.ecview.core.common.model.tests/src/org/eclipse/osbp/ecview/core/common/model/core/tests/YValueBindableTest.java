/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YValueBindable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YValue Bindable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YValueBindable#createValueEndpoint() <em>Create Value Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YValueBindableTest extends TestCase {

	/**
	 * The fixture for this YValue Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YValueBindable fixture = null;

	/**
	 * Constructs a new YValue Bindable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YValueBindableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YValue Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YValueBindable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YValue Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YValueBindable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YValueBindable#createValueEndpoint() <em>Create Value Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YValueBindable#createValueEndpoint()
	 * @generated
	 */
	public void testCreateValueEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YValueBindableTest
