/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YClass Delegate Validator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YClassDelegateValidatorTest extends YValidatorTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YClassDelegateValidatorTest.class);
	}

	/**
	 * Constructs a new YClass Delegate Validator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YClassDelegateValidatorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YClass Delegate Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YClassDelegateValidator getFixture() {
		return (YClassDelegateValidator)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ValidationFactory.eINSTANCE.createYClassDelegateValidator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YClassDelegateValidatorTest
