/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YDetailBeanSlot;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YDetail Bean Slot</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YDetailBeanSlotTest extends YBeanSlotTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YDetailBeanSlotTest.class);
	}

	/**
	 * Constructs a new YDetail Bean Slot test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDetailBeanSlotTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YDetail Bean Slot test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YDetailBeanSlot getFixture() {
		return (YDetailBeanSlot)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYDetailBeanSlot());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YDetailBeanSlotTest
