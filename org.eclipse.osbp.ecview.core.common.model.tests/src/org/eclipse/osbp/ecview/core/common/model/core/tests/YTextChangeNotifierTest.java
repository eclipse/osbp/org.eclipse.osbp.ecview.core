/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YTextChangeNotifier;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YText Change Notifier</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YTextChangeNotifierTest extends TestCase {

	/**
	 * The fixture for this YText Change Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTextChangeNotifier fixture = null;

	/**
	 * Constructs a new YText Change Notifier test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YTextChangeNotifierTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YText Change Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YTextChangeNotifier fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YText Change Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YTextChangeNotifier getFixture() {
		return fixture;
	}

} //YTextChangeNotifierTest
