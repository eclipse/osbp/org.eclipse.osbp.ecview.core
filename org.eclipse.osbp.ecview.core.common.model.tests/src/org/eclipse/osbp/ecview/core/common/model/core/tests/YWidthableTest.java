/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YWidthable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YWidthable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YWidthableTest extends TestCase {

	/**
	 * The fixture for this YWidthable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YWidthable fixture = null;

	/**
	 * Constructs a new YWidthable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YWidthableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YWidthable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YWidthable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YWidthable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YWidthable getFixture() {
		return fixture;
	}

} //YWidthableTest
