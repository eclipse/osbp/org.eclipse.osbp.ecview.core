/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.tests.YValueBindingEndpointTest;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableValueEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEmbeddable Value Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YEmbeddableValueEndpointTest extends YValueBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YEmbeddableValueEndpointTest.class);
	}

	/**
	 * Constructs a new YEmbeddable Value Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddableValueEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YEmbeddable Value Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YEmbeddableValueEndpoint getFixture() {
		return (YEmbeddableValueEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYEmbeddableValueEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YEmbeddableValueEndpointTest
