/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YMarginable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YMarginable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YMarginableTest extends TestCase {

	/**
	 * The fixture for this YMarginable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMarginable fixture = null;

	/**
	 * Constructs a new YMarginable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMarginableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YMarginable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YMarginable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YMarginable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMarginable getFixture() {
		return fixture;
	}

} //YMarginableTest
