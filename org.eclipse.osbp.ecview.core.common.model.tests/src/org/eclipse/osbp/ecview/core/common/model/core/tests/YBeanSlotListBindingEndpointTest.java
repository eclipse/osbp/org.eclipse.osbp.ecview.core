/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.tests.YListBindingEndpointTest;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotListBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBean Slot List Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YBeanSlotListBindingEndpointTest extends YListBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YBeanSlotListBindingEndpointTest.class);
	}

	/**
	 * Constructs a new YBean Slot List Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBeanSlotListBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YBean Slot List Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YBeanSlotListBindingEndpoint getFixture() {
		return (YBeanSlotListBindingEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYBeanSlotListBindingEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YBeanSlotListBindingEndpointTest
