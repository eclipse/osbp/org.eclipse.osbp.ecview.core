/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YMin Length Validator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YMinLengthValidatorTest extends YValidatorTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YMinLengthValidatorTest.class);
	}

	/**
	 * Constructs a new YMin Length Validator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMinLengthValidatorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YMin Length Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YMinLengthValidator getFixture() {
		return (YMinLengthValidator)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ValidationFactory.eINSTANCE.createYMinLengthValidator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YMinLengthValidatorTest
