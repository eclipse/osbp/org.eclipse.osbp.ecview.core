/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelListBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEC View Model List Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YECViewModelListBindingEndpointTest extends YListBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YECViewModelListBindingEndpointTest.class);
	}

	/**
	 * Constructs a new YEC View Model List Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YECViewModelListBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YEC View Model List Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YECViewModelListBindingEndpoint getFixture() {
		return (YECViewModelListBindingEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(BindingFactory.eINSTANCE.createYECViewModelListBindingEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YECViewModelListBindingEndpointTest
