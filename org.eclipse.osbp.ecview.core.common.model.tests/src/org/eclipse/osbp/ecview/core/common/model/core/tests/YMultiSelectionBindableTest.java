/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YMulti Selection Bindable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable#createMultiSelectionEndpoint() <em>Create Multi Selection Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YMultiSelectionBindableTest extends TestCase {

	/**
	 * The fixture for this YMulti Selection Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMultiSelectionBindable fixture = null;

	/**
	 * Constructs a new YMulti Selection Bindable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMultiSelectionBindableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YMulti Selection Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YMultiSelectionBindable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YMulti Selection Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YMultiSelectionBindable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable#createMultiSelectionEndpoint() <em>Create Multi Selection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YMultiSelectionBindable#createMultiSelectionEndpoint()
	 * @generated
	 */
	public void testCreateMultiSelectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YMultiSelectionBindableTest
