/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.YBeanValidationValidator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBean Validation Validator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YBeanValidationValidatorTest extends YValidatorTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YBeanValidationValidatorTest.class);
	}

	/**
	 * Constructs a new YBean Validation Validator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBeanValidationValidatorTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YBean Validation Validator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YBeanValidationValidator getFixture() {
		return (YBeanValidationValidator)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ValidationFactory.eINSTANCE.createYBeanValidationValidator());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YBeanValidationValidatorTest
