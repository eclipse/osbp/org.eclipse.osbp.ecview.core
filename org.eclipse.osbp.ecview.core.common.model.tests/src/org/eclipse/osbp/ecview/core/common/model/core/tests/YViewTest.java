/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YView</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YView#getOrCreateBindingSet() <em>Get Or Create Binding Set</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YAlignmentContainer#applyAlignment(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable, org.eclipse.osbp.ecview.core.common.model.core.YAlignment) <em>Apply Alignment</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YViewTest extends TestCase {

	/**
	 * The fixture for this YView test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YView fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YViewTest.class);
	}

	/**
	 * Constructs a new YView test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YViewTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YView test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YView fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YView test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YView getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYView());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YView#getOrCreateBindingSet() <em>Get Or Create Binding Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YView#getOrCreateBindingSet()
	 * @generated
	 */
	public void testGetOrCreateBindingSet() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YAlignmentContainer#applyAlignment(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable, org.eclipse.osbp.ecview.core.common.model.core.YAlignment) <em>Apply Alignment</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YAlignmentContainer#applyAlignment(org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable, org.eclipse.osbp.ecview.core.common.model.core.YAlignment)
	 * @generated
	 */
	public void testApplyAlignment__YEmbeddable_YAlignment() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YViewTest
