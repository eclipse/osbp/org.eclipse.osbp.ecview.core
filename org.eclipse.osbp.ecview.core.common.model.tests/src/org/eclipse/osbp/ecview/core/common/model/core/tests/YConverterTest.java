/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YConverter</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YConverterTest extends TestCase {

	/**
	 * The fixture for this YConverter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YConverter fixture = null;

	/**
	 * Constructs a new YConverter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YConverterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YConverter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YConverter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YConverter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YConverter getFixture() {
		return fixture;
	}

} //YConverterTest
