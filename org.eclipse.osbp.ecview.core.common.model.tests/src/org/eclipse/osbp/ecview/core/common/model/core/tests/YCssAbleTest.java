/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YCssAble;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YCss Able</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YCssAbleTest extends TestCase {

	/**
	 * The fixture for this YCss Able test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YCssAble fixture = null;

	/**
	 * Constructs a new YCss Able test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YCssAbleTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YCss Able test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YCssAble fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YCss Able test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YCssAble getFixture() {
		return fixture;
	}

} //YCssAbleTest
