/**
 */
package org.eclipse.osbp.ecview.core.common.model.visibility.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityFactory;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YVisibility Processor</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YVisibilityProcessorTest extends TestCase {

	/**
	 * The fixture for this YVisibility Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVisibilityProcessor fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YVisibilityProcessorTest.class);
	}

	/**
	 * Constructs a new YVisibility Processor test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVisibilityProcessorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YVisibility Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YVisibilityProcessor fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YVisibility Processor test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVisibilityProcessor getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(VisibilityFactory.eINSTANCE.createYVisibilityProcessor());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YVisibilityProcessorTest
