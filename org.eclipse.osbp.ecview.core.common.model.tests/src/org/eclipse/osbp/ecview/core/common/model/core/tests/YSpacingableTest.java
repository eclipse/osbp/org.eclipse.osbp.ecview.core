/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YSpacingable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YSpacingable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YSpacingableTest extends TestCase {

	/**
	 * The fixture for this YSpacingable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSpacingable fixture = null;

	/**
	 * Constructs a new YSpacingable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSpacingableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YSpacingable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YSpacingable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YSpacingable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSpacingable getFixture() {
		return fixture;
	}

} //YSpacingableTest
