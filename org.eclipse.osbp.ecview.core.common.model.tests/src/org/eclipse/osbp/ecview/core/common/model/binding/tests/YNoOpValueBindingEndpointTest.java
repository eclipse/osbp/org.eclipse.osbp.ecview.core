/**
 */
package org.eclipse.osbp.ecview.core.common.model.binding.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingFactory;
import org.eclipse.osbp.ecview.core.common.model.binding.YNoOpValueBindingEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YNo Op Value Binding Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YNoOpValueBindingEndpointTest extends YValueBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YNoOpValueBindingEndpointTest.class);
	}

	/**
	 * Constructs a new YNo Op Value Binding Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YNoOpValueBindingEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YNo Op Value Binding Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YNoOpValueBindingEndpoint getFixture() {
		return (YNoOpValueBindingEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(BindingFactory.eINSTANCE.createYNoOpValueBindingEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YNoOpValueBindingEndpointTest
