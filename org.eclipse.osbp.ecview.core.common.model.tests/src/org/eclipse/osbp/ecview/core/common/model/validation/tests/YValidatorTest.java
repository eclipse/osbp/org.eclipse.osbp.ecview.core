/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YValidator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YValidator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YValidatorTest extends TestCase {

	/**
	 * The fixture for this YValidator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YValidator fixture = null;

	/**
	 * Constructs a new YValidator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YValidatorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YValidator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YValidator fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YValidator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YValidator getFixture() {
		return fixture;
	}

} //YValidatorTest
