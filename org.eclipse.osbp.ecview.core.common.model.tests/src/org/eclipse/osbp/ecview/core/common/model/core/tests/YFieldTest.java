/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YField;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YField</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled() <em>Enabled</em>}</li>
 * </ul>
 * </p>
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YField#addValueChangeListener(org.eclipse.osbp.ecview.core.common.model.core.listeners.YValueChangeListener) <em>Add Value Change Listener</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YField#removeValueChangeListener(org.eclipse.osbp.ecview.core.common.model.core.listeners.YValueChangeListener) <em>Remove Value Change Listener</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YField#removeAllValueChangListeners() <em>Remove All Value Chang Listeners</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YFieldTest extends YEmbeddableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YFieldTest.class);
	}

	/**
	 * Constructs a new YField test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFieldTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YField test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YField getFixture() {
		return (YField)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYField());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable()
	 * @generated
	 */
	public void testIsEditable() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean) <em>Editable</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean)
	 * @generated
	 */
	public void testSetEditable() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled() <em>Enabled</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled()
	 * @generated
	 */
	public void testIsEnabled() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#setEnabled(boolean) <em>Enabled</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEnable#setEnabled(boolean)
	 * @generated
	 */
	public void testSetEnabled() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YField#addValueChangeListener(org.eclipse.osbp.ecview.core.common.model.core.listeners.YValueChangeListener) <em>Add Value Change Listener</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YField#addValueChangeListener(org.eclipse.osbp.ecview.core.common.model.core.listeners.YValueChangeListener)
	 * @generated
	 */
	public void testAddValueChangeListener__YValueChangeListener() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YField#removeValueChangeListener(org.eclipse.osbp.ecview.core.common.model.core.listeners.YValueChangeListener) <em>Remove Value Change Listener</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YField#removeValueChangeListener(org.eclipse.osbp.ecview.core.common.model.core.listeners.YValueChangeListener)
	 * @generated
	 */
	public void testRemoveValueChangeListener__YValueChangeListener() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YField#removeAllValueChangListeners() <em>Remove All Value Chang Listeners</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YField#removeAllValueChangListeners()
	 * @generated
	 */
	public void testRemoveAllValueChangListeners() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YFieldTest
