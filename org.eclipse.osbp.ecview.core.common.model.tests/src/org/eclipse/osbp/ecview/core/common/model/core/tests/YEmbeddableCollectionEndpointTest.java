/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.binding.tests.YListBindingEndpointTest;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableCollectionEndpoint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YEmbeddable Collection Endpoint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YEmbeddableCollectionEndpointTest extends YListBindingEndpointTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YEmbeddableCollectionEndpointTest.class);
	}

	/**
	 * Constructs a new YEmbeddable Collection Endpoint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YEmbeddableCollectionEndpointTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YEmbeddable Collection Endpoint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YEmbeddableCollectionEndpoint getFixture() {
		return (YEmbeddableCollectionEndpoint)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYEmbeddableCollectionEndpoint());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YEmbeddableCollectionEndpointTest
