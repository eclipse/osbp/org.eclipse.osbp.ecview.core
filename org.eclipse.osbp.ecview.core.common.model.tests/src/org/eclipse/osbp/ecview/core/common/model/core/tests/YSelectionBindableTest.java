/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YSelection Bindable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YSelectionBindableTest extends TestCase {

	/**
	 * The fixture for this YSelection Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSelectionBindable fixture = null;

	/**
	 * Constructs a new YSelection Bindable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSelectionBindableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YSelection Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YSelectionBindable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YSelection Bindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YSelectionBindable getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint() <em>Create Selection Endpoint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable#createSelectionEndpoint()
	 * @generated
	 */
	public void testCreateSelectionEndpoint() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YSelectionBindableTest
