/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.authorization.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.authorization.AuthorizationFactory;
import org.eclipse.osbp.ecview.core.common.model.core.authorization.YFieldAuthorization;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YField Authorization</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YFieldAuthorizationTest extends TestCase {

	/**
	 * The fixture for this YField Authorization test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFieldAuthorization fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YFieldAuthorizationTest.class);
	}

	/**
	 * Constructs a new YField Authorization test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YFieldAuthorizationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YField Authorization test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YFieldAuthorization fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YField Authorization test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YFieldAuthorization getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(AuthorizationFactory.eINSTANCE.createYFieldAuthorization());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YFieldAuthorizationTest
