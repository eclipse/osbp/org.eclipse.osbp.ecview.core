/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YBindable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBindable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YBindableTest extends TestCase {

	/**
	 * The fixture for this YBindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBindable fixture = null;

	/**
	 * Constructs a new YBindable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBindableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBindable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBindable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBindable getFixture() {
		return fixture;
	}

} //YBindableTest
