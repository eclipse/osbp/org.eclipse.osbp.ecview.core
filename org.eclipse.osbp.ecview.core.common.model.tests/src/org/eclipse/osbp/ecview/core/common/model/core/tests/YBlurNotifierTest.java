/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YBlurNotifier;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YBlur Notifier</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YBlurNotifierTest extends TestCase {

	/**
	 * The fixture for this YBlur Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBlurNotifier fixture = null;

	/**
	 * Constructs a new YBlur Notifier test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YBlurNotifierTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YBlur Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YBlurNotifier fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YBlur Notifier test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YBlurNotifier getFixture() {
		return fixture;
	}

} //YBlurNotifierTest
