/**
 */
package org.eclipse.osbp.ecview.core.common.model.validation.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.validation.YValidationConfig;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YValidation Config</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class YValidationConfigTest extends TestCase {

	/**
	 * The fixture for this YValidation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YValidationConfig fixture = null;

	/**
	 * Constructs a new YValidation Config test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YValidationConfigTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YValidation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YValidationConfig fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YValidation Config test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YValidationConfig getFixture() {
		return fixture;
	}

} //YValidationConfigTest
