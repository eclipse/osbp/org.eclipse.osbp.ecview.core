/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YDelegateConverter;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YDelegate Converter</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YDelegateConverterTest extends TestCase {

	/**
	 * The fixture for this YDelegate Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDelegateConverter fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YDelegateConverterTest.class);
	}

	/**
	 * Constructs a new YDelegate Converter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YDelegateConverterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YDelegate Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YDelegateConverter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YDelegate Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YDelegateConverter getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYDelegateConverter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YDelegateConverterTest
