/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.core.common.model.core.YHelperLayoutProvider;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YHelper Layout Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YHelperLayoutProvider#getHelperLayout() <em>Get Helper Layout</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public abstract class YHelperLayoutProviderTest extends TestCase {

	/**
	 * The fixture for this YHelper Layout Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YHelperLayoutProvider fixture = null;

	/**
	 * Constructs a new YHelper Layout Provider test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YHelperLayoutProviderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YHelper Layout Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YHelperLayoutProvider fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YHelper Layout Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YHelperLayoutProvider getFixture() {
		return fixture;
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YHelperLayoutProvider#getHelperLayout() <em>Get Helper Layout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YHelperLayoutProvider#getHelperLayout()
	 * @generated
	 */
	public void testGetHelperLayout() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YHelperLayoutProviderTest
