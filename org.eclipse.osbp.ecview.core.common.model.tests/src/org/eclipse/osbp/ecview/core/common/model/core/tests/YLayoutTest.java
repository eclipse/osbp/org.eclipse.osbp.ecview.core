/**
 */
package org.eclipse.osbp.ecview.core.common.model.core.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YLayout</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are tested:
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled() <em>Enabled</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class YLayoutTest extends YEmbeddableTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YLayoutTest.class);
	}

	/**
	 * Constructs a new YLayout test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YLayoutTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this YLayout test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected YLayout getFixture() {
		return (YLayout)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreModelFactory.eINSTANCE.createYLayout());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable() <em>Editable</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#isEditable()
	 * @generated
	 */
	public void testIsEditable() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean) <em>Editable</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEditable#setEditable(boolean)
	 * @generated
	 */
	public void testSetEditable() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled() <em>Enabled</em>}' feature getter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEnable#isEnabled()
	 * @generated
	 */
	public void testIsEnabled() {
		// TODO: implement this feature getter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link org.eclipse.osbp.ecview.core.common.model.core.YEnable#setEnabled(boolean) <em>Enabled</em>}' feature setter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YEnable#setEnabled(boolean)
	 * @generated
	 */
	public void testSetEnabled() {
		// TODO: implement this feature setter test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //YLayoutTest
