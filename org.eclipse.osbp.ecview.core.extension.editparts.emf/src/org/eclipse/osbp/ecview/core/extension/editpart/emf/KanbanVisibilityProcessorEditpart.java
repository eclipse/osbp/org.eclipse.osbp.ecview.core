/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.osbp.ecview.core.common.editpart.emf.visibility.VisibilityProcessorEditpart;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanbanVisibilityProcessor;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IKanbanVisibilityProcessor;

public class KanbanVisibilityProcessorEditpart extends VisibilityProcessorEditpart<YKanbanVisibilityProcessor>
		implements IKanbanVisibilityProcessor {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.visibility.
	 * VisibilityProcessorEditpart#loadProcessor()
	 */
	protected IVisibilityProcessor loadProcessor() {
		if (processor != null) {
			return processor;
		}

		KanbanVisibilityProcessor temp = new KanbanVisibilityProcessor();
		temp.setup(getViewContext(getModel()), getModel());

		processor = temp;

		return processor;
	}

}
