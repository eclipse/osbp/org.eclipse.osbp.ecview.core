/**
 * Copyright (c) 2013 Loetz GmbH&Co.KG(Heidelberg). All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0  which accompanies this distribution,
t https://www.eclipse.org/legal/epl-2.0/
t
t SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: Christophe Loetz (Loetz GmbH&Co.KG) - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.osbp.ecview.core.common.editpart.emf.ActionEditpart;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YButton;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IButtonEditpart;

/**
 * The implementation of the IButtonEditpart.
 */
public class ButtonEditpart extends ActionEditpart<YButton> implements
		IButtonEditpart {

	protected YButton createModel() {
		return (YButton) ExtensionModelFactory.eINSTANCE
				.createYButton();
	}

}
