/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.osbp.ecview.core.common.editpart.emf.FieldEditpart;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YComboBox;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IComboBoxEditpart;

/**
 * The implementation of the IUiComboBoxEditpart.
 */
public class ComboBoxEditpart extends FieldEditpart<YComboBox> implements
		IComboBoxEditpart {

	public ComboBoxEditpart() {
		super(ExtensionModelPackage.Literals.YCOMBO_BOX__DATATYPE);
	}

}
