/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.filter.IFilterProvidingPresentation;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IFilteringComponentEditpart;

/**
 * The implementation of the ISearchPanelEditpart.
 */
public class FilteringComponentEditpart extends EmbeddableEditpart<YFilteringComponent> implements
		IFilteringComponentEditpart {

	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		switch (featureId) {
		case ExtensionModelPackage.YFILTERING_COMPONENT__APPLY_FILTER:
			applyFilter();
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/**
	 * Accesses the presentation to return the filter and puts the new filter in
	 * the model. So other parties can react for the changed filter.
	 */
	private void applyFilter() {
		IFilterProvidingPresentation presentation = (IFilterProvidingPresentation) getPresentation();
		Object filter = presentation.getFilter();
		getModel().setFilter(filter);
	}

	@Override
	public List<IValidatorEditpart> getDatatypeValidators() {
		return null;
	}

	@Override
	public Object getFilter() {
		return null;
	}

}
