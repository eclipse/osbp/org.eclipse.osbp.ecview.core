/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.presentation.DelegatingPresenterFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTab;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTabSheet;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.ITabEditpart;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.ITabSheetEditpart;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.presentation.ITabPresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TabEditpart extends ElementEditpart<YTab> implements ITabEditpart {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(EmbeddableEditpart.class);
	private ITabPresentation<?> presentation;

	/**
	 * The default constructor.
	 */
	protected TabEditpart() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ITabSheetEditpart getParent() {
		YTabSheet yParent = getModel().getParent();
		return yParent != null ? (ITabSheetEditpart) getEditpart(viewContext, yParent)
				: null;
	}

	@Override
	public IViewEditpart getView() {
		YView yView = getModel().getView();
		return yView != null ? (IViewEditpart) getEditpart(viewContext, yView) : null;
	}

	/**
	 * Returns the instance of the presentation, but does not load it.
	 * 
	 * @param <A>
	 *            An instance of {@link IWidgetPresentation}
	 * @return presentation
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IWidgetPresentation<?>> A internalGetPresentation() {
		return (A) presentation;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <A extends ITabPresentation<?>> A getPresentation() {
		if (presentation == null) {
			presentation = createPresenter();
		}
		return (A) presentation;
	}

	/**
	 * Is called to created the presenter for this edit part.
	 */
	protected <A extends IWidgetPresentation<?>> A createPresenter() {
		IViewEditpart viewEditPart = getView();
		if (viewEditPart == null) {
			LOGGER.info("View is null");
			return null;
		}
		return DelegatingPresenterFactory.getInstance().createPresentation(
				viewEditPart.getContext(), this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			// if directly attached to a view, then remove it
//			ITabSheetEditpart parent = getParent();
//			if (parent != null) {
//				parent.removeTab(this);
//			}

			// dispose the presenter
			//
			if (presentation != null) {
				presentation.dispose();
				presentation = null;
			}

		} finally {
			super.internalDispose();
		}
	}
	
	@Override
	public void requestRender() {
		if (getParent() != null) {
			getParent().renderTab(this);
		} else {
			unrender();
		}
	}

	@Override
	public Object render(Object parentWidget) {
		return getPresentation().createWidget(parentWidget);
	}

	@Override
	public void requestUnrender() {
		if (getParent() != null) {
			getParent().unrenderTab(this);
		} else {
			unrender();
		}
	}

	@Override
	public void unrender() {
		getPresentation().unrender();
	}

	@Override
	public boolean isRendered() {
		return internalGetPresentation() != null
				&& internalGetPresentation().isRendered();
	}

	@Override
	public Object getWidget() {
		return getPresentation().getWidget();
	}

	@Override
	public void requestDispose() {
		if (getParent() != null) {
			getParent().disposeTab(this);
		} else {
			dispose();
		}
	}

}
