/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes;

import org.eclipse.osbp.ecview.core.common.editpart.emf.datatypes.DatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextAreaDatatype;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.datatypes.ITextAreaDatatypeEditpart;

/**
 * An edit part for datatypes.
 */
public class TextAreaDatatypeEditpart extends
		DatatypeEditpart<YTextAreaDatatype> implements
		ITextAreaDatatypeEditpart {

}
