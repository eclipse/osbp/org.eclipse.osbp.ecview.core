/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.EmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanban;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IKanbanEditpart;
import org.eclipse.osbp.runtime.common.validation.IStatus;

/**
 * The implementation of the IUiCheckBoxEditpart.
 */
public class KanbanEditpart extends EmbeddableEditpart<YKanban> implements IKanbanEditpart {

	public KanbanEditpart() {
		super();
	}

	@Override
	public void addValidator(IValidatorEditpart validator) {
		
	}

	@Override
	public void removeValidator(IValidatorEditpart validator) {
		
	}

	@Override
	public void addExternalStatus(IStatus status) {
		
	}

	@Override
	public void removeExternalStatus(IStatus status) {
		
	}

	@Override
	public void resetExternalStatus() {
		
	}

	@Override
	public List<IValidatorEditpart> getValidators() {
		return null;
	}

	@Override
	public IConverterEditpart getConverter() {
		return null;
	}

	@Override
	public List<IValidatorEditpart> getDatatypeValidators() {
		return null;
	}

}
