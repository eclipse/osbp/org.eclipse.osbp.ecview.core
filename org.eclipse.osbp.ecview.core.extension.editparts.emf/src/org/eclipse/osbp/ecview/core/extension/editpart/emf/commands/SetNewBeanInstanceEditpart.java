/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf.commands;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableValueEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.CommandEditpart;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSetNewBeanInstanceCommand;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.commands.ISetNewInstanceCommandEditpart;
import org.eclipse.osbp.runtime.common.state.ISharedStateContext;
import org.eclipse.osbp.runtime.common.types.ITypeProviderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SetNewBeanInstanceEditpart extends
		CommandEditpart<YSetNewBeanInstanceCommand> implements
		ISetNewInstanceCommandEditpart {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SetNewBeanInstanceEditpart.class);

	private boolean activated;
	private Binding triggerBinding;

	@Override
	public void activate() {
		// bind the values
		IECViewBindingManager bindingManager = getView().getContext()
				.getService(IECViewBindingManager.class.getName());

		// Bind the trigger
		IBindableValueEndpointEditpart triggerEPEditpart = (IBindableValueEndpointEditpart) getEditpart(viewContext, getModel()
				.createTriggerEndpoint());
		IObservableValue modelObservable = triggerEPEditpart.getObservable();
		IObservableValue targetObservable = PojoObservables.observeValue(this,
				"trigger");
		triggerBinding = bindingManager.bindValue(targetObservable,
				modelObservable, new UpdateValueStrategy(
						UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE));
		activated = true;
	}

	@Override
	public void execute() {
		try {
			Object newEntry = createNewBean();
			if (newEntry != null) {

				// add the entry to the shared state, if available
				ISharedStateContext sharedState = getView().getContext()
						.getService(ISharedStateContext.class.getName());
				if (sharedState != null) {
					sharedState.addNewTransient(newEntry);
				}

				// now pass the value to the target
				IBindableValueEndpointEditpart valueEPEditpart = (IBindableValueEndpointEditpart) getEditpart(viewContext, getModel()
						.getTarget());

				// TODO workaround for databinding -> New instance may be
				// polymorphic brother of the current instance. And if binding
				// the new instance, numeric field will not become unbound. So
				// lets set a new instance of current set instance before
				// setting the new entry.
				IObservableValue observableValue = valueEPEditpart
						.getObservable();
				Object value = observableValue.getValue();
				if (value != null) {
					Class<?> valueClass = value.getClass();
					try {
						// now all fields will become unbound from the current
						// instance
						observableValue.setValue(valueClass.newInstance());
					} catch (Exception e) {
						LOGGER.warn("Could not reset the value by {}",
								valueClass.getName());
					}
				}

				// and now set the new value
				observableValue.setValue(newEntry);
			}
		} catch (Exception e) {
			LOGGER.error("{}", e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Tries to create a new bean.
	 * 
	 * @return
	 */
	protected Object createNewBean() {

		Class<?> beanClass = getModel().getType();
		Object result = null;
		if (beanClass != null) {
			try {
				result = beanClass.newInstance();
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			}
		}

		if (result == null) {
			ITypeProviderService service = getViewContext(getModel())
					.getService(ITypeProviderService.class.getName());
			beanClass = service
					.forName(null, getModel().getTypeQualifiedName());
			try {
				if (beanClass != null) {
					result = beanClass.newInstance();
				}
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			}
		}

		if (result == null) {
			LOGGER.error("Could not create new bean instance for "
					+ getModel().getTypeQualifiedName());
		}

		return result;
	}

	/**
	 * Setting a value will trigger the command execution.
	 * 
	 * @param value
	 */
	public void setTrigger(Object value) {
		// execute the command
		if (activated && value != null) {
			execute();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			if (triggerBinding != null) {
				triggerBinding.dispose();
				triggerBinding = null;
			}
		} finally {
			super.internalDispose();
		}
	}

}
