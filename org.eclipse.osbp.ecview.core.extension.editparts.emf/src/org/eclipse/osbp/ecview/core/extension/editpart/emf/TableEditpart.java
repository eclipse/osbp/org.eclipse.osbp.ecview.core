/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.ILayoutEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.FieldEditpart;
import org.eclipse.osbp.ecview.core.common.filter.IFilterablePresentation;
import org.eclipse.osbp.ecview.core.common.filter.IRefreshRowsPresentation;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTable;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.ITableEditpart;

/**
 * The implementation of the IUiTableEditpart.
 */
public class TableEditpart extends FieldEditpart<YTable> implements
		ITableEditpart {

	public TableEditpart() {
		super(ExtensionModelPackage.Literals.YTABLE__DATATYPE);
	}

	/**
	 * {@inheritDoc}
	 */
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case ExtensionModelPackage.YTABLE__FILTER:
			IFilterablePresentation presentation = getPresentation();
			presentation.applyFilter(notification.getNewValue());
			break;
		case ExtensionModelPackage.YTABLE__REFRESH:
			if (isPresentationPresent()) {
				IRefreshRowsPresentation refresher = getPresentation();
				refresher.refreshRows();
			}
			break;
		default:
			super.handleModelSet(featureId, notification);
		}

	}

	@Override
	protected void handleModelMove(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case ExtensionModelPackage.YTABLE__COLUMNS:
			if (isPresentationPresent()) {
				if (getParent() instanceof ILayoutEditpart) {
					ILayoutEditpart parent = (ILayoutEditpart) getParent();
					parent.unrenderChild((IEmbeddableEditpart) getEditpart());
					parent.renderChild((IEmbeddableEditpart) getEditpart());
				}
			}
			break;
		default:
			super.handleModelMove(featureId, notification);
		}
	}

}
