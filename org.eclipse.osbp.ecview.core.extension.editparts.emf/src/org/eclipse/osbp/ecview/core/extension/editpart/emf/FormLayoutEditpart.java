/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.LayoutEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayoutCellStyle;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayoutCellStyle;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.IFormLayoutEditpart;

/**
 * The implementation of the IUigridLayoutEditpart.
 */
public class FormLayoutEditpart extends LayoutEditpart<YFormLayout> implements
		IFormLayoutEditpart {

	private StylesController stylesController;

	@Override
	public void initialize(IViewContext context, YFormLayout model) {
		super.initialize(context, model);

		stylesController = new StylesController();
		model.eAdapters().add(stylesController);
	}

	@Override
	protected void internalDispose() {
		try {
			getModel().eAdapters().remove(stylesController);
			stylesController = null;
		} finally {
			super.internalDispose();
		}
	}

	protected void updateCellStyle(YEmbeddable target) {
		if (isRendered() && !isDisposing()) {
			getPresentation().updateCellStyle(target);
		}
	}

	/**
	 * This class automatically connects to all {@link YAbsoluteLayoutCellStyle}
	 */
	public class StylesController extends EContentAdapter {

		/**
		 * Add this adapter only to EObjects from grid package.
		 */
		protected void addAdapter(Notifier notifier) {
			if (notifier instanceof EObject) {
				EObject e = (EObject) notifier;
				if (e instanceof YFormLayoutCellStyle) {
					notifier.eAdapters().add(this);
				}
			}
		}

		public void notifyChanged(Notification notification) {
			super.notifyChanged(notification);

			// notify the layout to update the cell style
			if (notification.getNotifier() instanceof YFormLayoutCellStyle) {
				updateCellStyle(((YFormLayoutCellStyle) notification
						.getNotifier()).getTarget());
			}
		}
	}

}
