/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.FieldEditpart;
import org.eclipse.osbp.ecview.core.common.notification.IReloadRequestService;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextField;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.ISuggestTextFieldEditpart;

public class SuggestTextFieldEditpart extends FieldEditpart<YSuggestTextField>
		implements ISuggestTextFieldEditpart,
		IReloadRequestService.ModeListener {

	public SuggestTextFieldEditpart() {
		super(ExtensionModelPackage.Literals.YSUGGEST_TEXT_FIELD__DATATYPE);
	}


	@Override
	public void notifyMode(boolean enabled) {
		getModel().setUseSuggestions(enabled);
	}

	protected void doInitPresentation(IWidgetPresentation<?> presentation) {
		super.doInitPresentation(presentation);

		IViewContext context = getView().getContext();
		IReloadRequestService service = context
				.getService(IReloadRequestService.class.getName());
		if (service != null) {
			service.addListener(this);
		}
	}

	@Override
	protected void internalDispose() {
		try {
			IViewContext context = getView().getContext();
			IReloadRequestService service = context
					.getService(IReloadRequestService.class.getName());
			if (service != null) {
				service.removeListener(this);
			}
		} finally {
			super.internalDispose();
		}
	}

}
