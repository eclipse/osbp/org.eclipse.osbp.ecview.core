/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.extension.editpart.emf;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.common.AbstractEditpartManager;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.commands.AddToTableEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.commands.RemoveFromTableEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.commands.SetNewBeanInstanceEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.BrowserDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.CheckBoxDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.ComboBoxDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.DateTimeDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.DecimalDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.ListDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.NumericDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.OptionsGroupDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.ProgressBarDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TabSheetDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TableDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TextDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.editpart.emf.datatypes.TreeDatatypeEditpart;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YBrowserDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YCheckBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YComboBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDecimalDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YListDataType;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YNumericDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YOptionsGroupDataType;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YProgressBarDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTabSheetDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTableDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextAreaDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTreeDatatype;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAbsoluteLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YAddToTableCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanReferenceField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBooleanSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBrowser;
import org.eclipse.osbp.ecview.core.extension.model.extension.YButton;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCheckBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YComboBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YCssLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDateTime;
import org.eclipse.osbp.ecview.core.extension.model.extension.YDecimalField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumComboBox;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumList;
import org.eclipse.osbp.ecview.core.extension.model.extension.YEnumOptionsGroup;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFormLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YGridLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YHorizontalLayout;
import org.eclipse.osbp.ecview.core.extension.model.extension.YImage;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanban;
import org.eclipse.osbp.ecview.core.extension.model.extension.YKanbanVisibilityProcessor;
import org.eclipse.osbp.ecview.core.extension.model.extension.YLabel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YList;
import org.eclipse.osbp.ecview.core.extension.model.extension.YMasterDetail;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YNumericSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YOptionsGroup;
import org.eclipse.osbp.ecview.core.extension.model.extension.YPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YPasswordField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YProgressBar;
import org.eclipse.osbp.ecview.core.extension.model.extension.YRemoveFromTableCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSetNewBeanInstanceCommand;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSlider;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSplitPanel;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSuggestTextField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTab;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTabSheet;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTable;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextArea;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextSearchField;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTree;
import org.eclipse.osbp.ecview.core.extension.model.extension.YVerticalLayout;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;

/**
 * An implementation of IEditPartManager for eObjects with
 * nsURI=http://eclipse.org/emf/emfclient/uimodel.
 */
@Component(immediate = true, service = { IEditPartManager.class })
public class EditpartManager extends AbstractEditpartManager {

	protected void activate(ComponentContext context) {

	}

	protected void deactivate(ComponentContext context) {

	}

	@Override
	public boolean isFor(Object element) {
		if (element instanceof EObject) {
			String uriString = ((EObject) element).eClass().getEPackage().getNsURI();
			return uriString.equals(ExtensionModelPackage.eNS_URI) || uriString.equals(ExtDatatypesPackage.eNS_URI);
		} else if (element instanceof String) {
			return element.equals(ExtensionModelPackage.eNS_URI) || element.equals(ExtDatatypesPackage.eNS_URI);
		}
		return false;
	}

	/**
	 * Creates a new instance of the edit part.
	 * 
	 * @param <A>
	 *            an instance of {@link IElementEditpart}
	 * @param yElement
	 *            the model element
	 * @return editpart
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IElementEditpart> A createEditpart(IViewContext context, Object yElement) {
		// asserts that no editpart was created already for the given element
		assertOneEditpartForModelelement(yElement);

		ElementEditpart<YElement> result = null;
		if (yElement instanceof YTextField) {
			result = createNewInstance(TextFieldEditpart.class);
		} else if (yElement instanceof YGridLayout) {
			result = createNewInstance(GridLayoutEditpart.class);
		} else if (yElement instanceof YCssLayout) {
			result = createNewInstance(CssLayoutEditpart.class);
		} else if (yElement instanceof YHorizontalLayout) {
			result = createNewInstance(HorizontalLayoutEditpart.class);
		} else if (yElement instanceof YVerticalLayout) {
			result = createNewInstance(VerticalLayoutEditpart.class);
		} else if (yElement instanceof YFormLayout) {
			result = createNewInstance(FormLayoutEditpart.class);
		} else if (yElement instanceof YTable) {
			result = createNewInstance(TableEditpart.class);
		} else if (yElement instanceof YTree) {
			result = createNewInstance(TreeEditpart.class);
		} else if (yElement instanceof YLabel) {
			result = createNewInstance(LabelEditpart.class);
		} else if (yElement instanceof YTextArea) {
			result = createNewInstance(TextAreaEditpart.class);
		} else if (yElement instanceof YButton) {
			result = createNewInstance(ButtonEditpart.class);
		} else if (yElement instanceof YCheckBox) {
			result = createNewInstance(CheckBoxEditpart.class);
		} else if (yElement instanceof YComboBox) {
			result = createNewInstance(ComboBoxEditpart.class);
		} else if (yElement instanceof YList) {
			result = createNewInstance(ListEditpart.class);
		} else if (yElement instanceof YOptionsGroup) {
			result = createNewInstance(OptionsGroupEditpart.class);
		} else if (yElement instanceof YNumericField) {
			result = createNewInstance(NumericFieldEditpart.class);
		} else if (yElement instanceof YDecimalField) {
			result = createNewInstance(DecimalFieldEditpart.class);
		} else if (yElement instanceof YDateTime) {
			result = createNewInstance(DateTimeEditpart.class);
		} else if (yElement instanceof YBrowser) {
			result = createNewInstance(BrowserEditpart.class);
		} else if (yElement instanceof YProgressBar) {
			result = createNewInstance(ProgressBarEditpart.class);
		} else if (yElement instanceof YTabSheet) {
			result = createNewInstance(TabSheetEditpart.class);
		} else if (yElement instanceof YTab) {
			result = createNewInstance(TabEditpart.class);
		} else if (yElement instanceof YBrowserDatatype) {
			result = createNewInstance(BrowserDatatypeEditpart.class);
		} else if (yElement instanceof YCheckBoxDatatype) {
			result = createNewInstance(CheckBoxDatatypeEditpart.class);
		} else if (yElement instanceof YComboBoxDatatype) {
			result = createNewInstance(ComboBoxDatatypeEditpart.class);
		} else if (yElement instanceof YDateTimeDatatype) {
			result = createNewInstance(DateTimeDatatypeEditpart.class);
		} else if (yElement instanceof YDecimalDatatype) {
			result = createNewInstance(DecimalDatatypeEditpart.class);
		} else if (yElement instanceof YListDataType) {
			result = createNewInstance(ListDatatypeEditpart.class);
		} else if (yElement instanceof YNumericDatatype) {
			result = createNewInstance(NumericDatatypeEditpart.class);
		} else if (yElement instanceof YOptionsGroupDataType) {
			result = createNewInstance(OptionsGroupDatatypeEditpart.class);
		} else if (yElement instanceof YProgressBarDatatype) {
			result = createNewInstance(ProgressBarDatatypeEditpart.class);
		} else if (yElement instanceof YTableDatatype) {
			result = createNewInstance(TableDatatypeEditpart.class);
		} else if (yElement instanceof YTabSheetDatatype) {
			result = createNewInstance(TabSheetDatatypeEditpart.class);
		} else if (yElement instanceof YTextAreaDatatype) {
			result = createNewInstance(TextAreaEditpart.class);
		} else if (yElement instanceof YTextDatatype) {
			result = createNewInstance(TextDatatypeEditpart.class);
		} else if (yElement instanceof YTreeDatatype) {
			result = createNewInstance(TreeDatatypeEditpart.class);
		} else if (yElement instanceof YMasterDetail) {
			result = createNewInstance(MasterDetailEditpart.class);
		} else if (yElement instanceof YImage) {
			result = createNewInstance(ImageEditpart.class);
		} else if (yElement instanceof YTextSearchField) {
			result = createNewInstance(TextSearchFieldEditpart.class);
		} else if (yElement instanceof YNumericSearchField) {
			result = createNewInstance(NumericSearchFieldEditpart.class);
		} else if (yElement instanceof YBooleanSearchField) {
			result = createNewInstance(BooleanSearchFieldEditpart.class);
		} else if (yElement instanceof YSplitPanel) {
			result = createNewInstance(SplitPanelEditpart.class);
		} else if (yElement instanceof YPanel) {
			result = createNewInstance(PanelEditpart.class);
		} else if (yElement instanceof YSearchPanel) {
			result = createNewInstance(SearchPanelEditpart.class);
		} else if (yElement instanceof YBeanReferenceField) {
			result = createNewInstance(BeanReferenceFieldEditpart.class);
		} else if (yElement instanceof YEnumComboBox) {
			result = createNewInstance(EnumComboBoxEditpart.class);
		} else if (yElement instanceof YEnumList) {
			result = createNewInstance(EnumListEditpart.class);
		} else if (yElement instanceof YEnumOptionsGroup) {
			result = createNewInstance(EnumOptionsGroupEditpart.class);
		} else if (yElement instanceof YAddToTableCommand) {
			result = createNewInstance(AddToTableEditpart.class);
		} else if (yElement instanceof YRemoveFromTableCommand) {
			result = createNewInstance(RemoveFromTableEditpart.class);
		} else if (yElement instanceof YSetNewBeanInstanceCommand) {
			result = createNewInstance(SetNewBeanInstanceEditpart.class);
		} else if (yElement instanceof YSlider) {
			result = createNewInstance(SliderEditpart.class);
		} else if (yElement instanceof YAbsoluteLayout) {
			result = createNewInstance(AbsoluteLayoutEditpart.class);
		} else if (yElement instanceof YSuggestTextField) {
			result = createNewInstance(SuggestTextFieldEditpart.class);
		} else if (yElement instanceof YPasswordField) {
			result = createNewInstance(PasswordFieldEditpart.class);
		} else if (yElement instanceof YFilteringComponent) {
			result = createNewInstance(FilteringComponentEditpart.class);
		} else if (yElement instanceof YKanban) {
			result = createNewInstance(KanbanEditpart.class);
		} else if (yElement instanceof YKanbanVisibilityProcessor) {
			result = createNewInstance(KanbanVisibilityProcessorEditpart.class);
		}

		if (result != null) {
			result.initialize(context, (YElement) yElement);
		}

		return (A) result;
	}

	/**
	 * Returns a new instance of the type.
	 * 
	 * @param type
	 *            The type of the editpart for which an instance should be
	 *            created.
	 * @return editPart
	 * @throws InstantiationException
	 *             e
	 * @throws IllegalAccessException
	 *             e
	 */
	protected IElementEditpart newInstance(Class<? extends IElementEditpart> type)
			throws InstantiationException, IllegalAccessException {
		return type.newInstance();
	}
}
