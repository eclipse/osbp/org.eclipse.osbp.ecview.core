/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.binding;

import org.eclipse.core.databinding.Binding;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;

// TODO: Auto-generated Javadoc
/**
 * An editpart resonsible to handle bindings.
 *
 * @param <A>
 *            the generic type
 */
public interface IBindingEditpart<A extends IBindableEndpointEditpart> extends
		IElementEditpart {

	/**
	 * Returns true, if the binding defined by this editpart is active. False
	 * otherwise.
	 *
	 * @return true, if is bound
	 */
	boolean isBound();

	/**
	 * Binds the target and model according the binding properties.
	 */
	void bind();

	/**
	 * Returns the binding or <code>null</code> if no binding is available.
	 *
	 * @return the binding
	 */
	Binding getBinding();

	/**
	 * Unbinds target and model.
	 */
	void unbind();

	/**
	 * Returns the target endpoint.
	 *
	 * @return the target endpoint
	 */
	A getTargetEndpoint();

	/**
	 * Sets the target endpoint.
	 *
	 * @param targetValue
	 *            the new target endpoint
	 */
	void setTargetEndpoint(A targetValue);

	/**
	 * Returns the model endpoint.
	 *
	 * @return the model endpoint
	 */
	A getModelEndpoint();

	/**
	 * Sets the target endpoint.
	 *
	 * @param modelValue
	 *            the new model endpoint
	 */
	void setModelEndpoint(A modelValue);

}
