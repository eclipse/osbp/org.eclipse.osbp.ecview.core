/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.memento;

import org.eclipse.osbp.runtime.common.validation.IStatus;

// TODO: Auto-generated Javadoc
/**
 * A memento manager to be used in ECView.
 */
public interface IECViewMementoManager {

	/**
	 * Loads the memento for the given memento id.
	 *
	 * @param mementoId
	 *            the memento id
	 * @return the object
	 */
	Object loadMemento(String mementoId);

	/**
	 * Saves the memento by the given mementoId.
	 *
	 * @param mementoId
	 *            the memento id
	 * @param memento
	 *            the memento
	 * @return status containing exceptions or IStatus.OK
	 */
	IStatus saveMemento(String mementoId, Object memento);

}
