/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.presentation;

import java.util.Map;
import java.util.concurrent.Future;

import org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.services.IUiKitBasedService;

// TODO: Auto-generated Javadoc
/**
 * View presenters are used to render UIs based on a view.<br>
 * Implementations are UI-kit specific.
 *
 * @param <C>
 *            the generic type
 */
public interface IViewPresentation<C> extends IWidgetPresentation<C> {

	/**
	 * Is called to render the view.
	 * 
	 * @param options
	 *            can contain different options used for rendering
	 */
	void render(Map<String, Object> options);

	/**
	 * Sets the editpart that should render the content.
	 * 
	 * @param editpart
	 *            The content editpart
	 */
	void setContent(IEmbeddableEditpart editpart);

	/**
	 * Returns the editpart of the content.
	 * 
	 * @return editpart
	 */
	IEmbeddableEditpart getContent();
	
	/**
	 * Is called to update the cell style of the content.
	 */
	void updateContentAlignment();
	
	/**
	 * Is called to update the spacing and margin.
	 */
	void updateSpacings();

	/**
	 * Executes the given runnable. It is ensured that the runnable will be
	 * executed within the context of the user interface related to this
	 * context.
	 *
	 * @param runnable
	 *            the runnable
	 */
	void exec(Runnable runnable);

	/**
	 * Executes the given runnable in a different thread. It is ensured that the
	 * runnable will be executed within the context of the user interface
	 * related to this context.
	 *
	 * @param runnable
	 *            the runnable
	 * @return the future
	 */
	Future<?> execAsync(Runnable runnable);

	/**
	 * Creates the fully configured service for the given service class.
	 *
	 * @param <A>
	 *            the generic type
	 * @param serviceClass
	 *            the service class
	 * @return the a
	 */
	<A extends IUiKitBasedService> A createService(Class<A> serviceClass);

	/**
	 * Opens the dialog.
	 *
	 * @param dialogEditpart
	 *            the dialog editpart
	 * @param inputData
	 *            this object contains information about the input data of the
	 *            target navigation page.
	 */
	void openDialog(IDialogEditpart dialogEditpart,
			IBindableEndpointEditpart inputData);

	/**
	 * Closes the dialog.
	 *
	 * @param dialogEditpart
	 *            the dialog editpart
	 */
	void closeDialog(IDialogEditpart dialogEditpart);

	/**
	 * Requests the focus for the given element.
	 *
	 * @param toFocus
	 *            the to focus
	 */
	void requestFocus(IElementEditpart toFocus);

}
