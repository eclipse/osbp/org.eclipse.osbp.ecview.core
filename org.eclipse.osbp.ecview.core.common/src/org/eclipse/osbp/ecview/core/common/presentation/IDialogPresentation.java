/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.presentation;

import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart;

// TODO: Auto-generated Javadoc
/**
 * Needs to render a dialog.
 *
 * @param <C>
 *            the generic type
 */
public interface IDialogPresentation<C> extends IWidgetPresentation<C> {

	/**
	 * Sets the content of the dialog.
	 *
	 * @param child
	 *            the new content
	 */
	void setContent(IEmbeddableEditpart child);

	/**
	 * Sets the input data. The binding endpoint contains information about the
	 * data that should be bound to the dialog.
	 *
	 * @param bindingEndpoint
	 *            the new input data binding endpoint
	 */
	void setInputDataBindingEndpoint(IBindableEndpointEditpart bindingEndpoint);

}
