/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.binding;

import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor;

// TODO: Auto-generated Javadoc
/**
 * An editpart resonsible to provide observable values to bind the visibility
 * processor instance. Note, that an instance about the processor needs to be
 * past to this instance before accessing the observable value.
 */
public interface IVisibilityProcessorValueBindingEndpointEditpart extends
		IBindableValueEndpointEditpart {

	/**
	 * Sets the visibility processor which should be bound.
	 *
	 * @param processor
	 *            the new visibility processor
	 */
	void setVisibilityProcessor(IVisibilityProcessor processor);

}
