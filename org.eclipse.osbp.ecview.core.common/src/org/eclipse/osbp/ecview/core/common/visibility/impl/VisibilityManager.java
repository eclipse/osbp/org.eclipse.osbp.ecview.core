/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.visibility.impl;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VisibilityManager implements IVisibilityManager {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(VisibilityManager.class);

	private IWidgetAssocationsService<?, ?> associations;
	private IViewContext context;

	public VisibilityManager(IViewContext context) {
		this.context = context;
		this.associations = context.getService(IWidgetAssocationsService.ID);
	}

	@Override
	public IViewContext getViewContext() {
		return context;
	}

	@Override
	public IVisibilityHandler getById(String id) {
		try {
			return new VisibilityHandler(associations, id);
		} catch (Exception e) {
			LOGGER.error("{}", e);
		}

		return null;
	}
}
