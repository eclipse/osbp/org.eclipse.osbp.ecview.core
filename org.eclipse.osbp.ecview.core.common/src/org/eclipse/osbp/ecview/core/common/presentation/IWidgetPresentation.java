/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.presentation;

import java.util.Set;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart.DatatypeChangeEvent;
import org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessable;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;

// TODO: Auto-generated Javadoc
/**
 * Widget presenter are used to create and provide widgets that will be
 * assembled to an UI.<br>
 * Implementations are UI-kit specific.
 *
 * @param <C>
 *            the generic type
 */
public interface IWidgetPresentation<C> extends IDisposable,
		IVisibilityProcessable {

	/**
	 * Returns the model associated with this presenter.<br>
	 * Or <code>null</code> if the presenter is not based on a model.
	 * 
	 * @return the model or <code>null</code>
	 */
	Object getModel();

	/**
	 * Is called to create the widget described by the presenter. If an usable
	 * instance is present then it has to be returned. Whether the child is
	 * added to the parent depends on the underlying UI-Kit.
	 * 
	 * @param parent
	 *            the parent where the widget should be placed in
	 * @return the created widget
	 */
	C createWidget(Object parent);

	/**
	 * Is called to return the widget created by that presenter. If an usable
	 * instance is present then it has to be returned. This method should never
	 * create new instances of a widget.
	 * 
	 * @return the created widget
	 */
	C getWidget();

	/**
	 * The widget presenter will unrender its widget. It does not necessarily
	 * have to dispose the widget but can do so. The kind of operation that will
	 * be processed depends on the underlying UI-Kit.
	 */
	void unrender();

	/**
	 * Returns true, if the ui element of the presentation was rendered.
	 * 
	 * @return rendered
	 */
	boolean isRendered();

	/**
	 * Returns the view context that is responsible for the given context.
	 *
	 * @return the view context
	 */
	IViewContext getViewContext();

	/**
	 * Returns the observable that is related with the given model element.
	 *
	 * @param model
	 *            The element representing the observable in the model.
	 * @return the observable value
	 */
	IObservable getObservableValue(Object model);

	/**
	 * Returns all bindings that are attached to the UI element.
	 *
	 * @return the UI bindings
	 */
	Set<Binding> getUIBindings();

	/**
	 * Notifies the presentation about a changed datatype.
	 *
	 * @param event
	 *            the event
	 */
	void notifyDatatypeChanged(DatatypeChangeEvent event);

}
