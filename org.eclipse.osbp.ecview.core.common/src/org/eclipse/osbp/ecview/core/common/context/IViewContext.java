/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.context;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.eclipse.osbp.ecview.core.common.beans.ISlot;
import org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleEvent;
import org.eclipse.osbp.ecview.core.common.services.IThirdPartyServiceDelegate;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager;
import org.eclipse.osbp.runtime.common.event.IEventBroker;

// TODO: Auto-generated Javadoc
/**
 * Context containing information about the current {@link IViewEditpart}
 * instance.<br>
 * Not intended to be subclassed.
 */
public interface IViewContext extends IContext {

	/**
	 * A Map&lt;String, Object&gt; can be provided by the parameter at rendering
	 * time. This map will be used to initialize the services.
	 */
	public static final String PARAM_SERVICES = "viewcontext.services";

	/**
	 * Allows to pass an instance of {@link IThirdPartyServiceDelegate}. It will
	 * be set to the view context before starting rendering.
	 */
	public static final String PARAM_THIRDPARTY_SERVICE_PROVIDER = "viewcontext.service.provider.thirdparty";

	/**
	 * Allows to pass an instance of {@link IConfiguration}. It will be set to
	 * the view context before starting rendering.
	 */
	public static final String PARAM_CONFIGURATION = "viewcontext.configuration";

	/** if set, a fixed language is to be used. */
	public static final String PRESET_LOCALE = "viewcontext.locale";

	/**
	 * Sends events by the {@link IEventBroker} if service available. Data is
	 * {@link ILifecycleEvent}.
	 */
	public static final String TOPIC_LIFECYCLE = "ecview/lifecycle";

	/**
	 * Use this property to pass the beanSlot that should be used for the dto.
	 * See properties map in {@link #render(Object, Map)}
	 */
	public static final String PROP_SLOT = "beanSlot";

	/**
	 * Returns the URI that specifies the UI kit to be used.
	 * 
	 * @return presentationURI
	 */
	String getPresentationURI();

	/**
	 * Returns the {@link IViewEditpart} that is related with this context.
	 * 
	 * @return viewEditpart
	 */
	IViewEditpart getViewEditpart();

	/**
	 * Sets the view editpart to be used. Setting twice is not allowed.
	 * 
	 * @param viewEditpart
	 */
	void setViewEditpart(IViewEditpart viewEditpart);

	/**
	 * Returns the visibility manager.
	 *
	 * @return the visibility manager
	 */
	IVisibilityManager getVisibilityManager();

	/**
	 * Returns a list of exposed actions. These actions must be supported by the
	 * UI element, that embeds the view rendered by this context.<br>
	 * Common usecases may be "loading models from JPA", "support JPA queries",
	 * "open different eclipse or vaaclipse views",...
	 *
	 * @return the exposed actions
	 */
	List<IExposedActionEditpart> getExposedActions();

	// TODO: check link getValueBean
	/**
	 * Registers an instance of value bean to the context. It can be accessed by
	 * the selector. See also {@link ISlot} or {link getValueBean(String)
	 * getValueBean}.
	 *
	 * @param selector
	 *            The selector string to identify the value bean instance.
	 * @param bean
	 *            The value bean.
	 */
	void setBean(String selector, Object bean);

	/**
	 * Returns the root layout where all UI elements should be child elements
	 * from it.
	 * 
	 * @return rootLayout
	 */
	Object getRootLayout();

	/**
	 * Renders the UI based on the {@link IViewEditpart} and the given
	 * presentationURI.
	 *
	 * @param presentationURI
	 *            The URI that specifies the UI kit to be used. May be
	 *            <code>null</code>.
	 * @param rootLayout
	 *            The given root layout is the base element for the elements to
	 *            be rendered.
	 * @param parameter
	 *            Parameters that can be passed for rendering
	 * @throws ContextException
	 *             if the UI was already rendered or the rootlayout is
	 *             <code>null</code>
	 */
	void render(String presentationURI, Object rootLayout,
			Map<String, Object> parameter) throws ContextException;

	/**
	 * Returns true, if the UI was already rendered.
	 * 
	 * @return rendered
	 */
	boolean isRendered();

	/**
	 * Returns the model element with the given id or <code>null</code> if no
	 * element could be found.
	 *
	 * @param id
	 *            the id
	 * @return the object
	 */
	Object findModelElement(String id);

	/**
	 * Returns the {@link YEmbeddable} model element with the given id or <code>null</code> if no
	 * element could be found.
	 *
	 * @param id
	 *            the id
	 * @return the object
	 */
	Object findYEmbeddableElement(String id);
	
	/**
	 * Returns the embeddable model element which binds the given bindingId
	 * matched by the given regex. Or <code>null</code> if no element could be
	 * found.
	 * <p>
	 * Following bindingIds are available in ECView:
	 * <ul>
	 * <li><code>beanslot/{beanslot}/{propertyPath}</code></li>
	 * <li><code>beanvalue/{beanClassName}/{propertyPath}</code></li>
	 * <li><code>modelBinding/{elementId}/{propertyPath}</code></li>
	 * <li><code>uiSelection/{elementId}/{propertyPath}</code></li>
	 * <li><code>uiValue/{elementId}</code></li>
	 * <li><code>uiVisibilityProcessor/{property}</code></li>
	 * <li><code>uiActivated/{elementId}</code></li>
	 * </ul>
	 *
	 * @param bindingIdRegex
	 *            the binding id regex
	 * @return the object
	 */
	Object findBoundField(String bindingIdRegex);

	/**
	 * Executes the given runnable. It is ensured that the runnable will be
	 * executed within the context of the user interface related to this
	 * context.
	 *
	 * @param runnable
	 *            the runnable
	 */
	void exec(Runnable runnable);

	/**
	 * Executes the given runnable in a different thread. It is ensured that the
	 * runnable will be executed within the context of the user interface
	 * related to this context.
	 *
	 * @param runnable
	 *            the runnable
	 * @return the future
	 */
	Future<?> execAsync(Runnable runnable);

	/**
	 * returns the original rendering parameters.
	 *
	 * @return the rendering params
	 */
	Map<String, Object> getRenderingParams();
	
	/**
	 * Sets the rendering params.
	 *
	 * @param params the params
	 */
	void setRenderingParams(Map<String, Object> params);
}
