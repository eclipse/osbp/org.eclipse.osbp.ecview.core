/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.beans;


// TODO: Auto-generated Javadoc
/**
 * Implementations are responsible to initialize the bean slots. This is
 * necessary since databinding needs the type of the value that will be stored
 * in the slot. Otherwise no context binding is possible.
 */
public interface IBeanSlotInitializer {

	/**
	 * Initializes the bean slots.
	 *
	 * @param registry
	 *            the registry
	 */
	void intialize(IBeanRegistry registry);

}
