/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.context;

import java.util.Locale;

import org.eclipse.osbp.runtime.common.dispose.IDisposable;

// TODO: Auto-generated Javadoc
/**
 * This service can be consumed by the viewContext or viewSetContext and will
 * notify listener about a locale change.
 */
public interface ILocaleChangedService extends IDisposable {

	/** The id. */
	public static String ID = ILocaleChangedService.class.getName();

	/**
	 * Adds a locale listener to the service.
	 *
	 * @param listener
	 *            the listener
	 */
	void addLocaleListener(LocaleListener listener);

	/**
	 * Removes the given locale listener from the service.
	 *
	 * @param listener
	 *            the listener
	 */
	void removeLocaleListener(LocaleListener listener);

	/**
	 * Notifies the listener about the changed locale.
	 *
	 * @param locale
	 *            the locale
	 */
	void notifyLocaleChanged(Locale locale);

	/**
	 * The listener interface for receiving locale events. The class that is
	 * interested in processing a locale event implements this interface, and
	 * the object created with that class is registered with a component using
	 * the component's <code>addLocaleListener</code> method. When
	 * the locale event occurs, that object's appropriate
	 * method is invoked.
	 *
	 */
	interface LocaleListener {

		/**
		 * Notifies the listener about a locale change.
		 *
		 * @param locale
		 *            the locale
		 */
		void localeChanged(Locale locale);

	}

}
