/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.notification;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;

public class LifecycleEvent implements ILifecycleEvent {

	private final IElementEditpart editpart;
	private final String type;

	public LifecycleEvent(IElementEditpart editpart, String type) {
		super();
		this.editpart = editpart;
		this.type = type;
	}

	@Override
	public IElementEditpart getEditpart() {
		return editpart;
	}

	@Override
	public String getType() {
		return type;
	}

}
