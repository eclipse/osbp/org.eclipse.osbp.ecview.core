/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.dnd;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;

// TODO: Auto-generated Javadoc
/**
 * This factory may be implemented by clients. But API may change later. It
 * provides a UI-Kit specific designer drop handler for a given layout. And
 * needs to be registered at the IViewContext before rendering.
 */
public interface IDropTargetStrategy {

	/**
	 * Sets up the given layout with an UI-Kit specific designer drop handler
	 * for the given layout-model element.
	 *
	 * @param context
	 *            the context
	 * @param layout
	 *            the layout
	 * @param yLayout
	 *            the y layout
	 */
	void setupDropTarget(IViewContext context, Object layout, YLayout yLayout);

}
