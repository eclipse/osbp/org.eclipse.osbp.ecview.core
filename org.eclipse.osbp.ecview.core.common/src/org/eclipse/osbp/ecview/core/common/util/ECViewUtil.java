/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.util;

import java.util.List;

import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringComponent;
import org.eclipse.osbp.ecview.core.extension.model.extension.YFilteringType;
import org.eclipse.osbp.runtime.common.annotations.Filter;
import org.eclipse.osbp.runtime.common.util.BeanUtils;

public class ECViewUtil {

	/**
	 * Fills the given {@link YFilteringComponent} with definitions required to
	 * show a filtering UI.
	 * 
	 * @param type
	 * @param depth
	 * @param yFilterComponent
	 */
	public static void fill(Class<?> type, int depth, YFilteringComponent yFilterComponent) {

		yFilterComponent.setType(type);
		yFilterComponent.setTypeQualifiedName(type.getCanonicalName());

		List<String> properties = BeanUtils.getAllFilteringProperties(type, depth);
		for (String property : properties) {
			if (BeanUtils.isAnnotationPresent(type, property, Filter.class)) {
				yFilterComponent.addFilterDescriptor(YFilteringType.COMPARE, property);
			} else {
				yFilterComponent.addFilterDescriptor(YFilteringType.RANGE, property);
			}
			yFilterComponent.addTableDescriptor(property);
		}
	}
}
