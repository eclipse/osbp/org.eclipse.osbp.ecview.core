/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.common.notification;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractReloadRequestService implements
		IReloadRequestService {

	private List<ModeListener> listeners;
	private Boolean mode = null;

	@Override
	public boolean getMode() {
		return mode.booleanValue();
	}

	@Override
	public void setMode(boolean enabled) {
		Boolean oldMode = this.mode;
		this.mode = new Boolean(enabled);

		if (oldMode != mode) {
			notifyListeners();
		}
	}

	private void notifyListeners() {
		if (listeners != null) {
			for (ModeListener listener : new ArrayList<>(listeners)) {
				listener.notifyMode(getMode());
			}
		}
	}

	@Override
	public void addListener(ModeListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<ModeListener>();
		}

		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	@Override
	public void removeListener(ModeListener listener) {
		if (listeners == null) {
			return;
		}
		listeners.remove(listener);
	}

}
