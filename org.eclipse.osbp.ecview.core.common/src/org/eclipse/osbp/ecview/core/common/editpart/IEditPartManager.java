/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;

/**
 * Is responsible to return editparts based on the given element.
 */
public interface IEditPartManager {

	/**
	 * Returns true, if the factory can be used for the given element.
	 * 
	 * @param modelElement Any kind of object that contains information how to create the edit part.
	 * @return result True if the manager can be used for the modelElement
	 */
	boolean isFor(Object modelElement);

	/**
	 * Tries to find an existing edit part for the given element. If no edit part could be found, a new one will be
	 * created.
	 * 
	 * @param <A> An instance of {@link IElementEditpart}
	 * @param context the view context
	 * @param modelElement Any kind of object that contains information how to create the edit part.
	 * @return editpart The found or created editPart.
	 */
	<A extends IElementEditpart> A getEditpart(IViewContext context, Object modelElement);

	/**
	 * Tries to find an existing instance of the editpart. If no instance could be found then <code>null</code> will be
	 * returned.
	 * 
	 * @param <A> An instance of {@link IElementEditpart}
	 * @param modelElement Any kind of object that contains information how to create the edit part.
	 * @return editpart The found editpart or <code>null</code>
	 */
	<A extends IElementEditpart> A findEditpart(Object modelElement);

//	/**
//	 * Is used to create a new instance of an edit part, without having an model element.
//	 * 
//	 * @param <A> An instance of {@link IElementEditpart}
//	 * @param context the view context
//	 * @param selector The selector is used to find the proper edit part manager that is responsible to handle that
//	 *            call.
//	 * @param editPartClazz The type of edit part an instance of should be prepared.
//	 * @return editpart The created editpart
//	 */
//	<A extends IElementEditpart> A createEditpart(IViewContext context, Object selector, Class<A> editPartClazz);

}