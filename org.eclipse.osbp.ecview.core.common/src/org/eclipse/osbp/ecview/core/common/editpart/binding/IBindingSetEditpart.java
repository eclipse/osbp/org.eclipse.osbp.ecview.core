/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.binding;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;

// TODO: Auto-generated Javadoc
/**
 * An editpart resonsible to handle bindings. If new bindings are added, they
 * will become processed. Also leaving bindings will become unbind.
 */
public interface IBindingSetEditpart extends IElementEditpart {

	/**
	 * Returns true if the binding set is active.
	 *
	 * @return true, if is active
	 */
	boolean isActive();

	/**
	 * Activates the binding set manager. It will create all pending bindings.
	 */
	void activate();

	/**
	 * Returns the view that binding set is attached to.
	 *
	 * @return the view
	 */
	IViewEditpart getView();

	/**
	 * Returns a list of all available bindings.
	 *
	 * @return the bindings
	 */
	List<IBindingEditpart<?>> getBindings();

	/**
	 * Adds the given binding to the list of bindings. Adding a binding will
	 * bind the contained values if not done so.
	 *
	 * @param binding
	 *            the binding
	 */
	void addBinding(IBindingEditpart<?> binding);

	/**
	 * Removes the given binding from the list of bindings. Removing a binding
	 * will unbind the contained values.
	 *
	 * @param binding
	 *            the binding
	 */
	void removeBinding(IBindingEditpart<?> binding);
	
	/**
	 * Returns a list of all available transient bindings.
	 *
	 * @return the transient bindings
	 */
	List<IBindingEditpart<?>> getTransientBindings();

	/**
	 * Adds the given binding to the list of transient bindings. Adding a
	 * binding will bind the contained values if not done so.
	 *
	 * @param binding
	 *            the binding
	 */
	void addTransientBinding(IBindingEditpart<?> binding);

	/**
	 * Removes the given binding from the list of transient bindings. Removing a
	 * binding will unbind the contained values.
	 *
	 * @param binding
	 *            the binding
	 */
	void removeTransientBinding(IBindingEditpart<?> binding);

	/**
	 * Returns the binding manager that should be used to bind values.
	 *
	 * @return the binding manager
	 */
	IECViewBindingManager getBindingManager();

	/**
	 * Sets the binding manager that should be used to bind values.
	 *
	 * @param bindingManager
	 *            the new binding manager
	 */
	void setBindingManager(IECViewBindingManager bindingManager);

	/**
	 * Returns a list with all bindings that are related to the given
	 * elementModel.
	 *
	 * @param elementModel
	 *            the element model
	 * @return the list
	 */
	List<IBindingEditpart<?>> findBindings(Object elementModel);

}
