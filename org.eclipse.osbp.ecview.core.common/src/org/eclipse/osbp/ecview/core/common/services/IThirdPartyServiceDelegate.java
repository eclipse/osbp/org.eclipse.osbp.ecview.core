/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.services;

import org.eclipse.osbp.ecview.core.common.context.IContext;

// TODO: Auto-generated Javadoc
/**
 * Thirdparties may provide an implementation to the view context. The view
 * context will try to find a service using this delegate.
 */
public interface IThirdPartyServiceDelegate {

	/**
	 * Returns true if the provider is responsible for creating services of the
	 * given type for the context.
	 *
	 * @param selector
	 *            the selector
	 * @param context
	 *            the context
	 * @return true, if is for
	 */
	boolean isFor(String selector, IContext context);

	/**
	 * Returns the service of the given type for the context.
	 *
	 * @param <A>
	 *            the generic type
	 * @param selector
	 *            the selector
	 * @param context
	 *            the context
	 * @return the a
	 */
	<A> A createService(String selector, IContext context);

}