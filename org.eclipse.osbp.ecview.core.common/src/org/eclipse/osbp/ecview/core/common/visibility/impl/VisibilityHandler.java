/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.visibility.impl;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.visibility.IUiElementAccess;
import org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessable;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;
import org.eclipse.osbp.ecview.core.common.visibility.Color;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class VisibilityHandler.
 */
public class VisibilityHandler implements IVisibilityHandler {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(VisibilityHandler.class);

	/** The associations. */
	private final IWidgetAssocationsService<?, ?> associations;

	/** The id. */
	private final String id;

	/** The foreground color. */
	private Color foregroundColor;

	/** The background color. */
	private Color backgroundColor;

	/** The foreground color code. */
	private String foregroundColorCode;

	/** The background color code. */
	private String backgroundColorCode;

	/** The editable. */
	private boolean editable;

	/** The enabled. */
	private boolean enabled;

	/** The visible. */
	private boolean visible;

	/** The bold. */
	private boolean bold;

	/** The border. */
	private boolean border;

	/** The italic. */
	private boolean italic;

	/** The strikethrough. */
	private boolean strikethrough;

	/** The underline. */
	private boolean underline;

	/** The css class. */
	private String cssClass;

	/** The css id. */
	private String cssId;

	/**
	 * Instantiates a new visibility handler.
	 *
	 * @param associations
	 *            the associations
	 * @param id
	 *            the id
	 * @throws NotValidProcessableException
	 *             the not valid processable exception
	 */
	public VisibilityHandler(IWidgetAssocationsService<?, ?> associations, String id)
			throws NotValidProcessableException {
		this.associations = associations;
		this.id = id;

		// access the processable. Exception is thrown if not a valid element.
		getProcessable();

		reset();
	}

	/**
	 * Returns the {@link IVisibilityProcessable} for the id.
	 *
	 * @return the processable
	 * @throws NotValidProcessableException
	 *             the not valid processable exception
	 */
	protected IVisibilityProcessable getProcessable() throws NotValidProcessableException {
		IElementEditpart editpart = associations.getEditpart(id);
		// if (editpart == null || !(editpart instanceof
		// IVisibilityProcessable)) {
		// throw new IllegalArgumentException(id + " is not a valid
		// IVisibilityProcessable");
		// }

		return (IVisibilityProcessable) editpart;
	}

	/**
	 * Returns the {@link IUiElementAccess} for the id.
	 *
	 * @return the i ui element access
	 * @throws NotValidProcessableException
	 *             the not valid processable exception
	 */
	protected IUiElementAccess internalGetUiAccess() throws NotValidProcessableException {
		IElementEditpart editpart = associations.getEditpart(id);
		if (editpart == null || !(editpart instanceof IUiElementAccess)) {
			throw new IllegalArgumentException(id + " is not a valid IUiElementAccess");
		}

		return (IUiElementAccess) editpart;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler#reset()
	 */
	@Override
	public void reset() {
		foregroundColor = Color.UNDEFINED;
		backgroundColor = Color.UNDEFINED;
		foregroundColorCode = "";
		backgroundColorCode = "";
		editable = true;
		enabled = true;
		visible = true;
		bold = false;
		border = false;
		italic = false;
		strikethrough = false;
		underline = false;
	}

	/**
	 * Gets the foreground color.
	 *
	 * @return the foregroundColor
	 */
	public Color getForegroundColor() {
		return foregroundColor;
	}

	/**
	 * Sets the foreground color.
	 *
	 * @param foregroundColor
	 *            the foregroundColor to set
	 */
	public void setForegroundColor(Color foregroundColor) {
		this.foregroundColor = foregroundColor;
	}

	/**
	 * Gets the background color.
	 *
	 * @return the backgroundColor
	 */
	public Color getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * Sets the background color.
	 *
	 * @param backgroundColor
	 *            the backgroundColor to set
	 */
	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	/**
	 * Gets the foreground color code.
	 *
	 * @return the foregroundColorCode
	 */
	public String getForegroundColorCode() {
		return foregroundColorCode;
	}

	/**
	 * Sets the foreground color code.
	 *
	 * @param foregroundColorCode
	 *            the foregroundColorCode to set
	 */
	public void setForegroundColorCode(String foregroundColorCode) {
		this.foregroundColorCode = foregroundColorCode;
	}

	/**
	 * Gets the background color code.
	 *
	 * @return the backgroundColorCode
	 */
	public String getBackgroundColorCode() {
		return backgroundColorCode;
	}

	/**
	 * Sets the background color code.
	 *
	 * @param backgroundColorCode
	 *            the backgroundColorCode to set
	 */
	public void setBackgroundColorCode(String backgroundColorCode) {
		this.backgroundColorCode = backgroundColorCode;
	}

	/**
	 * Checks if is editable.
	 *
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * Sets the editable.
	 *
	 * @param editable
	 *            the editable to set
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Checks if is visible.
	 *
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Sets the visible.
	 *
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * Checks if is bold.
	 *
	 * @return the bold
	 */
	public boolean isBold() {
		return bold;
	}

	/**
	 * Sets the bold.
	 *
	 * @param bold
	 *            the bold to set
	 */
	public void setBold(boolean bold) {
		this.bold = bold;
	}

	/**
	 * Checks if is border.
	 *
	 * @return the border
	 */
	public boolean isBorder() {
		return border;
	}

	/**
	 * Sets the border.
	 *
	 * @param border
	 *            the border to set
	 */
	public void setBorder(boolean border) {
		this.border = border;
	}

	/**
	 * Checks if is italic.
	 *
	 * @return the italic
	 */
	public boolean isItalic() {
		return italic;
	}

	/**
	 * Sets the italic.
	 *
	 * @param italic
	 *            the italic to set
	 */
	public void setItalic(boolean italic) {
		this.italic = italic;
	}

	/**
	 * Checks if is strikethrough.
	 *
	 * @return the strikethrough
	 */
	public boolean isStrikethrough() {
		return strikethrough;
	}

	/**
	 * Sets the strikethrough.
	 *
	 * @param strikethrough
	 *            the strikethrough to set
	 */
	public void setStrikethrough(boolean strikethrough) {
		this.strikethrough = strikethrough;
	}

	/**
	 * Checks if is underline.
	 *
	 * @return the underline
	 */
	public boolean isUnderline() {
		return underline;
	}

	/**
	 * Sets the underline.
	 *
	 * @param underline
	 *            the underline to set
	 */
	public void setUnderline(boolean underline) {
		this.underline = underline;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler#
	 * getCssClass()
	 */
	@Override
	public String getCssClass() {
		return cssClass;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler#
	 * setCssClass(java.lang.String)
	 */
	@Override
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler#
	 * getCssId()
	 */
	@Override
	public String getCssId() {
		return cssId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler#
	 * setCssId(java.lang.String)
	 */
	@Override
	public void setCssId(String cssId) {
		this.cssId = cssId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler#apply()
	 */
	@Override
	public void apply() {
		try {
			if (getProcessable() != null)
				getProcessable().apply(this);
		} catch (Exception e) {
			LOGGER.error("{}", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler#
	 * getUiAccess()
	 */
	@Override
	public IUiElementAccess getUiAccess() {
		try {
			return new UiElementAccessImpl(internalGetUiAccess());
		} catch (Exception e) {
			LOGGER.error("{}", e);
		}
		return null;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("enabled:");
		sb.append(this.isEnabled());
		sb.append(" editable:");
		sb.append(this.isEditable());
		sb.append(" visible:");
		sb.append(this.isVisible());
		
		return sb.toString();
	}
	
	/**
	 * The Class UiElementAccessImpl.
	 */
	private static class UiElementAccessImpl implements IUiElementAccess {

		/** The access. */
		private final IUiElementAccess access;

		/**
		 * Instantiates a new ui element access impl.
		 *
		 * @param access
		 *            the access
		 */
		public UiElementAccessImpl(IUiElementAccess access) {
			this.access = access;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.osbp.ecview.core.common.editpart.visibility.
		 * IUiElementAccess#containsTag(java.lang.String)
		 */
		@Override
		public boolean containsTag(String tag) {
			return access.containsTag(tag);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.osbp.ecview.core.common.editpart.visibility.
		 * IUiElementAccess#containsProperty(java.lang.String)
		 */
		@Override
		public boolean containsProperty(String key) {
			return access.containsProperty(key);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.osbp.ecview.core.common.editpart.visibility.
		 * IUiElementAccess#getPropertyValue(java.lang.String)
		 */
		@Override
		public String getPropertyValue(String key) {
			return access.getPropertyValue(key);
		}

	}

}
