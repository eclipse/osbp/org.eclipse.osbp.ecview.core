/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

// TODO: Auto-generated Javadoc
/**
 * The Interface IEmbeddableParent.
 */
public interface IEmbeddableParent {

	/**
	 * Calling this method will render the child editpart without adding it to
	 * the model.
	 *
	 * @param child
	 *            the child
	 */
	void renderChild(IEmbeddableEditpart child);

	/**
	 * Calling this method will unrender the child editpart without removing it
	 * from the model.
	 *
	 * @param child
	 *            the child
	 */
	void unrenderChild(IEmbeddableEditpart child);

	/**
	 * Calling this method will dispose the child editpart without removing it
	 * from the model.
	 *
	 * @param child
	 *            the child
	 */
	void disposeChild(IEmbeddableEditpart child);

}
