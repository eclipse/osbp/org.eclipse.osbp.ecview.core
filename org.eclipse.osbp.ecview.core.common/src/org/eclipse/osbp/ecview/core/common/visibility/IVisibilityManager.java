/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.visibility;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;

// TODO: Auto-generated Javadoc
/**
 * The Interface IVisibilityManager.
 */
public interface IVisibilityManager {

	/**
	 * Returns the view context.
	 *
	 * @return the view context
	 */
	IViewContext getViewContext();

	/**
	 * Returns the visibility handler for the given id. Or <code>null</code> if
	 * no element could be found, or if the element is not a valid
	 * IVisibilityProcessable.
	 *
	 * @param id
	 *            the id
	 * @return the by id
	 */
	IVisibilityHandler getById(String id);

}
