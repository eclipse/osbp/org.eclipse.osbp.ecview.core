/**
 * Copyright (c) 2013 Loetz GmbH&Co.KG(Heidelberg). All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0  which accompanies this distribution,
t https://www.eclipse.org/legal/epl-2.0/
t
t SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: Christophe Loetz (Loetz GmbH&Co.KG) - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.context;

import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.IAccessible;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;

// TODO: Auto-generated Javadoc
/**
 * Marker interface for context.
 * 
 * @author dominguez
 * 
 */
public interface IContext extends IDisposable, IAccessible {

	/**
	 * Sets the current locale of the view or viewset.
	 *
	 * @param locale
	 *            the new locale
	 */
	void setLocale(Locale locale);

	/**
	 * Returns the current locale of the view or viewset.
	 *
	 * @return the locale
	 */
	Locale getLocale();

}