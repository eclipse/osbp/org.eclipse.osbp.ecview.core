/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.presentation;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;

/**
 * Is responsible to return presenter based on the given element.
 */
public interface IPresentationFactory {

	/**
	 * Returns true, if the factory can be used for the given element.
	 * 
	 * @param uiContext
	 *            contains information about the current ui instance
	 * @param editpart
	 *            the editpart for which a presenter should be created.
	 * @return result
	 */
	boolean isFor(IViewContext uiContext, IElementEditpart editpart);

	/**
	 * Is used to create a new instance of a presenter.
	 * 
	 * @param <A>
	 *            An instance of {@link IWidgetPresentation}
	 * @param uiContext
	 *            contains information about the current ui instance
	 * @param editpart
	 *            the editpart for which a presenter should be created.
	 * @return presentation
	 * 
	 * @throws IllegalArgumentException
	 *             if no presentation could be created.
	 */
	<A extends IWidgetPresentation<?>> A createPresentation(
			IViewContext uiContext, IElementEditpart editpart)
			throws IllegalArgumentException;

}