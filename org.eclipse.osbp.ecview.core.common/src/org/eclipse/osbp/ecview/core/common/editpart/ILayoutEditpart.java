/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import java.util.List;

/**
 * An IUiLayoutEditpart is an abstraction of a uiElementContainer. Itself can
 * contain {@link IEmbeddableEditpart} and so layouts can also contain other
 * layouts.
 */
public interface ILayoutEditpart extends IEmbeddableEditpart, IEmbeddableParent {

	/**
	 * Returns an unmodifiable list with all children contained in the layout.
	 * 
	 * @return elements The children editparts contained in this layoutEditpart
	 */
	List<IEmbeddableEditpart> getElements();

	/**
	 * Adds an element to the layout. Adding the same element twice has no
	 * effect.
	 * 
	 * @param element
	 *            The editpart to be added.
	 */
	@Deprecated // use model instead
	void addElement(IEmbeddableEditpart element);

	/**
	 * Removes an element from the layout.
	 * 
	 * @param element
	 *            The editpart to be removed.
	 */
	@Deprecated // use model instead
	void removeElement(IEmbeddableEditpart element);

}
