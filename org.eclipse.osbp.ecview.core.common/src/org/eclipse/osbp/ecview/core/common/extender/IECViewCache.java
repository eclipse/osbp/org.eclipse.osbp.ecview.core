/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.extender;

import java.net.URL;
import java.util.List;

import org.eclipse.osbp.ecview.core.common.model.core.YView;

// TODO: Auto-generated Javadoc
/**
 * Some view models have been autowired by the IDE. These YView models can be
 * located in any bundle. This cache aims to centralize all the different models
 * and gives you access to these instances.
 * 
 * @noimplement by clients
 * @Scope(singleton)
 */
// TODO
public interface IECViewCache {

	/**
	 * Returns the view model element with the given id.
	 *
	 * @param id
	 *            the id
	 * @return the view
	 */
	YView getView(String id);

	/**
	 * Returns all view models matching the given input type in the given
	 * beanSlot. The beanSlot may be <code>null</code>.
	 *
	 * @param inputType
	 *            the input type
	 * @param beanSlot
	 *            may be null.
	 * @return the views
	 */
	List<YView> getViews(Class<?> inputType, String beanSlot);

	/**
	 * Registers new views.
	 *
	 * @param urls
	 *            the urls
	 */
	void registerViews(List<URL> urls);

	/**
	 * Unregisters views.
	 *
	 * @param urls
	 *            the urls
	 */
	void unregisterViews(List<URL> urls);

}
