/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Future;

import org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleEvent;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleService;
import org.eclipse.osbp.ecview.core.common.notification.LifecycleEvent;
import org.eclipse.osbp.ecview.core.common.services.IThirdPartyServiceDelegate;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;
import org.eclipse.osbp.ecview.core.common.tooling.IWidgetMouseClickService;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager;
import org.eclipse.osbp.ecview.core.common.visibility.impl.VisibilityManager;
import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of the {@link IViewContext}.
 */
public class ViewContext extends DisposableContext implements IViewContext {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(ViewContext.class);

	/** The final dispose. */
	// preparations for final disposal after view has been disposed
	private Map<String, Object> finalDispose = new HashMap<String, Object>();

	/** The rendering parameters. */
	private Map<String, Object> renderingParameters = new HashMap<>();

	/** The visibility manager. */
	private VisibilityManager visibilityManager;

	/** The view editpart. */
	private IViewEditpart viewEditpart;

	/** The root layout. */
	private Object rootLayout;

	/** The presentation uri. */
	private String presentationURI;

	/** The rendered. */
	private boolean rendered;

	/** The thirdparty service delegate. */
	private IThirdPartyServiceDelegate thirdpartyServiceDelegate;

	/**
	 * Default Constructor for tests only! Do not use.
	 */
	public ViewContext() {
	}

	public void setViewEditpart(IViewEditpart viewEditpart) {
		if (this.viewEditpart == null) {
			this.viewEditpart = viewEditpart;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPresentationURI() {
		checkDisposed();

		return presentationURI;
	}

	/**
	 * Delegates the service request to the view set.
	 *
	 * @param <S>
	 *            the generic type
	 * @param selector
	 *            the selector
	 * @return the s
	 */
	@SuppressWarnings("unchecked")
	protected <S> S delegateGetService(String selector) {
		if (selector.equals(ILocaleChangedService.ID)) {
			LocaleChangedService service = new LocaleChangedService();
			registerService(selector, service);
			return (S) service;
		} else if (selector.equals(IWidgetMouseClickService.ID)) {
			IWidgetMouseClickService service = getViewEditpart().createService(IWidgetMouseClickService.class);
			registerService(selector, service);
			return (S) service;
		} else if (selector.equals(IWidgetAssocationsService.ID)) {
			IWidgetAssocationsService<?, ?> service = getViewEditpart().createService(IWidgetAssocationsService.class);
			registerService(selector, service);
			return (S) service;
		} else {
			if (thirdpartyServiceDelegate != null) {
				if (thirdpartyServiceDelegate.isFor(selector, this)) {
					Object service = thirdpartyServiceDelegate.createService(selector, this);
					// do NOT register the service since another call may return
					// a different service instance.
					return (S) service;
				}
			}
		}

		return null;
	}

	/**
	 * Sets the URI that is used to determine the UI kit that should be used to
	 * render the UI.
	 * 
	 * @param presentationURI
	 *            URI selecting the UI-Kit
	 */
	public void setPresentationURI(String presentationURI) {
		checkDisposed();

		if (isRendered()) {
			throw new IllegalArgumentException("Not valid if already rendered!");
		}

		this.presentationURI = presentationURI;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IViewEditpart getViewEditpart() {
		checkDisposed();

		return viewEditpart;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.context.IViewContext#
	 * getVisibilityManager()
	 */
	@Override
	public IVisibilityManager getVisibilityManager() {
		if (visibilityManager == null) {
			visibilityManager = new VisibilityManager(this);
		}
		return visibilityManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.context.IViewContext#
	 * getExposedActions()
	 */
	@Override
	public List<IExposedActionEditpart> getExposedActions() {
		return getViewEditpart().getExposedActions();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getRootLayout() {
		checkDisposed();

		return rootLayout;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.runtime.common.dispose.AbstractDisposable#dispose()
	 */
	@Override
	synchronized public void dispose() {
		preDispose();

		super.dispose();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void render(String presentationURI, Object rootLayout, Map<String, Object> parameter)
			throws ContextException {
		checkDisposed();
		renderingParameters = parameter;
		
		if (rootLayout == null) {
			throw new ContextException("RootLayout must not be null!");
		}

		if (rendered) {
			throw new ContextException("Has already been rendered!");
		}

		try {
			this.presentationURI = presentationURI;
			this.rootLayout = rootLayout;

			// start to render
			//
			IViewEditpart editPart = getViewEditpart();

			if (parameter != null) {
				// Set the configuration
				IConfiguration config = (IConfiguration) parameter.get(IViewContext.PARAM_CONFIGURATION);
				if (config != null) {
					editPart.setConfiguration(config);
				}

				// Set the third party service delegate
				thirdpartyServiceDelegate = (IThirdPartyServiceDelegate) parameter
						.get(IViewContext.PARAM_THIRDPARTY_SERVICE_PROVIDER);

				// Register all services
				@SuppressWarnings("unchecked")
				Map<String, Object> services = (Map<String, Object>) parameter.get(IViewContext.PARAM_SERVICES);
				if (services != null) {
					for (Map.Entry<String, Object> entry : services.entrySet()) {
						if (thirdpartyServiceDelegate == null
								&& entry.getKey().equals(IViewContext.PARAM_THIRDPARTY_SERVICE_PROVIDER)) {
							thirdpartyServiceDelegate = (IThirdPartyServiceDelegate) entry.getValue();
						}

						registerService(entry.getKey(), entry.getValue());
					}
				}
			}

			// render the UI
			editPart.render(parameter);

			ILifecycleService lifecycleService = getService(ILifecycleService.class.getName());
			// send event that context is disposed
			if (lifecycleService != null) {
				lifecycleService
						.notifyLifecycle(new LifecycleEvent(getViewEditpart(), ILifecycleEvent.CONTEXT_CREATED));
			}
		} catch (Exception ex) {
			logger.error("{}", ex);
		} finally {
			rendered = true;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRendered() {
		checkDisposed();

		return rendered;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.context.IViewContext#exec(java.lang.
	 * Runnable)
	 */
	@Override
	public void exec(Runnable runnable) {
		checkDisposed();

		getViewEditpart().exec(runnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.context.IViewContext#execAsync(java.
	 * lang.Runnable)
	 */
	@Override
	public Future<?> execAsync(Runnable runnable) {
		checkDisposed();

		return getViewEditpart().execAsync(runnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.context.DisposableContext#
	 * updateLocale(java.util.Locale)
	 */
	@Override
	protected void updateLocale(Locale locale) {
		ILocaleChangedService service = getService(ILocaleChangedService.ID);
		if (service != null) {
			service.notifyLocaleChanged(locale);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.context.IViewContext#findModelElement
	 * (java.lang.String)
	 */
	@Override
	public Object findModelElement(String id) {
		return viewEditpart.findModelElement(id);
	}

	
	@Override
	public Object findYEmbeddableElement(String id) {
		return viewEditpart.findYEmbeddableElement(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.context.IViewContext#findBoundField(
	 * java.lang.String)
	 */
	@Override
	public Object findBoundField(String bindingIdRegex) {
		return viewEditpart.findBoundField(bindingIdRegex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.context.DisposableContext#preDispose(
	 * )
	 */
	protected void preDispose() {
		super.preDispose();

		finalDispose.put(IWidgetAssocationsService.class.getName(), getService(IWidgetAssocationsService.ID));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void internalDispose() {
		ILifecycleService lifecycleService = getService(ILifecycleService.class.getName());
		try {
			viewEditpart.dispose();

			// Clear all associations
			@SuppressWarnings("rawtypes")
			IWidgetAssocationsService service = (IWidgetAssocationsService) finalDispose
					.get(IWidgetAssocationsService.ID);
			if (service != null) {
				service.clear();
			}
			finalDispose = null;
			visibilityManager = null;
		} finally {
			super.internalDispose();

			// send event that context is disposed
			if (lifecycleService != null) {
				lifecycleService
						.notifyLifecycle(new LifecycleEvent(getViewEditpart(), ILifecycleEvent.CONTEXT_DISPOSED));
			}
		}
	}

	/**
	 * This service notifies interested parties about a locale change.
	 */
	public class LocaleChangedService extends AbstractDisposable implements ILocaleChangedService {

		/** The listeners. */
		private List<LocaleListener> listeners;

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.osbp.ecview.core.common.context.ILocaleChangedService#
		 * addLocaleListener(org.eclipse.osbp.ecview.core.common.context.
		 * ILocaleChangedService.LocaleListener)
		 */
		@Override
		public void addLocaleListener(LocaleListener listener) {
			checkDisposed();

			if (listeners == null) {
				listeners = new ArrayList<ILocaleChangedService.LocaleListener>();
			}

			if (!listeners.contains(listener)) {
				listeners.add(listener);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.osbp.ecview.core.common.context.ILocaleChangedService#
		 * removeLocaleListener(org.eclipse.osbp.ecview.core.common.context.
		 * ILocaleChangedService.LocaleListener)
		 */
		@Override
		public void removeLocaleListener(LocaleListener listener) {
			checkDisposed();

			if (listeners == null) {
				return;
			}

			listeners.remove(listener);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.osbp.ecview.core.common.context.ILocaleChangedService#
		 * notifyLocaleChanged(java.util.Locale)
		 */
		public void notifyLocaleChanged(Locale locale) {
			if (listeners == null) {
				return;
			}

			for (LocaleListener listener : listeners.toArray(new LocaleListener[listeners.size()])) {
				listener.localeChanged(locale);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.osbp.runtime.common.dispose.AbstractDisposable#
		 * internalDispose()
		 */
		@Override
		protected void internalDispose() {
			if (listeners != null) {
				listeners.clear();
				listeners = null;
			}
		}

	}

	@Override
	public Map<String, Object> getRenderingParams() {
		return renderingParameters;
	}

	@Override
	public void setRenderingParams(Map<String, Object> params) {
		renderingParameters.clear();
		renderingParameters.putAll(params);
	}
}
