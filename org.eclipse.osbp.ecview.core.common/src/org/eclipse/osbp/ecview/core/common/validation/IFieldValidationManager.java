/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.validation;

import java.util.List;

import org.eclipse.osbp.runtime.common.dispose.IDisposable;
import org.eclipse.osbp.runtime.common.validation.IStatus;

// TODO: Auto-generated Javadoc
/**
 * This class is responsible to handle validation results on UI level.
 */
public interface IFieldValidationManager extends IDisposable {

	/**
	 * Register validation status for the given context.
	 *
	 * @param context
	 *            the context
	 * @param status
	 *            the status
	 */
	void registerResult(Object context, List<IStatus> status);

	/**
	 * Returns all validation results for the given context.
	 *
	 * @param context
	 *            the context
	 * @return the results
	 */
	List<IStatus> getResults(Object context);

	/**
	 * Sets the enhancer.
	 *
	 * @param enhancer
	 *            the new enhancer
	 */
	void setEnhancer(Enhancer enhancer);

	/**
	 * Adds a new validation listener.
	 *
	 * @param listener
	 *            the listener
	 */
	void addListener(Listener listener);

	/**
	 * Removes the given validation listener.
	 *
	 * @param listener
	 *            the listener
	 */
	void removeListener(Listener listener);

	/**
	 * An enhancer may be used, to enhance the IStatus with additional
	 * information. For instance a property for the currently active view.
	 */
	interface Enhancer {

		/**
		 * Enhances the status.
		 *
		 * @param status
		 *            the status
		 */
		void enhance(IStatus status);

	}

	/**
	 * Notifies the listener about a changed validation.
	 */
	interface Listener {

		/**
		 * Is called, if the status changed.
		 *
		 * @param event
		 *            the event
		 */
		void validationChanged(Event event);
	}

	/**
	 * The Class Event.
	 */
	public static class Event {

		/** The context. */
		private final Object context;
		
		/** The old status. */
		private final List<IStatus> oldStatus;
		
		/** The new status. */
		private final List<IStatus> newStatus;

		/**
		 * Instantiates a new event.
		 *
		 * @param context
		 *            the context
		 * @param oldStatus
		 *            the old status
		 * @param newStatus
		 *            the new status
		 */
		public Event(Object context, List<IStatus> oldStatus,
				List<IStatus> newStatus) {
			super();
			this.context = context;
			this.oldStatus = oldStatus;
			this.newStatus = newStatus;
		}

		/**
		 * Gets the context.
		 *
		 * @return the context
		 */
		public Object getContext() {
			return context;
		}

		/**
		 * Gets the old status.
		 *
		 * @return the oldStatus
		 */
		public List<IStatus> getOldStatus() {
			return oldStatus;
		}

		/**
		 * Gets the new status.
		 *
		 * @return the newStatus
		 */
		public List<IStatus> getNewStatus() {
			return newStatus;
		}

	}
}
