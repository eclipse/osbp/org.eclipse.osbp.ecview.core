/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.visibility;

import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler;

// TODO: Auto-generated Javadoc
/**
 * An element that can handle visibility properties.
 */
public interface IVisibilityProcessable {

	/**
	 * Is called to apply the visibility properties. If <code>null</code> is
	 * passed, it means the same as {@link #resetVisibilityProperties()}.
	 *
	 * @param handler
	 *            the handler
	 */
	void apply(IVisibilityHandler handler);

	/**
	 * Is called to reset the currently set visibility properties.
	 */
	void resetVisibilityProperties();

}
