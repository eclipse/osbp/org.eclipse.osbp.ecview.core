/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.uri;


// TODO: Auto-generated Javadoc
/**
 * Abstract class to unify all the methods used in the extended scope classes.
 *
 * @author dominguez
 */
public abstract class AbstractScope implements IURIScope {

	/** The segment. */
	private String segment;

	/**
	 * Instantiates a new abstract scope.
	 */
	protected AbstractScope() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.uri.IURIScope#getURISegment()
	 */
	public String getURISegment() {
		return segment;
	}

	/**
	 * Sets the URI segment. See {@link #getURISegment()}.
	 *
	 * @param segment
	 *            the new URI segment
	 */
	protected void setURISegment(String segment) {
		this.segment = segment;
	}

}
