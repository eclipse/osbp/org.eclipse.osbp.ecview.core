/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.filter;

// TODO: Auto-generated Javadoc
/**
 * The presentation will provide filters that may be used for queries. Therefore
 * it collects all filters from its search fields and creates a compound filter.<br>
 * The type of filter depends on the used UI-Kit.
 */
public interface IFilterProvidingPresentation {

	/**
	 * Returns the filter that can be used for queries.
	 *
	 * @return the filter
	 */
	Object getFilter();

}
