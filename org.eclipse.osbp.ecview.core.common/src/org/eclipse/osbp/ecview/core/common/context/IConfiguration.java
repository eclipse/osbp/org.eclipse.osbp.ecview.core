/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.context;

// TODO: Auto-generated Javadoc
/**
 * This class is called if the view becomes rendered.
 * 
 * @noimplement should not be implemented by clients. Use ConfigurationAdapter
 *              instead.
 */
public interface IConfiguration {

	/**
	 * Is called before the UI rendering is started.
	 *
	 * @param context
	 *            the context
	 */
	void beforeUiRendering(IContext context);

	/**
	 * Is called after the UI rendering was finished.
	 *
	 * @param context
	 *            the context
	 */
	void afterUiRendering(IContext context);

	/**
	 * Is called before the binding is activated.
	 *
	 * @param context
	 *            the context
	 */
	void beforeBind(IContext context);

	/**
	 * Is called after the binding was activated.
	 *
	 * @param context
	 *            the context
	 */
	void afterBind(IContext context);

}
