/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */

package org.eclipse.osbp.ecview.core.common.notification;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IValueBindingEditpart;

// TODO: Auto-generated Javadoc
/**
 * Lifecycle events are sent by different editparts to notify the
 * {@link IViewEditpart} about it. A common case is, that an
 * {@link IEmbeddableEditpart} becomes disposed and the view need to dispose the
 * {@link IValueBindingEditpart} too, since the binding editPart is not part of
 * the containment tree of the embeddable..
 */
public interface ILifecycleEvent {

	
	public static final String TYPE_RENDERING = "rendering";
	
	/**
	 * If a new context was created and rendered.
	 */
	public static final String CONTEXT_CREATED = "contextCreated";

	/**
	 * If the context was disposed.
	 */
	public static final String CONTEXT_DISPOSED = "contextDisposed";

	/**
	 * If any component is rendered.
	 */
	public static final String TYPE_RENDERED = "rendered";

	/**
	 * If any component is unrendered.
	 */
	public static final String TYPE_UNRENDERED = "unrendered";

	/**
	 * If any component is disposed.
	 */
	public static final String TYPE_DISPOSED = "disposed";

	/**
	 * Returns the editpart that sent the event.
	 *
	 * @return the editpart
	 */
	IElementEditpart getEditpart();

	/**
	 * Returns the type of the event.
	 *
	 * @return the type
	 */
	String getType();
}
