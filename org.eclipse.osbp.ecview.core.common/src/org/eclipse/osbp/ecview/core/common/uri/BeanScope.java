/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.uri;

import java.net.URI;

import org.eclipse.osbp.ecview.core.common.beans.IBeanRegistry;
import org.eclipse.osbp.ecview.core.common.beans.ISlot;

// TODO: Auto-generated Javadoc
/**
 * The Class BeanScope.
 */
public class BeanScope extends AbstractScope {

	/** The Constant URI_AUTHORITY. */
	public static final String URI_AUTHORITY = "bean";
	
	/** The Constant SEGMENT_TEMPLATE. */
	private static final String SEGMENT_TEMPLATE = "bean/%s";
	
	/** The parent. */
	private final AccessibleScope parent;
	
	/** The selector. */
	private final String selector;

	/** The fragment scope. */
	private FragmentScope fragmentScope;

	/**
	 * Instantiates a new bean scope.
	 *
	 * @param parent
	 *            the parent
	 * @param selector
	 *            the selector
	 */
	public BeanScope(AccessibleScope parent, String selector) {
		this.parent = parent;
		this.selector = selector;

		setURISegment(String.format(SEGMENT_TEMPLATE, selector));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.uri.IURIScope#root()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends AccessibleScope> A root() {
		return (A) parent;
	}

	/**
	 * Gets the view scope.
	 *
	 * @return the view scope
	 */
	protected AccessibleScope getViewScope() {
		return root();
	}

	/**
	 * Returns the URI representation of this bean scope.
	 * 
	 * @return URI
	 */
	public URI toURI() {
		return URI.create(toURIString());
	}

	/**
	 * Returns the URI string of the produced string token.
	 * 
	 * @return String string
	 */
	public String toURIString() {
		return parent.getURISegment().concat(getURISegment());
	}

	/**
	 * Returns the fragment scope including the given fragment. <br>
	 * Is part of fluent API.
	 *
	 * @param fragment
	 *            the fragment
	 * @return the fragment scope
	 */
	public FragmentScope fragment(String fragment) {
		if (fragment == null || fragment.equals("")) {
			return null;
		}

		fragmentScope = new FragmentScope(this, fragment);
		return fragmentScope;
	}

	/**
	 * Accesses the object in the given context that is described by the uri
	 * segment of this scope. Also delegates to fragment scope if exists.
	 *
	 * @param accessible
	 *            the accessible
	 * @return the object
	 */
	public Object access(IBeanRegistry accessible) {
		Object result = null;
		Object bean = accessible.getBeanSlot(selector);
		if (fragmentScope != null) {
			result = fragmentScope.access(bean);
		} else {
			result = bean;
		}
		return result;
	}

	/**
	 * Accesses the bean slot in the given context and returns its value. URI
	 * fragments are ignored.
	 *
	 * @param accessible
	 *            the accessible
	 * @return the object
	 */
	public Object accessBean(IBeanRegistry accessible) {
		return accessBeanSlot(accessible).getValue();
	}

	/**
	 * Accesses the bean slot in the given context and returns it. URI fragments
	 * are ignored.
	 *
	 * @param accessible
	 *            the accessible
	 * @return the i slot
	 */
	public ISlot accessBeanSlot(IBeanRegistry accessible) {
		return accessible.getBeanSlot(selector);
	}

	/**
	 * Creates a new bean slot if not present yet.
	 *
	 * @param accessible
	 *            the accessible
	 * @param type
	 *            The type of the object that will be stored in the bean slot
	 * @return the i slot
	 */
	public ISlot createBeanSlot(IBeanRegistry accessible, Class<?> type) {
		ISlot slot = accessible.getBeanSlot(selector);
		if (slot == null) {
			slot = accessible.createBeanSlot(selector, type);
		}
		return slot;
	}

	/**
	 * Returns the selector that points to the target value in the bean scope.
	 *
	 * @return the selector
	 */
	public String getSelector() {
		return selector;
	}

	/**
	 * Returns the fragment scope.
	 *
	 * @return FragmentScope
	 */
	public FragmentScope getFragmentScope() {
		return fragmentScope;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return toURIString();
	}

}
