/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.validation;

import java.util.Locale;

import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.common.validation.IStatus;

// TODO: Auto-generated Javadoc
/**
 * A validator that returns true by default, if the given value is not a String.
 */
public abstract class StringValidator extends AbstractCollectingValidator
		implements IValidator {

	/** The error code. */
	protected final String errorCode;
	
	/** The locale. */
	protected Locale locale;
	
	/** The i18n service. */
	protected II18nService i18nService;

	/**
	 * Instantiates a new string validator.
	 *
	 * @param errorCode
	 *            the error code
	 */
	public StringValidator(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Returns the raw message containing replacement patterns.
	 *
	 * @return the message
	 */
	protected String getMessage() {
		String result = null;
		if (i18nService != null) {
			if (isStringValid(errorCode)) {
				result = i18nService.getValue(errorCode, locale);
			}
			if (!isStringValid(result)) {
				result = getDefaultMessage();
			}
			if (!isStringValid(result)) {
				result = i18nService.getValue(getDefaultErrorCode(), locale);
			}
			if (!isStringValid(result)) {
				result = "Error message missing for " + getDefaultErrorCode();
			}
		} else {
			result = getDefaultMessage();
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.validation.IValidator#isCheckValidType()
	 */
	@Override
	public boolean isCheckValidType() {
		return true;
	}

	/**
	 * Checks if is string valid.
	 *
	 * @param result
	 *            the result
	 * @return true, if is string valid
	 */
	protected boolean isStringValid(String result) {
		return result != null && !result.equals("");
	}

	/**
	 * Returns the default error code.
	 *
	 * @return the default error code
	 */
	protected abstract String getDefaultErrorCode();

	/**
	 * Returns the default message.
	 *
	 * @return the default message
	 */
	protected abstract String getDefaultMessage();

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.validation.IValidator#getType()
	 */
	@Override
	public Class<?> getType() {
		return String.class;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.validation.IValidator#validateValue(java.lang.Object)
	 */
	@Override
	public IStatus validateValue(Object value) {
		if (!(value instanceof String)) {
			return IStatus.OK;
		}

		IStatus result = doValidate((String) value);
		resetCurrentStatus();
		addCurrentStatus(result);
		return result;
	}

	/**
	 * Delegates the given value.
	 *
	 * @param value
	 *            the value
	 * @return IStatus - the validation status.
	 */
	protected abstract IStatus doValidate(String value);

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.validation.IValidator#setLocale(java.util.Locale)
	 */
	@Override
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.validation.IValidator#setI18nService(org.eclipse.osbp.runtime.common.i18n.II18nService)
	 */
	@Override
	public void setI18nService(II18nService i18nService) {
		this.i18nService = i18nService;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.validation.AbstractCollectingValidator#internalDispose()
	 */
	@Override
	protected void internalDispose() {

	}

}
