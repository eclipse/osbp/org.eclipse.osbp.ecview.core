/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.context;

/**
 * Exception thrown by contexts.
 */
@SuppressWarnings("serial")
public class ContextException extends Exception {

	/**
	 * Constructor.
	 */
	public ContextException() {
		super();
	}

	/**
	 * Constructor.
	 * @param message Message to be contained
	 */
	public ContextException(String message) {
		super(message);
	}

}