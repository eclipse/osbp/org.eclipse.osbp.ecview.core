/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.validation;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;

// TODO: Auto-generated Javadoc
/**
 * An editpart responsible to handle validations. If the set config was
 * disposed, the validator editpart will not handle this. It is the issue of the
 * parent editparts to handle this.
 */
public interface IValidatorEditpart extends IElementEditpart {

	/**
	 * Returns the validator for this editpart.
	 *
	 * @param <A>
	 *            the generic type
	 * @return the validator
	 */
	<A extends IValidator> A getValidator();

	/**
	 * Registers the validation config at the validator editpart. The config
	 * provides information how the validator has to validate an UI element. For
	 * instance the maxLength of a MaxLengthValidator.
	 *
	 * @param config
	 *            the new config
	 */
	void setConfig(IValidationConfig config);

}
