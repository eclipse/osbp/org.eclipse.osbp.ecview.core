/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.validation;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;

/**
 * A validation config marks a class that it provides information how to
 * validate an element. For instance datatype editparts may implement that
 * interface.
 */
public interface IValidationConfig {

	/**
	 * Returns the setting of the config. The type of the return value depends
	 * on the implementation of the editpart.
	 *
	 * @return the validation settings
	 */
	Object getValidationSettings();
	
	/**
	 * Returns the view context.
	 * @return
	 */
	IViewContext getContext();

}
