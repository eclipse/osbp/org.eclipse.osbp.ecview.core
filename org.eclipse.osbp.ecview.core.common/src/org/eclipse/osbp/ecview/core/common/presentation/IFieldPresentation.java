/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.presentation;

import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.runtime.common.validation.IStatus;

// TODO: Auto-generated Javadoc
/**
 * FieldPresentations are elements that display information but can not embed
 * any further widgets like layouts.
 *
 * @param <C>
 *            the generic type
 */
public interface IFieldPresentation<C> extends IWidgetPresentation<C> {

	/**
	 * Adds the validator to the field.
	 *
	 * @param validator
	 *            the validator
	 */
	void addValidator(IValidator validator);

	/**
	 * Removes the validator from the field.
	 *
	 * @param validator
	 *            the validator
	 */
	void removeValidator(IValidator validator);

	/**
	 * Sets the converter that should be used for this field to converter data
	 * "from and to model" and "from and to UI".
	 *
	 * @param object
	 *            the new converter
	 */
	void setConverter(Object object);

	/**
	 * Adds an external status to the validations of the field.
	 *
	 * @param status
	 *            the status
	 */
	void addExternalStatus(IStatus status);

	/**
	 * Removes an external status from the validations of the field.
	 *
	 * @param status
	 *            the status
	 */
	void removeExternalStatus(IStatus status);

	/**
	 * Resets all external status assigments.
	 */
	public void resetExternalStatus();

	/**
	 * An event to indicate added or removed validator.
	 */
	public static class Event {

		/** The Constant ADD. */
		public static final byte ADD = 0;
		
		/** The Constant REMOVE. */
		public static final byte REMOVE = 1;

		/** The type. */
		private final short type;
		
		/** The validator. */
		private final IValidator validator;

		/**
		 * Instantiates a new event.
		 *
		 * @param type
		 *            the type
		 * @param validator
		 *            the validator
		 */
		public Event(byte type, IValidator validator) {
			super();
			this.type = type;
			this.validator = validator;
		}

		/**
		 * Returns the event. May be {@link #ADD} or {@link #REMOVE}.
		 *
		 * @return the type
		 */
		public short getType() {
			return type;
		}

		/**
		 * Returns the validator.
		 *
		 * @return the validator
		 */
		public IValidator getValidator() {
			return validator;
		}

	}

}
