/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.common.validation;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;
import org.eclipse.osbp.runtime.common.validation.IStatus;

public abstract class AbstractCollectingValidator extends AbstractDisposable
		implements IValidator {

	protected Set<IStatus> currentStatus = new HashSet<IStatus>();

	@Override
	public Set<IStatus> getCurrentStatus() {
		return currentStatus;
	}

	protected void resetCurrentStatus() {
		currentStatus.clear();
	}

	protected void addCurrentStatus(IStatus status) {
		currentStatus.add(status);
	}
	
	protected void addCurrentStatus(Set<IStatus> status) {
		currentStatus.addAll(status);
	}

	protected void internalDispose() {
		currentStatus.clear();
		currentStatus = null;
	}
}
