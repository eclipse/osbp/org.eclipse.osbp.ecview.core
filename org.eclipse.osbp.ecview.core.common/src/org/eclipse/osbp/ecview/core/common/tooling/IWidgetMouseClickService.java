/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.tooling;

import org.eclipse.osbp.ecview.core.common.services.IUiKitBasedService;
import org.eclipse.osbp.runtime.common.event.IEventBroker;

// TODO: Auto-generated Javadoc
/**
 * Is used to become notified about click events on elements. Attention: On
 * activating that service, ALL mouse events will be sent to the listener.
 */
public interface IWidgetMouseClickService extends IUiKitBasedService {

	/** The Constant ID. */
	public static final String ID = IWidgetMouseClickService.class.getName();
	
	/**
	 * If a mouse click occured, then an event is sent to this topic by
	 * {@link IEventBroker}, if the event broker is available.<br>
	 * As data a map is sent with the keys {@value #PROP_WIDGET} and {@value #PROP_MODEL_ELEMENT}
	 */
	public static final String EVENT_TOPIC = "ecview/mouseclick";
	
	/** The Constant PROP_WIDGET. */
	public static final String PROP_WIDGET = "widget";
	
	/** The Constant PROP_MODEL_ELEMENT. */
	public static final String PROP_MODEL_ELEMENT = "modelElement";

	/**
	 * Activates the service. All mouse events will be forwarded to the
	 * listeners.
	 */
	void activate();

	/**
	 * Dectivates the service. Mouse events will not be forwarded to the
	 * listeners.
	 */
	void dactivate();

	/**
	 * Adds a mouse click listener.
	 *
	 * @param listener
	 *            the listener
	 */
	void addListener(Listener listener);

	/**
	 * Removes the mouse click listener.
	 *
	 * @param listener
	 *            the listener
	 */
	void removeListener(Listener listener);

	/**
	 * Resolves the model element for the given UI element.
	 */
	interface ModelResolver {

		/**
		 * Returns the model element for the given ui element.
		 *
		 * @param uiWidget
		 *            the ui widget
		 * @return the object
		 */
		Object resolve(Object uiWidget);

	}

	/**
	 * The Interface Listener.
	 */
	interface Listener {

		/**
		 * Notifies.
		 *
		 * @param modelElement
		 *            the model element
		 */
		void clicked(Object modelElement);

	}
}
