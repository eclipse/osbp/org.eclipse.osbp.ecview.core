/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.notification;

// TODO: Auto-generated Javadoc
/**
 * {@link ILifecycleHandler} may use that service to register for
 * {@link ILifecycleEvent lifecycle events}.
 * <p>
 * Should not be implemented by clients.
 */
public interface ILifecycleService {

	/**
	 * Returns the state of the lifecycle. Following ILifecycleEvent#{STATES}.
	 * 
	 * @return
	 */
	String getState();

	/**
	 * Adds a handler to the service. Calling the method twice has no effect.
	 *
	 * @param handler
	 *            the handler
	 */
	void addHandler(ILifecycleHandler handler);

	/**
	 * Removes the given handler.
	 *
	 * @param handler
	 *            the handler
	 */
	void removeHandler(ILifecycleHandler handler);

	/**
	 * Sends a lifecycle event to all registered handler.
	 *
	 * @param event
	 *            the event
	 */
	void notifyLifecycle(ILifecycleEvent event);

}
