/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.uri;

import org.eclipse.osbp.ecview.core.common.IAccessible;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewScope.
 */
public class ViewScope extends AccessibleScope {

	/** The Constant SCHEMA. */
	public static final String SCHEMA = "view";
	
	/** The Constant URI_SEGMENT. */
	private static final String URI_SEGMENT = SCHEMA + "://";

	/**
	 * Instantiates a new view scope.
	 */
	public ViewScope() {
		super(URI_SEGMENT);
	}

	/**
	 * Accesses the object in the given context that is described by the uri of
	 * this scope.
	 *
	 * @param context
	 *            the context
	 * @return the object
	 */
	public Object access(IViewContext context) {
		if (beanScope != null) {
			return beanScope.access(context);
		} else if (serviceScope != null) {
			return serviceScope.access(context);
		}

		return null;
	}

	/**
	 * Accesses the object in the given context that is described by the uri of
	 * this scope.
	 *
	 * @param accessible
	 *            the accessible
	 * @return the object
	 */
	public Object access(IAccessible accessible) {

		if (!(accessible instanceof IViewContext)) {
			throw new IllegalStateException(String.format(
					"%s is not a legal accessible!", accessible.getClass()
							.getName()));
		}

		return access((IViewContext) accessible);
	}
}
