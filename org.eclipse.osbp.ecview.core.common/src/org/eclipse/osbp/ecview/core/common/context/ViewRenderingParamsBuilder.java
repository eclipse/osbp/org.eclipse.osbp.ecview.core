/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.context;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.osbp.ecview.core.common.services.IThirdPartyServiceDelegate;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewRenderingParamsBuilder.
 */
public class ViewRenderingParamsBuilder {

	/** The params. */
	private Map<String, Object> params = new HashMap<>();
	
	/** The services map. */
	private Map<String, Object> servicesMap = new HashMap<>();

	/**
	 * Instantiates a new view rendering params builder.
	 */
	private ViewRenderingParamsBuilder() {

	}

	/**
	 * Creates the builder.
	 *
	 * @return the view rendering params builder
	 */
	public static ViewRenderingParamsBuilder builder() {
		return new ViewRenderingParamsBuilder();
	}

	/**
	 * Adds the given key and value to the params map.
	 *
	 * @param key
	 *            the key
	 * @param service
	 *            the service
	 * @return the view rendering params builder
	 */
	public ViewRenderingParamsBuilder put(String key, Object service) {
		params.put(key, service);
		return this;
	}

	/**
	 * The key is the name of the service and the Object is the instance.
	 *
	 * @param key
	 *            the key
	 * @param service
	 *            the service
	 * @return the view rendering params builder
	 */
	public ViewRenderingParamsBuilder service(String key, Object service) {
		servicesMap.put(key, service);
		return this;
	}

	/**
	 * Adds the given map as services. The key is the name of the service and
	 * the Object the instance.
	 *
	 * @param services
	 *            the services
	 * @return the view rendering params builder
	 */
	public ViewRenderingParamsBuilder services(Map<String, Object> services) {
		servicesMap.putAll(services);
		return this;
	}

	/**
	 * Adds the {@link IConfiguration} to the rendering params.
	 *
	 * @param configuration
	 *            the configuration
	 * @return the view rendering params builder
	 */
	public ViewRenderingParamsBuilder config(IConfiguration configuration) {
		params.put(IViewContext.PARAM_CONFIGURATION, configuration);
		return this;
	}

	/**
	 * Adds the {@link IThirdPartyServiceDelegate} to the rendering params.
	 *
	 * @param delegate
	 *            the delegate
	 * @return the view rendering params builder
	 */
	public ViewRenderingParamsBuilder thirdPartyServices(
			IThirdPartyServiceDelegate delegate) {
		params.put(IViewContext.PARAM_THIRDPARTY_SERVICE_PROVIDER, delegate);
		return this;
	}

	/**
	 * Returns the resulting parameter map.
	 *
	 * @return the map
	 */
	public Map<String, Object> get() {
		params.put(IViewContext.PARAM_SERVICES, servicesMap);
		return params;
	}

}
