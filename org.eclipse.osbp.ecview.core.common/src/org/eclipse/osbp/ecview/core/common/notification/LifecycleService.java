/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.notification;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.runtime.common.event.IEventBroker;

public class LifecycleService implements ILifecycleService {

	private Set<ILifecycleHandler> handlers = Collections.synchronizedSet(new HashSet<ILifecycleHandler>());

	private final IEventBroker eventBroker;

	private String state = ILifecycleEvent.TYPE_RENDERING;

	/**
	 * The constructor.
	 * 
	 * @param eventBroker
	 *            may be <code>null</code>.
	 */
	public LifecycleService(IEventBroker eventBroker) {
		this.eventBroker = eventBroker;
	}

	@Override
	public void addHandler(ILifecycleHandler handler) {
		handlers.add(handler);
	}

	@Override
	public void removeHandler(ILifecycleHandler handler) {
		handlers.remove(handler);
	}

	@Override
	public void notifyLifecycle(ILifecycleEvent event) {

		synchronized (handlers) {
			for (ILifecycleHandler handler : handlers) {
				handler.notifyLifecycle(event);
			}
		}

		// dispatch the event by the event broker
		if (eventBroker != null) {
			eventBroker.send(IViewContext.TOPIC_LIFECYCLE, event);
		}

		if (!(event.getEditpart().getModel() instanceof YView)) {
			return;
		}

		state = event.getType();

	}

	@Override
	public String getState() {
		return state;
	}
}
