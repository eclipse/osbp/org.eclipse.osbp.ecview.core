/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;

// TODO: Auto-generated Javadoc
/**
 * The root editpart. An editpart is a controller that handles the model and the
 * ui presentations.
 */
public interface IElementEditpart extends IDisposable {

	/**
	 * Returns the unique id of that edit part.
	 * 
	 * @return id The id of the editpart
	 */
	String getId();

	/**
	 * Returns the model associated with this edit part.<br>
	 * Or <code>null</code> if the edit parts are not based on a model.
	 * 
	 * @return the model or <code>null</code>
	 */
	Object getModel();

	/**
	 * Returns the view context. It offers access to more view related
	 * information.
	 * 
	 * @return context The view context
	 */
	IViewContext getContext();
	
	/**
	 * Requests disposal of the current editpart. This method will forward the
	 * request to the parent if available and the parent will handle the steps
	 * required to dispose the editpart properly.
	 */
	void requestDispose();
}
