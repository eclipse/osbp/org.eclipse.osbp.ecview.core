/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.validation;

import java.util.Locale;
import java.util.Set;

import org.eclipse.osbp.runtime.common.dispose.IDisposable;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.common.validation.IStatus;

/**
 * A validator is used to validate the given value.
 */
public interface IValidator extends IDisposable {

	/**
	 * Is called to update the validation parameter of this validator. For
	 * instance the minLength of a MinLengthValidator.
	 *
	 * @param model
	 *            the model
	 */
	void updateParameter(Object model);

	/**
	 * Checks if the given value is valid.
	 *
	 * @param value
	 *            the value
	 * @return the i status
	 */
	IStatus validateValue(Object value);

	/**
	 * Returns all currently collected status instances.
	 *
	 * @return the current status
	 */
	Set<IStatus> getCurrentStatus();

	/**
	 * Returns true, if isTypeValid() should be checked by this validator.
	 *
	 * @return true, if is check valid type
	 */
	boolean isCheckValidType();

	/**
	 * Returns the expected type of the value. If <code>null</code> is returned,
	 * the type will not become checked.
	 * 
	 * @return type - the expected type of the value or <code>null</code>
	 */
	Class<?> getType();

	/**
	 * Sets the locale the messages should be shown in.
	 *
	 * @param locale
	 *            the new locale
	 */
	void setLocale(Locale locale);

	/**
	 * Sets the i18n service to translate the messages.
	 *
	 * @param i18nService
	 *            the new i18n service
	 */
	void setI18nService(II18nService i18nService);

}
