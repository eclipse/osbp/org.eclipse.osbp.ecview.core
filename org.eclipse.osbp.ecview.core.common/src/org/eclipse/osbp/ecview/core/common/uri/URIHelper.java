/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.uri;

import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class URIHelper.
 */
public class URIHelper {

	/** The Constant logger. */
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory
			.getLogger(URIHelper.class);

	/**
	 * Creates a new view context scope.
	 *
	 * @return the view scope
	 */
	public static ViewScope view() {
		return new ViewScope();
	}

	/**
	 * Uses the uriString to create the {@link AccessibleScope} based on that
	 * information.
	 *
	 * @param <A>
	 *            the generic type
	 * @param uriString
	 *            the uri string
	 * @return the a
	 */
	public static <A extends AccessibleScope> A toScope(String uriString) {
		return toScope(URI.create(uriString));
	}

	/**
	 * Uses the uri to create the {@link AccessibleScope} based on that
	 * information.
	 *
	 * @param <A>
	 *            the generic type
	 * @param uri
	 *            the uri
	 * @return the a
	 */
	public static <A extends AccessibleScope> A toScope(URI uri) {
		A scope = null;
		if (ViewScope.SCHEMA.equals(uri.getScheme())) {
			scope = parse(uri, view());
		}
		return scope;
	}

	/**
	 * Parses the uri and invokes the related methods on the viewScope.
	 *
	 * @param <A>
	 *            the generic type
	 * @param uri
	 *            the uri
	 * @param viewScope
	 *            the view scope
	 * @return the a
	 */
	@SuppressWarnings("unchecked")
	private static <A extends AccessibleScope> A parse(URI uri,
			AccessibleScope viewScope) {

		// uri.getPath() always starts with "/", but as selector within the
		// context it is used without therefor the creation of selector-String
		//
		String selector = uri.getPath().substring(1);
		if (BeanScope.URI_AUTHORITY.equals(uri.getAuthority())) {
			viewScope.bean(selector).fragment(uri.getFragment());
		} else if (ServiceScope.URI_AUTHORITY.equals(uri.getAuthority())) {
			viewScope.service(selector);
		}
		return (A) viewScope;
	}

}
