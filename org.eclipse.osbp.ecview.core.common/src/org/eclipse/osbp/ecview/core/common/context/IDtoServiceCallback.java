package org.eclipse.osbp.ecview.core.common.context;

public interface IDtoServiceCallback {
	void persist(IViewContext dialogViewContext);
}
