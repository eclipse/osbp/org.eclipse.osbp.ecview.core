/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.concurrent;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;

// TODO: Auto-generated Javadoc
/**
 * The Class ECViewRunable.
 */
public abstract class ECViewRunable implements Runnable {

	/** The context. */
	private final IViewContext context;

	/**
	 * Instantiates a new EC view runable.
	 *
	 * @param context
	 *            the context
	 */
	public ECViewRunable(IViewContext context) {
		super();
		this.context = context;
	}

	/**
	 * The view context, this runnable is assigned to. May be <code>null</code>.
	 *
	 * @return the context
	 */
	public IViewContext getContext() {
		return context;
	}

}
