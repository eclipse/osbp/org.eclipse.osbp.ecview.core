/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.beans;

import org.eclipse.osbp.ecview.core.common.uri.AccessibleScope;
import org.eclipse.osbp.ecview.core.common.uri.URIHelper;

// TODO: Auto-generated Javadoc
/**
 * The Class BeanSlotInitializerAdapter.
 */
public abstract class BeanSlotInitializerAdapter implements
		IBeanSlotInitializer {

	/** The registry. */
	private IBeanRegistry registry;

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.beans.IBeanSlotInitializer#intialize(org.eclipse.osbp.ecview.core.common.beans.IBeanRegistry)
	 */
	@Override
	public void intialize(IBeanRegistry registry) {
		this.registry = registry;
		doIntialize();
	}

	/**
	 * Creates a bean slot in the given registry with the given type.
	 *
	 * @param uriString
	 *            the uri string
	 * @param type
	 *            the type
	 */
	protected void createBeanSlot(String uriString, Class<?> type) {
		AccessibleScope accessible = URIHelper.toScope(uriString);
		accessible.getBeanScope().createBeanSlot(registry, type);
	}

	/**
	 * The bean slot have to become initialized.
	 */
	protected abstract void doIntialize();

}
