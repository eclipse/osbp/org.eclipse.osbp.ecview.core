/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.runtime.common.validation.IStatus;

// TODO: Auto-generated Javadoc
/**
 * An IUiFieldEditpart is an abstraction of an input field. For normal it does
 * not contain any children, but is contained by {@link ILayoutEditpart}.<br>
 * See also {@link IEmbeddableEditpart}
 */
public interface IFieldEditpart extends IEmbeddableEditpart {

	/**
	 * Adds a validator to the field. Adding a validator twice has no effect.
	 *
	 * @param validator
	 *            the validator
	 */
	@Deprecated // use model instead
	void addValidator(IValidatorEditpart validator);

	/**
	 * Removes the validator from the field.
	 *
	 * @param validator
	 *            the validator
	 */
	@Deprecated // use model instead
	void removeValidator(IValidatorEditpart validator);

	/**
	 * Adds an external status to the validations of the field.
	 *
	 * @param status
	 *            the status
	 */
	void addExternalStatus(IStatus status);

	/**
	 * Removes an external status from the validations of the field.
	 *
	 * @param status
	 *            the status
	 */
	void removeExternalStatus(IStatus status);
	
	/**
	 * Resets all external status assignments.
	 */
	void resetExternalStatus();

	/**
	 * Returns an unmodifiable list of validators.
	 *
	 * @return the validators
	 */
	List<IValidatorEditpart> getValidators();

	/**
	 * Returns the converter editpart or <code>null</code> if no converter is
	 * available.
	 *
	 * @return the converter
	 */
	IConverterEditpart getConverter();

}