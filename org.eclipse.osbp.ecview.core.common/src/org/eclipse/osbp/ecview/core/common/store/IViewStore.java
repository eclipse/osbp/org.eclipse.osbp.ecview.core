/*
 *                                                                            
 *  Copyright (c) 2013, 2016 - Loetz GmbH&Co.KG, 69115 Heidelberg, Germany 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                               
 * 
 */
package org.eclipse.osbp.ecview.core.common.store;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.model.core.YView;

// TODO: check reference in class description
/**
 * A store to access saved views.
 */
public interface IViewStore {

	/**
	 * Returns the most recent view. If no save view could be found, the
	 * {link org.eclipse.osbp.ecview.extension.api.IECViewCache} is used to
	 * find a proper view.
	 *
	 * @param userId
	 *            the user id
	 * @param id
	 *            the id
	 * @return the most recent view
	 */
	YView getMostRecentView(String userId, String id);
 
	/**
	 * Saves the given view.
	 *
	 * @param userId
	 *            the user id
	 * @param yView
	 *            the y view
	 */
	void saveView(String userId, YView yView);

	/**
	 * Returns all available view versions for the given user id and the view
	 * id.
	 *
	 * @param userId
	 *            the user id
	 * @param id
	 *            the id
	 * @return the all view versions
	 */
	List<YView> getAllViewVersions(String userId, String id);

	/**
	 * Deletes the view with the given parameters.
	 *
	 * @param userId
	 *            the user id
	 * @param id
	 *            the id
	 * @param version
	 *            the version
	 */
	void deleteView(String userId, String id, String version);

	/**
	 * Exports the given view to the folder. If generateDialog is true, an
	 * additional file for the dialog dsl will be generated.
	 *
	 * @param view
	 *            the view
	 * @param folder
	 *            the folder
	 * @param generateDialog
	 *            the generate dialog
	 */
	void export(YView view, String folder, boolean generateDialog);

}
