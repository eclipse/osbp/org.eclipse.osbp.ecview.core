/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.visibility;

// TODO: Auto-generated Javadoc
/**
 * The Interface IVisibilityProcessor.
 */
public interface IVisibilityProcessor {

	/**
	 * Is called to initialize the visibility processor.
	 *
	 * @param manager
	 *            the manager
	 */
	void init(final IVisibilityManager manager);

	/**
	 * Fires the visibility processor.
	 */
	void fire();

	/**
	 * Sets the current input that triggers the visibility processor.
	 *
	 * @param input
	 *            the new input
	 */
	void setInput(Object input);

}
