/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.eclipse.osbp.ecview.core.common.context.ContextException;
import org.eclipse.osbp.ecview.core.common.context.IConfiguration;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart;
import org.eclipse.osbp.ecview.core.common.presentation.IViewPresentation;
import org.eclipse.osbp.ecview.core.common.services.IUiKitBasedService;

/**
 * An IUiViewEditpart is an abstraction of the root ui element of a composite
 * structure based on {@link IEmbeddableEditpart}. The IUiViewEditpart should
 * not be contained in a {@link ILayoutEditpart} but it can contain several
 * embeddables.
 */
public interface IViewEditpart extends IElementEditpart, IEmbeddableParent {

	/**
	 * Name for "content" property.
	 */
	String PROP_CONTENT = "content";

	/**
	 * An internal bean slot that provides the view editpart as a bean.
	 */
	String BEANSLOT__ECVIEW_CONTEXT = "ecviewContext";

	// /**
	// * Sets the view context. It offers access to more view related
	// information.
	// *
	// * @param context
	// * The view context
	// *
	// * @throws RuntimeException
	// * if the context changes and the editpart was already rendered.
	// */
	// // BEGIN SUPRESS CATCH EXCEPTION
	// void setContext(IViewContext context) throws RuntimeException;

	// END SUPRESS CATCH EXCEPTION

	/**
	 * Returns the name of the view.
	 * 
	 * @return name The name of the view.
	 */
	String getName();

	/**
	 * Is called to set the content of the view.
	 * 
	 * @param content
	 *            The content of the view.
	 */
	@Deprecated
	void setContent(IEmbeddableEditpart content);

	/**
	 * Sets the configuration. It will become invoked at rendering time.
	 *
	 * @param configuration
	 *            the new configuration
	 */
	void setConfiguration(IConfiguration configuration);

	/**
	 * Returns the content of the view.
	 * 
	 * @return content
	 */
	IEmbeddableEditpart getContent();

	/**
	 * Returns the view presenter for the edit part.
	 * 
	 * @param <A>
	 *            An instance of {@link IViewPresentation}
	 * @return presentation The presentation used to render the UI.
	 */
	<A extends IViewPresentation<?>> A getPresentation();

	/**
	 * Returns the bindingSet that is responsible to handle bindings.
	 *
	 * @return the binding set
	 */
	IBindingSetEditpart getBindingSet();

	/**
	 * Returns the exposed actions. These actions have to be handled by the
	 * wrapping UI elements. For instance an Eclipse view, Eclipse editor or a
	 * Vaaclipse view. Exposed actions are used to define the actions provided
	 * by the wrapping UI element.
	 *
	 * @return the exposed actions
	 */
	List<IExposedActionEditpart> getExposedActions();

	/**
	 * Is called to set the bindingSet of the view.
	 * 
	 * @param bindingSet
	 *            The bindingSet of the view.
	 */
	@Deprecated
	void setBindingSet(IBindingSetEditpart bindingSet);

	/**
	 * Returns the commandSet that is responsible to handle commands.
	 *
	 * @return the command set
	 */
	ICommandSetEditpart getCommandSet();

	/**
	 * Is called to set the commandSet of the view.
	 * 
	 * @param commandSet
	 *            The commandSet of the view.
	 */
	@Deprecated
	void setCommandSet(ICommandSetEditpart commandSet);

	/**
	 * Is called to render the view.
	 *
	 * @param options
	 *            can contain different options used for rendering
	 * @throws ContextException
	 *             the context exception
	 */
	void render(Map<String, Object> options) throws ContextException;

	/**
	 * Executes the given runnable. It is ensured that the runnable will be
	 * executed within the context of the user interface related to this
	 * context.
	 *
	 * @param runnable
	 *            the runnable
	 */
	void exec(Runnable runnable);

	/**
	 * Executes the given runnable in a different thread. It is ensured that the
	 * runnable will be executed within the context of the user interface
	 * related to this context.
	 *
	 * @param runnable
	 *            the runnable
	 * @return the future
	 */
	Future<?> execAsync(Runnable runnable);

	/**
	 * Returns a fully configured service for the given serviceClass.
	 *
	 * @param <A>
	 *            the generic type
	 * @param serviceClass
	 *            the service class
	 * @return the a
	 */
	<A extends IUiKitBasedService> A createService(Class<A> serviceClass);

	/**
	 * Opens the dialog.
	 *
	 * @param dialogEditpart
	 *            the dialog editpart
	 * @param inputData
	 *            this object contains information about the input data of the
	 *            target navigation page.
	 */
	void openDialog(IDialogEditpart dialogEditpart, IBindableEndpointEditpart inputData);

	/**
	 * Closes the dialog.
	 *
	 * @param dialogEditpart
	 *            the dialog editpart
	 */
	void closeDialog(IDialogEditpart dialogEditpart);

	/**
	 * Returns the model element with the given id or <code>null</code> if no
	 * element could be found.
	 *
	 * @param id
	 *            the id
	 * @return the object
	 */
	Object findModelElement(String id);

	/**
	 * Returns the YEmbeddable model element with the given id or
	 * <code>null</code> if no element could be found.
	 *
	 * @param id
	 *            the id
	 * @return the object
	 */
	Object findYEmbeddableElement(String id);

	/**
	 * Returns the embeddable model element which binds the given bindingId
	 * matching the given bindingIdRegex. Or <code>null</code> if no element
	 * could be found.
	 * <p>
	 * Following bindingIds are available in ECView:
	 * <ul>
	 * <li><code>beanslot/{beanslot}/{propertyPath}</code></li>
	 * <li><code>beanvalue/{beanClassName}/{propertyPath}</code></li>
	 * <li><code>modelBinding/{elementId}/{propertyPath}</code></li>
	 * <li><code>uiSelection/{elementId}/{propertyPath}</code></li>
	 * <li><code>uiValue/{elementId}</code></li>
	 * <li><code>uiVisibilityProcessor/{property}</code></li>
	 * <li><code>uiActivated/{elementId}</code></li>
	 * </ul>
	 *
	 * @param bindingIdRegex
	 *            the binding id regex
	 * @return the object
	 */
	Object findBoundField(String bindingIdRegex);

}
