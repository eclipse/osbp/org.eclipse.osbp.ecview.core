/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessable;

// TODO: Auto-generated Javadoc
/**
 * Exposed actions are defined inside the view, but have to be handeled in the
 * wrapping view, editor,...
 */
public interface IExposedActionEditpart extends IElementEditpart,
		IVisibilityProcessable {

	/**
	 * Returns the view edit part of this embeddable edit part.<br>
	 * May be <code>null</code> as far as this element and no of its parents are
	 * attached to a view.
	 * 
	 * @return editpart The view editpart
	 */
	IViewEditpart getView();

	/**
	 * Returns the name of the icon including the path information.
	 *
	 * @return the icon name
	 */
	String getIconName();

	/**
	 * Returns the description for the given action.
	 *
	 * @return the description
	 */
	String getDescription();

	/**
	 * Requests disposal of the current editpart. This method will forward the
	 * request to the parent if available and the parent will handle the steps
	 * required to dispose the editpart properly.
	 */
	void requestDispose();

	/**
	 * Returns the parent that contains this command.
	 *
	 * @return the parent
	 */
	IElementEditpart getParent();

}
