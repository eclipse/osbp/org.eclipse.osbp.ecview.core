/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.datatypes;

import java.util.Collections;
import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;

// TODO: Auto-generated Javadoc
/**
 * An editpart responsible to handle datatypes.
 */
public interface IDatatypeEditpart extends IElementEditpart {

	/**
	 * Returns the initial information about the datatype. The change object
	 * will be <code>null</code>.
	 *
	 * @return the current state
	 */
	DatatypeChangeEvent getCurrentState();

	/**
	 * Adds the given bridge to the datatype editpart. See
	 * {@link DatatypeBridge}.
	 * <p>
	 * Note that the datatype is not part of the containment tree of the UI
	 * element. So many UI elements may share the same instance of the datatype.
	 *
	 * @param bridge
	 *            the bridge
	 */
	void addBridge(DatatypeBridge bridge);

	/**
	 * Unregisters the given bridge from the datatype. See
	 * {@link DatatypeBridge}.
	 *
	 * @param bridge
	 *            the bridge
	 */
	void removeBridge(DatatypeBridge bridge);

	/**
	 * This event is fired if the datatype changed.
	 */
	public static class DatatypeChangeEvent {

		/** The editpart. */
		private final IDatatypeEditpart editpart;
		
		/** The change object. */
		private final Object changeObject;
		
		/** The removed validators. */
		private final List<IValidatorEditpart> removedValidators;
		
		/** The added validators. */
		private final List<IValidatorEditpart> addedValidators;
		
		/** The unset event. */
		private final boolean unsetEvent;

		/**
		 * Instantiates a new datatype change event.
		 *
		 * @param editpart
		 *            the editpart
		 * @param changeObject
		 *            the change object
		 * @param addedValidators
		 *            the added validators
		 * @param removedValidators
		 *            the removed validators
		 */
		public DatatypeChangeEvent(IDatatypeEditpart editpart,
				Object changeObject, List<IValidatorEditpart> addedValidators,
				List<IValidatorEditpart> removedValidators) {
			this(false, editpart, changeObject, addedValidators,
					removedValidators);
		}

		/**
		 * Instantiates a new datatype change event.
		 *
		 * @param unsetEvent
		 *            the unset event
		 * @param editpart
		 *            the editpart
		 * @param changeObject
		 *            the change object
		 * @param addedValidators
		 *            the added validators
		 * @param removedValidators
		 *            the removed validators
		 */
		public DatatypeChangeEvent(boolean unsetEvent,
				IDatatypeEditpart editpart, Object changeObject,
				List<IValidatorEditpart> addedValidators,
				List<IValidatorEditpart> removedValidators) {
			super();
			this.unsetEvent = unsetEvent;
			this.editpart = editpart;
			this.changeObject = changeObject;
			this.addedValidators = addedValidators;
			this.removedValidators = removedValidators;
		}

		/**
		 * Returns the datatype editpart that was changed.
		 *
		 * @return the editpart
		 */
		public IDatatypeEditpart getEditpart() {
			return editpart;
		}

		/**
		 * Returns the object containing the changes. The type depends on the
		 * editpart implementation and may be <code>null</code>.
		 *
		 * @return the change object
		 */
		public Object getChangeObject() {
			return changeObject;
		}

		/**
		 * Returns a list of validators that should be attached to the ui
		 * fields.
		 *
		 * @return the added validators
		 */
		public List<IValidatorEditpart> getAddedValidators() {
			return addedValidators != null ? addedValidators : Collections
					.<IValidatorEditpart> emptyList();
		}

		/**
		 * Returns a list of validators that should be removed from the ui
		 * fields.
		 *
		 * @return the removed validators
		 */
		public List<IValidatorEditpart> getRemovedValidators() {
			return removedValidators != null ? removedValidators : Collections
					.<IValidatorEditpart> emptyList();
		}

		/**
		 * If true, then the datatype is unset.
		 * 
		 * @return the unsetEvent
		 */
		public boolean isUnsetEvent() {
			return unsetEvent;
		}

	}

	/**
	 * This bridge allows the implementor to react for changes in the datatype.
	 * And it provides the datatype editpart with available validators that have
	 * been provided by the datatype editpart.
	 */
	public static interface DatatypeBridge {

		/**
		 * Notifies the listener about the change.
		 *
		 * @param event
		 *            the event
		 */
		void notifyDatatypeChanged(DatatypeChangeEvent event);

		/**
		 * Returns a list with currently available validators that have been
		 * provided by the datatype editpart. Must never return
		 * <code>null</code>.
		 *
		 * @return the datatype validators
		 */
		List<IValidatorEditpart> getDatatypeValidators();
	}
}
