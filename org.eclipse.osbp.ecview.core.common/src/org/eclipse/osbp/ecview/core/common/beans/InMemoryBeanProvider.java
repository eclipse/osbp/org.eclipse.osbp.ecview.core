/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */


package org.eclipse.osbp.ecview.core.common.beans;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * Implementations may return beans of any kind that are not persisted, but used
 * In-Memory. For instance as a suggestion list for combo boxes.
 */
public interface InMemoryBeanProvider {

	/**
	 * Returns a list of beans.
	 *
	 * @return the beans
	 */
	List<?> getBeans();

}
