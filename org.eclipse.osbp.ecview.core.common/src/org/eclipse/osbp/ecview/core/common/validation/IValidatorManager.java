/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.validation;

/**
 * Is responsible to return validators based on the given element.
 */
public interface IValidatorManager {

	/**
	 * Returns true, if the factory can be used for the given element.
	 * 
	 * @param modelElement
	 *            Any kind of object that contains information how to create the
	 *            validators.
	 * @return result True if the manager can be used for the modelElement
	 */
	boolean isFor(Object modelElement);

	/**
	 * Tries to create an validator for the given element.
	 * 
	 * @param <A>
	 *            An instance of {@link IValidator}
	 * @param modelElement
	 *            Any kind of object that contains information how to create the
	 *            validator.
	 * @return validator The created validator.
	 */
	<A extends IValidator> A createValidator(Object modelElement);

}