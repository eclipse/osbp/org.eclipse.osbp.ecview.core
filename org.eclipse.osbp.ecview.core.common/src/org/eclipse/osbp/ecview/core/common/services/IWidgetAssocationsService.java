/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.services;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;

// TODO: Auto-generated Javadoc
/**
 * Is used to associate widgets with their model elements.
 *
 * @param <UI>
 *            the generic type
 * @param <MODEL>
 *            the generic type
 */
public interface IWidgetAssocationsService<UI, MODEL> extends IUiKitBasedService {

	/** The Constant ID. */
	public static final String ID = IWidgetAssocationsService.class.getName();

	/**
	 * Returns the model element for the given ui widget.
	 *
	 * @param uiWidget
	 *            the ui widget
	 * @return the model element
	 */
	MODEL getModelElement(UI uiWidget);

	/**
	 * Returns the model element for the given id.
	 *
	 * @param id
	 *            the id
	 * @return the model element
	 */
	MODEL getModelElement(String id);

	/**
	 * Returns the model element for the given order index within the layout.
	 *
	 * @param layoutIdx
	 *            the layout index
	 * @return the model element
	 */
	MODEL getModelElement(int layoutIdx);

	/**
	 * Returns the editpart for the given id.
	 *
	 * @param id
	 *            the id
	 * @return the editpart
	 */
	IElementEditpart getEditpart(String id);

	/**
	 * Returns the widget for the given model element.
	 *
	 * @param modelElement
	 *            the model element
	 * @return the widget
	 */
	UI getWidget(MODEL modelElement);

	/**
	 * Associates the uiWidget with the given model element. One model element can become assigned to different uiWidgets.
	 *
	 * @param uiWidget
	 *            the ui widget
	 * @param modelElement
	 *            the model element
	 */
	void associate(UI uiWidget, MODEL modelElement);

	/**
	 * Removes the association for the given uiWidget.
	 *
	 * @param uiWidget
	 *            the ui widget
	 */
	void remove(UI uiWidget);

	/**
	 * Returns all model elements.
	 *
	 * @return the model elements
	 */
	List<MODEL> getModelElements();

	/**
	 * Clears all associations.
	 */
	void clear();

}
