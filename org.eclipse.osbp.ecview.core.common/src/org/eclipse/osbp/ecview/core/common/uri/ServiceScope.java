/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.uri;

import java.net.URI;

import org.eclipse.osbp.ecview.core.common.services.IServiceRegistry;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceScope.
 */
public class ServiceScope extends AbstractScope {

	/** The Constant URI_AUTHORITY. */
	public static final String URI_AUTHORITY = "service";
	
	/** The Constant SEGMENT_TEMPLATE. */
	private static final String SEGMENT_TEMPLATE = "service/%s";
	
	/** The parent. */
	private final AccessibleScope parent;
	
	/** The selector. */
	private final String selector;

	/**
	 * Instantiates a new service scope.
	 *
	 * @param parent
	 *            the parent
	 * @param selector
	 *            the selector
	 */
	public ServiceScope(AccessibleScope parent, String selector) {
		this.parent = parent;
		this.selector = selector;

		setURISegment(String.format(SEGMENT_TEMPLATE, selector));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.uri.IURIScope#root()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends AccessibleScope> A root() {
		return (A) parent;
	}

	/**
	 * Gets the view scope.
	 *
	 * @return the view scope
	 */
	protected AccessibleScope getViewScope() {
		return root();
	}

	/**
	 * Returns the URI representation of this bean scope.
	 * 
	 * @return URI
	 */
	public URI toURI() {
		return URI.create(toURIString());
	}

	/**
	 * Returns the URI string of the produced string token.
	 * 
	 * @return String string
	 */
	public String toURIString() {
		return parent.getURISegment().concat(getURISegment());
	}

	/**
	 * Accesses the object in the given context that is described by the uri
	 * segment of this scope.
	 *
	 * @param context
	 *            the context
	 * @return the object
	 */
	public Object access(IServiceRegistry context) {
		return context.getService(selector);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return toURIString();
	}
}
