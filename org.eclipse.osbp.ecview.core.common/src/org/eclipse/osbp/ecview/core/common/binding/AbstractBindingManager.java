/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.binding;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateListStrategy;
import org.eclipse.core.databinding.UpdateSetStrategy;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;

// TODO: Auto-generated Javadoc
/**
 * Is used to bind values. For details see {@link IECViewBindingManager}.
 */
public abstract class AbstractBindingManager extends AbstractDisposable
		implements IECViewBindingManager {

	/** The validation realm. */
	private final Realm validationRealm;
	
	/** The dbc. */
	private final DataBindingContext dbc;

	/**
	 * Instantiates a new abstract binding manager.
	 *
	 * @param validationRealm
	 *            the validation realm
	 */
	public AbstractBindingManager(Realm validationRealm) {
		super();
		this.validationRealm = validationRealm;
		dbc = createDatabindingContext(validationRealm);
	}

	/**
	 * Creates a new instance of the databinding context.
	 *
	 * @param validationRealm
	 *            the validation realm
	 * @return the data binding context
	 */
	protected DataBindingContext createDatabindingContext(Realm validationRealm) {
		return new DataBindingContext(validationRealm);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#getValidationRealm()
	 */
	public Realm getValidationRealm() {
		return validationRealm;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#getDatabindingContext()
	 */
	@Override
	public DataBindingContext getDatabindingContext() {
		return dbc;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#bindValue(org.eclipse.core.databinding.observable.value.IObservableValue, org.eclipse.core.databinding.observable.value.IObservableValue)
	 */
	@Override
	public Binding bindValue(IObservableValue target, IObservableValue model) {
		return bindValue(target, model, new UpdateValueStrategy(
				UpdateValueStrategy.POLICY_UPDATE), new UpdateValueStrategy(
				UpdateValueStrategy.POLICY_UPDATE));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#bindValue(org.eclipse.core.databinding.observable.value.IObservableValue, org.eclipse.core.databinding.observable.value.IObservableValue, org.eclipse.core.databinding.UpdateValueStrategy, org.eclipse.core.databinding.UpdateValueStrategy)
	 */
	public final Binding bindValue(IObservableValue target,
			IObservableValue model, UpdateValueStrategy targetToModel,
			UpdateValueStrategy modelToTarget) {
		return dbc.bindValue(target, model, targetToModel, modelToTarget);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#bindList(org.eclipse.core.databinding.observable.list.IObservableList, org.eclipse.core.databinding.observable.list.IObservableList)
	 */
	@Override
	public Binding bindList(IObservableList target, IObservableList model) {
		return bindList(target, model, new UpdateListStrategy(
				UpdateListStrategy.POLICY_UPDATE), new UpdateListStrategy(
				UpdateListStrategy.POLICY_UPDATE));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#bindList(org.eclipse.core.databinding.observable.list.IObservableList, org.eclipse.core.databinding.observable.list.IObservableList, org.eclipse.core.databinding.UpdateListStrategy, org.eclipse.core.databinding.UpdateListStrategy)
	 */
	@Override
	public Binding bindList(IObservableList target, IObservableList model,
			UpdateListStrategy targetToModel, UpdateListStrategy modelToTarget) {
		return getDatabindingContext().bindList(target, model, targetToModel,
				modelToTarget);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#bindSet(org.eclipse.core.databinding.observable.set.IObservableSet, org.eclipse.core.databinding.observable.set.IObservableSet)
	 */
	@Override
	public Binding bindSet(IObservableSet target, IObservableSet model) {
		return bindSet(target, model, new UpdateSetStrategy(
				UpdateSetStrategy.POLICY_UPDATE), new UpdateSetStrategy(
				UpdateSetStrategy.POLICY_UPDATE));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#bindSet(org.eclipse.core.databinding.observable.set.IObservableSet, org.eclipse.core.databinding.observable.set.IObservableSet, org.eclipse.core.databinding.UpdateSetStrategy, org.eclipse.core.databinding.UpdateSetStrategy)
	 */
	@Override
	public Binding bindSet(IObservableSet target, IObservableSet model,
			UpdateSetStrategy targetToModel, UpdateSetStrategy modelToTarget) {
		return getDatabindingContext().bindSet(target, model,
				new UpdateSetStrategy(UpdateListStrategy.POLICY_UPDATE),
				new UpdateSetStrategy(UpdateListStrategy.POLICY_UPDATE));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#updateModel()
	 */
	@Override
	public void updateModel() {
		dbc.updateModels();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager#updateTarget()
	 */
	@Override
	public void updateTarget() {
		dbc.updateTargets();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.runtime.common.dispose.AbstractDisposable#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		if (isDisposed()) {
			return;
		}

		dbc.dispose();
	}

}
