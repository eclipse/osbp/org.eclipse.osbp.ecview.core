/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.uri;

// TODO: Auto-generated Javadoc
/**
 * URI scopes are used to build an accessor for a given URI.
 */
public interface IURIScope {

	/**
	 * Returns the segment that is contributed by the current scope to the
	 * resulting URI.
	 * <p>
	 * For that scope may provide "bean/org.my.bean" to an URI that results in
	 * <code>"view://bean/org.my.bean#person.address.country"</code>.
	 * 
	 * @return String
	 */
	public String getURISegment();

	/**
	 * Returns the view scope or view set scope. <br>
	 * Is part of fluent API.
	 *
	 * @param <A>
	 *            the generic type
	 * @return root scope
	 */
	public <A extends AccessibleScope> A root();

}
