/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * An editpart resonsible to handle commands. If new commands are added, they
 * will become processed. Also leaving commands will become unbind.
 */
public interface ICommandSetEditpart extends IElementEditpart {

	/**
	 * Returns true if the command set is active.
	 *
	 * @return true, if is active
	 */
	boolean isActive();

	/**
	 * Activates the command set manager. It will create all pending commands.
	 */
	void activate();

	/**
	 * Returns the view that command set is attached to.
	 *
	 * @return the view
	 */
	IViewEditpart getView();

	/**
	 * Returns a list of all available commands.
	 *
	 * @return the commands
	 */
	List<ICommandEditpart> getCommands();

	/**
	 * Adds the given command to the list of commands. Adding a command will
	 * bind the contained values if not done so.
	 *
	 * @param command
	 *            the command
	 */
	@Deprecated // use model instead
	void addCommand(ICommandEditpart command);

	/**
	 * Removes the given command from the list of commands. Removing a command
	 * will unbind the contained values.
	 *
	 * @param command
	 *            the command
	 */
	@Deprecated // use model instead
	void removeCommand(ICommandEditpart command);

}
