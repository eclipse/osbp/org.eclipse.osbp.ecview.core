/**
 * Copyright (c) 2013 Loetz GmbH&Co.KG(Heidelberg). All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0  which accompanies this distribution,
t https://www.eclipse.org/legal/epl-2.0/
t
t SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: Christophe Loetz (Loetz GmbH&Co.KG) - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.services;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.osbp.ecview.core.common.context.IContext;

// TODO: Auto-generated Javadoc
/**
 * Provide services.
 * 
 * @author dominguez
 * 
 */
public interface IServiceProvider {

	/**
	 * The property of the OSGi-service to assign the provider to view or
	 * viewset level. No setting means view level.
	 */
	public static final String OSGI_PROPERTY = "ecview.serviceprovider.level";

	/** The Constant LEVEL_VIEW. */
	public static final String LEVEL_VIEW = "view";

	/** The Constant LEVEL_VIEW_SET. */
	public static final String LEVEL_VIEW_SET = "viewset";

	/** The Constant VIEW_PROPERTIES. */
	public static final Map<String, Object> VIEW_PROPERTIES = PropertiesInitializer
			.createViewProperties();

	/** The Constant VIEW_SET_PROPERTIES. */
	public static final Map<String, Object> VIEW_SET_PROPERTIES = PropertiesInitializer
			.createViewSetProperties();

	/**
	 * Returns true if the provider is responsible for creating services of the
	 * given type for the context.
	 *
	 * @param selector
	 *            the selector
	 * @param context
	 *            the context
	 * @return true, if is for
	 */
	boolean isFor(String selector, IContext context);

	/**
	 * Returns the service of the given type for the context.
	 *
	 * @param <A>
	 *            the generic type
	 * @param selector
	 *            the selector
	 * @param context
	 *            the context
	 * @return the a
	 */
	<A> A createService(String selector, IContext context);

	/**
	 * The Class PropertiesInitializer.
	 */
	public static class PropertiesInitializer {

		/**
		 * Creates the properties to assign services to views.
		 *
		 * @return the map
		 */
		public static Map<String, Object> createViewProperties() {
			Map<String, Object> props = new HashMap<String, Object>();
			props.put(OSGI_PROPERTY, LEVEL_VIEW);
			return Collections.unmodifiableMap(props);
		}

		/**
		 * Creates the properties to assign services to views.
		 *
		 * @return the map
		 */
		public static Map<String, Object> createViewSetProperties() {
			Map<String, Object> props = new HashMap<String, Object>();
			props.put(OSGI_PROPERTY, LEVEL_VIEW_SET);
			return Collections.unmodifiableMap(props);
		}
	}

}