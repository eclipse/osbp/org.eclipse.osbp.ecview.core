/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.binding;

import org.eclipse.core.databinding.observable.list.IObservableList;

// TODO: Auto-generated Javadoc
/**
 * An editpart resonsible to provide observable values.
 */
public interface IBindableListEndpointEditpart extends IBindableEndpointEditpart {

	/**
	 * Returns the target observable that can be used in databinding.
	 *
	 * @param <A>
	 *            the generic type
	 * @return the observable
	 */
	<A extends IObservableList> A getObservable();

	/**
	 * Register a refresh provider.
	 *
	 * @param refresh
	 *            the new refresh provider
	 */
	void setRefreshProvider(RefreshProvider refresh);

	/**
	 * The Interface RefreshProvider.
	 */
	interface RefreshProvider {

		/**
		 * Is called to refresh the given binding.
		 */
		void refresh();

	}

}
