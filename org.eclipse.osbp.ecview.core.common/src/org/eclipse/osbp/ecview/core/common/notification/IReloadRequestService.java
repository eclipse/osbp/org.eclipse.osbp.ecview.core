/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.notification;

import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

// TODO: Auto-generated Javadoc
/**
 * This service is used to notify an owner of the {@link YView} that a component
 * requested a reload operation of the base bean slot.
 * <p>
 * Interested parties can also register listeners to become notified, if the
 * mode changes. If enabled==false then no reload requests are accepted.
 */
public interface IReloadRequestService {

	/**
	 * Is called if a component (eq YSuggestTextField) requests a reload of the
	 * main beanslot. The implementation of this service should handle dirty
	 * states of the view and reset the bound bean in the main beanslot.
	 *
	 * @param component
	 *            the component
	 * @param bean
	 *            the bean
	 */
	void requestReload(YEmbeddable component, Object bean);

	/**
	 * Returns the current mode. False means, that no reload requests are
	 * processed.
	 *
	 * @return the mode
	 */
	boolean getMode();

	/**
	 * Sets a new mode. False means, that no reload requests are processed.
	 *
	 * @param enabled
	 *            the new mode
	 */
	void setMode(boolean enabled);

	/**
	 * Adds the mode listener.
	 *
	 * @param listener
	 *            the listener
	 */
	void addListener(ModeListener listener);

	/**
	 * Adds the mode listener.
	 *
	 * @param listener
	 *            the listener
	 */
	void removeListener(ModeListener listener);

	/**
	 * The listener interface for receiving mode events. The class that is
	 * interested in processing a mode event implements this interface, and the
	 * object created with that class is registered with a component using the
	 * component's <code>addModeListener</code> method. When
	 * the mode event occurs, that object's appropriate
	 * method is invoked.
	 *
	 */
	public interface ModeListener {

		/**
		 * Notifies about mode changes.
		 *
		 * @param enabled
		 *            the enabled
		 */
		void notifyMode(boolean enabled);

	}

}
