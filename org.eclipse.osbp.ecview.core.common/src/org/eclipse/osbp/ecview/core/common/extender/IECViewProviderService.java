/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.extender;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

// TODO: Auto-generated Javadoc
/**
 * Is responsible to create an ECView view context for a given dto.
 */
public interface IECViewProviderService {

	/**
	 * Returns a proper configured view context for the given dtoClass in the
	 * given slot. The slot may be <code>null</code>. Then any bean slot is
	 * checked.
	 *
	 * @param dtoClass
	 *            the dto class
	 * @param beanSlot
	 *            the bean slot in YView. May be null.
	 * @return the view context
	 */
	IViewContext getViewContext(Class<?> dtoClass, String beanSlot);

	/**
	 * Returns a proper configured view context for the given viewId.
	 *
	 * @param viewId
	 *            the view id
	 * @return the view context
	 */
	IViewContext getViewContext(String viewId);

	/**
	 * Returns a proper configured view context for the given yView.
	 *
	 * @param yView
	 *            the y view
	 * @return the view context
	 */
	IViewContext getViewContext(YView yView);

}
