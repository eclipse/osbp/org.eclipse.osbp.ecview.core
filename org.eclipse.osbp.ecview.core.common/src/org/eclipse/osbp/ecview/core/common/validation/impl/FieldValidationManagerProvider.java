/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.validation.impl;

import org.eclipse.osbp.ecview.core.common.context.IContext;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.services.IServiceProvider;
import org.eclipse.osbp.ecview.core.common.validation.IFieldValidationManager;
import org.osgi.service.component.annotations.Component;

/**
 * Provides the {@link IFieldValidationManager} implementation to the services of
 * {@link IViewContext}.
 */
@Component
public class FieldValidationManagerProvider implements IServiceProvider {

	@Override
	public boolean isFor(String selector, IContext context) {
		return selector.equals(IFieldValidationManager.class.getName());
	}

	@SuppressWarnings("unchecked")
	@Override
	public <A> A createService(String selector, IContext context) {
		return (A) new FieldValidationManager();
	}
}