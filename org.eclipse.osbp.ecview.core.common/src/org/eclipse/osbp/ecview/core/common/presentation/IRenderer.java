/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.presentation;

import java.util.Map;

import org.eclipse.osbp.ecview.core.common.context.ContextException;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

/**
 * Is used to render the the given view part based on the information of the
 * view context.
 */
public interface IRenderer {

	/**
	 * Renders the UI for the given componentContainer and UI model.
	 * 
	 * @param componentContainer
	 *            The componentContainer the should be the parent for the
	 *            rendered UI
	 * @param yView
	 *            The view model.
	 * @param options
	 *            rendering options
	 * @return viewContext - the rendered view context
	 * @throws ContextException
	 *             e
	 */
	IViewContext render(Object rootContainer, YView yView,
			Map<String, Object> options) throws ContextException;

	/**
	 * Is called to render the given view part based on the view context.
	 * 
	 * @param viewContext
	 *            the context that is responsible to define the UI kit an to
	 *            offer additional information to the edit part
	 * @param rootLayout
	 *            the root layout that should the base for all UI elements
	 * @param options
	 *            rendering options. Depend on the underlying UI-Kit
	 * @throws ContextException
	 *             if the view can not be rendered
	 */
	void render(IViewContext viewContext, Object rootLayout,
			Map<String, Object> options) throws ContextException;

}
