/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.filter;

// TODO: Auto-generated Javadoc
/**
 * The presentation accepts filters that may be used for queries.
 */
public interface IFilterablePresentation {

	/**
	 * Sets the filter to the presentation. If <code>null</code> is passed, all
	 * filters must be removed.
	 *
	 * @param filter
	 *            the filter
	 */
	void applyFilter(Object filter);

}
