/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.beans;

// TODO: Auto-generated Javadoc
/**
 * An abstraction for a slot in the context. Slots can keep a value and maybe in
 * future other things too. The implementation should be bindable with eclipse
 * databinding and offer PropertyChangeSupport.
 * <p>
 * For a use case see IViewContext#getBeanSlot(String, Class). <br>
 * 
 * @noimplement Not to be implemented by clients.
 */
public interface ISlot {

	/** The Constant PROP_VALUE. */
	public static final String PROP_VALUE = "value";

	/**
	 * Returns the given value.
	 * 
	 * @return value
	 */
	Object getValue();

	/**
	 * Sets the given value.
	 * 
	 * @param value
	 *            The value to be set
	 */
	void setValue(Object value);

	/**
	 * Returns the type of the value.
	 *
	 * @return the value type
	 */
	Class<?> getValueType();

}
