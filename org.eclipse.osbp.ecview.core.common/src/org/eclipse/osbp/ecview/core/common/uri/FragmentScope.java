/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.uri;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class FragmentScope.
 */
public class FragmentScope extends AbstractScope {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(FragmentScope.class);

	/** The Constant SEGMENT_TEMPLATE. */
	private static final String SEGMENT_TEMPLATE = "#%s";
	
	/** The parent. */
	private final BeanScope parent;
	
	/** The selector. */
	private final String selector;

	/**
	 * Instantiates a new fragment scope.
	 *
	 * @param parent
	 *            the parent
	 * @param selector
	 *            the selector
	 */
	public FragmentScope(BeanScope parent, String selector) {
		this.parent = parent;
		this.selector = selector;

		setURISegment(String.format(SEGMENT_TEMPLATE, selector));
	}

	/**
	 * Returns the URI representation of this bean scope.
	 * 
	 * @return URI
	 */
	public URI toURI() {
		return URI.create(toURIString());
	}

	/**
	 * Returns the URI string of the produced string token.
	 * 
	 * @return String string
	 */
	public String toURIString() {
		return parent.toURIString().concat(getURISegment());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.uri.IURIScope#root()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends AccessibleScope> A root() {
		return (A) parent.getViewScope();
	}

	/**
	 * Access.
	 *
	 * @param bean
	 *            the bean
	 * @return the object
	 */
	public Object access(Object bean) {
		Object result = null;
		try {
			result = PropertyUtils.getProperty(bean, selector);
		} catch (IllegalAccessException e) {
			logger.error("{}", e);
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			logger.error("{}", e);
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			logger.error("{}", e);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Returns the URISegment but removing the first #.
	 *
	 * @return the fragment content
	 */
	public String getFragmentContent() {
		return getURISegment().replace("#", "");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return toURIString();
	}

}
