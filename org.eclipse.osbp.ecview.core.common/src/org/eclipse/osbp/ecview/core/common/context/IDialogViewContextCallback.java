package org.eclipse.osbp.ecview.core.common.context;

public interface IDialogViewContextCallback {
	IViewContext getDialogViewContext(Class<?> type);
}
