/*******************************************************************************
 * Copyright (c) 2011, 2015 - Florian Pirchner
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 * Florian Pirchner - initial API and implementation
 *******************************************************************************/
package org.eclipse.osbp.ecview.core.common.binding;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateListStrategy;
import org.eclipse.core.databinding.UpdateSetStrategy;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;

// TODO: Auto-generated Javadoc
/**
 * A manager that is responsible for binding data for one view instance. Each
 * binding manager has to be connected to exactly one view. All the bindings
 * contained are bindings related to the associated view.
 */
public interface IECViewBindingManager extends IDisposable {

	/** The Constant DEFAULT_GROUPING. */
	public static final boolean DEFAULT_GROUPING = true;
	
	/** The Constant DEFAULT_MARK_NEGATIVE. */
	public static final boolean DEFAULT_MARK_NEGATIVE = true;
	
	/** The Constant DEFAULT_PRECISION. */
	public static final int DEFAULT_PRECISION = 2;

	/**
	 * Returns the validation realm for the binding manager.
	 *
	 * @return the validation realm
	 */
	Realm getValidationRealm();

	/**
	 * Returns the databinding context that is responsible for binding the view.
	 *
	 * @return the databinding context
	 */
	DataBindingContext getDatabindingContext();

	/**
	 * Binds the target to the model. Policy update will be used. Model -&gt;
	 * Target and Target -&gt; Model automatically. See UpdateValueStrategy.
	 *
	 * @param target
	 *            the target
	 * @param model
	 *            the model
	 * @return The binding
	 */
	Binding bindValue(IObservableValue target, IObservableValue model);

	/**
	 * Binds the target to the model.
	 *
	 * @param target
	 *            the target
	 * @param model
	 *            the model
	 * @param targetToModel
	 *            the target to model
	 * @param modelToTarget
	 *            the model to target
	 * @return The binding
	 */
	Binding bindValue(IObservableValue target, IObservableValue model,
			UpdateValueStrategy targetToModel, UpdateValueStrategy modelToTarget);

	/**
	 * Binds the target to the model.
	 *
	 * @param target
	 *            the target
	 * @param model
	 *            the model
	 * @return The binding
	 */
	Binding bindList(IObservableList target, IObservableList model);

	/**
	 * Binds the target to the model.
	 *
	 * @param target
	 *            the target
	 * @param model
	 *            the model
	 * @param targetToModel
	 *            the target to model
	 * @param modelToTarget
	 *            the model to target
	 * @return The binding
	 */
	Binding bindList(IObservableList target, IObservableList model,
			UpdateListStrategy targetToModel, UpdateListStrategy modelToTarget);

	/**
	 * Binds the target to the model.
	 *
	 * @param target
	 *            the target
	 * @param model
	 *            the model
	 * @return The binding
	 */
	Binding bindSet(IObservableSet target, IObservableSet model);

	/**
	 * Binds the target to the model.
	 *
	 * @param target
	 *            the target
	 * @param model
	 *            the model
	 * @param targetToModel
	 *            the target to model
	 * @param modelToTarget
	 *            the model to target
	 * @return The binding
	 */
	Binding bindSet(IObservableSet target, IObservableSet model,
			UpdateSetStrategy targetToModel, UpdateSetStrategy modelToTarget);
	
	/**
	 * Updates the model with target values.
	 */
	void updateModel();
	
	/**
	 * Updates the target with the model values.
	 */
	void updateTarget();
	
	
}