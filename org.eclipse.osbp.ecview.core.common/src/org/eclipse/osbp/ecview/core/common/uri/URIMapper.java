/*
 *                                                                            
 *  Copyright (c) 2011, 2014 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Initial contribution:                                                      
 *     Loetz GmbH & Co. KG                               
 * 
 */


package org.eclipse.osbp.ecview.core.common.uri;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * Added URI will be transformed to the most pathed way or fragmented way.
 * <h3>pathed URI</h3>
 * This means, that the URI-fragment is shorten to the first element and the
 * rest of the URI-Elements are placed at path structure.
 * <p>
 * For instance<br>
 * input =
 * <code>view://bean/root/invoiceInfo#value.customer.address.country</code><br>
 * pathed =
 * <code>view://bean/root/invoiceInfo/customer/address#value.country</code>
 * 
 * <h3>fragmented URI</h3> This means, that the URI-path is shorten to the first
 * element and the rest of the URI-Elements are placed at fragment structure.
 * <p>
 * 
 * For instance<br>
 * input =
 * <code>view://bean/root/invoiceInfo#value.customer.address.country</code><br>
 * fragmented =
 * <code>view://bean/root#value.invoiceInfo.customer.address.country</code>
 * 
 * @author dominguez
 * 
 */
public class URIMapper {

	/** The uri schema delimiter. */
	private final String URI_SCHEMA_DELIMITER = "://";
	
	/** The uri path delimiter. */
	private final String URI_PATH_DELIMITER = "/";
	
	/** The uri fragment delimiter. */
	private final String URI_FRAGMENT_DELIMITER = ".";
	
	/** The uri fragment path delimiter. */
	private final String URI_FRAGMENT_PATH_DELIMITER = "#";
	
	/** The mapping. */
	private Mapping mapping;

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(URIMapper.class);

	/**
	 * Adds the uri.
	 *
	 * @param uri
	 *            the uri
	 * @return the mapping
	 */
	public Mapping addURI(URI uri) {
		mapping = new Mapping(uri);
		return mapping;
	}

	/**
	 * Activate.
	 */
	public void activate() {
		mapping.activate = true;
	}

	/**
	 * Deactivate.
	 */
	public void deactivate() {
		mapping.activate = false;
	}

	/**
	 * The Class Mapping.
	 */
	public class Mapping {
		
		/** The uri. */
		private final URI uri;
		
		/** The path elems. */
		private final String[] pathElems;
		
		/** The fragments. */
		private final String[] fragments;
		
		/** The uri elems. */
		private final List<String> uriElems;
		
		/** The activate. */
		private boolean activate;

		/**
		 * Instantiates a new mapping.
		 *
		 * @param uri
		 *            the uri
		 */
		public Mapping(URI uri) {
			this.uri = uri;
			pathElems = uri.getPath().split("/");
			fragments = uri.getFragment().split("\\.");
			uriElems = new ArrayList<String>(pathElems.length
					+ fragments.length - 2);
			uriElems.addAll(Arrays.asList(Arrays.copyOfRange(pathElems, 1,
					pathElems.length)));
			uriElems.addAll(Arrays.asList(Arrays.copyOfRange(fragments, 1,
					fragments.length)));
		}

		/**
		 * Gets the uri.
		 *
		 * @return the uri
		 */
		public URI getUri() {
			return uri;
		}

		/**
		 * Gets the URI prefix.
		 *
		 * @return the URI prefix
		 */
		private StringBuffer getURIPrefix() {
			StringBuffer strBuf = new StringBuffer();
			String schema = uri.getScheme();
			String auth = uri.getAuthority();
			strBuf.append(schema).append(URI_SCHEMA_DELIMITER).append(auth);
			return strBuf;
		}

		/**
		 * Provides the URI in the most fragmented way. This means, that the
		 * URI-path is shorten to the first element and the rest of the
		 * URI-Elements are placed at fragment structure.
		 * 
		 * @return URI
		 */
		public URI getFragmentedURI() {
			StringBuffer strBuf = getURIPrefix();
			// not starting at pathElems[0], because that Element is blank.
			strBuf.append(URI_PATH_DELIMITER).append(pathElems[1])
					.append(URI_FRAGMENT_PATH_DELIMITER).append(fragments[0]);
			combineURI(strBuf, 1, pathElems.length, URI_FRAGMENT_DELIMITER);
			combineURI(strBuf, pathElems.length, uriElems.size(),
					URI_FRAGMENT_DELIMITER);
			logger.debug("getFragmentedURI() - strBuf: " + strBuf);
			return URI.create(strBuf.toString());
		}

		/**
		 * Combine uri.
		 *
		 * @param strBuf
		 *            the str buf
		 * @param pathStart
		 *            the path start
		 * @param pathEnd
		 *            the path end
		 * @param delimiter
		 *            the delimiter
		 * @return the string buffer
		 */
		private StringBuffer combineURI(StringBuffer strBuf, int pathStart,
				int pathEnd, String delimiter) {
			if (validateStartEnd(uriElems, pathStart, pathEnd)) {
				for (int i = pathStart; i < pathEnd; i++) {
					String pathElemsSplit = uriElems.get(i);
					strBuf.append(delimiter).append(pathElemsSplit);
				}
			}
			return strBuf;
		}

		/**
		 * Validate start end.
		 *
		 * @param list
		 *            the list
		 * @param listStart
		 *            the list start
		 * @param listEnd
		 *            the list end
		 * @return true, if successful
		 */
		private boolean validateStartEnd(Collection<String> list,
				int listStart, int listEnd) {
			if (listStart < 0 || listEnd > list.size()) {
				return false;
			}
			return true;
		}

		/**
		 * Provides the URI in the most pathed way. This means, that the
		 * URI-fragment is shorten to the first element and the rest of the
		 * URI-Elements are placed at path structure.
		 * 
		 * @return URI
		 */
		public URI getPathedURI() {
			StringBuffer strBuf = getURIPrefix();
			strBuf.append(uri.getPath());
			combineURI(strBuf, pathElems.length - 1, uriElems.size() - 1,
					URI_PATH_DELIMITER);
			strBuf.append(URI_FRAGMENT_PATH_DELIMITER).append(fragments[0]);
			// the first fragmentElement is already in the URi
			if ((fragments.length - 1) > 0) {
				strBuf.append(URI_FRAGMENT_DELIMITER).append(
						fragments[fragments.length - 1]);
			}
			logger.debug("getPathedURI() - strBuf: " + strBuf);
			return URI.create(strBuf.toString());
		}

		/**
		 * Helper method to prepare the mappings.
		 *
		 * @return List<Mapping>
		 */
		private List<Mapping> getAllMappings() {
			int idx = uriElems.size();
			List<Mapping> mappings = new ArrayList<Mapping>(idx + 1);
			int start = 0;
			int end = idx;
			for (int i = 0; i < idx; i++) {
				StringBuffer strBuf = getURIPrefix();
				// first path element is always required
				int pathEnd = end - 1;
				if (end == 1) {
					pathEnd = end;
				}
				combineURI(strBuf, start, pathEnd, URI_PATH_DELIMITER);
				strBuf.append(URI_FRAGMENT_PATH_DELIMITER).append(fragments[0]);
				// no more fragment available
				if (end > 1) {
					combineURI(strBuf, pathEnd, end, URI_FRAGMENT_DELIMITER);
				}
				Mapping mapping = new Mapping(URI.create(strBuf.toString()));
				mappings.add(mapping);
				end--;
			}
			return mappings;
		}

		/**
		 * Returns all the mappings contained in this mapper.
		 *
		 * @return List&lt;Mapping&gt;
		 */
		public List<Mapping> getMappings() {
			if (!activate) {
				List<Mapping> mappings = new ArrayList<URIMapper.Mapping>(1);
				mappings.add(this);
				return mappings;
			} else {
				return getAllMappings();
			}
		}

	}
}