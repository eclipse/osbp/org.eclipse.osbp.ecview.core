/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.visibility;

// TODO: Auto-generated Javadoc
/**
 * The Interface IUiElementAccess.
 */
public interface IUiElementAccess {

	/**
	 * Returns true, of the UI element contains a tag with the given tagName.
	 *
	 * @param tag
	 *            the tag
	 * @return true, if successful
	 */
	boolean containsTag(String tag);

	/**
	 * Returns true, of the UI element contains a property with the given key.
	 *
	 * @param key
	 *            the key
	 * @return true, if successful
	 */
	boolean containsProperty(String key);

	/**
	 * Returns the property value from the UI element with the given key.
	 *
	 * @param key
	 *            the key
	 * @return the property value
	 */
	String getPropertyValue(String key);

}
