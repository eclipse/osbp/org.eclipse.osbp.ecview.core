/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart;

import org.eclipse.osbp.runtime.common.keystroke.KeyCode;
import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeDefinition;

// TODO: Auto-generated Javadoc
/**
 * Deals with keystroke definitions.
 */
public interface IKeyStrokeDefinitionEditpart extends IElementEditpart {

	/**
	 * Returns the key stroke definition as a immutable POJO. See
	 * {@link KeyCode}
	 *
	 * @return the definition
	 */
	KeyStrokeDefinition getDefinition();

}
