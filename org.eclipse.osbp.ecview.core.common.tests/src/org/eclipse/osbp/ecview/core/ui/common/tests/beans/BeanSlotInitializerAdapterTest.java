/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.beans;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;

import org.eclipse.osbp.ecview.core.common.beans.BeanSlotInitializerAdapter;
import org.eclipse.osbp.ecview.core.common.beans.ISlot;
import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.presentation.DelegatingPresenterFactory;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("restriction")
public class BeanSlotInitializerAdapterTest {

	private DelegatingPresenterFactory presenterFactory = DelegatingPresenterFactory
			.getInstance();
	private DelegatingEditPartManager editpartManager = DelegatingEditPartManager
			.getInstance();

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		editpartManager.clear();
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.common.editpart.emf.EditpartManager());
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.extension.editpart.emf.EditpartManager());
		presenterFactory.clear();
	}

	/**
	 * Tests the context.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test() {
		ViewContext context = new ViewContext();
		IViewEditpart viewEditpart = (IViewEditpart) editpartManager
				.getEditpart(context, CoreModelFactory.eINSTANCE.createYView());

		assertNull(context.getBeanSlot("bean1"));

		new BeanSlotInitializerAdapter() {
			@Override
			protected void doIntialize() {
				createBeanSlot("view://bean/bean1", String.class);
			}
		}.intialize(context);

		ISlot slot = context.getBeanSlot("bean1");
		assertSame(String.class, slot.getValueType());
	}
}
