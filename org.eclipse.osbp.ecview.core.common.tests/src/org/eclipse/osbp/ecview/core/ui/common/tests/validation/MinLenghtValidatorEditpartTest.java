/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.MinLengthValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidationConfig;
import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidator;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YTextDatatype;
import org.eclipse.osbp.ecview.core.extension.model.extension.util.SimpleExtensionModelFactory;
import org.eclipse.osbp.ecview.core.util.emf.ModelUtil;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;
import org.eclipse.osbp.runtime.common.validation.IStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MinLenghtValidatorEditpartTest {

	private DelegatingEditPartManager editpartManager = DelegatingEditPartManager
			.getInstance();
	private SimpleExtensionModelFactory factory = new SimpleExtensionModelFactory();

	/**
	 * Setup.
	 */
	@SuppressWarnings("restriction")
	@Before
	public void setup() {
		editpartManager.clear();
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.common.editpart.emf.EditpartManager());
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.extension.editpart.emf.EditpartManager());
	}

	/**
	 * Tests the validator result status
	 */
	@Test
	public void test_minLength() {
		YMinLengthValidator yValidator = ValidationFactory.eINSTANCE
				.createYMinLengthValidator();
		IValidatorEditpart validatorEditpart = editpartManager
				.getEditpart(new ViewContext(), yValidator);
		IValidator validator = validatorEditpart.getValidator();
		yValidator.setMinLength(5);

		IStatus status = validator.validateValue("123456");
		assertTrue(status.isOK());
		assertEquals(IStatus.Severity.OK, status.getSeverity());
		assertEquals("", status.getMessage());

		status = validator.validateValue("123");
		assertFalse(status.isOK());
		assertEquals(IStatus.Severity.ERROR, status.getSeverity());
		assertEquals("Minimum length is 5. Length of \"123\" is 3!",
				status.getMessage());

	}

	/**
	 * Tests whether updating validator parameters changes results
	 */
	@Test
	public void test_updateParameter() {
		YMinLengthValidator yValidator = ValidationFactory.eINSTANCE
				.createYMinLengthValidator();
		IValidatorEditpart validatorEditpart = editpartManager
				.getEditpart(new ViewContext(), yValidator);
		IValidator validator = validatorEditpart.getValidator();

		yValidator.setMinLength(5);
		IStatus status = validator.validateValue("123456");
		assertTrue(status.isOK());
		assertEquals(IStatus.Severity.OK, status.getSeverity());
		assertEquals("", status.getMessage());

		yValidator.setMinLength(8);
		status = validator.validateValue("123456");
		assertFalse(status.isOK());
		assertEquals(IStatus.Severity.ERROR, status.getSeverity());
		assertEquals("Minimum length is 8. Length of \"123456\" is 6!",
				status.getMessage());
	}

	@Test
	public void test_changeDatatype_Property() {
		// if the datatype was updated, the validator needs to updated too.
		YMinLengthValidationConfig yConfig = factory.createTextDatatype();
		IValidationConfig config = ModelUtil
				.getEditpart(new ViewContext(), (YTextDatatype) yConfig);

		YMinLengthValidator yValidator = ValidationFactory.eINSTANCE
				.createYMinLengthValidator();
		MinLengthValidatorEditpart editpart = ModelUtil.getEditpart(new ViewContext(), yValidator);
		editpart.setConfig(config);

		assertEquals(0, yValidator.getMinLength());

		yConfig.setMinLength(100);
		assertEquals(100, yValidator.getMinLength());

		yConfig.setMinLength(-1);
		assertEquals(-1, yValidator.getMinLength());
	}

	@Test
	public void test_dispose() {
		
		YMinLengthValidator yValidator = ValidationFactory.eINSTANCE
				.createYMinLengthValidator();
		MinLengthValidatorEditpart editpart = ModelUtil.getEditpart(new ViewContext(), yValidator);
		editpart.dispose();
		
		try {
			editpart.addDisposeListener(new IDisposable.Listener() {
				@Override
				public void notifyDisposed(IDisposable notifier) {
				}
			});
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			editpart.getId();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			editpart.getModel();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			editpart.setConfig(null);
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}
	}

}
