/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.editparts.emf.binding;

import org.eclipse.core.databinding.observable.Realm;
import org.eclipse.osbp.ecview.core.common.binding.AbstractBindingManager;

public class DefaultBindingManager extends AbstractBindingManager {

	public DefaultBindingManager() {
		super(new DefaultRealm());
	}

	public static class DefaultRealm extends Realm {

		public DefaultRealm() {
			setDefault(this);
		}

		@Override
		public boolean isCurrent() {
			return true;
		}
	}
}
