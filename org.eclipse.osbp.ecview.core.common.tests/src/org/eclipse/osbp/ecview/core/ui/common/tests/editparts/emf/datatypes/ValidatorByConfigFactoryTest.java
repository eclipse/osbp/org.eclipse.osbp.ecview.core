/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.editparts.emf.datatypes;

import org.junit.Assert;

public class ValidatorByConfigFactoryTest {

	public void test_createMinLengthValidator() {
		Assert.fail();
	}

	public void test_createMaxLengthValidator() {
		Assert.fail();
	}

	public void test_createRegExpressionValidator() {
		Assert.fail();
	}

	public void test_internalGetValidatorsDelta() {
		Assert.fail();
	}

	public void test_findToRemoveValidators() {
		Assert.fail();
	}

	public void test_getAllValidators() {
		Assert.fail();
	}

	public void test_createMaxLengthValidator_ChangingProperty_FromNoValidatorToValidator() {
		Assert.fail();
	}

	public void test_createMinLengthValidator_ChangingProperty_FromNoValidatorToValidator() {
		Assert.fail();
	}

	public void test_createRegexpValidator_ChangingProperty_FromNoValidatorToValidator() {
		Assert.fail();
	}

	public void test_createMaxLengthValidator_ChangingProperty_FromValidatorToNoValidator() {
		Assert.fail();
	}

	public void test_createMinLengthValidator_ChangingProperty_FromValidatorToNoValidator() {
		Assert.fail();
	}

	public void test_createRegexpValidator_ChangingProperty_FromValidatorToNoValidator() {
		Assert.fail();
	}

	public void test_createMaxLengthValidator_ChangingProperty_FromValidatorToValidator() {
		Assert.fail();
	}

	public void test_createMinLengthValidator_ChangingProperty_FromValidatorToValidator() {
		Assert.fail();
	}

	public void test_createRegexpValidator_ChangingProperty_FromValidatorToValidator() {
		Assert.fail();
	}
}
