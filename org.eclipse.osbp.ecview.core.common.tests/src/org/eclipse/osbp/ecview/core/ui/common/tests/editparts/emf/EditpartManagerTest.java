/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.editparts.emf;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpartProvider;
import org.eclipse.osbp.ecview.core.common.editpart.IFieldEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.ILayoutEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.EditpartManager;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.YViewSet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the editpartManager.
 */
@SuppressWarnings("restriction")
public class EditpartManagerTest {

	private EditpartManager editpartManager = new EditpartManager();
	private ResourceSetImpl resourceSet;
	private CoreModelFactory modelFactory = CoreModelFactory.eINSTANCE;

	/**
	 * Casts element to eObject.
	 * 
	 * @param element
	 * @return
	 */
	protected EObject castEObject(Object element) {
		return (EObject) element;
	}

	/**
	 * Setup the test.
	 */
	@Before
	public void setup() {
		resourceSet = new ResourceSetImpl();
		resourceSet
				.getResourceFactoryRegistry()
				.getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION,
						new XMIResourceFactoryImpl());
		resourceSet.getPackageRegistry().put(CoreModelPackage.eNS_URI,
				CoreModelPackage.eINSTANCE);

		DelegatingEditPartManager manager = DelegatingEditPartManager
				.getInstance();
		manager.clear();
		manager.addDelegate(new org.eclipse.osbp.ecview.core.common.editpart.emf.EditpartManager());
		manager.addDelegate(new org.eclipse.osbp.ecview.core.extension.editpart.emf.EditpartManager());
	}

	/**
	 * Tests that the editpart manager always returns the same instance on
	 * calling getInstance().
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_getInstance() {
		// END SUPRESS CATCH EXCEPTION
		Resource resource = resourceSet.createResource(URI
				.createURI("http://eclipse.org/emf/emfclient/uimodel"));
		YViewSet viewSet = modelFactory.createYViewSet();
		resource.getContents().add(castEObject(viewSet));

		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		resource.getContents().add(castEObject(view1));
		viewSet.getViews().add(view1);
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		resource.getContents().add(castEObject(view2));
		viewSet.getViews().add(view2);
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		IViewContext context = new ViewContext();
		// access the editparts the editpartManager
		//
		// view1
		IViewEditpart view1Editpart = editpartManager.getEditpart(context,
				view1);
		// layout1
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		// field1
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				field1);
		// view 2
		IViewEditpart view2Editpart = editpartManager.getEditpart(context,
				view2);
		// layout2
		ILayoutEditpart layout2Editpart = editpartManager.getEditpart(context,
				layout2);
		// field2
		IFieldEditpart field2Editpart = editpartManager.getEditpart(context,
				field2);

		// ensure that the editpartManager also returns the singleton instance
		//
		Assert.assertSame(view1Editpart,
				editpartManager.getEditpart(context, view1));
		Assert.assertSame(layout1Editpart,
				editpartManager.getEditpart(context, layout1));
		Assert.assertSame(field1Editpart,
				editpartManager.getEditpart(context, field1));
		Assert.assertSame(view2Editpart,
				editpartManager.getEditpart(context, view2));
		Assert.assertSame(layout2Editpart,
				editpartManager.getEditpart(context, layout2));
		Assert.assertSame(field2Editpart,
				editpartManager.getEditpart(context, field2));
	}

	/**
	 * Tests that no edit part is created for findEditpart(modelObject).
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_findEditpart() {
		// END SUPRESS CATCH EXCEPTION
		Resource resource = resourceSet.createResource(URI
				.createURI("http://eclipse.org/emf/emfclient/uimodel"));
		YViewSet viewSet = modelFactory.createYViewSet();
		resource.getContents().add(castEObject(viewSet));

		// viewSet
		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		resource.getContents().add(castEObject(view1));
		viewSet.getViews().add(view1);
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		resource.getContents().add(castEObject(view2));
		viewSet.getViews().add(view2);
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		// try to find the editparts from the editpartManager
		//
		// viewSet
		Assert.assertNull(editpartManager.findEditpart(viewSet));
		// view1
		Assert.assertNull(editpartManager.findEditpart(view1));
		// layout1
		Assert.assertNull(editpartManager.findEditpart(layout1));
		// field1
		Assert.assertNull(editpartManager.findEditpart(field1));
		// view 2
		Assert.assertNull(editpartManager.findEditpart(view2));
		// layout2
		Assert.assertNull(editpartManager.findEditpart(layout2));
		// field2
		Assert.assertNull(editpartManager.findEditpart(field2));
	}

	/**
	 * Tests that the findEditpart(modelObject) will find the same instance as
	 * getInstance.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_findEditpart_afterGetEditpart() {
		// END SUPRESS CATCH EXCEPTION
		Resource resource = resourceSet.createResource(URI
				.createURI("http://eclipse.org/emf/emfclient/uimodel"));
		YViewSet viewSet = modelFactory.createYViewSet();
		resource.getContents().add(castEObject(viewSet));

		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		resource.getContents().add(castEObject(view1));
		viewSet.getViews().add(view1);
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		resource.getContents().add(castEObject(view2));
		viewSet.getViews().add(view2);
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		IViewContext context = new ViewContext();

		// access the editparts the editpartManager
		//
		// view1
		IViewEditpart view1Editpart = editpartManager.getEditpart(context,
				view1);
		// layout1
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		// field1
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				field1);
		// view 2
		IViewEditpart view2Editpart = editpartManager.getEditpart(context,
				view2);
		// layout2
		ILayoutEditpart layout2Editpart = editpartManager.getEditpart(context,
				layout2);
		// field2
		IFieldEditpart field2Editpart = editpartManager.getEditpart(context,
				field2);

		// ensure that the editpartManager also finds the singleton instance
		//
		Assert.assertSame(view1Editpart, editpartManager.findEditpart(view1));
		Assert.assertSame(layout1Editpart,
				editpartManager.findEditpart(layout1));
		Assert.assertSame(field1Editpart, editpartManager.findEditpart(field1));
		Assert.assertSame(view2Editpart, editpartManager.findEditpart(view2));
		Assert.assertSame(layout2Editpart,
				editpartManager.findEditpart(layout2));
		Assert.assertSame(field2Editpart, editpartManager.findEditpart(field2));
	}

	/**
	 * Tests that no ADDITIONAL {@link IElementEditpartProvider} is registered
	 * on the model object for calling getEditpart(context,modelObject). Just
	 * one for the first create.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_getInstance_justOneAdapterInstanceRegistered() {
		// END SUPRESS CATCH EXCEPTION
		Resource resource = resourceSet.createResource(URI
				.createURI("http://eclipse.org/emf/emfclient/uimodel"));

		// viewSet
		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		resource.getContents().add(castEObject(view1));
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		resource.getContents().add(castEObject(view2));
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		ViewContext context = new ViewContext();

		// access the editparts the editpartManager
		//
		// view1
		editpartManager.getEditpart(context, view1);
		editpartManager.getEditpart(context, view1);
		editpartManager.findEditpart(view1);
		assertIUiElementEditpartProviderCount(1, view1);
		// layout1
		editpartManager.getEditpart(context, layout1);
		assertIUiElementEditpartProviderCount(1, layout1);
		// field1
		editpartManager.getEditpart(context, field1);
		editpartManager.getEditpart(context, field1);
		editpartManager.getEditpart(context, field1);
		editpartManager.getEditpart(context, field1);
		assertIUiElementEditpartProviderCount(1, field1);
		// view 2
		editpartManager.getEditpart(context, view2);
		assertIUiElementEditpartProviderCount(1, view2);
		// layout2
		editpartManager.getEditpart(context, layout2);
		editpartManager.getEditpart(context, layout2);
		assertIUiElementEditpartProviderCount(1, layout2);
		// field2
		editpartManager.getEditpart(context, field2);
		assertIUiElementEditpartProviderCount(1, field2);
	}

	/**
	 * Asserts that the number of added editpartProvider adapter equals the
	 * given one.
	 * 
	 * @param expectedCount
	 * @param eObject
	 */
	private void assertIUiElementEditpartProviderCount(int expectedCount,
			Object object) {
		int count = 0;
		for (Adapter adapter : castEObject(object).eAdapters()) {
			if (adapter instanceof IElementEditpartProvider) {
				count++;
			}
		}

		Assert.assertEquals(expectedCount, count);
	}
}
