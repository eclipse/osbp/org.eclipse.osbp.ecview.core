/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.editparts.emf;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IFieldEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.ILayoutEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationFactory;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidator;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;

/**
 * Tests the ui field edit part.
 * 
 * @author admin
 * 
 */
@SuppressWarnings("restriction")
public class FieldEditpartTest {

	private DelegatingEditPartManager editpartManager = DelegatingEditPartManager
			.getInstance();
	private CoreModelFactory modelFactory = CoreModelFactory.eINSTANCE;

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		editpartManager.clear();
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.common.editpart.emf.EditpartManager());
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.extension.editpart.emf.EditpartManager());
	}

	/**
	 * Tests the disposal.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_dispose() {
		// END SUPRESS CATCH EXCEPTION
		// ...> yLayout1
		// ......> yField1
		
		ViewContext context = new ViewContext();
		
		
		YField yField1 = modelFactory.createYField();
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context, yField1);

		Assert.assertFalse(field1Editpart.isDisposed());
		field1Editpart.dispose();
		Assert.assertTrue(field1Editpart.isDisposed());

		field1Editpart.isDisposed();
		field1Editpart.dispose();

		try {
			field1Editpart.addDisposeListener(new IDisposable.Listener() {
				@Override
				public void notifyDisposed(IDisposable notifier) {
				}
			});
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			field1Editpart.getId();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			field1Editpart.getModel();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			field1Editpart.getParent();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			field1Editpart.getView();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			field1Editpart.removeDisposeListener(new IDisposable.Listener() {
				@Override
				public void notifyDisposed(IDisposable notifier) {
				}
			});
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			field1Editpart.getValidators();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}
	}

	/**
	 * Tests the automatic disposal of validators.
	 */
	@Test
	public void test_validatorsDisposed_AfterDispose() {
		// ...> yLayout1
		// ......> yField1
		
		ViewContext context = new ViewContext();
		
		YLayout yLayout1 = modelFactory.createYLayout();
		YField yField1 = modelFactory.createYField();
		yLayout1.getElements().add(yField1);
		IFieldEditpart fieldEditpart = editpartManager.getEditpart(context, yField1);
		assertFalse(fieldEditpart.isDisposed());

		// create and add validator
		//
		YMinLengthValidator yValidator = ValidationFactory.eINSTANCE
				.createYMinLengthValidator();
		YMinLengthValidator yValidator2 = ValidationFactory.eINSTANCE
				.createYMinLengthValidator();
		IValidatorEditpart validatorEditpart = editpartManager
				.getEditpart(context, yValidator);
		IValidatorEditpart validatorEditpart2 = editpartManager
				.getEditpart(context, yValidator2);
		fieldEditpart.addValidator(validatorEditpart);
		fieldEditpart.addValidator(validatorEditpart2);
		assertFalse(validatorEditpart.isDisposed());
		assertFalse(validatorEditpart2.isDisposed());

		// dispose parent
		//
		fieldEditpart.dispose();
		assertTrue(fieldEditpart.isDisposed());
		assertTrue(validatorEditpart.isDisposed());
		assertTrue(validatorEditpart2.isDisposed());
	}

	/**
	 * Tests the addition and removal of validators by the edit parts.
	 */
	@Test
	public void test_addRemoveValidator() {

		ViewContext context = new ViewContext();
		
		// ...> yLayout1
		// ......> yField1
		YLayout yLayout1 = modelFactory.createYLayout();
		YField yField1 = modelFactory.createYField();
		yLayout1.getElements().add(yField1);
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context, yField1);

		// create and add validator by editpart
		//
		YMinLengthValidator yValidator = ValidationFactory.eINSTANCE
				.createYMinLengthValidator();
		IValidatorEditpart validatorEditpart = editpartManager
				.getEditpart(context, yValidator);

		assertEquals(0, field1Editpart.getValidators().size());
		assertEquals(0, yField1.getValidators().size());

		// add
		field1Editpart.addValidator(validatorEditpart);

		assertEquals(1, field1Editpart.getValidators().size());
		assertEquals(1, yField1.getValidators().size());
		assertSame(validatorEditpart, field1Editpart.getValidators().get(0));

		// add twice -> no effect
		field1Editpart.addValidator(validatorEditpart);

		assertEquals(1, field1Editpart.getValidators().size());
		assertEquals(1, yField1.getValidators().size());

		// remove validator by editpart
		//
		field1Editpart.removeValidator(validatorEditpart);

		assertEquals(0, field1Editpart.getValidators().size());
		assertEquals(0, yField1.getValidators().size());

		// remove twice
		//
		field1Editpart.removeValidator(validatorEditpart);

		assertEquals(0, field1Editpart.getValidators().size());
		assertEquals(0, yField1.getValidators().size());
	}

	/**
	 * Tests the addition and removal of validators by the model.
	 */
	@Test
	public void test_addRemoveValidatorByModel() {
		
		ViewContext context = new ViewContext();
		
		// ...> yLayout1
		// ......> yField1
		YLayout yLayout1 = modelFactory.createYLayout();
		YField yField1 = modelFactory.createYField();
		yLayout1.getElements().add(yField1);
		ILayoutEditpart layout1Editpart = (ILayoutEditpart) editpartManager
				.getEditpart(context, yLayout1);
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context, yField1);

		Assert.assertEquals(1, layout1Editpart.getElements().size());
		Assert.assertSame(yLayout1, yField1.getParent());

		// create validator
		//
		ValidationFactory vf = ValidationFactory.eINSTANCE;
		YMinLengthValidator yValidator = vf.createYMinLengthValidator();
		assertEquals(0, yField1.getValidators().size());
		assertEquals(0, field1Editpart.getValidators().size());

		// add by model

		yField1.getValidators().add(yValidator);

		assertEquals(1, yField1.getValidators().size());
		assertEquals(1, field1Editpart.getValidators().size());
		assertSame(yValidator, yField1.getValidators().get(0));

		// add twice -> no effect
		//
		yField1.getValidators().add(yValidator);

		assertEquals(1, yField1.getValidators().size());
		assertEquals(1, field1Editpart.getValidators().size());
		assertSame(yValidator, yField1.getValidators().get(0));

		// remove validator by model
		//
		yField1.getValidators().remove(yValidator);

		assertEquals(0, yField1.getValidators().size());
		assertEquals(0, field1Editpart.getValidators().size());

		// remove twice
		//
		yField1.getValidators().remove(yValidator);

		assertEquals(0, yField1.getValidators().size());
		assertEquals(0, field1Editpart.getValidators().size());

	}

	/**
	 * Tests whether the list of validators for an editpart is unmodifiable
	 * (only the list within the model may be modified).
	 */
	@Test
	public void test_Validators_unmodifiable() {
		
		ViewContext context = new ViewContext();
		
		YField yField1 = modelFactory.createYField();
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context, yField1);

		ValidationFactory vf = ValidationFactory.eINSTANCE;
		YMinLengthValidator yValidator = vf.createYMinLengthValidator();
		IValidatorEditpart validatorEditpart = editpartManager
				.getEditpart(context, yValidator);

		try {
			field1Editpart.getValidators().add(validatorEditpart);
			Assert.fail("Must be unmodifieable");
		} catch (Exception e) {
		}
	}
}
