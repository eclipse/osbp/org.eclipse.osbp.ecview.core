/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.editparts.emf;

import static org.junit.Assert.assertSame;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Future;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.osbp.ecview.core.common.beans.ISlot;
import org.eclipse.osbp.ecview.core.common.context.ContextException;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IFieldEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.ILayoutEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.YViewSet;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityManager;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.YTextField;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the common editparts issues.
 */
@SuppressWarnings("restriction")
public class EditpartsTest {

	private DelegatingEditPartManager editpartManager = DelegatingEditPartManager
			.getInstance();
	private ResourceSetImpl resourceSet;
	private CoreModelFactory modelFactory = CoreModelFactory.eINSTANCE;

	/**
	 * Casts element to EObject.
	 * 
	 * @param element
	 * @return
	 */
	protected EObject castEObject(Object element) {
		return (EObject) element;
	}

	/**
	 * Setup the test.
	 */
	@Before
	public void setup() {
		resourceSet = new ResourceSetImpl();
		resourceSet
				.getResourceFactoryRegistry()
				.getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION,
						new XMIResourceFactoryImpl());
		resourceSet.getPackageRegistry().put(CoreModelPackage.eNS_URI,
				CoreModelPackage.eINSTANCE);

		editpartManager.clear();
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.common.editpart.emf.EditpartManager());
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.extension.editpart.emf.EditpartManager());
	}

	/**
	 * Tests that only one editpart instance is created for one model instance.<br>
	 * Note that the editpartManager.getEditpart(context, ) was used.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_SingletonEdipartPerEObjectInstance__FirstAccessByParentEditpart() {
		// END SUPRESS CATCH EXCEPTION
		Resource resource = resourceSet.createResource(URI
				.createURI(CoreModelPackage.eNS_URI));
		YViewSet viewSet = modelFactory.createYViewSet();
		resource.getContents().add(castEObject(viewSet));

		// viewSet
		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		viewSet.getViews().add(view1);
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		viewSet.getViews().add(view2);
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		IViewContext context = new ViewContext();
		// access the editparts from their parents
		//
		// view1
		IViewEditpart view1Editpart = editpartManager.getEditpart(context,
				view1);
		// layout1
		ILayoutEditpart layout1Editpart = (ILayoutEditpart) view1Editpart
				.getContent();
		// field1
		IFieldEditpart field1Editpart = (IFieldEditpart) layout1Editpart
				.getElements().get(0);
		// view 2
		IViewEditpart view2Editpart = editpartManager.getEditpart(context,
				view2);
		// layout2
		ILayoutEditpart layout2Editpart = (ILayoutEditpart) view2Editpart
				.getContent();
		// field2
		IFieldEditpart field2Editpart = (IFieldEditpart) layout2Editpart
				.getElements().get(0);

		// ensure that the eObject of the edit part is the same
		// as the eObject from the ui model
		//
		Assert.assertSame(view1, view1Editpart.getModel());
		Assert.assertSame(layout1, layout1Editpart.getModel());
		Assert.assertSame(field1, field1Editpart.getModel());
		Assert.assertSame(view2, view2Editpart.getModel());
		Assert.assertSame(layout2, layout2Editpart.getModel());
		Assert.assertSame(field2, field2Editpart.getModel());

		// ensure that the editpart can be accessed by its model element
		//
		Assert.assertSame(view1Editpart, ElementEditpart.findEditPartFor(view1));
		Assert.assertSame(layout1Editpart,
				ElementEditpart.findEditPartFor(layout1));
		Assert.assertSame(field1Editpart,
				ElementEditpart.findEditPartFor(field1));
		Assert.assertSame(view2Editpart, ElementEditpart.findEditPartFor(view2));
		Assert.assertSame(layout2Editpart,
				ElementEditpart.findEditPartFor(layout2));
		Assert.assertSame(field2Editpart,
				ElementEditpart.findEditPartFor(field2));

		// ensure that the editpartManager also returns the singleton instance
		//
		Assert.assertSame(view1Editpart,
				editpartManager.getEditpart(context, view1));
		Assert.assertSame(layout1Editpart,
				editpartManager.getEditpart(context, layout1));
		Assert.assertSame(field1Editpart,
				editpartManager.getEditpart(context, field1));
		Assert.assertSame(view2Editpart,
				editpartManager.getEditpart(context, view2));
		Assert.assertSame(layout2Editpart,
				editpartManager.getEditpart(context, layout2));
		Assert.assertSame(field2Editpart,
				editpartManager.getEditpart(context, field2));
	}

	/**
	 * Tests that only one editpart instance is created for one model instance.<br>
	 * It's the same as {@link #test_SingletonEdipartPerEObjectInstance()} but
	 * the editpartManager.getEditpart(context, object) first accesses the
	 * editpart. Afterwards it is determined by the edit parts parent.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_SingletonEdipartPerEObjectInstance__FirstAccessByEditpartManager() {
		// END SUPRESS CATCH EXCEPTION
		Resource resource = resourceSet.createResource(URI
				.createURI(CoreModelPackage.eNS_URI));
		YViewSet viewSet = modelFactory.createYViewSet();
		resource.getContents().add(castEObject(viewSet));

		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		viewSet.getViews().add(view1);
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		viewSet.getViews().add(view2);
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		ViewContext context = new ViewContext();

		// access the editparts the editpartManager
		//
		// view1
		IViewEditpart view1Editpart = editpartManager.getEditpart(context,
				view1);
		// layout1
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		// field1
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				field1);
		// view 2
		IViewEditpart view2Editpart = editpartManager.getEditpart(context,
				view2);
		// layout2
		ILayoutEditpart layout2Editpart = editpartManager.getEditpart(context,
				layout2);
		// field2
		IFieldEditpart field2Editpart = editpartManager.getEditpart(context,
				field2);

		// ensure that the eObject of the edit part is the same
		// as the eObject from the ui model
		//
		Assert.assertSame(view1, view1Editpart.getModel());
		Assert.assertSame(layout1, layout1Editpart.getModel());
		Assert.assertSame(field1, field1Editpart.getModel());
		Assert.assertSame(view2, view2Editpart.getModel());
		Assert.assertSame(layout2, layout2Editpart.getModel());
		Assert.assertSame(field2, field2Editpart.getModel());

		// ensure that the editpart can be accessed by its model element
		//
		Assert.assertSame(view1Editpart, ElementEditpart.findEditPartFor(view1));
		Assert.assertSame(layout1Editpart,
				ElementEditpart.findEditPartFor(layout1));
		Assert.assertSame(field1Editpart,
				ElementEditpart.findEditPartFor(field1));
		Assert.assertSame(view2Editpart, ElementEditpart.findEditPartFor(view2));
		Assert.assertSame(layout2Editpart,
				ElementEditpart.findEditPartFor(layout2));
		Assert.assertSame(field2Editpart,
				ElementEditpart.findEditPartFor(field2));

		// ensure that the editpart parents also returns the singleton instance
		//
		Assert.assertSame(view1Editpart,
				editpartManager.getEditpart(context, view1));
		Assert.assertSame(layout1Editpart, view1Editpart.getContent());
		Assert.assertSame(field1Editpart, layout1Editpart.getElements().get(0));
		Assert.assertSame(view2Editpart,
				editpartManager.getEditpart(context, view2));
		Assert.assertSame(layout2Editpart, view2Editpart.getContent());
		Assert.assertSame(field2Editpart, layout2Editpart.getElements().get(0));
	}

	/**
	 * Test the getParent method by emf model.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_getParent_WithEMFModel() {
		// END SUPRESS CATCH EXCEPTION

		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		// access the editparts from their parents
		//

		ViewContext context_1 = new ViewContext();
		ViewContext context_2 = new ViewContext();
		
		// view1
		IViewEditpart view1Editpart = editpartManager.getEditpart(context_1,
				view1);
		// layout1
		ILayoutEditpart layout1Editpart = (ILayoutEditpart) view1Editpart
				.getContent();
		// field1
		IFieldEditpart field1Editpart = (IFieldEditpart) layout1Editpart
				.getElements().get(0);
		// view 2
		IViewEditpart view2Editpart = editpartManager.getEditpart(context_2,
				view2);
		// layout2
		ILayoutEditpart layout2Editpart = (ILayoutEditpart) view2Editpart
				.getContent();
		// field2
		IFieldEditpart field2Editpart = (IFieldEditpart) layout2Editpart
				.getElements().get(0);

		// ensure that the eObject of the edit part is the same
		// as the eObject from the ui model
		//
		Assert.assertSame(view1Editpart, layout1Editpart.getParent());
		Assert.assertSame(layout1Editpart, field1Editpart.getParent());
		Assert.assertSame(view2Editpart, layout2Editpart.getParent());
		Assert.assertSame(layout2Editpart, field2Editpart.getParent());

		// check the parents of the emf model
		//
		// TODO check this - view is not a layout!
		Assert.assertNull(layout1.getParent());
		Assert.assertSame(layout1, field1.getParent());
		Assert.assertNull(layout2.getParent());
		Assert.assertSame(layout2, field2.getParent());
	}

	// BEGIN SUPRESS CATCH EXCEPTION
	@Test
	public void test_getView_WithEMFModel() {
		// END SUPRESS CATCH EXCEPTION

		// ...> view1
		// ......> layout1
		// ...........> field1
		// ...> view2
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		ViewContext context_1 = new ViewContext();
		ViewContext context_2 = new ViewContext();

		// access the editparts from their parents
		//
		// view1
		IViewEditpart view1Editpart = editpartManager.getEditpart(context_1,
				view1);
		// layout1
		ILayoutEditpart layout1Editpart = (ILayoutEditpart) view1Editpart
				.getContent();
		// field1
		IFieldEditpart field1Editpart = (IFieldEditpart) layout1Editpart
				.getElements().get(0);
		// view 2
		IViewEditpart view2Editpart = editpartManager.getEditpart(context_2,
				view2);
		// layout2
		ILayoutEditpart layout2Editpart = (ILayoutEditpart) view2Editpart
				.getContent();
		// field2
		IFieldEditpart field2Editpart = (IFieldEditpart) layout2Editpart
				.getElements().get(0);

		// checks that all the edit parts are returning the proper view
		//
		Assert.assertSame(view1Editpart, layout1Editpart.getView());
		Assert.assertSame(view1Editpart, field1Editpart.getView());
		Assert.assertSame(view2Editpart, layout2Editpart.getView());
		Assert.assertSame(view2Editpart, field2Editpart.getView());

		// checks that all the model elements are returning the proper view
		//
		Assert.assertSame(view1, layout1.getView());
		Assert.assertSame(view1, field1.getView());
		Assert.assertSame(view2, layout2.getView());
		Assert.assertSame(view2, field2.getView());
	}

	/**
	 * Tests that only one editpart instance is created for one model instance.<br>
	 * It's the same as {@link #test_SingletonEdipartPerEObjectInstance()} but
	 * the editpartManager.getEditpart(context, object) first accesses the
	 * editpart. Afterwards it is determined by the edit parts parent.
	 */
	// BEGIN SUPRESS CATCH EXCEPTION
	@Test
	public void test_ExtensionModel() {
		// END SUPRESS CATCH EXCEPTION
		YTextField textField = ExtensionModelFactory.eINSTANCE
				.createYTextField();

		ViewContext context = new ViewContext();

		// access the editparts the editpartManager
		//
		// viewSet
		IEmbeddableEditpart textEditPart = editpartManager.getEditpart(context,
				textField);

		// ensure that the eObject of the edit part is the same
		// as the eObject from the ui model
		//
		Assert.assertSame(textField, textEditPart.getModel());

		// ensure that the editpart can be accessed by its model element
		//
		Assert.assertSame(textEditPart,
				ElementEditpart.findEditPartFor(textField));

		// ensure that the editpart parents also returns the singleton instance
		//
		Assert.assertSame(textEditPart,
				editpartManager.getEditpart(context, textField));
	}

	// BEGIN SUPRESS CATCH EXCEPTION
	@Test
	public void test_ID() {
		// END SUPRESS CATCH EXCEPTION
		// Start with an empty id and let the edit part create one
		//

		ViewContext context = new ViewContext();

		final YLayout yLayout = modelFactory.createYLayout();
		Assert.assertNull(yLayout.getId());
		final ILayoutEditpart layoutEditPart = editpartManager.getEditpart(
				context, yLayout);
		Assert.assertEquals(yLayout.getId(), layoutEditPart.getId());
		Assert.assertNotNull(yLayout.getId());

		try {
			yLayout.setId("MyId");
			Assert.fail("Exception must be thrown!");
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
		}

		// Start with an given id
		//
		final YLayout yLayout2 = modelFactory.createYLayout();
		yLayout2.setId("Huhuhu");
		final ILayoutEditpart layoutEditPart2 = editpartManager.getEditpart(
				context, yLayout2);
		Assert.assertEquals(yLayout2.getId(), layoutEditPart2.getId());
		Assert.assertEquals("Huhuhu", yLayout2.getId());

		try {
			yLayout2.setId("MyId");
			Assert.fail("Exception must be thrown!");
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
		}
	}

	@Test
	public void test_EditpartFromModel_DifferentEditpartsRegisteredAsAdapter() {
		// Different adapters (editparts) are registered as listener at the
		// eObject.
		// ElementEditpart.findEditpart(yElement) needs to return the editpart
		// that is associated with the yElement.
		YView view1 = modelFactory.createYView();
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField yField1 = modelFactory.createYField();
		layout1.getElements().add(yField1);
		YField yField2 = modelFactory.createYField();
		layout1.getElements().add(yField2);

		ViewContext context = new ViewContext();

		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				yField1);
		IFieldEditpart field2Editpart = editpartManager.getEditpart(context,
				yField2);

		yField1.eAdapters().add((Adapter) field2Editpart);

		assertSame(field1Editpart, ElementEditpart.findEditPartFor(yField1));
	}

	/**
	 * Tests the disposal of edit parts.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_Dispose_Containements() {
		// END SUPRESS CATCH EXCEPTION
		Resource resource = resourceSet.createResource(URI
				.createURI(CoreModelPackage.eNS_URI));
		YViewSet viewSet = modelFactory.createYViewSet();
		resource.getContents().add(castEObject(viewSet));

		// ......> layout1
		// ...........> field1
		// ......> layout2
		// ...........> field2
		YView view1 = modelFactory.createYView();
		viewSet.getViews().add(view1);
		YLayout layout1 = modelFactory.createYLayout();
		view1.setContent(layout1);
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);

		YView view2 = modelFactory.createYView();
		viewSet.getViews().add(view2);
		YLayout layout2 = modelFactory.createYLayout();
		view2.setContent(layout2);
		YField field2 = modelFactory.createYField();
		layout2.getElements().add(field2);

		// access the editparts the editpartManager
		//
		ViewContext context_1 = new ViewContext();
		
		ViewContext context_2 = new ViewContext();

		IViewEditpart view1Editpart = editpartManager.getEditpart(context_1,
				view1);
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context_1,
				layout1);
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context_1,
				field1);
		IViewEditpart view2Editpart = editpartManager.getEditpart(context_2,
				view2);
		ILayoutEditpart layout2Editpart = editpartManager.getEditpart(context_2,
				layout2);
		IFieldEditpart field2Editpart = editpartManager.getEditpart(context_2,
				field2);

		Assert.assertFalse(view1Editpart.isDisposed());
		Assert.assertFalse(layout1Editpart.isDisposed());
		Assert.assertFalse(field1Editpart.isDisposed());
		Assert.assertFalse(view2Editpart.isDisposed());
		Assert.assertFalse(layout2Editpart.isDisposed());
		Assert.assertFalse(field2Editpart.isDisposed());

		// dispose the root and all contained edit parts will be disposed too
		//
		view1Editpart.dispose();

		Assert.assertTrue(view1Editpart.isDisposed());
		Assert.assertTrue(layout1Editpart.isDisposed());
		Assert.assertTrue(field1Editpart.isDisposed());
		Assert.assertFalse(view2Editpart.isDisposed());
		Assert.assertFalse(layout2Editpart.isDisposed());
		Assert.assertFalse(field2Editpart.isDisposed());
	}

	/**
	 * Tests the disposal of edit parts.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_UiViewEditPart_content() {
		// END SUPRESS CATCH EXCEPTION

		ViewContext context = new ViewContext();

		// view
		// ...> layout1
		YView view = modelFactory.createYView();
		IViewEditpart viewEditPart = editpartManager.getEditpart(context, view);

		Assert.assertNull(view.getContent());
		Assert.assertNull(viewEditPart.getContent());

		// set layout by model
		YLayout layout1 = modelFactory.createYLayout();
		view.setContent(layout1);
		Assert.assertSame(layout1, view.getContent());
		Assert.assertSame(editpartManager.getEditpart(context, layout1),
				viewEditPart.getContent());
	}

	/**
	 * Tests the disposal of edit parts.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_UiLayoutEditPart_elements() {
		// END SUPRESS CATCH EXCEPTION
		YLayout rootLayout = modelFactory.createYLayout();
		// layout
		// ...> layout1
		// ...> field1
		YLayout layout1 = modelFactory.createYLayout();
		rootLayout.getElements().add(layout1);

		YField field1 = modelFactory.createYField();
		rootLayout.getElements().add(field1);

		ViewContext context = new ViewContext();

		// access the editparts by the editpartManager
		//
		ILayoutEditpart rootLayoutEditPart = editpartManager.getEditpart(
				context, rootLayout);

		Assert.assertEquals(2, rootLayoutEditPart.getElements().size());
		Assert.assertEquals(rootLayout.getElements().size(), rootLayoutEditPart
				.getElements().size());

		// add 3rd field by model
		YField field3 = modelFactory.createYField();
		rootLayout.getElements().add(field3);
		Assert.assertEquals(3, rootLayoutEditPart.getElements().size());
		Assert.assertEquals(rootLayout.getElements().size(), rootLayoutEditPart
				.getElements().size());

		// add 4th layout by edit part
		YLayout layout4 = modelFactory.createYLayout();
		rootLayout.getElements().add(layout4);
		Assert.assertEquals(4, rootLayoutEditPart.getElements().size());
		Assert.assertEquals(rootLayout.getElements().size(), rootLayoutEditPart
				.getElements().size());
		ILayoutEditpart layout4Editpart = editpartManager.findEditpart(layout4);

		// remove 3rd layout by model
		rootLayout.getElements().remove(field3);
		Assert.assertEquals(3, rootLayoutEditPart.getElements().size());
		Assert.assertEquals(rootLayout.getElements().size(), rootLayoutEditPart
				.getElements().size());

		// remove 4rd layout by edit part
		rootLayoutEditPart.removeElement(layout4Editpart);
		Assert.assertEquals(2, rootLayoutEditPart.getElements().size());
		Assert.assertEquals(rootLayout.getElements().size(), rootLayoutEditPart
				.getElements().size());
	}

	// BEGIN SUPRESS CATCH EXCEPTION
	@Test
	public void test_setContext() {
		// END SUPRESS CATCH EXCEPTION
		// ...........> field2

		YView view1 = modelFactory.createYView();

		ViewContext context1 = new ViewContext();
		IViewEditpart view1EditPart = editpartManager.getEditpart(context1,
				view1);
		assertSame(context1, view1EditPart.getContext());

		// does not create a new editpart and does not set context
		ViewContext context2 = new ViewContext();
		IViewEditpart view1_2EditPart = editpartManager.getEditpart(context2,
				view1);
		assertSame(context1, view1_2EditPart.getContext());
		assertSame(view1EditPart, view1_2EditPart);
	}

	/**
	 * Internal context for testing.
	 */
	private static class InternalViewContext implements IViewContext {

		private boolean rendered;

		@Override
		public boolean isDisposed() {
			return false;
		}

		@Override
		public void dispose() {
		}

		@Override
		public void addDisposeListener(Listener listener) {
		}

		@Override
		public void removeDisposeListener(Listener listener) {
		}

		@Override
		public String getPresentationURI() {
			return null;
		}

		@Override
		public IViewEditpart getViewEditpart() {
			return null;
		}

		@Override
		public Object getBean(String selector) {
			return null;
		}

		@Override
		public void setBean(String selector, Object bean) {
		}

		@Override
		public Object getRootLayout() {
			return null;
		}

		@Override
		public void render(String presentationURI, Object rootLayout,
				Map<String, Object> parameter) throws ContextException {

		}

		@Override
		public boolean isRendered() {
			return rendered;
		}

		@Override
		public <S> S getService(String selector) {
			return null;
		}

		@Override
		public void registerService(String selector, Object service) {

		}

		@Override
		public void unregisterService(String selector) {

		}

		@Override
		public ISlot getBeanSlot(String selector) {
			return null;
		}

		@Override
		public ISlot createBeanSlot(String selector, Class<?> type) {
			return null;
		}

		@Override
		public void exec(Runnable runnable) {

		}

		@Override
		public Future<?> execAsync(Runnable runnable) {
			return null;
		}

		@Override
		public void setLocale(Locale locale) {

		}

		@Override
		public Locale getLocale() {
			return null;
		}

		@Override
		public boolean isDisposing() {

			return false;
		}

		@Override
		public ISlot createBeanSlot(String selector, Class<?> type,
				String eventTopic) {

			return null;
		}

		@Override
		public IVisibilityManager getVisibilityManager() {

			return null;
		}

		@Override
		public List<IExposedActionEditpart> getExposedActions() {

			return null;
		}

		@Override
		public Object findModelElement(String id) {

			return null;
		}

		@Override
		public Object findBoundField(String bindingIdRegex) {
			return null;
		}

		@Override
		public void setViewEditpart(IViewEditpart viewEditpart) {
			// TODO Auto-generated method stub

		}

		@Override
		public Map<String, ISlot> getValueBeans() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object findYEmbeddableElement(String id) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Map<String, Object> getRenderingParams() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setRenderingParams(Map<String, Object> params) {
			// TODO Auto-generated method stub
			
		}
	}
}
