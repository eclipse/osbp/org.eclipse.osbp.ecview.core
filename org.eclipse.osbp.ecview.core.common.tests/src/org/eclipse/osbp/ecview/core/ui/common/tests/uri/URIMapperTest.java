/**
 * Copyright (c) 2013 Loetz GmbH&Co.KG(Heidelberg). All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0  which accompanies this distribution,
t https://www.eclipse.org/legal/epl-2.0/
t
t SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: Christophe Loetz (Loetz GmbH&Co.KG) - initial API and implementation
 */

package org.eclipse.osbp.ecview.core.ui.common.tests.uri;

import java.net.URI;

import org.junit.Assert;
import org.junit.Test;
import org.eclipse.osbp.ecview.core.common.uri.URIMapper;
import org.eclipse.osbp.ecview.core.common.uri.URIMapper.Mapping;

public class URIMapperTest {

	@Test
	public void testURI() {
		String uriStr = "view://bean/root/invoiceInfo#value.customer.address.country";
		URI uri = URI.create(uriStr);
		URIMapper uriMapper = new URIMapper();
		Mapping mapping = uriMapper.addURI(uri);
		Assert.assertEquals(
				URI.create("view://bean/root#value.invoiceInfo.customer.address.country"),
				mapping.getFragmentedURI());
		Assert.assertEquals(
				URI.create("view://bean/root/invoiceInfo/customer/address#value.country"),
				mapping.getPathedURI());
	}
}
