/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.editparts.emf.datatypes;

import org.junit.Assert;
import org.junit.Test;

public class DatatypeEditpartTest {

	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_dispose() {
		// END SUPRESS CATCH EXCEPTION
	}

	public void test_getCurrentState() {
		Assert.fail();
	}

	public void test_internalGetValidatorsDelta() {
		Assert.fail();
	}

	public void test_internalGetAllValidators() {
		Assert.fail();
	}

	public void test_addRemoveBridge() {
		Assert.fail();
	}

	public void test_getValidatorsForBridge() {
		Assert.fail();
	}

	public void test_datatypeChangeEvent() {
		Assert.fail();
	}

	public void test_datatypeChangeEvent_ByChangingValiationConfigAttribute() {
		// for instance change the minLength property in datatype
		Assert.fail();
	}

	public void test_notifyDatatypeChanged() {
		Assert.fail();
	}

	public void test_internalGetValidationSettings() {
		Assert.fail();
	}

	public void test_findToRemoveValidators() {
		Assert.fail();
	}

	public void test_ClassValidatorDelta() {
		Assert.fail();
	}

	public void test_MemoryLeaks() {
		Assert.fail();
	}

	public void test_ConcurrentAccess() {
		Assert.fail();
	}

	public void test_validatorDelta_ForDifferentNotifications() {
		// ValidatorDelta should be tested:
		// addValidators
		// removeValidators
		// and different cases between
		Assert.fail();
	}

}
