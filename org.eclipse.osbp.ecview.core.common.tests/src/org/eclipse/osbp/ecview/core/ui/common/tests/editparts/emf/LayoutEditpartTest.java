/**
 * Copyright (c) 2012, 2015 - Lunifera GmbH (Austria), Loetz GmbH&Co.KG and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.ui.common.tests.editparts.emf;

import org.eclipse.osbp.ecview.core.common.context.ViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IFieldEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.ILayoutEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests the ui layout edit part.
 */
@SuppressWarnings("restriction")
public class LayoutEditpartTest {

	private DelegatingEditPartManager editpartManager = DelegatingEditPartManager
			.getInstance();
	private CoreModelFactory modelFactory = CoreModelFactory.eINSTANCE;

	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		editpartManager.clear();
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.common.editpart.emf.EditpartManager());
		editpartManager
				.addDelegate(new org.eclipse.osbp.ecview.core.extension.editpart.emf.EditpartManager());
	}

	/**
	 * Tests the parent property.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_parent() {
		ViewContext context = new ViewContext();

		// ...> layout1
		// ......> field1
		YLayout layout1 = modelFactory.createYLayout();
		YLayout layout1_1 = modelFactory.createYLayout();
		layout1.getElements().add(layout1_1);
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		ILayoutEditpart layout1_1Editpart = editpartManager.getEditpart(
				context, layout1_1);

		Assert.assertEquals(1, layout1Editpart.getElements().size());
		Assert.assertSame(layout1Editpart, layout1_1Editpart.getParent());
		Assert.assertSame(layout1, layout1_1.getParent());

		// dispose the field
		//
		layout1_1Editpart.dispose();
		Assert.assertFalse(layout1Editpart.isDisposed());
		Assert.assertTrue(layout1_1Editpart.isDisposed());

		Assert.assertEquals(1, layout1Editpart.getElements().size());
		Assert.assertSame(layout1, layout1_1.getParent());
	}

	// END SUPRESS CATCH EXCEPTION

	/**
	 * Tests adding and removing embeddeables.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_addAndRemove_Embedded() {
		// END SUPRESS CATCH EXCEPTION

		ViewContext context = new ViewContext();

		// ...> layout
		// ......> field1
		YLayout layout = modelFactory.createYLayout();
		YField field1 = modelFactory.createYField();
		layout.getElements().add(field1);
		ILayoutEditpart layoutEditpart = (ILayoutEditpart) editpartManager
				.getEditpart(context, layout);
		Assert.assertEquals(1, layoutEditpart.getElements().size());
		Assert.assertSame(layout, field1.getParent());

		// ...> layout
		// ......> field1
		// ......> field2
		// add by model
		YField field2 = modelFactory.createYField();
		layout.getElements().add(field2);
		Assert.assertEquals(2, layoutEditpart.getElements().size());
		Assert.assertSame(layout, field2.getParent());

		// add model twice
		layout.getElements().add(field2);
		Assert.assertEquals(2, layout.getElements().size());
		Assert.assertEquals(2, layoutEditpart.getElements().size());
		Assert.assertSame(layout, field2.getParent());

		// ...> layout
		// ......> field1
		// ......> field2
		// ......> field3
		// add by edit part
		IFieldEditpart field3Editpart = editpartManager.getEditpart(context,
				CoreModelFactory.eINSTANCE.createYField());
		layoutEditpart.addElement(field3Editpart);
		Assert.assertSame(layoutEditpart, field3Editpart.getParent());
		Assert.assertEquals(3, layoutEditpart.getElements().size());

		// add edit part twice
		layoutEditpart.addElement(field3Editpart);
		Assert.assertSame(layoutEditpart, field3Editpart.getParent());
		Assert.assertEquals(3, layout.getElements().size());
		Assert.assertEquals(3, layoutEditpart.getElements().size());

		// remove by model
		layout.getElements().remove(field2);
		Assert.assertEquals(2, layoutEditpart.getElements().size());
		Assert.assertNull(field2.getParent());

		// remove by model twice
		layout.getElements().remove(field2);
		Assert.assertEquals(2, layoutEditpart.getElements().size());
		Assert.assertNull(field2.getParent());

		// remove by edit part
		layoutEditpart.removeElement(field3Editpart);
		Assert.assertEquals(1, layoutEditpart.getElements().size());
		Assert.assertNull(field3Editpart.getParent());

		// remove by edit part twice
		layoutEditpart.removeElement(field3Editpart);
		Assert.assertEquals(1, layoutEditpart.getElements().size());
		Assert.assertNull(field3Editpart.getParent());

	}

	/**
	 * Test the move of embedded elements by the model.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_moveEmbedded_byModel() {
		// END SUPRESS CATCH EXCEPTION

		ViewContext context = new ViewContext();

		// ...> layout1
		// ......> field1
		// ...> layout2
		YLayout layout1 = modelFactory.createYLayout();
		YField field1 = modelFactory.createYField();
		YLayout layout2 = modelFactory.createYLayout();
		layout1.getElements().add(field1);
		ILayoutEditpart layout1Editpart = (ILayoutEditpart) editpartManager
				.getEditpart(context, layout1);
		ILayoutEditpart layout2Editpart = (ILayoutEditpart) editpartManager
				.getEditpart(context, layout2);
		Assert.assertEquals(1, layout1Editpart.getElements().size());
		Assert.assertEquals(0, layout2Editpart.getElements().size());
		Assert.assertSame(layout1, field1.getParent());

		// move it
		//
		layout2.getElements().add(field1);
		Assert.assertEquals(0, layout1.getElements().size());
		Assert.assertEquals(1, layout2.getElements().size());
		Assert.assertSame(layout2, field1.getParent());

		Assert.assertEquals(0, layout1Editpart.getElements().size());
		Assert.assertEquals(1, layout2Editpart.getElements().size());
	}

	/**
	 * Tests the getView method.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_getView() {
		// END SUPRESS CATCH EXCEPTION

		ViewContext context = new ViewContext();

		// ...> view1
		// ......> layout1
		// .........> field1
		YView view1 = modelFactory.createYView();
		YLayout layout1 = modelFactory.createYLayout();
		YField field1 = modelFactory.createYField();
		IViewEditpart view1Editpart = editpartManager.getEditpart(context,
				view1);
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				field1);
		view1.setContent(layout1);
		layout1.addElement(field1);

		Assert.assertSame(view1Editpart, layout1Editpart.getView());
		Assert.assertSame(view1Editpart, field1Editpart.getView());
		Assert.assertSame(view1, layout1.getView());
		Assert.assertSame(view1, field1.getView());

		// ...> view1
		// ......> layout1
		// .........> field1
		ViewContext context2 = new ViewContext();
		YView view2 = modelFactory.createYView();
		IViewEditpart view2Editpart = editpartManager.getEditpart(context2,
				view2);
		view2.setContent(layout1);

		Assert.assertTrue(layout1Editpart.isDisposed());
		Assert.assertTrue(field1Editpart.isDisposed());

		layout1Editpart = editpartManager.getEditpart(context2, layout1);
		field1Editpart = editpartManager.getEditpart(context2, field1);

		Assert.assertSame(view2Editpart, layout1Editpart.getView());
		Assert.assertSame(view2Editpart, field1Editpart.getView());
		Assert.assertSame(view2, layout1.getView());
		Assert.assertSame(view2, field1.getView());

	}

	/**
	 * Tests the unmodifyable state of getElements collection.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_getElements_unmodifyable() {
		// END SUPRESS CATCH EXCEPTION

		ViewContext context = new ViewContext();

		YLayout layout1 = modelFactory.createYLayout();
		YField field1 = modelFactory.createYField();
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				field1);

		try {
			layout1Editpart.getElements().add(field1Editpart);
			Assert.fail("Must be unmodifieable");
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
		}
	}

	/**
	 * Tests the disposal of embedded elements.
	 */
	@Test
	// BEGIN SUPRESS CATCH EXCEPTION
	public void test_disposeEmbedded() {
		// END SUPRESS CATCH EXCEPTION

		ViewContext context = new ViewContext();

		// ...> layout1
		// ......> field1
		YLayout layout1 = modelFactory.createYLayout();
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				field1);

		Assert.assertEquals(1, layout1Editpart.getElements().size());
		Assert.assertSame(layout1Editpart, field1Editpart.getParent());
		Assert.assertSame(layout1, field1.getParent());

		// dispose the field
		//
		field1Editpart.dispose();
		Assert.assertFalse(layout1Editpart.isDisposed());
		Assert.assertTrue(field1Editpart.isDisposed());

		// disposed editpart are recreated on request
		Assert.assertEquals(1, layout1Editpart.getElements().size());
		Assert.assertSame(layout1, field1.getParent());
	}

	/**
	 * Tests the disposal of edit parts.
	 */
	// BEGIN SUPRESS CATCH EXCEPTION
	@Test
	public void test_dispose() {
		// END SUPRESS CATCH EXCEPTION

		ViewContext context = new ViewContext();

		// ...> layout1
		// ......> field1
		YLayout layout1 = modelFactory.createYLayout();
		YField field1 = modelFactory.createYField();
		layout1.getElements().add(field1);
		ILayoutEditpart layout1Editpart = editpartManager.getEditpart(context,
				layout1);
		IFieldEditpart field1Editpart = editpartManager.getEditpart(context,
				field1);

		Assert.assertFalse(layout1Editpart.isDisposed());
		Assert.assertFalse(field1Editpart.isDisposed());
		layout1Editpart.dispose();
		Assert.assertTrue(layout1Editpart.isDisposed());
		Assert.assertTrue(field1Editpart.isDisposed());

		layout1Editpart.isDisposed();
		layout1Editpart.dispose();

		try {
			layout1Editpart.addDisposeListener(new IDisposable.Listener() {
				@Override
				public void notifyDisposed(IDisposable notifier) {
				}
			});
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			layout1Editpart.addElement(field1Editpart);
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}
		try {
			layout1Editpart.getElements();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			layout1Editpart.getId();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			layout1Editpart.getModel();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			layout1Editpart.getParent();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			layout1Editpart.getView();
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			layout1Editpart.removeDisposeListener(new IDisposable.Listener() {
				@Override
				public void notifyDisposed(IDisposable notifier) {
				}
			});
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}

		try {
			layout1Editpart.removeElement(field1Editpart);
			Assert.fail();
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (Exception e) {
			// END SUPRESS CATCH EXCEPTION
			// expected
		}
	}
}
