/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.core.emf.api;

import java.util.Locale;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeDatatype;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YDateTimeResolution;

/**
 * Provides information about the dateformat and the resolution.
 */
public interface IDateFormatProvider {

	/**
	 * Returns the date format and resolution for the given parameters.
	 * 
	 * @param yDt
	 * @param locale
	 * @return
	 */
	Info getInfo(YDateTimeDatatype yDt, Locale locale);

	public static class Info {

		private String dateFormat;
		private YDateTimeResolution resolution;

		public Info(String dateFormat, YDateTimeResolution resolution) {
			super();
			this.dateFormat = dateFormat;
			this.resolution = resolution;
		}

		/**
		 * @return the dateFormat
		 */
		public String getDateFormat() {
			return dateFormat;
		}

		/**
		 * @return the resolution
		 */
		public YDateTimeResolution getResolution() {
			return resolution;
		}

	}

}
