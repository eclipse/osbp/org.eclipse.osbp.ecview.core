/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.editpart.IConverterEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.common.presentation.DelegatingConverterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The implementation of the IOpenDialogCommandEditpart.
 */
public class ConverterEditpart extends ElementEditpart<YConverter> implements
		IConverterEditpart {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ConverterEditpart.class);
	private Object delegate;

	@Override
	public Object getDelegate() {
		if (delegate == null) {
			EObject context = (EObject) getModel().eContainer();
			delegate = DelegatingConverterFactory.getInstance()
					.createConverter(getViewContext(context), this);

			LOGGER.info("Created converter for " + getModel());
		}

		return delegate;
	}
}
