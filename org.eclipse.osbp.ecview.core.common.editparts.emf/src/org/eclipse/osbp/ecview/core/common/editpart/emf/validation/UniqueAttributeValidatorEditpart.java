/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableValueEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator.UniqueAttributeValidator;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IUniqueAttributeValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidatorConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.runtime.common.annotations.DtoUtils;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.common.filter.ILFilterService;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.common.types.ITypeProviderService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class RegexpValidatorEditpart.
 */
public class UniqueAttributeValidatorEditpart extends ValidatorEditpart<YUniqueAttributeValidator>
		implements IUniqueAttributeValidatorEditpart {

	private static final Logger LOGGER = LoggerFactory.getLogger(UniqueAttributeValidatorEditpart.class);

	/** The bridge observer. */
	private ValidationConfigToValidatorBridge bridgeObserver;

	private IBindableValueEndpointEditpart boundBeanEP;

	public UniqueAttributeValidatorEditpart() {
		super(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE,
				ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE_FULLY_QUALIFIED_NAME,
				ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__PROPERTY_PATH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.validation.
	 * ValidatorEditpart#createValidator()
	 */
	@Override
	protected IValidator createValidator() {
		YUniqueAttributeValidator yModel = getModel();

		Class<?> beanClass = yModel.getValType();
		if (beanClass == null) {
			ITypeProviderService service = getViewContext(getModel()).getService(ITypeProviderService.class.getName());
			beanClass = service.forName(null, getModel().getValTypeFullyQualifiedName());
		}

		ILFilterService filterService = getViewContext(getModel()).getService(ILFilterService.class.getName());

		// this ep gains access to the bound row
		boundBeanEP = ElementEditpart.getEditpart(viewContext, yModel.getContainerValueBindingEndpoint());

		YField yField = (YField) yModel.eContainer();
		UniqueAttributeValidator validator = new UniqueAttributeValidator(beanClass, yModel.getPropertyPath(), boundBeanEP,
				DtoUtils.getIdField(beanClass).getName(), getDtoService(beanClass.getCanonicalName()), filterService,
				yField.getId(), yField.getLabelI18nKey());
		IViewContext context = getViewContext(getModel());
		validator.setI18nService((II18nService) context.getService(II18nService.class.getName()));
		validator.setLocale(context.getLocale());
		return validator;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <D> IDTOService<D> getDtoService(String dtoName) {
		Bundle bundle = FrameworkUtil.getBundle(getClass());

		String filterString = String.format("(&(objectClass=%s)(dto=%s))", IDTOService.class.getCanonicalName(),
				dtoName);
		try {
			BundleContext context = bundle.getBundleContext();
			Collection<ServiceReference<IDTOService>> references = context.getServiceReferences(IDTOService.class,
					filterString);
			if (!references.isEmpty()) {
				ServiceReference<IDTOService> reference = references.iterator().next();
				return context.getService(reference);
			}
		} catch (InvalidSyntaxException e) {
			LOGGER.error("{}", e);
		}

		LOGGER.error("No dtoService available for dto: {}", dtoName);

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.validation.
	 * IValidatorEditpart#setConfig(org.eclipse.osbp.ecview.core.common.
	 * validation.IValidationConfig)
	 */
	@Override
	public void setConfig(IValidationConfig config) {
		YUniqueAttributeValidatorConfig validatable = (YUniqueAttributeValidatorConfig) config.getValidationSettings();
		// create an observer that transfers the changes at the validatable to
		// the validator
		Map<EStructuralFeature, EStructuralFeature> mapping = new HashMap<>();
		mapping.put(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE,
				ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE);
		mapping.put(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE_FULLY_QUALIFIED_NAME,
				ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__VAL_TYPE_FULLY_QUALIFIED_NAME);
		mapping.put(ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__PROPERTY_PATH,
				ValidationPackage.Literals.YUNIQUE_ATTRIBUTE_VALIDATOR_CONFIG__PROPERTY_PATH);

		bridgeObserver = new ValidationConfigToValidatorBridge(validatable, getModel(), mapping);
	}

	@Override
	protected void internalDispose() {
		try {
			if (bridgeObserver != null) {
				bridgeObserver.dispose();
				bridgeObserver = null;
			}
			if(boundBeanEP != null) {
				boundBeanEP.dispose();
				boundBeanEP = null;
			}
		} finally {
			super.internalDispose();
		}
	}

}
