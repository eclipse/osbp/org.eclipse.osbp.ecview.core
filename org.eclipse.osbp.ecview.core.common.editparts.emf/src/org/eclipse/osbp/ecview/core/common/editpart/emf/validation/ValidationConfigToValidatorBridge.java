/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation;

import java.util.Collections;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

// TODO: Auto-generated Javadoc
/**
 * This class is responsible to observe changes at a validatable and to forward
 * the changes to the validator.
 */
public class ValidationConfigToValidatorBridge extends AdapterImpl {

	/** The source validatable. */
	private final EObject sourceValidatable;
	
	/** The target validator. */
	private final EObject targetValidator;

	/** The mapping. */
	private final Map<EStructuralFeature, EStructuralFeature> mapping;
	
	/** The disposed. */
	private boolean disposed;
	
	/** The called after dispose. */
	private boolean calledAfterDispose;

	/**
	 * Creates an instance for the given values.
	 *
	 * @param sourceValidatable
	 *            the source validatable
	 * @param sourceFeature
	 *            the source feature
	 * @param targetValidator
	 *            the target validator
	 * @param targetFeature
	 *            the target feature
	 * @return the validation config to validator bridge
	 */
	public static ValidationConfigToValidatorBridge createObserver(
			EObject sourceValidatable, EStructuralFeature sourceFeature,
			EObject targetValidator, EStructuralFeature targetFeature) {

		return new ValidationConfigToValidatorBridge(sourceValidatable,
				targetValidator, Collections.singletonMap(sourceFeature,
						targetFeature));
	}

	/**
	 * Instantiates a new validation config to validator bridge.
	 *
	 * @param sourceValidatable
	 *            the source validatable
	 * @param targetValidator
	 *            the target validator
	 * @param mapping
	 *            the mapping
	 */
	public ValidationConfigToValidatorBridge(EObject sourceValidatable,
			EObject targetValidator,
			Map<EStructuralFeature, EStructuralFeature> mapping) {
		super();
		this.sourceValidatable = sourceValidatable;
		this.targetValidator = targetValidator;
		this.mapping = mapping;

		sourceValidatable.eAdapters().add(this);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.common.notify.impl.AdapterImpl#notifyChanged(org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification) {
		if (calledAfterDispose) {
			throw new IllegalStateException("Observer is disposed!");
		}

		// this method is called after disposal once, since the eObjects are
		// nested. So one call after dispose is fine.
		if (disposed) {
			calledAfterDispose = true;
		}

		EStructuralFeature sourceFeature = (EStructuralFeature) notification
				.getFeature();

		// if there is a mapping available, then set the value from
		// source#sourceFeature to target#targetFeature
		if (mapping.containsKey(sourceFeature)) {
			EStructuralFeature targetFeature = mapping.get(sourceFeature);
			targetValidator.eSet(targetFeature, notification.getNewValue());
		}
	}

	/**
	 * Dispose.
	 */
	public void dispose() {
		try {
			sourceValidatable.eAdapters().remove(this);
		} finally {
			disposed = true;
		}
	}

}
