/**
 * Copyright (c) 2013 Loetz GmbH&Co.KG(Heidelberg). All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0  which accompanies this distribution,
t https://www.eclipse.org/legal/epl-2.0/
t
t SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors: Christophe Loetz (Loetz GmbH&Co.KG) - initial API and implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import java.util.Collections;
import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.IActionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * Implementation of {@link IActionEditpart}.
 *
 * @param <M>
 *            the generic type
 */
public class ActionEditpart<M extends YAction> extends EmbeddableEditpart<M>
		implements IActionEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ActionEditpart.class);

	/**
	 * A default constructor.
	 */
	protected ActionEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart.DatatypeBridge#getDatatypeValidators()
	 */
	@Override
	public List<IValidatorEditpart> getDatatypeValidators() {
		return Collections.emptyList();
	}

}
