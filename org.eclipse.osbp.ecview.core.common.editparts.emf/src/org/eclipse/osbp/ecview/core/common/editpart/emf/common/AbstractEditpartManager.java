/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.common;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpartProvider;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * Abstract base implementation of {@link IEditPartManager}.
 */
public abstract class AbstractEditpartManager implements IEditPartManager {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(AbstractEditpartManager.class);

	/**
	 * Returns the edit part for the given model yElement.
	 * 
	 * @param <A>
	 *            an instance of IUiElementEditpart
	 * @param yElement
	 *            the model element
	 * @return editpart
	 */
	@SuppressWarnings("unchecked")
	public static <A extends IElementEditpart> A findEditPartFor(
			YElement yElement) {
		if (yElement == null) {
			return null;
		}

		IElementEditpartProvider editPartProvider = null;
		for (Adapter adapter : ((EObject) yElement).eAdapters()) {
			if (adapter instanceof IElementEditpartProvider) {
				editPartProvider = (IElementEditpartProvider) adapter;
				// only break, if the editpart is for the given model element
				if (yElement == editPartProvider.getEditpart().getModel()) {
					break;
				}
			}
		}

		return editPartProvider != null ? (A) editPartProvider.getEditpart()
				: null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <A extends IElementEditpart> A getEditpart(IViewContext context,
			Object yElement) {
		IElementEditpart editPart = findEditpart(yElement);
		return (A) (editPart != null ? editPart : createEditpart(context,
				yElement));
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @param yElement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends IElementEditpart> A findEditpart(Object yElement) {
		return (A) findEditPartFor((YElement) yElement);
	}

	/**
	 * Creates a new instance of the edit part.
	 * 
	 * @param <A>
	 *            an instance of IUiElementEditpart
	 * @param context
	 *            the context
	 * @param yElement
	 *            the model element
	 * @return editpart
	 */
	protected abstract <A extends IElementEditpart> A createEditpart(
			IViewContext context, Object yElement);

	/**
	 * Creates a new instance of the required edit part.
	 * 
	 * @param <A>
	 *            An instance of {@link IElementEditpart}
	 * @param type
	 *            the type of the editpart to be returned
	 * @return editpart
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IElementEditpart> A createNewInstance(
			Class<? extends IElementEditpart> type) {
		A result = null;
		ElementEditpart<YElement> editPart = null;
		try {
			editPart = (ElementEditpart<YElement>) newInstance(type);
		} catch (InstantiationException e) {
			LOGGER.error("{}", e);
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			LOGGER.error("{}", e);
			throw new RuntimeException(e);
		}
		result = (A) editPart;
		return result;
	}

	/**
	 * Returns a new instance of the type.
	 * 
	 * @param type
	 *            the type of the editpart to be created
	 * @return editpart
	 * 
	 * @throws InstantiationException
	 *             exception
	 * @throws IllegalAccessException
	 *             exception
	 */
	protected abstract IElementEditpart newInstance(
			Class<? extends IElementEditpart> type)
			throws InstantiationException, IllegalAccessException;

	/**
	 * Casts element to eObject.
	 *
	 * @param element
	 *            the element
	 * @return the e object
	 */
	protected EObject castEObject(Object element) {
		return (EObject) element;
	}

	/**
	 * Asserts that only one IUiElementEditpartProvider exists for the given
	 * element.
	 * 
	 * @param element
	 *            the model element
	 */
	protected void assertOneEditpartForModelelement(Object element) {
		YElement yElement = (YElement) element;
		for (Adapter adapter : castEObject(yElement).eAdapters()) {
			if (adapter instanceof IElementEditpartProvider) {
				LOGGER.error("For a modelelement instance only one editpart can be created!");
				throw new RuntimeException(
						"For a modelelement instance only one editpart can be created!");
			}
		}
	}
}