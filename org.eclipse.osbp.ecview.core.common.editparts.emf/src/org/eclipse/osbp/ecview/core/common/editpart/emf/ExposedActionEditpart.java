/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YExposedAction;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler;
import org.eclipse.osbp.runtime.common.i18n.II18nService;

// TODO: Auto-generated Javadoc
/**
 * See {@link ElementEditpart}.
 */
public class ExposedActionEditpart extends ElementEditpart<YExposedAction>
		implements IExposedActionEditpart {

	/**
	 * The default constructor.
	 */
	protected ExposedActionEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart#getIconName()
	 */
	@Override
	public String getIconName() {
		return getModel().getIcon();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart#getDescription()
	 */
	@Override
	public String getDescription() {
		II18nService service = getView().getContext().getService(
				II18nService.ID);
		if (service == null) {
			String label = getModel().getLabel();
			if (label != null && !label.equals("")) {
				return label;
			} else {
				return getModel().getName();
			}
		} else {
			String labelI18nKey = getModel().getLabelI18nKey();
			if (labelI18nKey != null && !labelI18nKey.equals("")) {
				String value = service.getValue(labelI18nKey, getView()
						.getContext().getLocale());
				if (value == null) {
					String label = getModel().getLabel();
					if (label != null && !label.equals("")) {
						return label;
					} else {
						return getModel().getName();
					}
				} else {
					return value;
				}
			}
		}
		return getModel().getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IElementEditpart getParent() {
		YElement yParent = (YElement) getModel().eContainer();
		if (yParent != null) {
			return getEditpart(viewContext, yParent);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart#getView()
	 */
	@Override
	public IViewEditpart getView() {
		YView yView = getModel().getView();
		return yView != null ? (IViewEditpart) getEditpart(viewContext, yView) : null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessable#apply(org.eclipse.osbp.ecview.core.common.visibility.IVisibilityHandler)
	 */
	@Override
	public void apply(IVisibilityHandler handler) {
		getModel().setEnabled(handler.isEnabled());
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessable#resetVisibilityProperties()
	 */
	@Override
	public void resetVisibilityProperties() {
		getModel().setEnabled(getModel().isInitialEnabled());
	}

}
