/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.validation.YValidator;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * Implementation of {@link IValidatorEditpart}.
 *
 * @param <M>
 *            the generic type
 */
public abstract class ValidatorEditpart<M extends YValidator> extends
		ElementEditpart<M> implements IValidatorEditpart {

	/** The Constant LOGGER. */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ValidatorEditpart.class);

	/** The validator. */
	private IValidator validator;
	
	/** The change features. */
	private Set<EStructuralFeature> changeFeatures;

	/**
	 * A default constructor.
	 *
	 * @param changeFeatures
	 *            the change features
	 */
	public ValidatorEditpart(EStructuralFeature... changeFeatures) {
		if (changeFeatures != null) {
			this.changeFeatures = new HashSet<>();
			this.changeFeatures.addAll(Arrays.asList(changeFeatures));
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			if (validator != null) {
				validator.dispose();
			}
			validator = null;
			changeFeatures = null;
		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart#getValidator()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends IValidator> A getValidator() {
		checkDisposed();

		if (this.validator == null) {
			this.validator = createValidator();
		}
		return (A) this.validator;
	}

	/**
	 * Returns a new instance of the validator to use.
	 *
	 * @return the i validator
	 */
	protected abstract IValidator createValidator();

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelSet(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		// if the notification is for a registered feature, then update the
		// validator
		if (changeFeatures != null
				&& changeFeatures.contains(notification.getFeature())) {
			if (validator != null) {
				validator.updateParameter(notification.getNotifier());
			}
		}
		super.handleModelSet(featureId, notification);
	}

}
