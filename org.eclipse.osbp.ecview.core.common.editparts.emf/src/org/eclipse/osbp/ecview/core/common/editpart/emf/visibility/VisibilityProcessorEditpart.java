/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.visibility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IVisibilityProcessorValueBindingEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessorEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityPackage;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor;
import org.eclipse.osbp.runtime.common.types.ITypeProviderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class VisibilityProcessorEditpart.
 *
 * @param <M>
 *            the generic type
 */
public class VisibilityProcessorEditpart<M extends YVisibilityProcessor>
		extends ElementEditpart<M> implements IVisibilityProcessorEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(VisibilityProcessorEditpart.class);

	/** The data used. */
	private ArrayList<IBindingEditpart<?>> dataUsed;
	
	/** The triggers on. */
	private ArrayList<IBindingEditpart<?>> triggersOn;

	/** The processor. */
	protected IVisibilityProcessor processor;

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessorEditpart#activate()
	 */
	@Override
	public void activate() {
		processor = loadProcessor();
		if (processor == null) {
			LOGGER.error("Could not load class "
					+ getModel().getDelegateQualifiedName());
			return;
		}

		IViewContext context = getViewContext(getModel());
		// initializes the visibility processor
		processor.init(context.getVisibilityManager());
		for (IBindingEditpart<?> editpart : getDataUsed()) {
			bindEditpart(processor, editpart);
		}
		for (IBindingEditpart<?> editpart : getTriggersOn()) {
			bindEditpart(processor, editpart);
		}
	}

	/**
	 * Binds the editpart.
	 *
	 * @param processor
	 *            the processor
	 * @param editpart
	 *            the editpart
	 */
	protected void bindEditpart(IVisibilityProcessor processor,
			IBindingEditpart<?> editpart) {
		IBindingEditpart<?> bindingEditpart = (IBindingEditpart<?>) editpart;
		IBindableEndpointEditpart tempTargetEP = bindingEditpart
				.getTargetEndpoint();
		if (!(tempTargetEP instanceof IVisibilityProcessorValueBindingEndpointEditpart)) {
			LOGGER.error("Target endpoint MUST be of type IVisibilityProcessorValueBindingEndpointEditpart. Binding "
					+ bindingEditpart + " skipped");
			return;
		}

		// Set the visibility processor that should be bound
		IVisibilityProcessorValueBindingEndpointEditpart targetEP = (IVisibilityProcessorValueBindingEndpointEditpart) tempTargetEP;
		targetEP.setVisibilityProcessor(processor);

		bindingEditpart.bind();
	}

	/**
	 * Tries to load the processor.
	 *
	 * @return the i visibility processor
	 */
	protected IVisibilityProcessor loadProcessor() {
		if (processor != null) {
			return processor;
		}

		Class<?> processorClass = getDelegateClass();
		IVisibilityProcessor processor = null;
		if (processorClass != null) {
			try {
				processor = (IVisibilityProcessor) processorClass.newInstance();
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			}
		}

		if (processor == null) {
			ITypeProviderService service = getViewContext(getModel())
					.getService(ITypeProviderService.class.getName());
			if (service != null) {
				processorClass = service.forName(
						VisibilityPackage.Literals.YVISIBILITY_PROCESSOR,
						getModel().getDelegateQualifiedName());
				try {
					if (processorClass != null) {
						processor = (IVisibilityProcessor) processorClass
								.newInstance();
					}
				} catch (InstantiationException e) {
				} catch (IllegalAccessException e) {
				}
			} else {
				LOGGER.warn(
						"Could not load visibility processor for class {}!",
						getModel().getDelegateQualifiedName());
			}
		}

		return processor;
	}

	/**
	 * Gets the delegate class.
	 *
	 * @return the delegate class
	 */
	protected Class<?> getDelegateClass() {
		return getModel().getDelegate();
	}

	/**
	 * Gets the data used.
	 *
	 * @return the data used
	 */
	public List<IBindingEditpart<?>> getDataUsed() {
		checkDisposed();

		if (dataUsed == null) {
			internalLoadDataUsed();
		}
		return Collections.unmodifiableList(dataUsed);
	}

	/**
	 * Loads the rule.
	 */
	protected void internalLoadDataUsed() {
		checkDisposed();

		if (dataUsed == null) {
			dataUsed = new ArrayList<IBindingEditpart<?>>();

			for (YBinding yEp : getModel().getDataUsed()) {
				internalAddDataUsed((IBindingEditpart<?>) getEditpart(viewContext, yEp));
			}
		}
	}

	/**
	 * Internal add data used.
	 *
	 * @param editpart
	 *            the editpart
	 */
	protected void internalAddDataUsed(IBindingEditpart<?> editpart) {
		checkDisposed();

		if (dataUsed == null) {
			internalLoadDataUsed();
		}
		if (!dataUsed.contains(editpart)) {
			dataUsed.add(editpart);
			bindEditpart(loadProcessor(), editpart);
		}
	}

	/**
	 * Internal remove data used.
	 *
	 * @param editpart
	 *            the editpart
	 */
	protected void internalRemoveDataUsed(IBindingEditpart<?> editpart) {
		checkDisposed();

		if (dataUsed != null && editpart != null) {
			editpart.dispose();
			dataUsed.remove(editpart);
		}
	}

	/**
	 * Gets the triggers on.
	 *
	 * @return the triggers on
	 */
	public List<IBindingEditpart<?>> getTriggersOn() {
		checkDisposed();

		if (triggersOn == null) {
			internalLoadTriggersOn();
		}
		return Collections.unmodifiableList(triggersOn);
	}

	/**
	 * Loads the rule.
	 */
	protected void internalLoadTriggersOn() {
		checkDisposed();

		if (triggersOn == null) {
			triggersOn = new ArrayList<IBindingEditpart<?>>();

			for (YBinding yEp : getModel().getTriggersOn()) {
				internalAddTriggersOn((IBindingEditpart<?>) getEditpart(viewContext, yEp));
			}
		}
	}

	/**
	 * Internal add triggers on.
	 *
	 * @param editpart
	 *            the editpart
	 */
	protected void internalAddTriggersOn(IBindingEditpart<?> editpart) {
		checkDisposed();

		if (triggersOn == null) {
			internalLoadDataUsed();
		}
		if (!triggersOn.contains(editpart)) {
			triggersOn.add(editpart);
			bindEditpart(loadProcessor(), editpart);
		}
	}

	/**
	 * Internal remove triggers on.
	 *
	 * @param editpart
	 *            the editpart
	 */
	protected void internalRemoveTriggersOn(IBindingEditpart<?> editpart) {
		checkDisposed();

		if (triggersOn != null && editpart != null) {
			editpart.dispose();
			triggersOn.remove(editpart);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	protected void handleModelAdd(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case VisibilityPackage.YVISIBILITY_PROCESSOR__DATA_USED:
			YBinding yElement = (YBinding) notification.getNewValue();

			IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yElement);
			internalAddDataUsed(editPart);
			break;
		case VisibilityPackage.YVISIBILITY_PROCESSOR__TRIGGERS_ON:
			yElement = (YBinding) notification.getNewValue();

			editPart = (IBindingEditpart<?>) getEditpart(viewContext, yElement);
			internalAddTriggersOn(editPart);
			break;
		default:
			break;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	protected void handleModelRemove(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case VisibilityPackage.YVISIBILITY_PROCESSOR__DATA_USED:
			YBinding yElement = (YBinding) notification.getNewValue();

			IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yElement);
			internalRemoveDataUsed(editPart);
			break;
		case VisibilityPackage.YVISIBILITY_PROCESSOR__TRIGGERS_ON:
			yElement = (YBinding) notification.getNewValue();

			editPart = (IBindingEditpart<?>) getEditpart(viewContext, yElement);
			internalRemoveTriggersOn(editPart);
			break;
		default:
			break;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#internalDispose()
	 */
	protected void internalDispose() {
		try {
			if (dataUsed != null) {
				for (IBindingEditpart<?> editpart : dataUsed) {
					editpart.dispose();
				}
				dataUsed = null;
			}

			if (triggersOn != null) {
				for (IBindingEditpart<?> editpart : triggersOn) {
					editpart.dispose();
				}
				triggersOn = null;
			}
		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessorEditpart#fire()
	 */
	@Override
	public void fire() {
		if (processor != null) {
			processor.fire();
		}
	}

}
