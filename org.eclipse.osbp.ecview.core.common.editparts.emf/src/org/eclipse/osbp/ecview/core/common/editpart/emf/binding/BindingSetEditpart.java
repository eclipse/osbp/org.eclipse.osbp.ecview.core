/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.binding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * Implementation of {@link IBindingSetEditpart}.
 */
public class BindingSetEditpart extends ElementEditpart<YBindingSet> implements
		IBindingSetEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BindingSetEditpart.class);

	/** The lock. */
	private Object lock;

	/** The active. */
	private boolean active;
	
	/** The bindings. */
	private List<IBindingEditpart<?>> bindings;
	
	/** The transient bindings. */
	private List<IBindingEditpart<?>> transientBindings;
	
	/** The binding manager. */
	private IECViewBindingManager bindingManager;

	/**
	 * A default constructor.
	 */
	public BindingSetEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#getView()
	 */
	@Override
	public IViewEditpart getView() {
		checkDisposed();

		YView yView = getModel().getView();
		return yView != null ? (IViewEditpart) ElementEditpart
				.findEditPartFor(yView) : null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#getBindingManager()
	 */
	@Override
	public IECViewBindingManager getBindingManager() {
		IViewEditpart view = getView();
		if (view == null) {
			if (bindingManager != null) {
				return bindingManager;
			}
			throw new IllegalArgumentException(
					"View and BindingManager must not be null for now!");
		} else {
			IECViewBindingManager bm = view.getContext().getService(
					IECViewBindingManager.class.getName());
			if (bm == null) {
				bm = this.bindingManager;
			}
			return bm;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#setBindingManager(org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager)
	 */
	@Override
	public void setBindingManager(IECViewBindingManager bindingManager) {
		this.bindingManager = bindingManager;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#isActive()
	 */
	@Override
	public boolean isActive() {
		checkDisposed();

		return active;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#activate()
	 */
	@Override
	public void activate() {
		checkDisposed();

		try {
			for (IBindingEditpart<?> binding : getBindings()) {
				binding.bind();
			}

			for (IBindingEditpart<?> binding : getTransientBindings()) {
				binding.bind();
			}
		} finally {
			active = true;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#addBinding(org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart)
	 */
	@Override
	public void addBinding(IBindingEditpart<?> binding) {
		try {
			checkDisposed();

			// add the element by using the model
			//
			YBindingSet yBindingSet = getModel();
			YBinding yBinding = (YBinding) binding.getModel();
			yBindingSet.addBinding(yBinding);

			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#removeBinding(org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart)
	 */
	@Override
	public void removeBinding(IBindingEditpart<?> binding) {
		try {
			checkDisposed();

			// remove the element by using the model
			//
			YBindingSet yBindingSet = getModel();
			YBinding yBinding = (YBinding) binding.getModel();
			yBindingSet.removeBinding(yBinding);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#addTransientBinding(org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart)
	 */
	@Override
	public void addTransientBinding(IBindingEditpart<?> binding) {
		try {
			checkDisposed();

			// add the element by using the model
			//
			YBindingSet yBindingSet = getModel();
			YBinding yBinding = (YBinding) binding.getModel();
			yBindingSet.getTransientBindings().add(yBinding);

			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#removeTransientBinding(org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart)
	 */
	@Override
	public void removeTransientBinding(IBindingEditpart<?> binding) {
		try {
			checkDisposed();

			// remove the element by using the model
			//
			YBindingSet yBindingSet = getModel();
			YBinding yBinding = (YBinding) binding.getModel();
			yBindingSet.getTransientBindings().remove(yBinding);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}

	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelAdd(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelAdd(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case BindingPackage.YBINDING_SET__BINDINGS:
			YBinding yBinding = (YBinding) notification.getNewValue();

			IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
			internalAddBinding(editPart);
			break;
		case BindingPackage.YBINDING_SET__TRANSIENT_BINDINGS:
			yBinding = (YBinding) notification.getNewValue();

			editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
			internalAddTransientBinding(editPart);
			break;
		default:
			break;
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of bindings.
	 * 
	 * @param binding
	 *            The editpart to be added
	 */
	protected void internalAddBinding(IBindingEditpart<?> binding) {
		checkDisposed();

		ensureBindingsLoaded();
		if (!bindings.contains(binding)) {
			bindings.add(binding);

			// activates the binding
			binding.bind();
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of transientBindings.
	 * 
	 * @param binding
	 *            The editpart to be added
	 */
	protected void internalAddTransientBinding(IBindingEditpart<?> binding) {
		checkDisposed();
		ensureBindingsLoaded();
		if (!transientBindings.contains(binding)) {
			transientBindings.add(binding);

			// activates the binding
			binding.bind();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelRemove(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelRemove(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case BindingPackage.YBINDING_SET__BINDINGS:
			YBinding yBinding = (YBinding) notification.getOldValue();

			IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
			internalRemoveBinding(editPart);
			break;
		case BindingPackage.YBINDING_SET__TRANSIENT_BINDINGS:
			yBinding = (YBinding) notification.getOldValue();

			editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
			internalRemoveTransientBinding(editPart);
			break;
		default:
			break;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelRemoveMany(int, org.eclipse.emf.common.notify.Notification)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void handleModelRemoveMany(int featureId,
			Notification notification) {
		switch (featureId) {
		case BindingPackage.YBINDING_SET__BINDINGS:
			List<YBinding> yBindings = (List<YBinding>) notification
					.getOldValue();
			for (YBinding yBinding : yBindings) {
				IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
				internalRemoveBinding(editPart);
			}
			break;
		case BindingPackage.YBINDING_SET__TRANSIENT_BINDINGS:
			yBindings = (List<YBinding>) notification.getOldValue();
			for (YBinding yBinding : yBindings) {
				IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
				internalRemoveTransientBinding(editPart);
			}
			break;
		default:
			super.handleModelRemoveMany(featureId, notification);
			break;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelAddMany(int, org.eclipse.emf.common.notify.Notification)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void handleModelAddMany(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case BindingPackage.YBINDING_SET__BINDINGS:
			List<YBinding> yBindings = (List<YBinding>) notification
					.getNewValue();
			for (YBinding yBinding : yBindings) {
				IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
				internalAddBinding(editPart);
			}
			break;
		case BindingPackage.YBINDING_SET__TRANSIENT_BINDINGS:
			yBindings = (List<YBinding>) notification.getNewValue();
			for (YBinding yBinding : yBindings) {
				IBindingEditpart<?> editPart = (IBindingEditpart<?>) getEditpart(viewContext, yBinding);
				internalAddTransientBinding(editPart);
			}
			break;
		default:
			super.handleModelAddMany(featureId, notification);
			break;
		}
	}

	/**
	 * Ensures that the internal bindings are loaded properly.
	 */
	private void ensureBindingsLoaded() {
		if (bindings == null) {
			internalLoadBindings();
		}
	}

	/**
	 * Is called to load and initialize all bindings.
	 */
	protected void internalLoadBindings() {
		checkDisposed();

		if (bindings == null) {
			bindings = new ArrayList<IBindingEditpart<?>>();
			for (YBinding yBinding : getModel().getBindings()) {
				IBindingEditpart<?> editPart = getEditpart(viewContext, yBinding);
				internalAddBinding(editPart);
			}
		}
	}

	/**
	 * Ensures that the internal transientBindings are loaded properly.
	 */
	private void ensureTransientBindingsLoaded() {
		if (transientBindings == null) {
			internalLoadTransientBindings();
		}
	}

	/**
	 * Is called to load and initialize all transientBindings.
	 */
	protected void internalLoadTransientBindings() {
		checkDisposed();

		if (transientBindings == null) {
			transientBindings = new ArrayList<IBindingEditpart<?>>();
			for (YBinding yBinding : getModel().getTransientBindings()) {
				IBindingEditpart<?> editPart = getEditpart(viewContext, yBinding);
				internalAddTransientBinding(editPart);
			}
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of bindings.
	 * 
	 * @param binding
	 *            The editpart to be removed
	 */
	protected void internalRemoveBinding(IBindingEditpart<?> binding) {
		checkDisposed();

		if (bindings != null && binding != null) {
			bindings.remove(binding);
		}

		// unbinds the binding
		if (binding != null) {
			binding.unbind();
			binding.dispose();
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of transientBindings.
	 * 
	 * @param binding
	 *            The editpart to be removed
	 */
	protected void internalRemoveTransientBinding(IBindingEditpart<?> binding) {
		checkDisposed();

		if (transientBindings != null && binding != null) {
			transientBindings.remove(binding);
		}

		// unbinds the binding
		if (binding != null) {
			binding.unbind();
			binding.dispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#getBindings()
	 */
	@Override
	public List<IBindingEditpart<?>> getBindings() {
		ensureBindingsLoaded();
		return new ArrayList<IBindingEditpart<?>>(bindings);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#getTransientBindings()
	 */
	@Override
	public List<IBindingEditpart<?>> getTransientBindings() {
		ensureTransientBindingsLoaded();
		return new ArrayList<IBindingEditpart<?>>(transientBindings);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			if (lock == null) {
				lock = new Object();
			}
			synchronized (lock) {
				// lazy loading: edit parts also have to be disposed if they
				// have
				// not been loaded yet, but exist in the model.
				if (bindings != null || !getModel().getBindings().isEmpty()) {
					List<IBindingEditpart<?>> tempElements = getBindings();
					for (IBindingEditpart<?> binding : tempElements
							.toArray(new IBindingEditpart<?>[tempElements
									.size()])) {
						binding.dispose();
					}
				}
				bindings = null;

				if (transientBindings != null
						|| !getModel().getTransientBindings().isEmpty()) {
					List<IBindingEditpart<?>> tempElements = getTransientBindings();
					for (IBindingEditpart<?> binding : tempElements
							.toArray(new IBindingEditpart<?>[tempElements
									.size()])) {
						binding.dispose();
					}
				}
				transientBindings = null;
			}
			lock = null;
		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart#findBindings(java.lang.Object)
	 */
	@Override
	public List<IBindingEditpart<?>> findBindings(Object elementModel) {
		if (!(elementModel instanceof YElement)) {
			return Collections.emptyList();
		}

		List<IBindingEditpart<?>> result = new ArrayList<IBindingEditpart<?>>();
		YBindingSet yBindingSet = getModel();
		for (YBinding yBinding : yBindingSet.getBindings()) {
			if (yBinding.isBindsElement((YElement) elementModel)) {
				result.add((IBindingEditpart<?>) DelegatingEditPartManager
						.getInstance().getEditpart(viewContext, yBinding));
			}
		}

		for (YBinding yBinding : yBindingSet.getTransientBindings()) {
			if (yBinding.isBindsElement((YElement) elementModel)) {
				result.add((IBindingEditpart<?>) DelegatingEditPartManager
						.getInstance().getEditpart(viewContext, yBinding));
			}
		}

		return result;
	}

}
