/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator.ClassDelegateValidator;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IClassDelegateValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidator;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;

public class ClassDelegateValidatorEditpart extends
		ValidatorEditpart<YClassDelegateValidator> implements
		IClassDelegateValidatorEditpart {

	private ValidationConfigToValidatorBridge bridgeObserver;

	public ClassDelegateValidatorEditpart() {
		super(
				ValidationPackage.Literals.YCLASS_DELEGATE_VALIDATION_CONFIG__CLASS_NAME);
	}

	@Override
	protected IValidator createValidator() {
		IViewContext context = getViewContext(getModel());

		YField yField = (YField) getModel().eContainer();

		return new ClassDelegateValidator(getModel(), context, yField.getId(),
				yField.getLabelI18nKey());
	}

	@Override
	public void setConfig(IValidationConfig config) {
		YClassDelegateValidator validatable = (YClassDelegateValidator) config
				.getValidationSettings();
		// create an observer that transfers the changes at the validatable to
		// the validator
		bridgeObserver = ValidationConfigToValidatorBridge
				.createObserver(
						validatable,
						ValidationPackage.Literals.YCLASS_DELEGATE_VALIDATION_CONFIG__CLASS_NAME,
						getModel(),
						ValidationPackage.Literals.YCLASS_DELEGATE_VALIDATION_CONFIG__CLASS_NAME);
	}

	@Override
	protected void internalDispose() {
		try {
			if (bridgeObserver != null) {
				bridgeObserver.dispose();
				bridgeObserver = null;
			}
		} finally {
			super.internalDispose();
		}
	}

}
