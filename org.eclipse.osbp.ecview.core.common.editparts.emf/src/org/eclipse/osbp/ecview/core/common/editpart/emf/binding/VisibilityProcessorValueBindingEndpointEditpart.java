/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.binding;

import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.beans.PojoObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IVisibilityProcessorValueBindingEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.visibility.IVisibilityProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Responsible to create an observable. The model of this editpart is used to
 * access the getObservableValue from the underlying widget presentation.
 */
public class VisibilityProcessorValueBindingEndpointEditpart extends
		BindableValueEndpointEditpart<YVisibilityProcessorValueBindingEndpoint>
		implements IVisibilityProcessorValueBindingEndpointEditpart {

	private static final Logger logger = LoggerFactory
			.getLogger(VisibilityProcessorValueBindingEndpointEditpart.class);
	private RefreshProvider refresh;
	private IVisibilityProcessor processor;

	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		switch (featureId) {
		case BindingPackage.YBEAN_VALUE_BINDING_ENDPOINT__BEAN:
			refresh.refresh();
			break;
		default:
			break;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <A extends IObservableValue> A getObservable() {

		if (processor == null) {
			logger.error("No instance of processor was set!");
		}

		String property = getModel().getProperty();
		if (property == null || property.equals("")) {
			logger.warn("Property {} not valid!", processor, property);
			return null;
		}

		IObservableValue observable = null;
		try {
			processor.getClass().getMethod("addPropertyChangeListener",
					java.beans.PropertyChangeListener.class);
			observable = BeansObservables.observeValue(processor, property);
		} catch (NoSuchMethodException e) {
		} catch (SecurityException e) {
		}
		if (observable == null) {
			observable = PojoObservables.observeValue(processor, property);
		}

		return (A) observable;
	}

	@Override
	public void setVisibilityProcessor(IVisibilityProcessor processor) {
		this.processor = processor;
	}

	@Override
	public void setRefreshProvider(RefreshProvider refresh) {
		this.refresh = refresh;
	}

	@Override
	protected void internalDispose() {
		refresh = null;

		super.internalDispose();
	}

}
