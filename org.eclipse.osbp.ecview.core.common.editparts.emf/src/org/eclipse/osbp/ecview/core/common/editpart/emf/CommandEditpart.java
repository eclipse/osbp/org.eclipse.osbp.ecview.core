/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import org.eclipse.osbp.ecview.core.common.editpart.ICommandEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YCommand;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YView;

// TODO: Auto-generated Javadoc
/**
 * See {@link ElementEditpart}.
 *
 * @param <M>
 *            the generic type
 */
public abstract class CommandEditpart<M extends YCommand> extends
		ElementEditpart<M> implements ICommandEditpart {

	/**
	 * The default constructor.
	 */
	protected CommandEditpart() {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IElementEditpart getParent() {
		YElement yParent = (YElement) getModel().eContainer();
		return yParent != null ? getEditpart(viewContext, yParent) : null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.ICommandEditpart#getView()
	 */
	@Override
	public IViewEditpart getView() {
		YView yView = getModel().getView();
		return yView != null ? (IViewEditpart) getEditpart(viewContext, yView) : null;
	}
}
