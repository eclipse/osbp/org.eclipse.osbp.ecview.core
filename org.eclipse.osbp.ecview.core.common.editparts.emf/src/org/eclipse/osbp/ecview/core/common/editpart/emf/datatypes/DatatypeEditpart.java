/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.datatypes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatatype;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The datatype editpart is responsible to observe the {@link YDatatype} model
 * element and to update all UI elements that are registered at this instance.
 *
 * @param <M>
 *            the generic type
 */
public abstract class DatatypeEditpart<M extends YDatatype> extends
		ElementEditpart<M> implements IDatatypeEditpart, IValidationConfig {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory
			.getLogger(DatatypeEditpart.class);

	/** The bridges. */
	private Set<DatatypeBridge> bridges = Collections
			.synchronizedSet(new HashSet<DatatypeBridge>());

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart#getCurrentState()
	 */
	@Override
	public DatatypeChangeEvent getCurrentState() {
		ValidatorDelta delta = internalGetAllValidators();
		DatatypeChangeEvent event = new DatatypeChangeEvent(this, null,
				delta.getToAdd(), null);
		return event;
	}

	/**
	 * Returns a validator delta that should be applied to the internal
	 * validators of the containing UI element.
	 *
	 * @param bridge
	 *            The currently handled bridge. If <code>null</code> is passed,
	 *            then all validators have to be returned.
	 * @param notification
	 *            Based on the notification the subclasses have to calculate the
	 *            validators to add. If <code>null</code> is passed, then all
	 *            validators have to be returned.
	 * @return the validator delta
	 */
	protected ValidatorDelta internalGetValidatorsDelta(DatatypeBridge bridge,
			Notification notification) {
		return ValidatorByConfigFactory.internalGetValidatorsDelta(this,
				bridge, notification);
	}

	/**
	 * Collects all validators.
	 *
	 * @return the validator delta
	 */
	protected ValidatorDelta internalGetAllValidators() {
		return ValidatorByConfigFactory.getAllValidators(this);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart#addBridge(org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart.DatatypeBridge)
	 */
	@Override
	public void addBridge(DatatypeBridge bridge) {
		bridges.add(bridge);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart#removeBridge(org.eclipse.osbp.ecview.core.common.editpart.datatypes.IDatatypeEditpart.DatatypeBridge)
	 */
	@Override
	public void removeBridge(DatatypeBridge bridge) {
		if (bridges == null) {
			return;
		}
		bridges.remove(bridge);
	}

	/**
	 * Returns the validator editparts for the given bridge.
	 *
	 * @param bridge
	 *            the bridge
	 * @return the validators for bridge
	 */
	protected List<IValidatorEditpart> getValidatorsForBridge(
			DatatypeBridge bridge) {
		List<IValidatorEditpart> result = bridge.getDatatypeValidators();
		return result != null ? result : Collections
				.<IValidatorEditpart> emptyList();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#notifyChanged(org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	public void notifyChanged(Notification notification) {
		super.notifyChanged(notification);

		if (notification.getEventType() != Notification.REMOVING_ADAPTER) {
			// forward the notification to the listeners elements
			for (DatatypeBridge bridge : bridges
					.toArray(new DatatypeBridge[bridges.size()])) {

				ValidatorDelta delta = internalGetValidatorsDelta(bridge,
						notification);
				DatatypeChangeEvent event = new DatatypeChangeEvent(this,
						notification, delta.getToAdd(), delta.getToRemove());
				bridge.notifyDatatypeChanged(event);
			}
		}
	}

	/**
	 * Default implementation for all subclasses that implement
	 * {@link IValidationConfig}.
	 *
	 * @return the validation settings
	 */
	public Object getValidationSettings() {
		return getModel();
	}

	/**
	 * Returns an unmodifiable collection of bridges.
	 *
	 * @return the bridges
	 */
	protected Set<DatatypeBridge> getBridges() {
		checkDisposed();

		return Collections.unmodifiableSet(bridges);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {

			if (bridges.size() > 0) {
				logger.error("Attention: The DatatypeEditpart is disposed! "
						+ "All connections from consuming elements like UI fields are interrupted. "
						+ "And they can not be installed anymore without resetting the datatype feature again.");
			}

			// remove all validators and all settings from the observing
			// listeners
			for (DatatypeBridge bridge : bridges
					.toArray(new DatatypeBridge[bridges.size()])) {
				DatatypeChangeEvent event = new DatatypeChangeEvent(true, this, null,
						null, new ArrayList<>(bridge.getDatatypeValidators()));
				bridge.notifyDatatypeChanged(event);
			}

			bridges = null;
		} finally {
			super.internalDispose();
		}
	}

	/**
	 * A container for validators to be added and removed.
	 */
	public static class ValidatorDelta {

		/** The to add. */
		private final List<IValidatorEditpart> toAdd;
		
		/** The to remove. */
		private final List<IValidatorEditpart> toRemove;

		/**
		 * Instantiates a new validator delta.
		 *
		 * @param toAdd
		 *            the to add
		 * @param toRemove
		 *            the to remove
		 */
		public ValidatorDelta(List<IValidatorEditpart> toAdd,
				List<IValidatorEditpart> toRemove) {
			super();
			this.toAdd = toAdd;
			this.toRemove = toRemove;
		}

		/**
		 * Gets the to add.
		 *
		 * @return the to add
		 */
		public List<IValidatorEditpart> getToAdd() {
			return toAdd;
		}

		/**
		 * Gets the to remove.
		 *
		 * @return the to remove
		 */
		public List<IValidatorEditpart> getToRemove() {
			return toRemove;
		}

	}

}
