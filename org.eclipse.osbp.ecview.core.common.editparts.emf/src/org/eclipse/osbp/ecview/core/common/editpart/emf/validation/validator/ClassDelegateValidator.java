/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator;

import java.util.Locale;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidationConfig;
import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidator;
import org.eclipse.osbp.ecview.core.common.validation.AbstractCollectingValidator;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.common.types.ITypeProviderService;
import org.eclipse.osbp.runtime.common.validation.IStatus;
import org.eclipse.osbp.runtime.common.validation.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassDelegateValidator extends AbstractCollectingValidator
		implements IValidator {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ClassDelegateValidator.class);

	private ITypeProviderService service;
	private IValidator delegate;

	private Locale locale;
	private II18nService i18nService;

	private String fieldId;
	private String fieldI18nKey;

	public ClassDelegateValidator(YClassDelegateValidator yValidator,
			IViewContext context, String fieldId, String fieldI18nKey) {
		service = context.getService(ITypeProviderService.class.getName());
		i18nService = context.getService(II18nService.class.getName());
		locale = context.getLocale();
		this.fieldId = fieldId;
		this.fieldI18nKey = fieldI18nKey;

		updateParameter(yValidator);
	}

	@Override
	public Class<?> getType() {
		return delegate.getType();
	}

	@Override
	public IStatus validateValue(Object value) {
		if (delegate == null) {
			return Status.createStatus("", ClassDelegateValidator.class,
					IStatus.Severity.ERROR,
					"Error occured: Delegate class was null.");
		}
		IStatus result = delegate.validateValue(value);

		result.putProperty(IStatus.PROP_FIELD_ID, fieldId);
		result.putProperty(IStatus.PROP_FIELD_I18N_KEY, fieldI18nKey);

		resetCurrentStatus();
		addCurrentStatus(result);
		return result;
	}

	@Override
	public void updateParameter(Object model) {
		YClassDelegateValidationConfig yValidator = (YClassDelegateValidationConfig) model;
		Class<?> delegateClass = service.forName(
				ValidationPackage.Literals.YCLASS_DELEGATE_VALIDATOR,
				yValidator.getClassName());
		try {
			if (delegateClass != null) {
				delegate = (IValidator) delegateClass.newInstance();
				delegate.setI18nService(i18nService);
				delegate.setLocale(locale);
			} else {
				LOGGER.error("The class {} could not be loaded!",
						yValidator.getClassName());
			}

		} catch (InstantiationException e) {
			LOGGER.error(e.toString());
		} catch (IllegalAccessException e) {
			LOGGER.error(e.toString());
		}
	}

	@Override
	protected void internalDispose() {

	}

	@Override
	public void setLocale(Locale locale) {
		this.locale = locale;
		if (delegate != null) {
			delegate.setLocale(locale);
		}
	}

	@Override
	public void setI18nService(II18nService i18nService) {
		this.i18nService = i18nService;
		if (delegate != null) {
			delegate.setI18nService(i18nService);
		}
	}

	@Override
	public boolean isCheckValidType() {
		return delegate.isCheckValidType();
	}
}
