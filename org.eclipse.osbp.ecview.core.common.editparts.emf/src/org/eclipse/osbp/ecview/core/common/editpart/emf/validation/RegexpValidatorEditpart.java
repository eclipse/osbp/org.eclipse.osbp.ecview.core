/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator.RegexValidator;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IRegexpValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidationConfig;
import org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidator;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.runtime.common.i18n.II18nService;

// TODO: Auto-generated Javadoc
/**
 * The Class RegexpValidatorEditpart.
 */
public class RegexpValidatorEditpart extends
		ValidatorEditpart<YRegexpValidator> implements IRegexpValidatorEditpart {

	/** The bridge observer. */
	private ValidationConfigToValidatorBridge bridgeObserver;

	/**
	 * Checks if is valid reg expression.
	 *
	 * @param regexp
	 *            the regexp
	 * @return true, if is valid reg expression
	 */
	public static boolean isValidRegExpression(String regexp) {
		return regexp != null && !regexp.equals("");
	}

	/**
	 * Instantiates a new regexp validator editpart.
	 */
	public RegexpValidatorEditpart() {
		super(
				ValidationPackage.Literals.YREGEXP_VALIDATION_CONFIG__REG_EXPRESSION);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.validation.ValidatorEditpart#createValidator()
	 */
	@Override
	protected IValidator createValidator() {
		RegexValidator validator = new RegexValidator(getModel());
		IViewContext context = getViewContext(getModel());
		validator.setI18nService((II18nService) context
				.getService(II18nService.class.getName()));
		validator.setLocale(context.getLocale());
		return validator;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart#setConfig(org.eclipse.osbp.ecview.core.common.validation.IValidationConfig)
	 */
	@Override
	public void setConfig(IValidationConfig config) {
		YRegexpValidationConfig validatable = (YRegexpValidationConfig) config
				.getValidationSettings();
		// create an observer that transfers the changes at the validatable to
		// the validator
		bridgeObserver = ValidationConfigToValidatorBridge
				.createObserver(
						validatable,
						ValidationPackage.Literals.YREGEXP_VALIDATION_CONFIG__REG_EXPRESSION,
						getModel(),
						ValidationPackage.Literals.YREGEXP_VALIDATION_CONFIG__REG_EXPRESSION);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.validation.ValidatorEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			if (bridgeObserver != null) {
				bridgeObserver.dispose();
				bridgeObserver = null;
			}
		} finally {
			super.internalDispose();
		}
	}

}
