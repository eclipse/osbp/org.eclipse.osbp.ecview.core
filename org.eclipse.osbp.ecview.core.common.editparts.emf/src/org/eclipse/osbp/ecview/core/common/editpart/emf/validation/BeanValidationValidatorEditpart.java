/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.validation.ValidatorFactory;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.osbp.ecview.core.common.context.ILocaleChangedService;
import org.eclipse.osbp.ecview.core.common.context.ILocaleChangedService.LocaleListener;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator.BeanValidationValidator;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IBeanValidationValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YBeanValidationValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YBeanValidationValidatorConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.common.types.ITypeProviderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BeanValidationValidatorEditpart extends ValidatorEditpart<YBeanValidationValidator>
		implements IBeanValidationValidatorEditpart, LocaleListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(BeanValidationValidatorEditpart.class);

	private ValidationConfigToValidatorBridge bridgeObserver;

	private BeanValidationValidator validator;

	public BeanValidationValidatorEditpart() {
		super(ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_CLASS,
				ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_CLASS_FULLY_QUALIFIED_NAME,
				ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_PROPERTY);
	}

	@Override
	protected IValidator createValidator() {
		YBeanValidationValidator yModel = getModel();

		ValidatorFactory jsr303ValidatorFactory = (ValidatorFactory) getViewContext(yModel)
				.getService(ValidatorFactory.class.getName());
		if (jsr303ValidatorFactory == null) {
			LOGGER.error("JSR303ValidatorFactory not available.");
			return null;
		}

		Class<?> beanClass = yModel.getBvalClass();
		if (beanClass == null) {
			ITypeProviderService service = getViewContext(getModel()).getService(ITypeProviderService.class.getName());
			beanClass = service.forName(null, getModel().getBvalClassFullyQualifiedName());
		}

		IViewContext context = getViewContext(getModel());
		YField yField = (YField) yModel.eContainer();
		validator = new BeanValidationValidator(beanClass, yModel.getBvalProperty(), jsr303ValidatorFactory,
				yField.getId(), yField.getLabelI18nKey(), context.getLocale());
		validator.setI18nService((II18nService) context.getService(II18nService.class.getName()));

		ILocaleChangedService localeChangedService = context.getService(ILocaleChangedService.ID);
		localeChangedService.addLocaleListener(this);

		return validator;
	}

	@Override
	public void setConfig(IValidationConfig config) {
		YBeanValidationValidatorConfig validatable = (YBeanValidationValidatorConfig) config.getValidationSettings();
		// create an observer that transfers the changes at the validatable to
		// the validator
		Map<EStructuralFeature, EStructuralFeature> mapping = new HashMap<EStructuralFeature, EStructuralFeature>();
		mapping.put(ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_CLASS,
				ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_CLASS);
		mapping.put(ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_CLASS_FULLY_QUALIFIED_NAME,
				ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_CLASS_FULLY_QUALIFIED_NAME);
		mapping.put(ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_PROPERTY,
				ValidationPackage.Literals.YBEAN_VALIDATION_VALIDATOR_CONFIG__BVAL_PROPERTY);

		bridgeObserver = new ValidationConfigToValidatorBridge(validatable, getModel(), mapping);
	}

	@Override
	protected void internalDispose() {
		try {
			ILocaleChangedService localeChangedService = getContext().getService(ILocaleChangedService.ID);
			localeChangedService.removeLocaleListener(this);

			if (bridgeObserver != null) {
				bridgeObserver.dispose();
				bridgeObserver = null;
			}
		} finally {
			super.internalDispose();
		}
	}

	@Override
	public void localeChanged(Locale locale) {
		validator.setLocale(locale);
	}

}
