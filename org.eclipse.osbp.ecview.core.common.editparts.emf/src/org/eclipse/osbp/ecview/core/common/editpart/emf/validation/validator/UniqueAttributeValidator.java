/*
 * Copyright 2000-2014 Vaadin Ltd.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * Contribution:
 * Florian Pirchner - OSGi support
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator;

import java.beans.PropertyDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collections;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableValueEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.runtime.common.annotations.DtoUtils;
import org.eclipse.osbp.runtime.common.dispose.AbstractDisposable;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.common.filter.ILFilter;
import org.eclipse.osbp.runtime.common.filter.ILFilterService;
import org.eclipse.osbp.runtime.common.i18n.II18nService;
import org.eclipse.osbp.runtime.common.validation.IStatus;
import org.eclipse.osbp.runtime.common.validation.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UniqueAttributeValidator extends AbstractDisposable implements IValidator {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UniqueAttributeValidator.class);

	/** The bean class. */
	private final Class<?> dtoClass;

	/** The property name. */
	private final String propertyName;

	private final String idPropertyName;

	/** The property class. */
	private final Class<?> propertyClass;

	/** The locale. */
	private Locale locale;

	/** The i18n service. */
	@SuppressWarnings("unused")
	private II18nService i18nService;

	private final IDTOService<?> dtoService;

	/** The field id. */
	private final String fieldId;

	/** The field i18n key. */
	private final String fieldI18nKey;

	private final ILFilterService filterService;

	private final IBindableValueEndpointEditpart boundBeanEP;

	private IStatus currentStatus;

	public UniqueAttributeValidator(Class<?> dtoClass, String propertyName, IBindableValueEndpointEditpart boundBeanEP,		// NOSONAR
			String idPropertyName, IDTOService<?> dtoService, ILFilterService filterService, String fieldId,
			String fieldI18nKey) {
		this.dtoClass = dtoClass;
		this.dtoService = dtoService;
		this.filterService = filterService;
		this.propertyClass = getPropertyType(dtoClass, propertyName);
		this.propertyName = propertyName;
		this.boundBeanEP = boundBeanEP;
		this.idPropertyName = idPropertyName;
		this.locale = Locale.getDefault();
		this.fieldId = fieldId;
		this.fieldI18nKey = fieldI18nKey;
	}

	/**
	 * Gets the property type.
	 *
	 * @param beanClass
	 *            the bean class
	 * @param propertyName
	 *            the property name
	 * @return the property type
	 */
	protected Class<?> getPropertyType(Class<?> beanClass, String propertyName) {
		for (PropertyDescriptor desc : PropertyUtils.getPropertyDescriptors(beanClass)) {
			if (desc.getName().equals(propertyName)) {
				return desc.getPropertyType();
			}
		}
		throw new IllegalStateException("Property " + propertyName + " not available in class " + beanClass.getName());
	}

	@Override
	public Class<?> getType() {
		return propertyClass;
	}

	@Override
	public boolean isCheckValidType() {
		return false;
	}

	@Override
	public IStatus validateValue(Object value) {

		Object bean = boundBeanEP.getObservable().getValue();
		if (value == null || bean == null) {
			return IStatus.OK;

		}
		
		try {
			if(!DtoUtils.isDirty(bean)) {
				return IStatus.OK;
			}
		} catch (IllegalAccessException e1) {
			// nothing to do
		}

		Object rowId = DtoUtils.getIdValue(bean);
		if(rowId == null) {
			// bean has no id
			return IStatus.OK;
		}
		
		currentStatus = null;

		try {
			ILFilter filter = filterService.createUniqueEntryFilter(value, propertyName, rowId, idPropertyName);
			if (dtoService.contains(filterService.createQuery(filter))) {
				currentStatus = Status.createStatus("UniqueAttributeValidator", UniqueAttributeValidator.class,
						IStatus.Severity.ERROR, getMessage());
				currentStatus.putProperty(IStatus.PROP_FIELD_ID, fieldId);
				currentStatus.putProperty(IStatus.PROP_FIELD_I18N_KEY, fieldI18nKey);
			} else {
				return IStatus.OK;
			}
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			LOGGER.error("{}", sw.toString());
		}
		return currentStatus != null ? currentStatus : IStatus.OK;
	}

	protected String getMessage() {
		return i18nService.getValue("unique_attribute_validator_error", locale);
	}

	@Override
	public Set<IStatus> getCurrentStatus() {
		return currentStatus != null ? Collections.singleton(currentStatus) : Collections.<IStatus>emptySet();
	}

	/**
	 * Sets the locale used for validation error messages.
	 * 
	 * Revalidation is not automatically triggered by setting the locale.
	 *
	 * @param locale
	 *            the new locale
	 */
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	/**
	 * Gets the locale used for validation error messages.
	 * 
	 * @return locale used for validation
	 */
	public Locale getLocale() {
		return locale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.validation.IValidator#updateParameter
	 * (java.lang.Object)
	 */
	@Override
	public void updateParameter(Object model) {
		// not supported yet
		LOGGER.error("Update parameter for BeanValidationValidator not allowed yet!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.validation.
	 * AbstractCollectingValidator#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		// nothing to dispose
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.validation.IValidator#setI18nService(
	 * org.eclipse.osbp.runtime.common.i18n.II18nService)
	 */
	@Override
	public void setI18nService(II18nService i18nService) {
		this.i18nService = i18nService;
	}

}
