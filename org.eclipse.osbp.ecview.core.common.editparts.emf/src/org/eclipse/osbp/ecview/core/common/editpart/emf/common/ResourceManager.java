/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.common;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.osbp.ecview.core.common.editpart.emf.internal.OrphanViewsResource;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

/**
 * The implementation of {@link IResourceManager}.
 */
public class ResourceManager implements IResourceManager {

	private static IResourceManager instance = new ResourceManager();

	private ResourceSet resourceSet;

	/**
	 * Returns the instance of that manager.
	 * 
	 * @return the instance
	 */
	public static IResourceManager getInstance() {
		return instance;
	}

	@Override
	public Resource getResource(String uri) {
		return getResource(URI.createURI(uri));
	}

	@Override
	public Resource getResource(URI uri) {
		return getResourceSet().getResource(uri, true);
	}

	@Override
	public ResourceSet getResourceSet() {
		if (resourceSet == null) {
			resourceSet = new ResourceSetImpl();
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
			resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(ORPHAN_VIEWS_EXTENSION, new OrphanViewsResource.OrphanViewsResourceFactory());
			resourceSet.getPackageRegistry().put(CoreModelPackage.eNS_URI, CoreModelPackage.eINSTANCE);
		}
		return resourceSet;
	}

}
