/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.binding.BeanValueBindingEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.binding.BindingSetEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.binding.EnumListBindingEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.binding.ListBindingEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.binding.ValueBindingEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.binding.VisibilityProcessorValueBindingEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.common.AbstractEditpartManager;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.BeanValidationValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.ClassDelegateValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.MaxLengthValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.MinLengthValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.RegexpValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.UniqueAttributeValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator.UniqueAttributeValidator;
import org.eclipse.osbp.ecview.core.common.editpart.emf.visibility.VisibilityProcessorEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.binding.YBeanValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet;
import org.eclipse.osbp.ecview.core.common.model.binding.YDetailValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YEnumListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YListBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YVisibilityProcessorValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YAction;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotListBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YCommandSet;
import org.eclipse.osbp.ecview.core.common.model.core.YContextValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YDialog;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableCollectionEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableMultiSelectionEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableSelectionEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableValueEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YExposedAction;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YKeyStrokeDefinition;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand;
import org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YBeanValidationValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YClassDelegateValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YMaxLengthValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YRegexpValidator;
import org.eclipse.osbp.ecview.core.common.model.validation.YUniqueAttributeValidator;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityPackage;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;

/**
 * An implementation of IEditPartManager for eObjects with
 * nsURI=http://eclipse.org/emf/emfclient/uimodel.
 */
public class EditpartManager extends AbstractEditpartManager {

	@Override
	public boolean isFor(Object element) {
		if (element instanceof EObject) {
			String uriString = ((EObject) element).eClass().getEPackage()
					.getNsURI();
			return uriString.equals(CoreModelPackage.eNS_URI)
					|| uriString.equals(BindingPackage.eNS_URI)
					|| uriString.equals(ValidationPackage.eNS_URI)
					|| uriString.equals(VisibilityPackage.eNS_URI);
		} else if (element instanceof String) {
			return element.equals(CoreModelPackage.eNS_URI)
					|| element.equals(BindingPackage.eNS_URI)
					|| element.equals(ValidationPackage.eNS_URI)
					|| element.equals(VisibilityPackage.eNS_URI);
		}
		return false;
	}

//	@SuppressWarnings("unchecked")
//	@Override
//	public <A extends IElementEditpart> A createEditpart(IViewContext context, Object selector,
//			Class<A> editPartClazz) {
//		ElementEditpart<YElement> result = null;
//		if (editPartClazz.isAssignableFrom(IViewEditpart.class)) {
//			result = createNewInstance(ViewEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(ILayoutEditpart.class)) {
//			result = createNewInstance(LayoutEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IFieldEditpart.class)) {
//			result = createNewInstance(FieldEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IActionEditpart.class)) {
//			result = createNewInstance(ActionEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IBindingSetEditpart.class)) {
//			result = createNewInstance(BindingSetEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IDetailValueBindingEndpointEditpart.class)) {
//			result = createNewInstance(DetailValueBindingEndpointEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IValueBindingEditpart.class)) {
//			result = createNewInstance(ValueBindingEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IListBindingEditpart.class)) {
//			result = createNewInstance(ListBindingEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IContextValueBindingEndpointEditpart.class)) {
//			result = createNewInstance(ContextValueBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IBeanSlotValueBindingEndpointEditpart.class)) {
//			result = createNewInstance(BeanSlotValueBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IBeanSlotListBindingEndpointEditpart.class)) {
//			result = createNewInstance(BeanSlotListBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IEmbeddableValueBindingEndpointEditpart.class)) {
//			result = createNewInstance(EmbeddableValueBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IEmbeddableSelectionEndpointEditpart.class)) {
//			result = createNewInstance(EmbeddableSelectionBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IEmbeddableMultiSelectionEndpointEditpart.class)) {
//			result = createNewInstance(EmbeddableMultiSelectionBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IEmbeddableCollectionEndpointEditpart.class)) {
//			result = createNewInstance(EmbeddableCollectionBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IBeanValueBindingEndpointEditpart.class)) {
//			result = createNewInstance(BeanValueBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IECViewModelValueBindingEndpointEditpart.class)) {
//			result = createNewInstance(ECViewModelValueBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IECViewModelListBindingEndpointEditpart.class)) {
//			result = createNewInstance(ECViewModelListBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IEnumListBindingEndpointEditpart.class)) {
//			result = createNewInstance(EnumListBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IMinLengthValidatorEditpart.class)) {
//			result = createNewInstance(MinLengthValidatorEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IMaxLengthValidatorEditpart.class)) {
//			result = createNewInstance(MaxLengthValidatorEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IRegexpValidatorEditpart.class)) {
//			result = createNewInstance(RegexpValidatorEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IClassDelegateValidatorEditpart.class)) {
//			result = createNewInstance(ClassDelegateValidatorEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IClassDelegateValidatorEditpart.class)) {
//			result = createNewInstance(ClassDelegateValidatorEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IBeanValidationValidatorEditpart.class)) {
//			result = createNewInstance(BeanValidationValidatorEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IVisibilityProcessorEditpart.class)) {
//			result = createNewInstance(VisibilityProcessorEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IVisibilityPropertiesEditpart.class)) {
//			result = createNewInstance(VisibilityPropertiesEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(ICommandSetEditpart.class)) {
//			result = createNewInstance(CommandSetEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IDialogEditpart.class)) {
//			result = createNewInstance(DialogEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IOpenDialogCommandEditpart.class)) {
//			result = createNewInstance(OpenDialogCommandEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IVisibilityProcessorValueBindingEndpointEditpart.class)) {
//			result = createNewInstance(VisibilityProcessorValueBindingEndpointEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IVisibilityProcessorEditpart.class)) {
//			result = createNewInstance(VisibilityProcessorEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IExposedActionEditpart.class)) {
//			result = createNewInstance(ExposedActionEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(ISendEventCommandEditpart.class)) {
//			result = createNewInstance(SendEventCommandEditpart.class);
//		} else if (editPartClazz
//				.isAssignableFrom(IKeyStrokeDefinitionEditpart.class)) {
//			result = createNewInstance(KeyStrokeDefinitionEditpart.class);
//		} else if (editPartClazz.isAssignableFrom(IConverterEditpart.class)) {
//			result = createNewInstance(ConverterEditpart.class);
//		}
//
//		if (result != null) {
//			result.initialize();
//		}
//
//		return (A) result;
//	}

	/**
	 * Creates a new instance of the edit part.
	 * 
	 * @param <A>
	 *            an instance of {@link IElementEditpart}
	 * @param yElement
	 *            the model element
	 * @return editpart
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IElementEditpart> A createEditpart(IViewContext context, Object yElement) {
		// asserts that no editpart was created already for the given element
		assertOneEditpartForModelelement(yElement);

		ElementEditpart<YElement> result = null;
		if (yElement instanceof YView) {
			result = createNewInstance(ViewEditpart.class);
		} else if (yElement instanceof YLayout) {
			result = createNewInstance(LayoutEditpart.class);
		} else if (yElement instanceof YField) {
			result = createNewInstance(FieldEditpart.class);
		} else if (yElement instanceof YAction) {
			result = createNewInstance(ActionEditpart.class);
		} else if (yElement instanceof YBindingSet) {
			result = createNewInstance(BindingSetEditpart.class);
		} else if (yElement instanceof YValueBinding) {
			result = createNewInstance(ValueBindingEditpart.class);
		} else if (yElement instanceof YListBinding) {
			result = createNewInstance(ListBindingEditpart.class);
		} else if (yElement instanceof YContextValueBindingEndpoint) {
			result = createNewInstance(ContextValueBindingEndpointEditpart.class);
		} else if (yElement instanceof YBeanSlotValueBindingEndpoint) {
			result = createNewInstance(BeanSlotValueBindingEndpointEditpart.class);
		} else if (yElement instanceof YBeanSlotListBindingEndpoint) {
			result = createNewInstance(BeanSlotListBindingEndpointEditpart.class);
		} else if (yElement instanceof YDetailValueBindingEndpoint) {
			result = createNewInstance(DetailValueBindingEndpointEditpart.class);
		} else if (yElement instanceof YEmbeddableValueEndpoint) {
			result = createNewInstance(EmbeddableValueBindingEndpointEditpart.class);
		} else if (yElement instanceof YEmbeddableSelectionEndpoint) {
			result = createNewInstance(EmbeddableSelectionBindingEndpointEditpart.class);
		} else if (yElement instanceof YEmbeddableMultiSelectionEndpoint) {
			result = createNewInstance(EmbeddableMultiSelectionBindingEndpointEditpart.class);
		} else if (yElement instanceof YEmbeddableCollectionEndpoint) {
			result = createNewInstance(EmbeddableCollectionBindingEndpointEditpart.class);
		} else if (yElement instanceof YBeanValueBindingEndpoint) {
			result = createNewInstance(BeanValueBindingEndpointEditpart.class);
		} else if (yElement instanceof YEnumListBindingEndpoint) {
			result = createNewInstance(EnumListBindingEndpointEditpart.class);
		} else if (yElement instanceof YECViewModelValueBindingEndpoint) {
			result = createNewInstance(ECViewModelValueBindingEndpointEditpart.class);
		} else if (yElement instanceof YECViewModelListBindingEndpoint) {
			result = createNewInstance(ECViewModelListBindingEndpointEditpart.class);
		} else if (yElement instanceof YMinLengthValidator) {
			result = createNewInstance(MinLengthValidatorEditpart.class);
		} else if (yElement instanceof YMaxLengthValidator) {
			result = createNewInstance(MaxLengthValidatorEditpart.class);
		} else if (yElement instanceof YRegexpValidator) {
			result = createNewInstance(RegexpValidatorEditpart.class);
		} else if (yElement instanceof YClassDelegateValidator) {
			result = createNewInstance(ClassDelegateValidatorEditpart.class);
		} else if (yElement instanceof YBeanValidationValidator) {
			result = createNewInstance(BeanValidationValidatorEditpart.class);
		} else if (yElement instanceof YCommandSet) {
			result = createNewInstance(CommandSetEditpart.class);
		} else if (yElement instanceof YDialog) {
			result = createNewInstance(DialogEditpart.class);
		} else if (yElement instanceof YOpenDialogCommand) {
			result = createNewInstance(OpenDialogCommandEditpart.class);
		} else if (yElement instanceof YVisibilityProcessorValueBindingEndpoint) {
			result = createNewInstance(VisibilityProcessorValueBindingEndpointEditpart.class);
		} else if (yElement instanceof YVisibilityProcessor) {
			result = createNewInstance(VisibilityProcessorEditpart.class);
		} else if (yElement instanceof YExposedAction) {
			result = createNewInstance(ExposedActionEditpart.class);
		} else if (yElement instanceof YSendEventCommand) {
			result = createNewInstance(SendEventCommandEditpart.class);
		} else if (yElement instanceof YKeyStrokeDefinition) {
			result = createNewInstance(KeyStrokeDefinitionEditpart.class);
		} else if (yElement instanceof YConverter) {
			result = createNewInstance(ConverterEditpart.class);
		} else if (yElement instanceof YUniqueAttributeValidator) {
			result = createNewInstance(UniqueAttributeValidatorEditpart.class);
		}

		if (result != null) {
			result.initialize(context, (YElement) yElement);
		}

		return (A) result;
	}

	/**
	 * Returns a new instance of the type.
	 * 
	 * @param type
	 *            The type of the edit part to be created
	 * @return editpart
	 * @throws InstantiationException
	 *             e
	 * @throws IllegalAccessException
	 *             e
	 */
	protected IElementEditpart newInstance(
			Class<? extends IElementEditpart> type)
			throws InstantiationException, IllegalAccessException {
		return type.newInstance();
	}

}
