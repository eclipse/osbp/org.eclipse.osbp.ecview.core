/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import java.net.URI;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.osbp.ecview.core.common.binding.observables.ContextObservables;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.IContextValueBindingEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.binding.BindableValueEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YContextValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Responsible to create a context observables.
 */
public class ContextValueBindingEndpointEditpart extends
		BindableValueEndpointEditpart<YContextValueBindingEndpoint> implements
		IContextValueBindingEndpointEditpart {
	private static final Logger logger = LoggerFactory
			.getLogger(ContextValueBindingEndpointEditpart.class);

	@SuppressWarnings("unchecked")
	@Override
	public <A extends IObservableValue> A getObservable() {
		YView yView = null;
		try {
			yView = getModel().getBinding().getBindingSet().getView();
		} catch (NullPointerException e) {
			logger.error("{}", e);
			throw new RuntimeException("View must not be null!", e);
		}

		if (getModel().getUrlString() == null
				|| getModel().getUrlString().equals("")) {
			logger.error("URLString must not be null!");
			return null;
		}

		IViewEditpart viewEditpart = DelegatingEditPartManager.getInstance()
				.getEditpart(viewContext, yView);
		return (A) ContextObservables.observeValue(viewEditpart.getContext(),
				URI.create(getModel().getUrlString()));
	}

	@Override
	public void setRefreshProvider(RefreshProvider refresh) {
		// nothing to do
	}

}
