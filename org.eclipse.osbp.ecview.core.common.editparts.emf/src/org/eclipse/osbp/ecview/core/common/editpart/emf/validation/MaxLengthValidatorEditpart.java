/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator.MaxLengthValidator;
import org.eclipse.osbp.ecview.core.common.editpart.validation.IMaxLengthValidatorEditpart;
import org.eclipse.osbp.ecview.core.common.model.validation.ValidationPackage;
import org.eclipse.osbp.ecview.core.common.model.validation.YMaxLengthValidationConfig;
import org.eclipse.osbp.ecview.core.common.model.validation.YMaxLengthValidator;
import org.eclipse.osbp.ecview.core.common.validation.IValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.IValidator;
import org.eclipse.osbp.runtime.common.i18n.II18nService;

// TODO: Auto-generated Javadoc
/**
 * The Class MaxLengthValidatorEditpart.
 */
public class MaxLengthValidatorEditpart extends
		ValidatorEditpart<YMaxLengthValidator> implements
		IMaxLengthValidatorEditpart {

	/** The bridge observer. */
	private ValidationConfigToValidatorBridge bridgeObserver;

	/**
	 * Instantiates a new max length validator editpart.
	 */
	public MaxLengthValidatorEditpart() {
		super(
				ValidationPackage.Literals.YMAX_LENGTH_VALIDATION_CONFIG__MAX_LENGTH);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.validation.ValidatorEditpart#createValidator()
	 */
	@Override
	protected IValidator createValidator() {
		MaxLengthValidator validator = new MaxLengthValidator(getModel());
		IViewContext context = getViewContext(getModel());
		validator.setI18nService((II18nService) context
				.getService(II18nService.class.getName()));
		validator.setLocale(context.getLocale());
		return validator;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.validation.IValidatorEditpart#setConfig(org.eclipse.osbp.ecview.core.common.validation.IValidationConfig)
	 */
	@Override
	public void setConfig(IValidationConfig config) {
		YMaxLengthValidationConfig validatable = (YMaxLengthValidationConfig) config
				.getValidationSettings();
		// create an observer that transfers the changes at the validatable to
		// the validator
		bridgeObserver = ValidationConfigToValidatorBridge
				.createObserver(
						validatable,
						ValidationPackage.Literals.YMAX_LENGTH_VALIDATION_CONFIG__MAX_LENGTH,
						getModel(),
						ValidationPackage.Literals.YMAX_LENGTH_VALIDATION_CONFIG__MAX_LENGTH);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.validation.ValidatorEditpart#internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			if (bridgeObserver != null) {
				bridgeObserver.dispose();
				bridgeObserver = null;
			}
		} finally {
			super.internalDispose();
		}
	}

}
