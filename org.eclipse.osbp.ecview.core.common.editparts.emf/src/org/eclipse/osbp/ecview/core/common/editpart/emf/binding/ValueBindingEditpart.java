/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.binding;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager;
import org.eclipse.osbp.ecview.core.common.editpart.DelegatingEditPartManager;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableValueEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IValueBindingEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.databinding.emf.common.ECViewUpdateValueStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * Implementation of {@link IBindingSetEditpart}.
 */
public class ValueBindingEditpart extends ElementEditpart<YValueBinding>
		implements IValueBindingEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ValueBindingEditpart.class);

	/** The target value. */
	private IBindableValueEndpointEditpart targetValue;

	/** The model value. */
	private IBindableValueEndpointEditpart modelValue;

	/** The bound. */
	private boolean bound;

	/** The binding. */
	private Binding binding;

	/**
	 * A default constructor.
	 */
	public ValueBindingEditpart() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * handleModelSet(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		boolean shouldRebind = false;
		switch (featureId) {
		case BindingPackage.YVALUE_BINDING__TARGET_ENDPOINT:
			YBindingEndpoint YBindingEndpoint = (YBindingEndpoint) notification
					.getNewValue();

			IBindableValueEndpointEditpart editPart = (IBindableValueEndpointEditpart) getEditpart(
					viewContext, YBindingEndpoint);
			internalSetTargetValue(editPart);

			shouldRebind = true;
			break;
		case BindingPackage.YVALUE_BINDING__MODEL_ENDPOINT:
			YBindingEndpoint = (YBindingEndpoint) notification.getNewValue();

			editPart = (IBindableValueEndpointEditpart) getEditpart(
					viewContext, YBindingEndpoint);
			internalSetModelValue(editPart);

			shouldRebind = true;
			break;
		default:
			break;
		}

		if (shouldRebind) {
			unbind();
			bind();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #setTargetEndpoint(org.eclipse.osbp.ecview.core.common.editpart.binding.
	 * IBindableEndpointEditpart)
	 */
	public void setTargetEndpoint(IBindableValueEndpointEditpart target) {
		try {
			checkDisposed();

			// set the element by using the model
			//
			YValueBinding yBinding = getModel();
			YValueBindingEndpoint yElement = target != null ? (YValueBindingEndpoint) target
					.getModel() : null;
			yBinding.setTargetEndpoint(yElement);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #getTargetEndpoint()
	 */
	public IBindableValueEndpointEditpart getTargetEndpoint() {
		checkDisposed();

		if (targetValue == null) {
			loadTargetValue();
		}
		return targetValue;
	}

	/**
	 * Loads the content of the view.
	 */
	protected void loadTargetValue() {
		if (targetValue == null) {
			YBindingEndpoint yValue = getModel().getTargetEndpoint();
			internalSetTargetValue((IBindableValueEndpointEditpart) getEditpart(
					viewContext, yValue));
		}
	}

	/**
	 * May be invoked by a model change and the content of the edit part should
	 * be set.
	 * 
	 * @param targetValue
	 *            The content to be set
	 */
	protected void internalSetTargetValue(
			IBindableValueEndpointEditpart targetValue) {
		this.targetValue = targetValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #setModelEndpoint(org.eclipse.osbp.ecview.core.common.editpart.binding.
	 * IBindableEndpointEditpart)
	 */
	public void setModelEndpoint(IBindableValueEndpointEditpart model) {
		try {
			checkDisposed();

			// set the element by using the model
			//
			YValueBinding yBinding = getModel();
			YValueBindingEndpoint yElement = model != null ? (YValueBindingEndpoint) model
					.getModel() : null;
			yBinding.setModelEndpoint(yElement);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #getModelEndpoint()
	 */
	public IBindableValueEndpointEditpart getModelEndpoint() {
		checkDisposed();

		if (modelValue == null) {
			loadModelValue();
		}
		return modelValue;
	}

	/**
	 * Loads the content of the view.
	 */
	protected void loadModelValue() {
		if (modelValue == null) {
			YBindingEndpoint yValue = getModel().getModelEndpoint();
			internalSetModelValue((IBindableValueEndpointEditpart) getEditpart(
					viewContext, yValue));
		}
	}

	/**
	 * May be invoked by a model change and the content of the edit part should
	 * be set.
	 * 
	 * @param modelValue
	 *            The content to be set
	 */
	protected void internalSetModelValue(
			IBindableValueEndpointEditpart modelValue) {
		this.modelValue = modelValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #isBound()
	 */
	@Override
	public boolean isBound() {
		return bound;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #bind()
	 */
	@Override
	public void bind() {
		checkDisposed();

		if (bound) {
			return;
		}

		IECViewBindingManager bindingManager = null;
		if (getBindindSet() != null) {
			bindingManager = getBindindSet().getBindingManager();
		} else {
			bindingManager = getViewContext(getModel()).getService(
					IECViewBindingManager.class.getName());
		}
		if (bindingManager != null) {
			if (getTargetEndpoint() == null || getModelEndpoint() == null) {
				LOGGER.warn("Endpoints are null");
				return;
			}
			IObservableValue target = getTargetEndpoint().getObservable();
			IObservableValue model = getModelEndpoint().getObservable();
			if (target == null) {
				LOGGER.error("TargetValue must never be null! {}",
						getTargetEndpoint());
				return;
			}

			if (model == null) {
				LOGGER.error("ModelValue must never be null! {}",
						getModelEndpoint());
				return;
			}

			ECViewUpdateValueStrategy modelToTargetStrategy = getValueUpdateStrategy(getModel()
					.getModelToTargetStrategy());
			ECViewUpdateValueStrategy targetToModelStrategy = getValueUpdateStrategy(getModel()
					.getTargetToModelStrategy());
			binding = bindingManager.bindValue(target, model,
					targetToModelStrategy, modelToTargetStrategy);
			binding.updateTargetToModel();
			
			// set bound to true if everything went fine
			bound = true;
		} else {
			LOGGER.error("BindingManager is null!. No bindings processed!");
		}
	}

	/**
	 * Gets the value update strategy.
	 *
	 * @param strategy
	 *            the strategy
	 * @return the value update strategy
	 */
	private ECViewUpdateValueStrategy getValueUpdateStrategy(
			YBindingUpdateStrategy strategy) {
		ECViewUpdateValueStrategy result = null;
		switch (strategy) {
		case UPDATE:
			result = new ECViewUpdateValueStrategy(
					ECViewUpdateValueStrategy.POLICY_UPDATE);
			break;
		case NEVER:
			result = new ECViewUpdateValueStrategy(
					ECViewUpdateValueStrategy.POLICY_NEVER);
			break;
		case ON_REQUEST:
			result = new ECViewUpdateValueStrategy(
					ECViewUpdateValueStrategy.POLICY_ON_REQUEST);
			break;
		default:
			result = new ECViewUpdateValueStrategy(
					ECViewUpdateValueStrategy.POLICY_UPDATE);
		}
		return result;
	}

	/**
	 * Returns the binding set this binding belongs to.
	 *
	 * @return the bindind set
	 */
	protected IBindingSetEditpart getBindindSet() {
		if (getModel().getBindingSet() == null) {
			return null;
		}
		return DelegatingEditPartManager.getInstance().findEditpart(
				getModel().getBindingSet());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #unbind()
	 */
	@Override
	public void unbind() {
		checkDisposed();

		if (!bound) {
			return;
		}

		try {
			if (binding != null) {
				binding.dispose();
				binding = null;
			}
			if (modelValue != null) {
				modelValue.dispose();
				modelValue = null;
			}
			if (targetValue != null) {
				targetValue.dispose();
				targetValue = null;
			}
		} finally {
			bound = false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {
			if (binding != null) {
				binding.dispose();
				binding = null;
			}
			if (modelValue != null) {
				modelValue.dispose();
				modelValue = null;
			}
			if (targetValue != null) {
				targetValue.dispose();
				targetValue = null;
			}
		} finally {
			super.internalDispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart
	 * #getBinding()
	 */
	@Override
	public Binding getBinding() {
		return binding;
	}
}
