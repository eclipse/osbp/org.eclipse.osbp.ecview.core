/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.common;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

/**
 * Handles the resources of the ui models.
 */
public interface IResourceManager {

	/**
	 * The file extension used for the orphan views resource.
	 */
	String ORPHAN_VIEWS_EXTENSION = "orphan";

	/**
	 * Is used to register orphan views.
	 */
	String ORPHAN_VIEW_RESOURCE_URI_STRING = "http://eclipse.org/emf/emfclient/uimodel.orphan";
	
	/**
	 * The URI to access the resource for orphan views from the resource set.
	 */
	URI ORPHAN_VIEW_RESOURCE_URI = URI.createURI(ORPHAN_VIEW_RESOURCE_URI_STRING);

	/**
	 * Returns the resource for the given uri.
	 * 
	 * @param uri the uri to be used to access the resource
	 * @return resource
	 */
	Resource getResource(String uri);

	/**
	 * Returns the resource for the given uri.
	 * 
	 * @param uri the uri to be used to access the resource
	 * @return resource
	 */
	Resource getResource(URI uri);

	/**
	 * Returns the resource set.
	 * 
	 * @return resourceSet
	 */
	ResourceSet getResourceSet();

}
