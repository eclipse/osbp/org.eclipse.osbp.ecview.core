/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.ISendEventCommandEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YSendEventCommand;
import org.eclipse.osbp.runtime.common.event.IEventBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The implementation of the IOpenDialogCommandEditpart.
 */
public class SendEventCommandEditpart extends
		CommandEditpart<YSendEventCommand> implements ISendEventCommandEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(SendEventCommandEditpart.class);

	/** The activated. */
	private boolean activated;

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.ICommandEditpart#activate()
	 */
	@Override
	public void activate() {
		activated = true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#handleModelSet(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		YSendEventCommand yCommand = getModel();
		switch (featureId) {
		case CoreModelPackage.YSEND_EVENT_COMMAND__MESSAGE:
			if (yCommand.isAutoTrigger()) {
				execute();
			}
			break;
		case CoreModelPackage.YSEND_EVENT_COMMAND__TRIGGER:
			trigger(yCommand);
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	/**
	 * Setting a value will trigger the command execution.
	 *
	 * @param value
	 *            the value
	 */
	public void trigger(Object value) {
		// execute the command
		if (activated && value != null) {
			execute();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.ICommandEditpart#execute()
	 */
	@Override
	public void execute() {
		checkDisposed();

		YSendEventCommand yCommand = getModel();
		IEventBroker eventAdmin = getView().getContext().getService(
				IEventBroker.class.getName());
		if (eventAdmin != null) {
			try {
				Map<String, Object> data = new HashMap<String, Object>();
				data.put(IEventBroker.DATA, yCommand.getMessage());
				eventAdmin.post(yCommand.getEventTopic(), data);
			} catch (Exception e) {
				LOGGER.error("{}", e);
			}
		}
	}
}
