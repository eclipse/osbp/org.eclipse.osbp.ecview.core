/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IKeyStrokeDefinitionContainer;
import org.eclipse.osbp.ecview.core.common.editpart.IKeyStrokeDefinitionEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YKeyStrokeDefinition;
import org.eclipse.osbp.ecview.core.common.model.core.impl.custom.KeyStrokeUtil;
import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The implementation of the IKeyStrokeDefinitionEditpart.
 */
public class KeyStrokeDefinitionEditpart extends
		ElementEditpart<YKeyStrokeDefinition> implements
		IKeyStrokeDefinitionEditpart {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(KeyStrokeDefinitionEditpart.class);

	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		YKeyStrokeDefinition yElement = getModel();
		switch (featureId) {
		case CoreModelPackage.YKEY_STROKE_DEFINITION__KEY_CODE:
		case CoreModelPackage.YKEY_STROKE_DEFINITION__MODIFIER_KEYS:
			IElementEditpart parent = getEditpart(viewContext, (YElement) yElement
					.eContainer());
			LOGGER.info("Keystroke definition changed.");
			if (parent instanceof IKeyStrokeDefinitionContainer) {
				((IKeyStrokeDefinitionContainer) parent).keyStrokeChanged(this);
			}
			break;
		default:
			super.handleModelSet(featureId, notification);
		}
	}

	@Override
	public KeyStrokeDefinition getDefinition() {
		return KeyStrokeUtil.from(getModel());
	}
}
