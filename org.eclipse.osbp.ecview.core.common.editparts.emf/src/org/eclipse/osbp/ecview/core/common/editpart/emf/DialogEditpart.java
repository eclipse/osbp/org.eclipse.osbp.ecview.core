/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YDialog;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.presentation.DelegatingPresenterFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IDialogPresentation;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class DialogEditpart.
 */
public class DialogEditpart extends ElementEditpart<YDialog> implements
		IDialogEditpart {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(EmbeddableEditpart.class);
	
	/** The presentation. */
	private IDialogPresentation<?> presentation;
	
	/** The content. */
	private IEmbeddableEditpart content;

	/**
	 * The default constructor.
	 */
	protected DialogEditpart() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#getParent()
	 */
	@Override
	public IViewEditpart getParent() {
		return getView();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#getView()
	 */
	@Override
	public IViewEditpart getView() {
		YView yView = getModel().getView();
		return yView != null ? (IViewEditpart) getEditpart(viewContext, yView) : null;
	}

	/**
	 * Returns the instance of the presentation, but does not load it.
	 * 
	 * @param <A>
	 *            An instance of {@link IWidgetPresentation}
	 * @return presentation
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IWidgetPresentation<?>> A internalGetPresentation() {
		return (A) presentation;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#getPresentation()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends IDialogPresentation<?>> A getPresentation() {
		if (presentation == null) {
			presentation = createPresenter();
			presentation.setContent(getContent());
		}
		return (A) presentation;
	}

	/**
	 * Is called to created the presenter for this edit part.
	 *
	 * @param <A>
	 *            the generic type
	 * @return the a
	 */
	protected <A extends IWidgetPresentation<?>> A createPresenter() {
		IViewEditpart viewEditPart = getView();
		if (viewEditPart == null) {
			LOGGER.info("View is null");
			return null;
		}
		return DelegatingPresenterFactory.getInstance().createPresentation(
				viewEditPart.getContext(), this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			// dispose the presenter
			//
			if (presentation != null) {
				presentation.dispose();
				presentation = null;
			}
		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#requestRender()
	 */
	@Override
	public void requestRender() {
		if (getParent() != null) {
			getParent().openDialog(this, null);
		} else {
			unrender();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#render(java.lang.Object)
	 */
	@Override
	public Object render(Object parentWidget) {
		return getPresentation().createWidget(parentWidget);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#requestUnrender()
	 */
	@Override
	public void requestUnrender() {
		if (getParent() != null) {
			getParent().closeDialog(this);
		} else {
			unrender();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#unrender()
	 */
	@Override
	public void unrender() {
		getPresentation().unrender();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#isRendered()
	 */
	@Override
	public boolean isRendered() {
		return internalGetPresentation() != null
				&& internalGetPresentation().isRendered();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#getWidget()
	 */
	@Override
	public Object getWidget() {
		return getPresentation().getWidget();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#requestDispose()
	 */
	@Override
	public void requestDispose() {
		if (getParent() != null) {
			getParent().closeDialog(this);
		} else {
			dispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableParent#renderChild(org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart)
	 */
	@Override
	public void renderChild(IEmbeddableEditpart child) {
		if (child != content) {
			return;
		}

		getPresentation().setContent(child);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableParent#unrenderChild(org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart)
	 */
	@Override
	public void unrenderChild(IEmbeddableEditpart child) {
		if (child != content) {
			return;
		}

		// first remove the child presentation from the current presentation
		getPresentation().setContent(null);

		// then tell the child editpart to unrender its own presentation
		child.unrender();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableParent#disposeChild(org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart)
	 */
	@Override
	public void disposeChild(IEmbeddableEditpart child) {
		if (child != content) {
			return;
		}

		// first remove the child presentation from the current presentation
		getPresentation().setContent(null);

		// then tell the child editpart to dispose itself
		child.dispose();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CoreModelPackage.YDIALOG__CONTENT:
			IEmbeddableEditpart oldContent = content;
			if (oldContent != null) {
				oldContent.dispose();
				internalSetContent(null);
				if (isRendered()) {
					getPresentation().setContent(null);
				}
			}

			YEmbeddable yNewContent = (YEmbeddable) notification.getNewValue();
			IEmbeddableEditpart editPart = (IEmbeddableEditpart) getEditpart(viewContext, yNewContent);
			internalSetContent(editPart);

			// handle the presentation
			//
			if (isRendered()) {
				getPresentation().setContent(editPart);
			}

			break;
		default:
			break;
		}
	}

	/**
	 * Sets the content.
	 *
	 * @param content
	 *            the new content
	 */
	public void setContent(IEmbeddableEditpart content) {
		try {
			checkDisposed();

			// set the element by using the model
			//
			YDialog yDialog = getModel();
			YEmbeddable yElement = content != null ? (YEmbeddable) content
					.getModel() : null;
			yDialog.setContent(yElement);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/**
	 * Gets the content.
	 *
	 * @return the content
	 */
	public IEmbeddableEditpart getContent() {
		if (content == null) {
			loadContent();
		}
		return content;
	}

	/**
	 * Loads the content of the view.
	 */
	protected void loadContent() {
		if (content == null) {
			YEmbeddable yContent = getModel().getContent();
			internalSetContent((IEmbeddableEditpart) getEditpart(viewContext, yContent));
		}
	}

	/**
	 * May be invoked by a model change and the content of the edit part should
	 * be set.
	 * 
	 * @param content
	 *            The content to be set
	 */
	protected void internalSetContent(IEmbeddableEditpart content) {
		this.content = content;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart#setInputDataBindingEndpoint(org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart)
	 */
	@Override
	public void setInputDataBindingEndpoint(
			IBindableEndpointEditpart bindingEndpoint) {
		IDialogPresentation<?> presentation = getPresentation();
		presentation.setInputDataBindingEndpoint(bindingEndpoint);
	}
}
