/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator;

import org.eclipse.osbp.ecview.core.common.model.validation.YMinLengthValidationConfig;
import org.eclipse.osbp.ecview.core.common.validation.StringValidator;
import org.eclipse.osbp.runtime.common.validation.IStatus;
import org.eclipse.osbp.runtime.common.validation.Status;

public class MinLengthValidator extends StringValidator {

	public static final String DEFAULT_ERROR_CODE = "org.eclipse.osbp.ecview.core.common.editpart.emf.validation.validator.MinLengthValidator";
	private static final String DEF_MESSAGE = "Minimum length is ${minLength}. Length of \"${value}\" is ${currentLength}!";

	private String defaultMessage = DEF_MESSAGE;
	private int minLength;

	public MinLengthValidator(YMinLengthValidationConfig yValidator) {
		super(yValidator.getErrorCode());
		if (isStringValid(yValidator.getDefaultErrorMessage())) {
			defaultMessage = yValidator.getDefaultErrorMessage();
		}

		updateParameter(yValidator);
	}
 
	@Override
	public IStatus doValidate(String value) {
		if (value.trim().length() < minLength) {
			return Status.createStatus(errorCode, getClass(),
					IStatus.Severity.ERROR, createMessage(value));
		}

		return IStatus.OK;
	}

	/**
	 * Creates the message.
	 * 
	 * @param value
	 * @return
	 */
	protected String createMessage(String value) {
		String message = getMessage();
		if (!isStringValid(message)) {
			return "Error message missing!";
		}
		message = message.replaceAll("\\$\\{minLength\\}",
				Integer.toString(minLength));
		message = message.replaceAll("\\$\\{currentLength\\}",
				Integer.toString(value.trim().length()));
		message = message.replaceAll("\\$\\{value\\}", value);
		return message;
	}

	/**
	 * Creates the default message in english.
	 * 
	 * @return
	 */
	protected String getDefaultMessage() {
		return defaultMessage;
	}

	@Override
	public void updateParameter(Object model) {
		YMinLengthValidationConfig yValidator = (YMinLengthValidationConfig) model;
		this.minLength = yValidator.getMinLength();
	}

	@Override
	protected String getDefaultErrorCode() {
		return DEFAULT_ERROR_CODE;
	};

	@Override
	protected void internalDispose() {

	};

}
