/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.PojoObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.osbp.ecview.core.common.binding.IECViewBindingManager;
import org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IOpenDialogCommandEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableValueEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.model.core.YOpenDialogCommand;

// TODO: Auto-generated Javadoc
/**
 * The implementation of the IOpenDialogCommandEditpart.
 */
public class OpenDialogCommandEditpart extends
		CommandEditpart<YOpenDialogCommand> implements
		IOpenDialogCommandEditpart {

	/** The binding. */
	private Binding binding;
	
	/** The activated. */
	private boolean activated;

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.ICommandEditpart#activate()
	 */
	@Override
	public void activate() {
		// bind the values
		IECViewBindingManager bindingManager = getView().getContext()
				.getService(IECViewBindingManager.class.getName());

		// Bind the trigger
		IBindableValueEndpointEditpart modelValueEditpart = (IBindableValueEndpointEditpart) getEditpart(viewContext, getModel()
				.createTriggerDialogEndpoint());
		IObservableValue modelObservable = modelValueEditpart.getObservable();
		IObservableValue targetObservable = PojoObservables.observeValue(this,
				"trigger");
		binding = bindingManager.bindValue(targetObservable, modelObservable,
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_NEVER),
				new UpdateValueStrategy(UpdateValueStrategy.POLICY_UPDATE));
		activated = true;
	}

	/**
	 * Setting a value will trigger the command execution.
	 *
	 * @param value
	 *            the new trigger
	 */
	public void setTrigger(Object value) {
		// execute the command
		if (activated && value != null) {
			execute();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void internalDispose() {
		try {
			if (binding != null) {
				binding.dispose();
				binding = null;
			}
		} finally {
			super.internalDispose();
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.ecview.core.common.editpart.ICommandEditpart#execute()
	 */
	@Override
	public void execute() {
		checkDisposed();

		YOpenDialogCommand model = getModel();
		IDialogEditpart dialogEditpart = getEditpart(viewContext, model.getDialog());
		IBindableEndpointEditpart bindableEndpoint = getEditpart(viewContext, model
				.createTriggerDialogEndpoint());
		getView().openDialog(dialogEditpart, bindableEndpoint);
	}
}
