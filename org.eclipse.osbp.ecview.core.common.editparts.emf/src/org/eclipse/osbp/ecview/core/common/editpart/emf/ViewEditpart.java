/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.common.editpart.emf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.osbp.ecview.core.common.context.ContextException;
import org.eclipse.osbp.ecview.core.common.context.IConfiguration;
import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.editpart.ICommandSetEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IExposedActionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.visibility.IVisibilityProcessorEditpart;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingSet;
import org.eclipse.osbp.ecview.core.common.model.binding.YBindingUpdateStrategy;
import org.eclipse.osbp.ecview.core.common.model.binding.YECViewModelValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.binding.YValueBinding;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot;
import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlotValueBindingEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YCommandSet;
import org.eclipse.osbp.ecview.core.common.model.core.YDetailBeanSlot;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableSelectionEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddableValueEndpoint;
import org.eclipse.osbp.ecview.core.common.model.core.YExposedAction;
import org.eclipse.osbp.ecview.core.common.model.core.YFocusable;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.util.BindingIdUtil;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleEvent;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleHandler;
import org.eclipse.osbp.ecview.core.common.notification.ILifecycleService;
import org.eclipse.osbp.ecview.core.common.presentation.DelegatingPresenterFactory;
import org.eclipse.osbp.ecview.core.common.presentation.IViewPresentation;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.core.common.services.IUiKitBasedService;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;
import org.eclipse.osbp.runtime.common.event.IEventBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// TODO: Auto-generated Javadoc
/**
 * The implementation for {@link IViewEditpart}.
 *
 * @param <M>
 *            the generic type
 */
public class ViewEditpart<M extends YView> extends ElementEditpart<M> implements
		IViewEditpart, ILifecycleHandler {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ViewEditpart.class);

	/** The content. */
	private IEmbeddableEditpart content;

	/** The configuration. */
	private IConfiguration configuration;

	/** The presentation. */
	private IViewPresentation<?> presentation;

	/** The binding set. */
	private IBindingSetEditpart bindingSet;

	/** The command set. */
	private ICommandSetEditpart commandSet;

	/** The open dialogs. */
	private Set<IDialogEditpart> openDialogs;

	/** The v processor editparts. */
	private List<IVisibilityProcessorEditpart> vProcessorEditparts;

	/** The transient v processor editparts. */
	private List<IVisibilityProcessorEditpart> transientVProcessorEditparts;

	/** The exposed action editparts. */
	private List<IExposedActionEditpart> exposedActionEditparts;

	/**
	 * Default constructor.
	 */
	public ViewEditpart() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#findModelElement
	 * (java.lang.String)
	 */
	@Override
	public Object findModelElement(String id) {
		if (id == null || id.equals("")) {
			return null;
		}

		Iterator<EObject> iter = getModel().eAllContents();
		while (iter.hasNext()) {
			EObject eObject = iter.next();
			if (eObject instanceof YElement) {
				YElement yElement = (YElement) eObject;
				if (id.equals(yElement.getId())) {
					return yElement;
				}
			}
		}
		return null;
	}
	
	
	@Override
	public Object findYEmbeddableElement(String id) {
		if (id == null || id.equals("")) {
			return null;
		}

		Iterator<EObject> iter = getModel().eAllContents();
		while (iter.hasNext()) {
			EObject eObject = iter.next();
			if (eObject instanceof YEmbeddable) {
				YEmbeddable yElement = (YEmbeddable) eObject;
				if (id.equals(yElement.getId())) {
					return yElement;
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#findBoundField
	 * (java.lang.String)
	 */
	@Override
	public Object findBoundField(String bindingIdRegex) {
		if (bindingIdRegex == null || bindingIdRegex.equals("")) {
			return null;
		}

		Iterator<EObject> iter = getModel().eAllContents();
		while (iter.hasNext()) {
			EObject eObject = iter.next();
			if (eObject instanceof YBinding) {
				YBinding yBinding = (YBinding) eObject;

				if (!(yBinding instanceof YValueBinding)) {
					continue;
				}

				YValueBinding binding = (YValueBinding) yBinding;

				String modelBindingId = binding.getModelEndpoint()
						.getBindingIdString();
				String targetBindingId = binding.getTargetEndpoint()
						.getBindingIdString();

				if (modelBindingId.matches(bindingIdRegex)) {
					// we found a binding that matches the search value
					// now check, whether the opposite endpoint is a
					// yFieldEndpoint
					String oppositeId = binding.getTargetEndpoint()
							.getBindingIdString();
					if (oppositeId.startsWith(BindingIdUtil.UI_VALUE)) {
						// we found the field
						YEmbeddableValueEndpoint ep = (YEmbeddableValueEndpoint) binding
								.getTargetEndpoint();
						return ep.getElement();
					} else 	if (oppositeId.startsWith(BindingIdUtil.UI_SELECTION)) {
							// we found the field
							YEmbeddableSelectionEndpoint ep = (YEmbeddableSelectionEndpoint) binding.getTargetEndpoint();
							return ep.getElement();
					} else if (oppositeId.startsWith(BindingIdUtil.MODEL_BINDING)
							&& (oppositeId.endsWith("value") || oppositeId.endsWith("collection"))) {
						// we found the field
						YECViewModelValueBindingEndpoint ep = (YECViewModelValueBindingEndpoint) binding
								.getTargetEndpoint();
						return ep.getElement();
					}

				} else if (targetBindingId.matches(bindingIdRegex)) {

					// we found a binding that matches the search value
					// now check, whether the opposite endpoint is a
					// yFieldEndpoint
					String oppositeId = binding.getModelEndpoint()
							.getBindingIdString();
					if (oppositeId.startsWith(BindingIdUtil.UI_VALUE) ) {
						// we found the field
						YEmbeddableValueEndpoint ep = (YEmbeddableValueEndpoint) binding
								.getModelEndpoint();
						return ep.getElement();
					} else if (oppositeId.startsWith(BindingIdUtil.UI_SELECTION) ) {
							// we found the field
						YEmbeddableSelectionEndpoint ep = (YEmbeddableSelectionEndpoint) binding
								.getModelEndpoint();
						return ep.getElement();
					} else if (oppositeId.startsWith(BindingIdUtil.MODEL_BINDING)
							&& (oppositeId.endsWith("value") || oppositeId.endsWith("collection"))) {
						// we found the field
						YECViewModelValueBindingEndpoint ep = (YECViewModelValueBindingEndpoint) binding
								.getTargetEndpoint();
						return ep.getElement();
					}
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#render(java
	 * .util.Map)
	 */
	@Override
	public void render(Map<String, Object> options) throws ContextException {
		checkDisposed();
		try {
			validateModel();

			if (configuration != null) {
				configuration.beforeUiRendering(getContext());
			}

			// ensure that the bean slots are initialized
			for (YBeanSlot yBeanSlot : getModel().getBeanSlots()) {
				internalAddBeanSlot(yBeanSlot);
			}

			bindDetailBeanSlots();

			// register the context as a bean slot
			viewContext.createBeanSlot(BEANSLOT__ECVIEW_CONTEXT,
					IViewContext.class);
			viewContext.setBean(BEANSLOT__ECVIEW_CONTEXT, getContext());

			// Will create the editparts.
			internalLoadExposedActions();

			// render the view presentation
			renderPresentation(options);

			if (configuration != null) {
				configuration.afterUiRendering(getContext());
			}

			if (configuration != null) {
				configuration.beforeBind(getContext());
			}
			// render the bindings
			renderBindings(options);

			// render the commands
			renderCommands(options);

			// render the visibility processor
			activateVisibilityProcessor(options);

			if (configuration != null) {
				configuration.afterBind(getContext());
			}

			if (getContext() != null) {
				ILifecycleService service = getContext().getService(
						ILifecycleService.class.getName());
				if (service == null) {
					throw new RuntimeException("ILifecycleService is required");
				}
				service.addHandler(this);
			}

			// set the initial focus
			if (getModel().getInitialFocus() != null) {
				requestFocus(getModel().getInitialFocus());
			}
		} catch (Exception ex) {
			LOGGER.error("{}", ex);
		}
	}

	/**
	 * This methods binds the detail bean slots to their master slot.
	 */
	protected void bindDetailBeanSlots() {
		for (YBeanSlot beanSlot : getModel().getBeanSlots()) {
			if (beanSlot instanceof YDetailBeanSlot) {
				YDetailBeanSlot detailSlot = (YDetailBeanSlot) beanSlot;
				YBeanSlot masterSlot = detailSlot.getMasterBeanSlot();

				// TODO masterSlot may also be a detailSlot. So take care about
				// the order in creating bindings

				// the input of detail is the main slot
				YBeanSlotValueBindingEndpoint detailSlotEP = detailSlot
						.createBindingEndpoint("value");

				// the output of master is the property path from detail slot
				YBeanSlotValueBindingEndpoint masterSlotEP = masterSlot
						.createBindingEndpoint(detailSlot.getPropertyPath());

				// add the binding as a transient binding. Will not be persisted
				// if the model was persisted
				getModel().getOrCreateBindingSet().addTransientBinding(
						detailSlotEP, masterSlotEP,
						YBindingUpdateStrategy.NEVER,
						YBindingUpdateStrategy.UPDATE);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableParent#unrenderChild
	 * (org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart)
	 */
	@Override
	public void unrenderChild(IEmbeddableEditpart child) {
		if (child != content) {
			return;
		}

		// first remove the child presentation from the current presentation
		getPresentation().setContent(null);

		// then tell the child editpart to unrender its own presentation
		child.unrender();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableParent#disposeChild
	 * (org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart)
	 */
	@Override
	public void disposeChild(IEmbeddableEditpart child) {
		if (child != content) {
			return;
		}

		// first remove the child presentation from the current presentation
		getPresentation().setContent(null);
		content = null;

		// then tell the child editpart to dispose itself
		child.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableParent#renderChild
	 * (org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart)
	 */
	@Override
	public void renderChild(IEmbeddableEditpart child) {
		if (child != content) {
			return;
		}

		getPresentation().setContent(child);
	}

	/**
	 * Validates the model and throws an exception if model is not valid.
	 *
	 * @throws ContextException
	 *             the viewContext exception
	 */
	protected void validateModel() throws ContextException {
		YView yView = getModel();
		EValidator validator = EObjectValidator.INSTANCE;
		BasicDiagnostic diagnostic = new BasicDiagnostic();
		boolean valid = validator.validate(yView, diagnostic, null);
		if (!valid) {
			throw new ContextException(diagnostic.toString());
		}
	}

	/**
	 * Renders the presentation of that view.
	 *
	 * @param options
	 *            the options
	 * @throws ContextException
	 *             the viewContext exception
	 */
	protected void renderPresentation(Map<String, Object> options)
			throws ContextException {
		checkDisposed();

		IViewPresentation<?> presentation = getPresentation();
		if (presentation == null) {
			throw new ContextException("Presenter must not be null!");
		}
		presentation.render(options);
	}

	/**
	 * Renders the bindings of that view.
	 *
	 * @param options
	 *            the options
	 * @throws ContextException
	 *             the viewContext exception
	 */
	protected void renderBindings(Map<String, Object> options)
			throws ContextException {
		checkDisposed();

		IBindingSetEditpart bindingSet = getBindingSet();
		if (bindingSet == null) {
			LOGGER.info("BindingSet is null!");
		}
		// call to activate not required. Pending bindings are bound
		// automatically
	}

	/**
	 * Renders the commands of that view.
	 *
	 * @param options
	 *            the options
	 * @throws ContextException
	 *             the viewContext exception
	 */
	protected void renderCommands(Map<String, Object> options)
			throws ContextException {
		checkDisposed();

		ICommandSetEditpart commandSet = getCommandSet();
		if (commandSet == null) {
			LOGGER.info("CommandSet is null!");
		}
		// call to activate not required. Pending commands are activated
		// automatically
	}

	/**
	 * Activates the visibility processor of that view.
	 *
	 * @param options
	 *            the options
	 * @throws ContextException
	 *             the viewContext exception
	 */
	protected void activateVisibilityProcessor(Map<String, Object> options)
			throws ContextException {
		checkDisposed();

		getVisibilityProcessors();
		getTransientVisibilityProcessors();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.notification.ILifecycleHandler#
	 * notifyLifecycle
	 * (org.eclipse.osbp.ecview.core.common.notification.ILifecycleEvent)
	 */
	@Override
	public void notifyLifecycle(ILifecycleEvent event) {
		IElementEditpart editPart = event.getEditpart();

		switch (event.getType()) {
		case ILifecycleEvent.TYPE_DISPOSED: {
			YElement element = (YElement) editPart.getModel();
			if (element instanceof YBindable) {
				IBindingSetEditpart bindingSet = getBindingSet();
				if (bindingSet != null) {
					List<IBindingEditpart<?>> bindings = bindingSet
							.findBindings(element);
					for (IBindingEditpart<?> binding : bindings) {
						if (!binding.isDisposed()) {
							binding.dispose();
						}
					}
				}
			}

			// send the lifecycle event by event admin
			IEventBroker broker = getContext().getService(
					IEventBroker.class.getName());
			if (broker != null) {
				broker.send(IViewContext.TOPIC_LIFECYCLE, event);
			}

			break;
		}
		case ILifecycleEvent.TYPE_RENDERED: {
			YElement element = (YElement) editPart.getModel();
			if (element instanceof YBindable) {
				IBindingSetEditpart bindingSet = getBindingSet();
				if (bindingSet != null) {
					List<IBindingEditpart<?>> bindings = bindingSet
							.findBindings(element);
					for (IBindingEditpart<?> binding : bindings) {
						if (!binding.isBound()) {
							binding.bind();
						}
					}
				}
			}
			break;
		}
		case ILifecycleEvent.TYPE_UNRENDERED: {
			YElement element = (YElement) editPart.getModel();
			if (element instanceof YBindable) {
				IBindingSetEditpart bindingSet = getBindingSet();
				if (bindingSet != null) {
					List<IBindingEditpart<?>> bindings = bindingSet
							.findBindings(element);
					for (IBindingEditpart<?> binding : bindings) {
						binding.unbind();
					}
				}
			}
			break;
		}
		}
	}

	// /* (non-Javadoc)
	// * @see
	// org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#setContext(org.eclipse.osbp.ecview.core.common.context.IViewContext)
	// */
	// @Override
	// // BEGIN SUPRESS CATCH EXCEPTION
	// public void setContext(IViewContext context) throws RuntimeException {
	// // END SUPRESS CATCH EXCEPTION
	//
	// checkDisposed();
	//
	// if (this.context == context) {
	// return;
	// }
	//
	// if (this.context != null && this.context.isRendered()) {
	// throw new RuntimeException(
	// "Already rendered! Changing context not allowed!");
	// }
	// this.context = context;
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#setConfiguration
	 * (org.eclipse.osbp.ecview.core.common.context.IConfiguration)
	 */
	@Override
	public void setConfiguration(IConfiguration configuration) {
		checkDisposed();

		this.configuration = configuration;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#getName()
	 */
	@Override
	public String getName() {
		return getModel().getViewName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#setContent
	 * (org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart)
	 */
	@Override
	public void setContent(IEmbeddableEditpart content) {
		try {
			checkDisposed();

			// set the element by using the model
			//
			M yView = getModel();
			YEmbeddable yElement = content != null ? (YEmbeddable) content
					.getModel() : null;
			yView.setContent(yElement);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#getContent()
	 */
	@Override
	public IEmbeddableEditpart getContent() {
		if (content == null) {
			loadContent();
		}
		return content;
	}

	/**
	 * Loads the content of the view.
	 */
	protected void loadContent() {
		if (content == null) {
			YEmbeddable yContent = getModel().getContent();
			internalSetContent((IEmbeddableEditpart) getEditpart(viewContext,
					yContent));
		}
	}

	/**
	 * May be invoked by a model change and the content of the edit part should
	 * be set.
	 * 
	 * @param content
	 *            The content to be set
	 */
	protected void internalSetContent(IEmbeddableEditpart content) {
		this.content = content;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#setBindingSet
	 * (org
	 * .eclipse.osbp.ecview.core.common.editpart.binding.IBindingSetEditpart)
	 */
	@Override
	public void setBindingSet(IBindingSetEditpart bindingSet) {
		try {
			checkDisposed();

			// set the element by using the model
			//
			M yView = getModel();
			YBindingSet yBindingSet = bindingSet != null ? (YBindingSet) bindingSet
					.getModel() : null;
			yView.setBindingSet(yBindingSet);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#getBindingSet
	 * ()
	 */
	@Override
	public IBindingSetEditpart getBindingSet() {
		checkDisposed();

		if (bindingSet == null) {
			loadBindingSet();
		}
		return bindingSet;
	}

	/**
	 * Loads the bindingSet of the view.
	 */
	protected void loadBindingSet() {
		checkDisposed();

		if (bindingSet == null) {
			YBindingSet yBindingSet = getModel().getOrCreateBindingSet();
			internalSetBindingSet((IBindingSetEditpart) getEditpart(
					viewContext, yBindingSet));
		}
	}

	/**
	 * May be invoked by a model change and the bindingSet of the edit part
	 * should be set.
	 * 
	 * @param bindingSet
	 *            The bindingSet to be set
	 */
	protected void internalSetBindingSet(IBindingSetEditpart bindingSet) {
		checkDisposed();

		if (this.bindingSet == bindingSet) {
			return;
		}

		IBindingSetEditpart current = this.bindingSet;
		this.bindingSet = bindingSet;

		// dispose current binding set
		if (current != null) {
			current.dispose();
		}

		// activate the new binding set
		if (this.bindingSet != null && !this.bindingSet.isActive()) {
			this.bindingSet.activate();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#setCommandSet
	 * (org.eclipse.osbp.ecview.core.common.editpart.ICommandSetEditpart)
	 */
	@Override
	public void setCommandSet(ICommandSetEditpart commandSet) {
		try {
			checkDisposed();

			// set the element by using the model
			//
			M yView = getModel();
			YCommandSet yCommandSet = commandSet != null ? (YCommandSet) commandSet
					.getModel() : null;
			yView.setCommandSet(yCommandSet);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#getCommandSet
	 * ()
	 */
	@Override
	public ICommandSetEditpart getCommandSet() {
		checkDisposed();

		if (commandSet == null) {
			loadCommandSet();
		}
		return commandSet;
	}

	/**
	 * Loads the commandSet of the view.
	 */
	protected void loadCommandSet() {
		checkDisposed();

		if (commandSet == null) {
			YCommandSet yCommandSet = getModel().getCommandSet();
			internalSetCommandSet((ICommandSetEditpart) getEditpart(
					viewContext, yCommandSet));
		}
	}

	/**
	 * May be invoked by a model change and the commandSet of the edit part
	 * should be set.
	 * 
	 * @param commandSet
	 *            The commandSet to be set
	 */
	protected void internalSetCommandSet(ICommandSetEditpart commandSet) {
		checkDisposed();

		if (this.commandSet == commandSet) {
			return;
		}

		ICommandSetEditpart current = this.commandSet;
		this.commandSet = commandSet;

		// dispose current binding set
		if (current != null) {
			current.dispose();
		}

		// activate the new binding set
		if (this.commandSet != null && !this.commandSet.isActive()) {
			this.commandSet.activate();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void handleModelSet(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CoreModelPackage.YVIEW__CONTENT:

			IEmbeddableEditpart oldContent = content;
			if (oldContent != null) {
				oldContent.dispose();
				internalSetContent(null);
				if (isRendered()) {
					getPresentation().setContent(null);
				}
			}

			YEmbeddable yNewContent = (YEmbeddable) notification.getNewValue();
			IEmbeddableEditpart editPart = (IEmbeddableEditpart) getEditpart(
					viewContext, yNewContent);
			internalSetContent(editPart);

			// handle the presentation
			//
			if (isRendered()) {
				getPresentation().setContent(editPart);
			}

			break;
		case CoreModelPackage.YVIEW__BINDING_SET:
			YBindingSet yNewBindingSet = (YBindingSet) notification
					.getNewValue();
			IBindingSetEditpart bsEditPart = (IBindingSetEditpart) getEditpart(
					viewContext, yNewBindingSet);
			internalSetBindingSet(bsEditPart);
			break;
		case CoreModelPackage.YVIEW__COMMAND_SET:
			YCommandSet yNewCommandSet = (YCommandSet) notification
					.getNewValue();
			ICommandSetEditpart csEditPart = (ICommandSetEditpart) getEditpart(
					viewContext, yNewCommandSet);
			internalSetCommandSet(csEditPart);
			break;
		case CoreModelPackage.YVIEW__CURRENT_FOCUS:
			YFocusable yFocusable = (YFocusable) notification.getNewValue();
			requestFocus(yFocusable);
			break;
		case CoreModelPackage.YVIEW__CONTENT_ALIGNMENT:
			if (isRendered()) {
				getPresentation().updateContentAlignment();
			}
		case CoreModelPackage.YVIEW__MARGIN:
			if (isRendered()) {
				getPresentation().updateSpacings();
			}
		default:
			break;
		}
	}

	/**
	 * Requests the focus for the given element.
	 *
	 * @param yFocusable
	 *            the y focusable
	 */
	protected void requestFocus(YFocusable yFocusable) {
		getPresentation().requestFocus(
				getEditpart(viewContext, (YElement) yFocusable));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * handleModelAdd(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelAdd(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CoreModelPackage.YVIEW__BEAN_SLOTS:
			YBeanSlot yBeanSlot = (YBeanSlot) notification.getNewValue();
			internalAddBeanSlot(yBeanSlot);
			break;
		case CoreModelPackage.YVIEW__VISIBILITY_PROCESSORS:
			YVisibilityProcessor yElement = (YVisibilityProcessor) notification
					.getNewValue();
			internalAddVisibilityProcessor((IVisibilityProcessorEditpart) getEditpart(
					viewContext, yElement));
			break;
		case CoreModelPackage.YVIEW__TRANSIENT_VISIBILITY_PROCESSORS:
			yElement = (YVisibilityProcessor) notification.getNewValue();
			internalAddTransientVisibilityProcessor((IVisibilityProcessorEditpart) getEditpart(
					viewContext, yElement));
			break;
		case CoreModelPackage.YVIEW__EXPOSED_ACTIONS:
			YExposedAction yAction = (YExposedAction) notification
					.getNewValue();
			internalAddExposedAction((IExposedActionEditpart) getEditpart(
					viewContext, yAction));
			break;
		default:
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * handleModelRemove(int, org.eclipse.emf.common.notify.Notification)
	 */
	@Override
	protected void handleModelRemove(int featureId, Notification notification) {
		checkDisposed();

		switch (featureId) {
		case CoreModelPackage.YVIEW__BEAN_SLOTS:
			throw new IllegalStateException("Viewslots must not be removed!");
		case CoreModelPackage.YVIEW__VISIBILITY_PROCESSORS:
			YVisibilityProcessor yElement = (YVisibilityProcessor) notification
					.getOldValue();
			internalRemoveVisibilityProcessor((IVisibilityProcessorEditpart) getEditpart(
					viewContext, yElement));
			break;
		case CoreModelPackage.YVIEW__TRANSIENT_VISIBILITY_PROCESSORS:
			yElement = (YVisibilityProcessor) notification.getOldValue();
			internalRemoveTransientVisibilityProcessor((IVisibilityProcessorEditpart) getEditpart(
					viewContext, yElement));
			break;
		case CoreModelPackage.YVIEW__EXPOSED_ACTIONS:
			YExposedAction yAction = (YExposedAction) notification
					.getOldValue();
			internalRemoveExposedAction((IExposedActionEditpart) getEditpart(
					viewContext, yAction));
			break;
		default:
			break;
		}
	}

	@Override
	public void initialize(IViewContext context, M model) {
		super.initialize(context, model);

		context.setViewEditpart(this);
	}

	/**
	 * Registers the given bean slot.
	 *
	 * @param yBeanSlot
	 *            the y bean slot
	 */
	protected void internalAddBeanSlot(YBeanSlot yBeanSlot) {
		checkDisposed();

		if (viewContext == null) {
			LOGGER.warn("ViewEditpart is not rendered yet. Can not install beanslot!");
			return;
		}

		// redirect events are handled from outside
		viewContext.createBeanSlot(yBeanSlot.getName(), yBeanSlot
				.getValueType(), yBeanSlot.isRedirectEvents() ? null
				: yBeanSlot.getEventTopic());
	}

	/**
	 * Gets the visibility processors.
	 *
	 * @return the visibility processors
	 */
	public List<IVisibilityProcessorEditpart> getVisibilityProcessors() {
		if (vProcessorEditparts == null) {
			internalLoadVisibilityProcessors();
		}
		return Collections.unmodifiableList(vProcessorEditparts);
	}

	/**
	 * Gets the transient visibility processors.
	 *
	 * @return the transient visibility processors
	 */
	public List<IVisibilityProcessorEditpart> getTransientVisibilityProcessors() {
		if (transientVProcessorEditparts == null) {
			internalLoadTransientVisibilityProcessors();
		}
		return Collections.unmodifiableList(transientVProcessorEditparts);
	}

	/**
	 * Adds the visibility processor.
	 *
	 * @param element
	 *            the element
	 */
	public void addVisibilityProcessor(IVisibilityProcessorEditpart element) {
		try {
			checkDisposed();

			// add the element by using the model
			//
			M yVProcessor = getModel();
			YVisibilityProcessor yVisibilityProcessor = (YVisibilityProcessor) element
					.getModel();
			yVProcessor.getVisibilityProcessors().add(yVisibilityProcessor);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/**
	 * Removes the visibility processor.
	 *
	 * @param element
	 *            the element
	 */
	public void removeVisibilityProcessor(IVisibilityProcessorEditpart element) {
		try {
			checkDisposed();

			// remove the element by using the model
			//
			M yVProcessor = getModel();
			YVisibilityProcessor yVisibilityProcessor = (YVisibilityProcessor) element
					.getModel();
			yVProcessor.getVisibilityProcessors().remove(yVisibilityProcessor);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of processors.
	 * 
	 * @param editpart
	 *            The editpart to be added
	 */
	protected void internalAddVisibilityProcessor(
			IVisibilityProcessorEditpart editpart) {
		checkDisposed();

		if (vProcessorEditparts == null) {
			internalLoadVisibilityProcessors();
		}
		if (!vProcessorEditparts.contains(editpart)) {
			vProcessorEditparts.add(editpart);

			editpart.activate();
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of transientVProcessors.
	 * 
	 * @param editpart
	 *            The editpart to be added
	 */
	protected void internalAddTransientVisibilityProcessor(
			IVisibilityProcessorEditpart editpart) {
		checkDisposed();

		if (transientVProcessorEditparts == null) {
			internalLoadTransientVisibilityProcessors();
		}
		if (!transientVProcessorEditparts.contains(editpart)) {
			transientVProcessorEditparts.add(editpart);

			if (isRendered()) {
				editpart.activate();
			}
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of processors.
	 * 
	 * @param editpart
	 *            The editpart to be removed
	 */
	protected void internalRemoveVisibilityProcessor(
			IVisibilityProcessorEditpart editpart) {
		checkDisposed();

		if (vProcessorEditparts != null && editpart != null) {
			vProcessorEditparts.remove(editpart);

			editpart.dispose();
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of transient processors.
	 * 
	 * @param editpart
	 *            The editpart to be removed
	 */
	protected void internalRemoveTransientVisibilityProcessor(
			IVisibilityProcessorEditpart editpart) {
		checkDisposed();

		if (transientVProcessorEditparts != null && editpart != null) {
			transientVProcessorEditparts.remove(editpart);

			editpart.dispose();
		}
	}

	/**
	 * Is called to load and initialize all elements.
	 */
	protected void internalLoadVisibilityProcessors() {
		checkDisposed();

		if (vProcessorEditparts == null) {
			vProcessorEditparts = new ArrayList<IVisibilityProcessorEditpart>();
			for (YVisibilityProcessor yVisibilityProcessor : getModel()
					.getVisibilityProcessors()) {
				IVisibilityProcessorEditpart editPart = getEditpart(
						viewContext, yVisibilityProcessor);
				internalAddVisibilityProcessor(editPart);
			}
		}
	}

	/**
	 * Is called to load and initialize all transientVProcessors.
	 */
	protected void internalLoadTransientVisibilityProcessors() {
		checkDisposed();

		if (transientVProcessorEditparts == null) {
			transientVProcessorEditparts = new ArrayList<IVisibilityProcessorEditpart>();
			for (YVisibilityProcessor yVisibilityProcessor : getModel()
					.getTransientVisibilityProcessors()) {
				IVisibilityProcessorEditpart editPart = getEditpart(
						viewContext, yVisibilityProcessor);
				internalAddTransientVisibilityProcessor(editPart);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#getExposedActions
	 * ()
	 */
	public List<IExposedActionEditpart> getExposedActions() {
		if (exposedActionEditparts == null) {
			internalLoadExposedActions();
		}
		return Collections.unmodifiableList(exposedActionEditparts);
	}

	/**
	 * Adds the exposed actions.
	 *
	 * @param element
	 *            the element
	 */
	public void addExposedActions(IExposedActionEditpart element) {
		try {
			checkDisposed();

			// add the element by using the model
			//
			M yView = getModel();
			YExposedAction yExposedAction = (YExposedAction) element.getModel();
			yView.getExposedActions().add(yExposedAction);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/**
	 * Removes the exposed action.
	 *
	 * @param element
	 *            the element
	 */
	public void removeExposedAction(IExposedActionEditpart element) {
		try {
			checkDisposed();

			// remove the element by using the model
			//
			M yView = getModel();
			YExposedAction yExposedAction = (YExposedAction) element.getModel();
			yView.getExposedActions().remove(yExposedAction);
			// BEGIN SUPRESS CATCH EXCEPTION
		} catch (RuntimeException e) {
			// END SUPRESS CATCH EXCEPTION
			LOGGER.error("{}", e);
			throw e;
		}
	}

	/**
	 * Is called to change the internal state and add the given editpart to the
	 * list of processors.
	 * 
	 * @param editpart
	 *            The editpart to be added
	 */
	@SuppressWarnings("unchecked")
	protected void internalAddExposedAction(IExposedActionEditpart editpart) {
		checkDisposed();

		if (exposedActionEditparts == null) {
			internalLoadExposedActions();
		}
		if (!exposedActionEditparts.contains(editpart)) {

			@SuppressWarnings("rawtypes")
			IWidgetAssocationsService associations = viewContext
					.getService(IWidgetAssocationsService.ID);
			associations.associate(editpart, editpart.getModel());

			exposedActionEditparts.add(editpart);
		}
	}

	/**
	 * Is called to change the internal state and remove the given editpart from
	 * the list of processors.
	 * 
	 * @param editpart
	 *            The editpart to be removed
	 */
	@SuppressWarnings("unchecked")
	protected void internalRemoveExposedAction(IExposedActionEditpart editpart) {
		checkDisposed();

		if (exposedActionEditparts != null && editpart != null) {
			exposedActionEditparts.remove(editpart);

			@SuppressWarnings("rawtypes")
			IWidgetAssocationsService associations = viewContext
					.getService(IWidgetAssocationsService.ID);
			associations.remove(editpart);
		}
	}

	/**
	 * Is called to load and initialize all elements.
	 */
	protected void internalLoadExposedActions() {
		checkDisposed();

		if (exposedActionEditparts == null) {
			exposedActionEditparts = new ArrayList<IExposedActionEditpart>();
			for (YExposedAction exposedActionEditpart : getModel()
					.getExposedActions()) {
				IExposedActionEditpart editPart = getEditpart(viewContext,
						exposedActionEditpart);
				internalAddExposedAction(editPart);
			}
		}
	}

	/**
	 * Returns true, if the editpart is currently rendered.
	 *
	 * @return true, if is rendered
	 */
	private boolean isRendered() {
		checkDisposed();

		return internalGetPresentation() != null
				&& internalGetPresentation().isRendered();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.ecview.core.common.editpart.emf.ElementEditpart#
	 * internalDispose()
	 */
	@Override
	protected void internalDispose() {
		try {

			if (viewContext != null) {
				ILifecycleService service = viewContext
						.getService(ILifecycleService.class.getName());
				if (service != null) {
					// now remove the handler
					service.removeHandler(this);
				}
			}

			// dispose the presenter
			//
			if (presentation != null) {
				presentation.dispose();
				presentation = null;
			}

			if (getContent() != null) {
				content.dispose();
				content = null;
			}

			if (openDialogs != null) {
				for (IDialogEditpart dialog : openDialogs) {
					dialog.dispose();
				}
				openDialogs = null;
			}

			if (getBindingSet() != null) {
				bindingSet.dispose();
				bindingSet = null;
			}

			// dispose all visibility processor
			for (IVisibilityProcessorEditpart editpart : getVisibilityProcessors()) {
				editpart.dispose();
			}
			vProcessorEditparts = null;

			for (IVisibilityProcessorEditpart editpart : getTransientVisibilityProcessors()) {
				editpart.dispose();
			}
			transientVProcessorEditparts = null;

		} finally {
			super.internalDispose();
		}
	}

	/**
	 * Returns the instance of the presentation, but does not load it.
	 * 
	 * @param <A>
	 *            An instance of {@link IWidgetPresentation}
	 * @return presentation
	 */
	@SuppressWarnings("unchecked")
	protected <A extends IWidgetPresentation<?>> A internalGetPresentation() {
		checkDisposed();

		return (A) presentation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#getPresentation
	 * ()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <A extends IViewPresentation<?>> A getPresentation() {
		checkDisposed();

		if (presentation == null) {
			presentation = createPresentation();
		}
		return (A) presentation;
	}

	/**
	 * Is called to created the presenter for this edit part.
	 * 
	 * @param <A>
	 *            An instance of {@link IViewPresentation}
	 * 
	 * @return The created presentation.
	 */
	protected <A extends IViewPresentation<?>> A createPresentation() {
		return DelegatingPresenterFactory.getInstance().createPresentation(
				getContext(), this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#exec(java.
	 * lang.Runnable)
	 */
	@Override
	public void exec(Runnable runnable) {
		checkDisposed();
		getPresentation().exec(runnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#execAsync(
	 * java.lang.Runnable)
	 */
	@Override
	public Future<?> execAsync(Runnable runnable) {
		checkDisposed();
		return getPresentation().execAsync(runnable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#createService
	 * (java.lang.Class)
	 */
	@Override
	public <A extends IUiKitBasedService> A createService(Class<A> serviceClass) {
		return getPresentation().createService(serviceClass);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#openDialog
	 * (org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart,
	 * org.eclipse
	 * .osbp.ecview.core.common.editpart.binding.IBindableEndpointEditpart)
	 */
	@Override
	public void openDialog(IDialogEditpart dialogEditpart,
			IBindableEndpointEditpart inputData) {
		if (dialogEditpart == null) {
			return;
		}

		if (openDialogs == null) {
			openDialogs = new HashSet<IDialogEditpart>();
		}
		openDialogs.add(dialogEditpart);

		getPresentation().openDialog(dialogEditpart, inputData);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart#closeDialog
	 * (org.eclipse.osbp.ecview.core.common.editpart.IDialogEditpart)
	 */
	@Override
	public void closeDialog(IDialogEditpart dialogEditpart) {
		if (dialogEditpart == null) {
			return;
		}

		if (openDialogs != null) {
			openDialogs.remove(dialogEditpart);
		}

		getPresentation().closeDialog(dialogEditpart);
	}
}
