/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.ui.core.editparts.extension;

import org.eclipse.osbp.ecview.core.common.editpart.ILayoutEditpart;

/**
 * The abstraction for an grid layout. The grid layout extends layout and it is
 * an elementContainer that can contain other embeddables.
 */
public interface IGridLayoutEditpart extends ILayoutEditpart {

}
