/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.ui.core.editparts.extension.presentation;

import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;

/**
 * TabSheetPresentations are an abstraction above a tab an are responsible to
 * create the UI-Kit specific elements and to handle them. One important thing
 * to notice is, that the life cycle of presentations is handled by their edit
 * parts. And so an presentations must never dispose its tab presentations!
 *
 * @param <C>
 *            the generic type
 */
public interface ITabPresentation<C> extends IWidgetPresentation<C> {

}
