/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.ui.core.editparts.extension;

import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;

// TODO: Auto-generated Javadoc
/**
 * An edit part for master details.
 */
public interface IMasterDetailEditpart extends IEmbeddableEditpart {

	/**
	 * Returns the master editpart.
	 * 
	 * @return master The editpart that controls the master presentation.
	 */
	IEmbeddableEditpart getMaster();

	/**
	 * Returns the detail editpart.
	 * 
	 * @return detail The editpart that controls the detail presentation.
	 */
	IEmbeddableEditpart getDetail();

	/**
	 * Sets the editpart that controls the master presentation.
	 *
	 * @param master
	 *            the new master
	 */
	@Deprecated // use model instead
	void setMaster(IEmbeddableEditpart master);

	/**
	 * Sets the editpart that controls the detail presentation.
	 *
	 * @param detail
	 *            the new detail
	 */
	@Deprecated // use model instead
	void setDetail(IEmbeddableEditpart detail);

}
