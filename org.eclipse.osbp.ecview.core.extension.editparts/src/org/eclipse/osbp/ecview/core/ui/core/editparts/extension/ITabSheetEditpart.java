/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.ui.core.editparts.extension;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;

// TODO: Auto-generated Javadoc
/**
 * The abstraction for a tab sheet.
 */
public interface ITabSheetEditpart extends IEmbeddableEditpart {

	/**
	 * Returns an unmodifiable list with all tabs contained in the tabsheet.
	 * 
	 * @return elements The children editparts contained in this tabsheet
	 */
	List<ITabEditpart> getTabs();

	/**
	 * Adds a tab to the tabsheet. Adding the same tab twice has no effect.
	 * 
	 * @param tab
	 *            The editpart to be added.
	 */
	@Deprecated // use model instead
	void addTab(ITabEditpart tab);

	/**
	 * Removes a tab from the tabsheet.
	 *
	 * @param tab
	 *            the tab
	 */
	@Deprecated // use model instead
	void removeTab(ITabEditpart tab);

	/**
	 * Renders the given tab.
	 *
	 * @param tab
	 *            the tab
	 */
	void renderTab(ITabEditpart tab);

	/**
	 * Disposes the given tab.
	 *
	 * @param tab
	 *            the tab
	 */
	void disposeTab(ITabEditpart tab);

	/**
	 * Unrenders the given tab.
	 *
	 * @param tab
	 *            the tab
	 */
	void unrenderTab(ITabEditpart tab);
}
