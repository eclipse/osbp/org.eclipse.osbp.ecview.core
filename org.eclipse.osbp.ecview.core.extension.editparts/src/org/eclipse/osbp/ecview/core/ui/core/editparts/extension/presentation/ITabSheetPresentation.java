/**
 * Copyright (c) 2011, 2015 - Lunifera GmbH (Gross Enzersdorf, Austria), Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *         Florian Pirchner - Initial implementation
 */
package org.eclipse.osbp.ecview.core.ui.core.editparts.extension.presentation;

import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.ecview.core.ui.core.editparts.extension.ITabEditpart;

// TODO: Auto-generated Javadoc
/**
 * TabSheetPresentations are an abstraction above a tabSheet an are responsible
 * to create the UI-Kit specific elements and to handle them. One important
 * thing to notice is, that the life cycle of tabs is handled by their edit
 * parts. And so an tabs must never dispose its tab tabs!
 *
 * @param <C>
 *            the generic type
 */
public interface ITabSheetPresentation<C> extends IWidgetPresentation<C> {

	/**
	 * Will render the tab tabs.<br>
	 * <ul>
	 * <li><b>force == false:</b> Only elements will be rendered that are not
	 * rendered yet. Already rendered elements will not be touched.</li>
	 * <li><b>force == true:</b> First will unrender all tab tabs and afterward
	 * it will render them again.</li>
	 * </ul>
	 * 
	 * @param force
	 *            see method description
	 */
	void renderTabs(boolean force);

	/**
	 * Unrenders the given tab.
	 *
	 * @param editpart
	 *            the editpart
	 */
	void unrenderTab(ITabEditpart editpart);
}
